var baseUrl = window.location.protocol + "//" + window.location.host + "/web";
//var baseUrl = window.location.protocol + "//" + window.location.host;
var type = "1";
$(document).ready(function(){
	$(".entitylist").change(getPlans);
	$('input:radio[name="tipo"]').change(
	    function(){
	    	$(".number").val("");
        	$(".datefact").val("");
        	$(".neto").val("");
        	$(".copago").val("");
        	dt.fnClearTable();
	        if($(this).val() == '2'){
	        	$(".menu_ind").attr("style","display: none;");
	        	type = 2;
	        }else{
	        	$(".menu_ind").removeAttr("style");
	        	type = 1;
	        }
	        search();
	});
	$("body").on("change",'#check-all', function(){
	    if($(this).is(':checked')){
	        $(".chk").prop('checked', true);
	        $(".trselt").addClass("trsel");
	    } else{
	    	$(".chk").prop('checked', false);
	    	$(".trselt").removeClass("trsel"); 
	    }
	});
	$(".search").click(function(){
		$(".search").toggleClass('active');
		$(".search").attr('disabled','disabled');
		search();
	});
	$(".generate").click(function(){
		generate();
	});
	initCalendars();
});

function initCalendars(){
	 $.datepicker.setDefaults($.datepicker.regional['es']);
     $(".date").datepicker({changeMonth:true,changeYear:true,dateFormat: 'yy-mm-dd'});
}

function generate(){
	var itms = false;
	var xml_data = "<root>";
	if(type == 1){
		xml_data += "<invoice_number>" + $(".number").val() + "</invoice_number>";
		xml_data += "<invoice_date>" + $(".datefact").val() + "</invoice_date>";
		xml_data += "<invoice_value>" + $(".neto").val() + "</invoice_value>";
		xml_data += "<invoice_copago>" + $(".copago").val() + "</invoice_copago>";
		xml_data += "<type_gen>" + type + "</type_gen>";
		$("input:checkbox:checked").each(function(){
			if($(this).is(':checked')){
				itms = true;
				xml_data += "<service>" + $(this).attr("name") + "</service>";
			}
		});
	}else{
		xml_data += "<type_gen>" + type + "</type_gen>";
		$("input:checkbox:checked").each(function(){
			if($(this).is(':checked')){
				itms = true;
				var id = $(this).attr("name");
				xml_data += "<service>" + 
				"<service_id>" + id + "</service_id>" +
				"<service_copago>" + $(".copago" + id).val() + "</service_copago>" +
				"<service_neto>" + $(".neto" + id).val() + "</service_neto>" +
				"<service_date>" + $(".date" + id).val() + "</service_date>" +
				"<service_invoice>" + $(".invoice" + id).val() + "</service_invoice>" +
				"</service>";
			}
		});
	}
	xml_data += "</root>";
	if(itms)
		closeservices(xml_data);
	else
		swal({   
			title: "Debes seleccionar al menos un elemento de la lista",   
			text: "",   
			type: "error",   
			showCancelButton: false
		});
}

function closeservices(data_p){
	var xml_data = "<root>";
	$("input:checkbox:checked").each(function(){
		if($(this).is(':checked')){
			itms = true;
			xml_data += "<service>" + $(this).attr("name") + "</service>";
		}
	});
	xml_data +="</root>";
	var data = {
		xml_data : xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/verifycomplete',
        success: function(data){
        	if($(data).find("code").text() == '100' || $(data).find("code").text() == '200'){
        		if($(data).find("code").text() == '200'){
	        		swal({   
	        			title: $(data).find("msg").text(),   
	        			text: "",   
	        			type: "error",   
	        			html: true
	        		});
        		}else
        			confirm_generate(data_p);
        	}else{
        		swal({
    				title:"Ocurrio un error", 
    				text:"", 
    				type:"error",
    				showCancelButton: false,   
    				closeOnConfirm: true,
    			});  
        	}
        }
    });
}

function confirm_generate(xml_data){
	swal({   
		title: "Est&aacute;s seguro?",   
		text: "",   
		type: "warning",   
		confirmButtonText: "Si, generar", 
		confirmButtonColor: "#428bca",
		cancelButtonText: "No, salir",
		showCancelButton: true,   
		closeOnConfirm: false,   
		showLoaderOnConfirm: true,
		html: true
	}, function(){
		var data = {
			xml_data: xml_data	
		};
		$.ajax({
	        type: 'POST',
	        data: data,
	        url: baseUrl + '/admin/generate',
	        success: function(data){
	        	search();
	        	$(".number").val("");
	        	$(".datefact").val("");
	        	$(".neto").val("");
	        	$(".copago").val("");
	        	swal({   
	        		title: "",   
	        		text: "",   
	        		timer: 0,   
	        		showConfirmButton: false 
	        	});
	        	location.href = baseUrl + "/admin/download-" + $(data).find("file").text();
	        }
	    });
	});
}

function search(){
	var xml_data = "<root>";
	xml_data += "<pid>" + $(".entityplans option:selected").val() + "</pid>";
	xml_data += "<eid>" + $(".entitylist option:selected").val() + "</eid>";
	xml_data += "<type>" + $(".service option:selected").val() + "</type>";
	xml_data += "<start1>" + start1 + "</start1>";
	xml_data += "<end1>" + end1 + "</end1>";
	xml_data += "<start2>" + start2 + "</start2>";
	xml_data += "<end2>" + end2 + "</end2>";
	xml_data +="</root>";
	var data = {
		xml_data : xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/searchbilling',
        success: function(data){
        	$(".search").removeClass('active');
    		$(".search").removeAttr('disabled');
        	dt.fnClearTable();
        	var b = false;
        	$(data).find("service").each(function() {
        		b = true;
        		if(type == 1){
		        	dt.fnAddData([
		        	     '<td><div class="form-group">'+
		        	     '<div class="col-sm-7 controls">'+
					     '<div class="vd_checkbox">'+
					     '<input type="checkbox" class="chk chk' + $(this).find("id").text() + '" id="lbloption' + $(this).find("id").text() + '" name="' + $(this).find("id").text() + '">'+
					    '<label class="lblcheck" for="lbloption' + $(this).find("id").text() + '" id="' + $(this).find("id").text() + '"></label>'+
					     '</div></div></div></td>',
		                 $(this).find("full_name").text(),
		                 $(this).find("identity").text(),
		                 $(this).find("service_name").text(),
		                 $(this).find("nro_auth").text(),
		                 $(this).find("start_date").text(),
		                 $(this).find("end_date").text(),
		                 $(this).find("entity").text(),
		                 $(this).find("plan").text(),
		                 $(this).find("invoice_number").text(),
		                 //'<label style="color:' + $(this).find("color").text() + '">' + $(this).find("status").text() + '</label>',
		            ]);
        		}else{
        			var invoice = $(this).find("invoice_number").text() == ""? false : true;
        			var content = "";
        			if(invoice){
        				content = $(this).find("invoice_number").text();
        			}else{
        				content = "<div class='row' style='width:600px;'>";
        					content += "<div class='col-md-3'>";
        						content += "<input type='number' class='col-md-3 invoice" + $(this).find("id").text() + "' placeholder='N. FACTURA'>";
        					content += "</div>";
        					content += "<div class='col-md-3'>";
        						content += "<input type='date' class='col-md-3 date" + $(this).find("id").text() + "' placeholder='F. FACTURA'>";
        					content += "</div>";
        					content += "<div class='col-md-3'>";
        						content += "<input type='number' class='col-md-3 copago" + $(this).find("id").text() + "' placeholder='Copago'>";
	    					content += "</div>";
	    					content += "<div class='col-md-3'>";
	    						content += "<input type='number' class='col-md-3 neto" + $(this).find("id").text() + "' placeholder='Neto'>";
	    					content += "</div>";
        				content += "</div>";
        			}
        			dt.fnAddData([
		        	     '<td><div class="form-group">'+
		        	     '<div class="col-sm-7 controls">'+
					     '<div class="vd_checkbox">'+
					     '<input type="checkbox" class="chk chk' + $(this).find("id").text() + '" id="lbloption' + $(this).find("id").text() + '" name="' + $(this).find("id").text() + '">'+
					    '<label class="lblcheck" for="lbloption' + $(this).find("id").text() + '" id="' + $(this).find("id").text() + '"></label>'+
					     '</div></div></div></td>',
		                 $(this).find("full_name").text(),
		                 $(this).find("identity").text(),
		                 $(this).find("service_name").text(),
		                 $(this).find("nro_auth").text(),
		                 $(this).find("start_date").text(),
		                 $(this).find("end_date").text(),
		                 $(this).find("entity").text(),
		                 $(this).find("plan").text(),
		                 content
		            ]);
	                 
        		}
        	});
        	$('#container').css( 'display', 'block' );
        	//$(".lblcheck").click();
        	if(b)
        		dt.columns.adjust().draw();
        }
    });
}

function getPlans(){
	var xml_data = "<root><eid>" + $(".entitylist").val() + "</eid></root>";
	var data = {
		xml_data: xml_data
	};
	 $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/getPlans',
        success: function(data){
        	var html = "<option value='*'>Todos</option>";
        	$(data).find("plan").each(function() {
	            html += "<option value='" + $(this).find("id").text() + "'>" + $(this).find("name").text().toUpperCase() + "</option>";
        	});
            $(".entityplans").html(html);
        }
    });
}