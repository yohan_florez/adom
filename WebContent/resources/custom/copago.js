var baseUrl = window.location.protocol + "//" + window.location.host + "/web";
//var baseUrl = window.location.protocol + "//" + window.location.host;
var type = "1";
var completes = 0;
var services = [];
var xml_details = "";
$(document).ready(function(){
	$("body").on("change",'#check-all', function(){
	    if($(this).is(':checked')){
	        $(".chk").prop('checked', true);
	        $(".trselt").addClass("trsel");
	    } else{
	    	$(".chk").prop('checked', false);
	    	$(".trselt").removeClass("trsel"); 
	    }
	});
	$(".search").click(function(){
		$(".search").toggleClass('active');
		$(".search").attr('disabled','disabled');
		search();
	});
	$(".generate").click(function(){
		confirm_generate(xml_details);
	});
	$(".preview").click(openModalPrev);
	//$(".generate_pdf").click(confirm_generate(xml_details));
	initCalendars();
});

function openModalPrev(){
	$(".preview").toggleClass('active');
	$(".preview").attr('disabled','disabled');
	preview(function(response){
		$(".preview").removeClass('active');
		$(".preview").removeAttr('disabled');
		if(completes == 0){
			$(".warning_msg_text").html("Debes Completar al menos una fila");
			$(".warning_msg").removeAttr("style");
		}else
			if(!response){
				$(".warning_msg_text").html("Atenci&oacute;n! Debes completar la informaci&oacute;n");
				$(".warning_msg").removeAttr("style");
			}else{
				$(".warning_msg").attr("style", "visibility: hidden");
				updateInfo();
			}
	});
}

function updateInfo(){
	var lservices = "";
	for(var i=0; i<services.length; i++)
		lservices += "<serviceid>" + services[i] + "</serviceid>";
	var xml_data = "<root>";
	xml_data += "<pid>" + $(".pid").val() + "</pid>";
	xml_data += lservices;
	xml_data +="</root>";
	var data = {
		xml_data : xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/searchcopagobyservices',
        success: function(data){
    		var html = "";
    		var now = new Date();
    		var formatter = new Intl.DateTimeFormat("es", { month: "long" });
    		month1 = formatter.format(now);
    		var date_now = month1 + " " + now.getDate() + ", " + now.getFullYear();
    		$(".pname").html("<b>Profesional:</b> " + $(data).find("professional").text());
    		$(".lblrec").html("Recibi&oacute; en ADOM: "+ $(data).find("by").text());
    		$(".lbldoc").html("<b>Documento:</b> " + $(data).find("document").text());
    		$(".lbldate").html("<b>Fecha de entrega:</b> " + date_now);
    		var total_qty_services = 0;
    		var total_efr = 0;
    		var total_efe = 0;
    		var total_dto = 0;
    		var total_neto = 0;
    		var total_general = 0;
    		xml_details = "<root>";
    		$(data).find("copago").each(function(){
    			var id = $(this).find("id").text();
    			var count_s = parseInt($(this).find("complete").text());
    			var service_val = parseInt($(this).find("service_value").text());
    			var cr = parseInt($(".cr" + id).val());
    			var ce = parseInt($(".ce" + id).val());
    			var dto = parseInt($(".dto" + id).val());
    			var neto = count_s * service_val;
    			var total_servicio = neto - ((parseInt(cr) - parseInt(ce))) -dto;
    			/**/
	    			total_qty_services += count_s;
	    			total_efr += cr;
	    			total_efe += ce;
	    			total_dto += dto;
	    			total_neto += neto;
	    			total_general += total_servicio;
    			/**/
	    		xml_details += "<detail>";
		    		xml_details += "<ids>" + $(this).find("ids").text() + "</ids>";
		    		xml_details += "<cash_received>" + cr + "</cash_received>";
		    		xml_details += "<cash_delivered>" + ce + "</cash_delivered>";
		    		xml_details += "<dto>" + dto + "</dto>";
    			xml_details += "</detail>"
    			html += "<tr>";
				html += '<td>' + $(this).find("full_name").text() + '</td>';
				html += '<td>' + $(this).find("entity").text() + '</td>';
				html += '<td>' + $(this).find("service").text() + '</td>';
				html += '<td><span class="pull-right">$' + formattNumber(service_val) + '</span></td>';
				html += '<td><span class="pull-right">' + count_s + '</span></td>';
				html += '<td><span class="pull-right">$' + formattNumber(cr) + '</span></td>';
				html += '<td><span class="pull-right">$' + formattNumber(ce) + '</span></td>';
				html += '<td><span class="pull-right">$' + formattNumber(dto) + '</span></td>';
				//html += '<td><span class="pull-right">$' + neto + '</span></td>';
				html += '<td><span class="pull-right">$' + formattNumber(total_servicio) + '</span></td>';
				html += '</tr>';
    		});
    		xml_details += "</root>";
    		html += '<tr class="h20">';
    		html += '<td colspan="3" class="btrans"></td>';
    		html += '<td class="btrans"><span class="pull-right"><b>Totales</b></span></td>';
    		html += '<td class="btrans"><span class="pull-right"><b>' + total_qty_services + '</b></span></td>';
    		html += '<td class="btrans"><span class="pull-right"><b>$' + formattNumber(total_efr) + '</b></span></td>';
    		html += '<td class="btrans"><span class="pull-right"><b>$' + formattNumber(total_efe) + '</b></span></td>';
    		html += '<td class="btrans"><span class="pull-right"><b>$' + formattNumber(total_dto) + '</b></span></td>';
    		//html += '<td class="btrans"><span class="pull-right"><b>$' + total_neto + '</b></span></td>';
    		html += '<td class="btrans"></td>';
    		html += '</tr>';
    		html += '<tr class="h20">';
    		html += '<td colspan="10" class="btrans"></td>';
    		html += '</tr>';
    		html += '<tr>';
    		html += '<td colspan="8" class="btrans"><span class="pull-right"><b>SALDO</b></span></td>';
    		html += '<td class="btrans"><span class="pull-right"><b>$' + formattNumber(total_general) + '</b></span></td>';
    		html += '</tr>';
    		$(".tbodyprev").html(html);
    		$("#myModal").modal('show');
    		$(".preview").removeClass('active');
    		$(".preview").removeAttr('disabled');
        }
    });
}

function formattNumber(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function preview(action){
	var response = true;
	completes = 0;
	services = [];
	$(".row_service").each(function() {
		var id = $(this).attr("name");
		var cr = $(".cr" + id).val();
		var ce = $(".ce" + id).val();
		var dto = $(".dto" + id).val();
		if(!$.trim(cr) == "" && !$.trim(ce) == "" && !$.trim(dto) == ""){
			$(".cr" + id).attr("style", "border:1px solid gray !important");
			$(".ce" + id).attr("style", "border:1px solid gray !important");
			$(".dto" + id).attr("style", "border:1px solid gray !important");
			completes++;
			services.push(id);
		}else{
			$(".cr" + id).attr("style", "border:1px solid gray !important");
			$(".ce" + id).attr("style", "border:1px solid gray !important");
			$(".dto" + id).attr("style", "border:1px solid gray !important");
			if(!cr == "" || !ce == "" || !dto == ""){
				if(cr == "")
					$(".cr" + id).attr("style", "border:1px solid red !important");
				else
					$(".cr" + id).attr("style", "border:1px solid gray !important");
				if(ce == "")
					$(".ce" + id).attr("style", "border:1px solid red !important");
				else
					$(".ce" + id).attr("style", "border:1px solid gray !important");
				if(dto == "")
					$(".dto" + id).attr("style", "border:1px solid red !important");
				else
					$(".dto" + id).attr("style", "border:1px solid gray !important");
				response = false;
				completes = 1;
			}
		}
	});
	action(response);
}

function initCalendars(){
	 $.datepicker.setDefaults($.datepicker.regional['es']);
     $(".date").datepicker({changeMonth:true,changeYear:true,dateFormat: 'yy-mm-dd'});
}

function confirm_generate(xml_data){
	swal({   
		title: "Est&aacute;s seguro?",   
		text: "",   
		type: "warning",   
		confirmButtonText: "Si, generar", 
		confirmButtonColor: "#428bca",
		cancelButtonText: "No, salir",
		showCancelButton: true,   
		closeOnConfirm: false,   
		showLoaderOnConfirm: true,
		html: true
	}, function(){
		var data = {
			xml_data: xml_data	
		};
		$.ajax({
	        type: 'POST',
	        data: data,
	        url: baseUrl + '/admin/generate_copago',
	        success: function(data){
	        	search();
	        	swal({   
	        		title: "",   
	        		text: "",   
	        		timer: 0,   
	        		showConfirmButton: false 
	        	});
	        	$("#myModal").modal('hide')
	        	location.href = baseUrl + "/admin/downloadpdf-" + $(data).find("file").text();
	        }
	    });
	});
}

function search(){
	var xml_data = "<root>";
	xml_data += "<pid>" + $(".professional").val() + "</pid>";
	xml_data += "<e1>" + $(".estado option:selected").val() + "</e1>";
	xml_data += "<e2>" + $(".estado2 option:selected").val() + "</e2>";
	xml_data +="</root>";
	$(".pid").val($(".professional").val());
	var data = {
		xml_data : xml_data	
	};
	dt.fnClearTable();
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/searchcopago',
        success: function(data){
        	$(".search").removeClass('active');
    		$(".search").removeAttr('disabled');
        	var b = false;
        	$(data).find("copago").each(function() {
        		b = true;
        		var disabled = 'enabled';
        		var style = "";
        		var val_ce = "";
        		var val_cr = "";
        		var val_dto = "";
        		var id= $(this).find("id").text();
        		if($(this).find("status").text() == "PROGRAMADO" || $(this).find("status_c").text() == "E"){
        			disabled = 'disabled';
        			style = "background-color: #F7F6F6;";
        		}
        		if($(this).find("status_c").text() == "E"){
        			val_ce = $(this).find("delivered").text();
            		val_cr = $(this).find("received").text();
            		val_dto = $(this).find("dto").text();
            		id = "";
        		}
    			var rowid = dt.fnAddData([
	                 $(this).find("full_name").text(),
	                 $(this).find("document").text(),
	                 $(this).find("service").text(),
	                 $(this).find("auth").text(),
	                 $(this).find("total").text(),
	                 $(this).find("complete").text(),
	                 '<label style="color:' + $(this).find("color").text() + ';">' + $(this).find("status").text() + '</label>',
	                 $(this).find("date_end").text(),
	                 $(this).find("entity").text(),
	                 $(this).find("frecuency").text(),
	                 "$" + $(this).find("copago_").text(),
	                 "$" + $(this).find("total_copagos").text(),
	                 '<center><label style="color:' + $(this).find("color_c").text() + ';">' + $(this).find("status_c").text() + '</label></center>',
	                 "<input type='text' value='" + val_cr + "' style='" + style + "' " + disabled + " name='" + $(this).find("id").text() + "' class='col-md-12 cr" + id + "' >",
	                 "<input type='text' value='" + val_ce + "' style='" + style + "' " + disabled + " name='" + $(this).find("id").text() + "' class='col-md-12 ce" + id + "' >",
	                 "<input type='text' value='" + val_dto + "' style='" + style + "' " + disabled + " name='" + $(this).find("id").text() + "' class='col-md-12 dto" + id + "'>"
	            ]);
    			var theNode = dt.fnSettings().aoData[rowid[0]].nTr;
    			theNode.setAttribute('class','row_service');
    			theNode.setAttribute('name', $(this).find("id").text());
        	});
        	if(b){
        		$(".div-prev").removeClass("hidden");
        		$(".div-prev").css("display","inline !important");
        		//dt.columns.adjust().draw();
        	}else{
        		$(".div-prev").addClass("hidden");
        	}
        }
    });
}
