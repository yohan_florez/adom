var baseUrl = window.location.protocol + "//" + window.location.host + "/web";
//var baseUrl = window.location.protocol + "//" + window.location.host;
var initDt = false;
var dt = null;
$(document).ready(function() {
	$(".savep").click(savep);
	getallprofessionals();
	$(".clear").click(function(){
		clear();
	});
	$('.search_text').keydown(function(event) {
        if (event.keyCode == 13) {
        	search();
            return false;
         }
	 });
	initCalendars();
	$("body").on("click",".gen_pass",function(){
		var id = $(this).attr("name");
		swal({   
			title: "Est&aacute;s seguro de generar una nueva contrase&ntilde;a para este usuario?",   
			text: "",   
			type: "warning",   
			confirmButtonText: "Si, generar", 
			confirmButtonColor: "#428bca",
			cancelButtonText: "No, salir",
			showCancelButton: true,   
			closeOnConfirm: false,   
			showLoaderOnConfirm: true,
			html: true
		}, function(){   
			generate_newpass(id);
		});
	});
	$("body").on("click",".edit-po",function(){
		openmpa($(this).attr("name"));
	});
	$(".update-p").click(function(){
		updatep($(this).attr("name"));
	});
	$("body").on("click",".change_status",function(){
		var id = $(this).attr("id");
		var status = $(this).attr("status") == '1' ? 'desactivar' : 'activar';
		swal({   
			title: "Est&aacute;s seguro de " + status + " este usuario?",   
			text: "",   
			type: "warning",   
			confirmButtonText: "Si, " + status, 
			confirmButtonColor: "#428bca",
			cancelButtonText: "No, salir",
			showCancelButton: true,   
			closeOnConfirm: false,   
			showLoaderOnConfirm: true,
			html: true
		}, function(){   
			change_status(id, status);
		});
	});
});

function updatep(pid){
	var xml_data = "<root>";
	xml_data += "<pid>" + pid + "</pid>";
	xml_data += "<name1>" + $(".n11").val() + "</name1>";
	xml_data += "<name2>" + $(".n21").val() + "</name2>";
	xml_data += "<last1>" + $(".l11").val() + "</last1>";
	xml_data += "<last2>" + $(".l21").val() + "</last2>";
	xml_data += "<gender>" + $(".g1").val() + "</gender>";
	xml_data += "<espec>" + $(".tu1").val() + "</espec>";
	xml_data += "<age>" + $(".fn1").val() + "</age>";
	xml_data += "<distric>" + $(".d1").val() + "</distric>";
	xml_data += "<address>" + $(".add1").val() + "</address>";
	xml_data += "<zone>" + $(".z1").val() + "</zone>";
	xml_data += "<phone1>" + $(".p11").val() + "</phone1>";
	xml_data += "<phone2>" + $(".p21").val() + "</phone2>";
	xml_data += "<email>" + $(".e1").val() + "</email>";
	xml_data += "<account>" + $(".nc1").val() + "</account>";
	xml_data += "<codebank>" + $(".cb1").val() + "</codebank>";
	xml_data += "<type_account>" + $(".tc1").val() + "</type_account>";
	xml_data += "</root>";
    var data = {
		xml_data   : xml_data
    };
    $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/updateprofessional',
        success: function(data, responseText, arguments){
        	getallprofessionals();
            $("#modal-edit-p").modal("hide");
            clear();
        }
    });
}

function openmpa(cid){
	var xml_data = "<root><pid>" + cid + "</pid></root>";
	var data = {
		xml_data : xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/getoneprofessional',
        success: function(data){
        	$(".td1 option[value='" + $(data).find("type_doc").text()  + "']").attr('selected', 'selected');
	    	$(".nd1").val($(data).find("num_doc").text());
	    	$(".n11").val($(data).find("name1").text());
	    	$(".n21").val($(data).find("name2").text());
	    	$(".l11").val($(data).find("last1").text());
	    	$(".l21").val($(data).find("last2").text());
	    	$(".fn1").val($(data).find("fn").text());
	    	$(".g1 option[value='" + $(data).find("gender").text()  + "']").attr('selected', 'selected');
	    	$(".tu1 option[value='" + $(data).find("espec").text()  + "']").attr('selected', 'selected');
	    	$(".d1").val($(data).find("district").text());
	    	$(".add1").val($(data).find("address").text());
	    	$(".z1").val($(data).find("zone").text());
	    	$(".e1").val($(data).find("email").text());
	    	$(".p11").val($(data).find("phone1").text());
	    	$(".p21").val($(data).find("phone2").text());
	    	$(".nc1").val($(data).find("number").text());
	    	$(".cb1").val($(data).find("codbank").text());
	    	$(".update-p").attr("name",cid);
            $("#modal-edit-p").modal('show');
        }
    });
}

function initCalendars(){
	 $.datepicker.setDefaults($.datepicker.regional['es']);
    $(".date").datepicker({changeMonth:true,changeYear:true,dateFormat: 'yy-mm-dd'});
}

function clear(){
	$('input').each(function(){
		$(this).val("");
	});
}

function change_status(id, status){
	var xml_data = "<root><uid>" + id + "</uid></root>";
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/change_status',
        success: function(data){
        	if($(data).find("code").text() == 100){
        		getallprofessionals(function(){
        			swal({   
        				title: "Se han aplicado los cambios",   
        				text: "",   
        				type: "success",   
        				showCancelButton: false
        			});
        		});
        	}else{
        		swal({   
    				title: "Ocurrio un error, no se pudo " + status + " el usuario",   
    				text: "",   
    				type: "error",   
    				showCancelButton: false
    			});
        	}
        }
    });
}

function generate_newpass(id){
	var xml_data = "<root><uid>" + id + "</uid></root>";
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/generate_password',
        success: function(data){
        	if($(data).find("code").text() == 100){
        		swal({   
    				title: "Una nueva contrase&ntilde;a se ha enviado al usuario",   
    				text: "",   
    				type: "success",   
    				showCancelButton: false,
    				html:true
    			});
        	}else{
        		swal({   
    				title: "Ocurrio un error, no se pudo generar la contrase&ntilde;a",   
    				text: "",   
    				type: "error",   
    				showCancelButton: false,
    				html:true
    			});
        	}
        }
    });
}

function search(){
	var xml_data = "<root>";
	xml_data += "<search_txt>" + $(".search_text").val() + "</search_txt>";
	xml_data +="</root>";
	var data = {
		xml_data : xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/searchprofessionals',
        success: function(data){
        	var html = "";
        	if(dt !=null)
        		dt.fnClearTable();
        	$(data).find("user").each(function() {
        		var status = $(this).find("status").text() == '1' ? true : false;
    			var color = status ? "btn-danger" : "btn-success";
    			var msg = status ? "Desactivar" : "Activar";
        		if(dt != null){
        			var status = $(this).find("status").text() == '1' ? true : false;
        			var color = status ? "btn-danger" : "btn-success";
        			var msg = status ? "Desactivar" : "Activar";
	        		dt.fnAddData([
	                     $(this).find("type_doc").text(),
	                     $(this).find("num_doc").text(),
	                     $(this).find("full_name").text(),
	                     $(this).find("espec").text(),
	                    "<a class='btn color-black btn-default btn-xs dt mr-5 edit-po' name='" + $(this).find("id").text() + "'> Ver detalles </a>" +
	                    (status ? "<a class='btn color-black btn-default btn-xs gen_pass mr-5' name='" + $(this).find("id").text() + "'> Generar Contrase&ntilde;a </a>" : "") +
			            "<a class='btn " + color + " btn-xs change_status mr-5' status='" + $(this).find("status").text() + "' id='" + $(this).find("id").text() + "'>" + msg + "</a>"
	                ]);
        		}
        	});
        }
    });
}

function initDataTables(){
	dt = $('.data-tables').dataTable({
	    language: {
	        "sProcessing":     "Procesando...",
	        "sLengthMenu":     "Mostrar _MENU_ registros",
	        "sZeroRecords":    "No se encontraron resultados",
	        "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
	        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	        "sInfoPostFix":    "",
	        "sSearch":         "Buscar:  ",
	        "sUrl":            "",
	        "sInfoThousands":  ",",
	        "sLoadingRecords": "Cargando...",
	        "oPaginate": {
	            "sFirst":    "Primero",
	            "sLast":     "Último",
	            "sNext":     "Siguiente",
	            "sPrevious": "Anterior"
	        },
	        "oAria": {
	            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	        }
	    },
	    "paging":false,
	    "searching":false,
	    "info":false,
	    "scrollY": "400px",
        "scrollCollapse": false,
        "scrollX": true,
        "autoWidth": false
	});
	initDt = true;
}

function getallprofessionals(callback){
	 $.ajax({
        type: 'POST',
        url: baseUrl + '/admin/getallprofessionals',
        success: function(data){
        	var html = "";
        	if(dt !=null)
        		dt.fnClearTable();
        	$(data).find("user").each(function() {
        		var status = $(this).find("status").text() == '1' ? true : false;
    			var color = status ? "btn-danger" : "btn-success";
    			var msg = status ? "Desactivar" : "Activar";
        		if(dt != null){
	        		dt.fnAddData([
	                     $(this).find("type_doc").text(),
	                     $(this).find("num_doc").text(),
	                     $(this).find("full_name").text(),
	                     $(this).find("espec").text(),
	                    "<a class='btn color-black btn-default btn-xs dt mr-5  edit-po' name='" + $(this).find("id").text() + "'> Ver detalles </a>" +
	                    (status ? "<a class='btn color-black btn-default btn-xs gen_pass mr-5' id='" + $(this).find("id").text() + "'> Generar Contrase&ntilde;a </a>" : "") +
			            "<a class='btn " + color + " btn-xs change_status mr-5' status='" + $(this).find("status").text() + "' id='" + $(this).find("id").text() + "'>" + msg + "</a>"
	                ]);
        		}else{
		            html += "<tr>";
		            html += "<td>" + $(this).find("type_doc").text() + "</td>";
		            html += "<td>" + $(this).find("num_doc").text() + "</td>";
		            html += "<td>" + $(this).find("full_name").text() + "</td>";
		            html += "<td>" + $(this).find("espec").text() + "</td>";
		            html += "<td><a class='btn color-black btn-default btn-xs dt mr-5  edit-po' name='" + $(this).find("id").text() + "'> Ver detalles </a>";
		            if(status)
		            	html += "<a class='btn color-black btn-default btn-xs gen_pass mr-5' name='" + $(this).find("id").text() + "'> Generar Contrase&ntilde;a </a>";
		            html += "<a class='btn " + color + " btn-xs change_status mr-5' status='" + $(this).find("status").text() + "' id='" + $(this).find("id").text() + "'>" + msg + "</a></td>";
		            html += "</tr>";
        		}
        	});
        	if(dt ==null){
	            $(".tbody").html(html);
	            initDataTables();
        	}
        	if(callback)
        		callback();
        }
    });
}

function savep(){
	var xml_data = "<root>";
	xml_data += "<type_doc>" + $(".td").val() + "</type_doc>";
	xml_data += "<num_doc>" + $(".nd").val() + "</num_doc>";
	xml_data += "<name1>" + $(".n1").val() + "</name1>";
	xml_data += "<name2>" + $(".n2").val() + "</name2>";
	xml_data += "<last1>" + $(".l1").val() + "</last1>";
	xml_data += "<last2>" + $(".l2").val() + "</last2>";
	xml_data += "<gender>" + $(".g").val() + "</gender>";
	xml_data += "<espec>" + $(".tu").val() + "</espec>";
	xml_data += "<age>" + $(".fn").val() + "</age>";
	xml_data += "<distric>" + $(".d").val() + "</distric>";
	xml_data += "<address>" + $(".add").val() + "</address>";
	xml_data += "<zone>" + $(".z").val() + "</zone>";
	xml_data += "<phone1>" + $(".p1").val() + "</phone1>";
	xml_data += "<phone2>" + $(".p2").val() + "</phone2>";
	xml_data += "<email>" + $(".e").val() + "</email>";
	xml_data += "<account>" + $(".nc").val() + "</account>";
	xml_data += "<codebank>" + $(".cb").val() + "</codebank>";
	xml_data += "<type_account>" + $(".tc").val() + "</type_account>";
	xml_data += "</root>";
    var data = {
		xml_data   : xml_data
    };
    $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/registerprofessional',
        success: function(data, responseText, arguments){
        	var html = "<tr>";
            html += "<td>" + $(data).find("type_doc").text() + "</td>";
            html += "<td>" + $(data).find("num_doc").text() + "</td>";
            html += "<td>" + $(data).find("full_name").text() + "</td>";
            html += "<td>" + $(data).find("espec").text() + "</td>";
            html += "<td><a class='btn btn-success btn-xs'> Ver detalles </a></td>";
            html += "</tr>";
            $(".tbody").html(html);
            $("#myModal").modal("hide");
            clear();
        }
    });
}