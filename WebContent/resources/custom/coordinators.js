var baseUrl = window.location.protocol + "//" + window.location.host + "/web";
//var baseUrl = window.location.protocol + "//" + window.location.host;
var dt = null;
$(document).ready(function() {
	$(".savec").click(savec);
	getallcoordinators();
	$("body").on("click", ".dt", function(){
		getPermissions($(this));
	});
	$("body").on("switchChange.bootstrapSwitch", ".chkperm", function(event, state){
		$(".img-permi").removeAttr("style");
		$(".msg-permi").removeAttr("style");
		var value = state ? 1 : 0;
		var xml_data = "<root><pid>" + $(this).attr("id") + "</pid><value>" + value + "</value></root>";
		var data = {
			xml_data: xml_data	
		};
		$.ajax({
	        type: 'POST',
	        url: baseUrl + '/admin/changePermit',
	        data:data,
	        success: function(data){
	        	$(".img-permi").attr("style","display:none;");
	    		$(".msg-permit").html("Se han cambiado los permisos");
	        }
	    });
	});
	$(".clear").click(function(){
		clear();
	});
	$('.search_text').keydown(function(event) {
        if (event.keyCode == 13) {
        	search();
            return false;
         }
	 });
	$("body").on("click",".edit-co",function(){
		openmpa($(this).attr("id"));
	});
	$(".update-c").click(function(){
		updatec($(this).attr("id"));
	});
	$("body").on("click",".gen_pass",function(){
		var id = $(this).attr("id");
		swal({   
			title: "Est&aacute;s seguro de generar una nueva contrase&ntilde;a para este usuario?",   
			text: "",   
			type: "warning",   
			confirmButtonText: "Si, generar", 
			confirmButtonColor: "#428bca",
			cancelButtonText: "No, salir",
			showCancelButton: true,   
			closeOnConfirm: false,   
			showLoaderOnConfirm: true,
			html: true
		}, function(){   
			generate_newpass(id);
		});
	});
	$("body").on("click",".change_status",function(){
		var id = $(this).attr("id");
		var status = $(this).attr("status") == '1' ? 'desactivar' : 'activar';
		swal({   
			title: "Est&aacute;s seguro  de " + status + " este usuario?",   
			text: "",   
			type: "warning",   
			confirmButtonText: "Si, " + status, 
			confirmButtonColor: "#428bca",
			cancelButtonText: "No, salir",
			showCancelButton: true,   
			closeOnConfirm: false,   
			showLoaderOnConfirm: true,
			html: true
		}, function(){   
			change_status(id, status);
		});
	});
});

function change_status(id, status){
	var xml_data = "<root><uid>" + id + "</uid></root>";
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/change_status',
        success: function(data){
        	if($(data).find("code").text() == 100){
        		swal({   
    				title: "Se han aplicado los cambios",   
    				text: "",   
    				type: "success",   
    				showCancelButton: false
    			});
        		getallcoordinators();
        	}else{
        		swal({   
    				title: "Ocurrio un error, no se pudo " + status + " el usuario",   
    				text: "",   
    				type: "error",   
    				showCancelButton: false
    			});
        	}
        }
    });
}

function generate_newpass(id){
	var xml_data = "<root><uid>" + id + "</uid></root>";
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/generate_password',
        success: function(data){
        	if($(data).find("code").text() == 100){
        		swal({   
    				title: "Una nueva contrase&ntilde;a se ha enviado al usuario",   
    				text: "",   
    				type: "success",   
    				showCancelButton: false,
    				html: true
    			});
        	}else{
        		swal({   
    				title: "Ocurrio un error, no se pudo generar la contraseña",   
    				text: "",   
    				type: "error",   
    				showCancelButton: false,
    				html: true
    			});
        	}
        }
    });
}

function updatec(uid){
	var xml_data = "<root>";
	xml_data += "<cid>" + uid + "</cid>";
	xml_data += "<type_doc>" + $(".td1").val() + "</type_doc>";
	xml_data += "<num_doc>" + $(".nd1").val() + "</num_doc>";
	xml_data += "<name1>" + $(".n11").val() + "</name1>";
	xml_data += "<name2>" + $(".n21").val() + "</name2>";
	xml_data += "<last1>" + $(".l11").val() + "</last1>";
	xml_data += "<last2>" + $(".l21").val() + "</last2>";
	xml_data += "<gender>" + $(".g1").val() + "</gender>";
	xml_data += "<fn>" + $(".fn1").val() + "</fn>";
	xml_data += "<phone1>" + $(".p11").val() + "</phone1>";
	xml_data += "<phone2>" + $(".p21").val() + "</phone2>";
	xml_data += "<email>" + $(".e1").val() + "</email>";
	xml_data += "</root>";
    var data = {
		xml_data   : xml_data
    };
    $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/updatecoordinator',
        success: function(data, responseText, arguments){
            location.reload();
        }
    });
}

function openmpa(cid){
	var xml_data = "<root><cid>" + cid + "</cid></root>";
	var data = {
		xml_data : xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/getcoordinator',
        success: function(data){
        	$(".td1 option[value='" + $(data).find("type_doc").text()  + "']").attr('selected', 'selected');
	    	$(".nd1").val($(data).find("num_doc").text());
	    	$(".n11").val($(data).find("name1").text());
	    	$(".n21").val($(data).find("name2").text());
	    	$(".l11").val($(data).find("last1").text());
	    	$(".l21").val($(data).find("last2").text());
	    	$(".fn1").val($(data).find("fn").text());
	    	$(".g1 option[value='" + $(data).find("gender").text()  + "']").attr('selected', 'selected');
	    	$(".e1").val($(data).find("email").text());
	    	$(".p11").val($(data).find("phone1").text());
	    	$(".p21").val($(data).find("phone2").text());
	    	$(".update-c").attr("id",$(data).find("id").text());
            $("#modal_edit").modal('show');
        }
    });
}

function clear(){
	$('input').each(function(){
		$(this).val("");
	});
}

function search(){
	var xml_data = "<root>";
	xml_data += "<search_txt>" + $(".search_text").val() + "</search_txt>";
	xml_data +="</root>";
	var data = {
		xml_data : xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/searchcoord',
        success: function(data){
        	var html = "";
        	if(dt !=null)
        		dt.fnClearTable();
        	$(data).find("user").each(function() {
        		var status = $(this).find("status").text() == '1' ? true : false;
    			var color = status ? "btn-danger" : "btn-success";
    			var msg = status ? "Desactivar" : "Activar";
        		dt.fnAddData([
                     $(this).find("type_doc").text(),
                     $(this).find("num_doc").text(),
                     $(this).find("full_name").text(),
                     $(this).find("email").text(),
                     (status ? "<a class='btn color-black btn-default btn-xs dt mr-5' name='" + $(this).find("id").text() + "'> Permisos </a>" : "") +
                    "<a class='btn color-black btn-default btn-xs edit-co mr-5' id='" + $(this).find("id").text() + "'> Detalle </a>" +
                    (status ? "<a class='btn color-black btn-default btn-xs gen_pass mr-5' id='" + $(this).find("id").text() + "'> Generar Contrase&ntilde;a </a>" : "") +
		            "<a class='btn " + color + " btn-xs change_status mr-5' status='" + $(this).find("status").text() + "' id='" + $(this).find("id").text() + "'>" + msg + "</a>"
                ]);
        	});
        }
    });
}

function getPermissions(element){
	var xml_data = "<root><uid>" + $(element).attr("name") + "</uid></root>";
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/getpermissions',
        success: function(data){
        	var html = "";
        	$(data).find("permit").each(function() {
        		html += '<div class="form-group">';
        		html += '<div class="col-sm-2 col-xs-2 col-md-2 col-lg-2 controls">';
        		html += '<input type="checkbox" data-rel="switch" data-size="mini" data-wrapper-class="blue" id="' + $(this).find("id_permit").text() + '" class="chkperm" ' + $(this).find("value").text() + '>';
        		html += '</div>';
        		html += '<label class="col-sm-10 col-xs-10 col-md-10 col-lg-10">' + $(this).find("name").text() + '</label>';
        		html += '</div>';
        	});
            $(".frm-perm").html(html);
            $("#modal_perm").modal("show");
            $(".chkperm").bootstrapSwitch();
        }
    });
}

function getallcoordinators(){
	 $.ajax({
        type: 'POST',
        url: baseUrl + '/admin/getallcoordinators',
        success: function(data){
        	var html = "";
        	if(dt !=null)
        		dt.fnClearTable();
        	$(data).find("user").each(function() {
        		var status = $(this).find("status").text() == '1' ? true : false;
    			var color = status ? "btn-danger" : "btn-success";
    			var msg = status ? "Desactivar" : "Activar";
        		if(dt != null){
	        		dt.fnAddData([
	                     $(this).find("type_doc").text(),
	                     $(this).find("num_doc").text(),
	                     $(this).find("full_name").text(),
	                     $(this).find("email").text(),
	                    (status ? "<a class='btn color-black btn-default btn-xs dt mr-5 col-md-6' name='" + $(this).find("id").text() + "'> Permisos </a>" : "") +
	                    "<a class='btn color-black btn-default btn-xs edit-co mr-5 col-md-5' id='" + $(this).find("id").text() + "'> Detalle </a>" +
	                    (status ? "<a class='btn color-black btn-default btn-xs gen_pass mr-5 col-md-6' id='" + $(this).find("id").text() + "'> Generar Contrase&ntilde;a </a>" : "") +
			            "<a class='btn " + color + " btn-xs change_status mr-5 col-md-5' status='" + $(this).find("status").text() + "' id='" + $(this).find("id").text() + "'>" + msg + "</a>"
	                ]);
        		}else{
		            html += "<tr>";
		            html += "<td>" + $(this).find("type_doc").text() + "</td>";
		            html += "<td>" + $(this).find("num_doc").text() + "</td>";
		            html += "<td>" + $(this).find("full_name").text() + "</td>";
		            html += "<td>" + $(this).find("email").text() + "</td><td>";
		            if(status)
		            	html += "<a class='btn color-black btn-default btn-xs dt mr-5 col-md-6' name='" + $(this).find("id").text() + "'> Permisos </a>";
		            html += "<a class='btn color-black btn-default btn-xs edit-co mr-5 col-md-5' id='" + $(this).find("id").text() + "'> Detalle </a>";
		            if(status)
		            	html += "<a class='btn color-black btn-default btn-xs gen_pass mr-5 col-md-6' id='" + $(this).find("id").text() + "'> Generar Contrase&ntilde;a </a>";
		            html += "<a class='btn " + color + " btn-xs change_status mr-5 col-md-5' status='" + $(this).find("status").text() + "' id='" + $(this).find("id").text() + "'>" + msg + "</a></td>";
		            html += "</tr>";
        		}
        	});
        	if(dt ==null){
	            $(".tbody").html(html);
	            initDataTables();
        	}
        }
    });
}

function savec(){
	var xml_data = "<root>";
	xml_data += "<type_doc>" + $(".td").val() + "</type_doc>";
	xml_data += "<num_doc>" + $(".nd").val() + "</num_doc>";
	xml_data += "<name1>" + $(".n1").val() + "</name1>";
	xml_data += "<name2>" + $(".n2").val() + "</name2>";
	xml_data += "<last1>" + $(".l1").val() + "</last1>";
	xml_data += "<last2>" + $(".l2").val() + "</last2>";
	xml_data += "<gender>" + $(".g").val() + "</gender>";
	xml_data += "<age>" + $(".fn").val() + "</age>";
	xml_data += "<phone1>" + $(".p1").val() + "</phone1>";
	xml_data += "<phone2>" + $(".p2").val() + "</phone2>";
	xml_data += "<email>" + $(".e").val() + "</email>";
	xml_data += "</root>";
    var data = {
		xml_data   : xml_data
    };
    $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/registercoordinator',
        success: function(data, responseText, arguments){
            location.reload();
        }
    });
}

function initDataTables(){
    dt = $('.data-tables').dataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:  ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "paging":false,
        "searching":false,
        "info":false,
        "scrollY": "400px",
        "scrollX": true,
        "scrollCollapse": false,
    });
}