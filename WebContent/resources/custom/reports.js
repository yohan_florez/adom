var baseUrl = window.location.protocol + "//" + window.location.host + "/web";
//var baseUrl = window.location.protocol + "//" + window.location.host;
$(document).ready(function(){
	$(".entitylist").change(getPlans);
	$(".entitylist2").change(getPlans2);
	$(".entitylist3").change(getPlans3);
	$(".generate").click(function(){
		$(".generate").toggleClass('active');
		$(".generate").attr('disabled','disabled');
		generate();
	});
	$(".generate2").click(function(){
		$(".generate2").toggleClass('active');
		$(".generate2").attr('disabled','disabled');
		generate2();
	});
	$(".generate3").click(function(){
		$(".generate3").toggleClass('active');
		$(".generate3").attr('disabled','disabled');
		generate3();
	});
	$(".generate4").click(function(){
		$(".generate4").toggleClass('active');
		$(".generate4").attr('disabled','disabled');
		generate4();
	});
	$(".generate5").click(function(){
		$(".generate5").toggleClass('active');
		$(".generate5").attr('disabled','disabled');
		generate5();
	});
	$(".generate6").click(function(){
		$(".generate6").toggleClass('active');
		$(".generate6").attr('disabled','disabled');
		generate6();
	});
	$(".generate7").click(function(){
		$(".generate7").toggleClass('active');
		$(".generate7").attr('disabled','disabled');
		generate7();
	});
	$(".generate8").click(function(){
		$(".generate8").toggleClass('active');
		$(".generate8").attr('disabled','disabled');
		generate8();
	});
});

function generate(){
	var xml_data = "<root>";
	xml_data += "<r>1</r>";
	xml_data += "<pid>" + $(".entityplans option:selected").val() + "</pid>";
	xml_data += "<eid>" + $(".entitylist option:selected").val() + "</eid>";
	xml_data += "<start>" + startd + "</start>";
	xml_data += "<end>" + endd + "</end>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data
	}
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/generate_report',
        success: function(data){
        	$(".generate").removeClass('active');
    		$(".generate").removeAttr('disabled');
        	location.href = baseUrl + "/admin/download-" + $(data).find("file").text();
        }
    });
}

function generate2(){
	var xml_data = "<root>";
	xml_data += "<r>2</r>";
	xml_data += "<pid>" + $(".entityplans2 option:selected").val() + "</pid>";
	xml_data += "<eid>" + $(".entitylist2 option:selected").val() + "</eid>";
	xml_data += "<start>" + startd + "</start>";
	xml_data += "<end>" + endd + "</end>";
	xml_data += "<type>" + $(".type2 option:selected").val() + "</type>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data
	}
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/generate_report',
        success: function(data){
        	$(".generate2").removeClass('active');
    		$(".generate2").removeAttr('disabled');
        	location.href = baseUrl + "/admin/download-" + $(data).find("file").text();
        }
    });
}

function generate3(){
	var xml_data = "<root>";
	xml_data += "<r>3</r>";
	xml_data += "<pid>" + $(".entityplans3 option:selected").val() + "</pid>";
	xml_data += "<eid>" + $(".entitylist3 option:selected").val() + "</eid>";
	xml_data += "<start>" + startd + "</start>";
	xml_data += "<end>" + endd + "</end>";
	xml_data += "<start1>" + startd1 + "</start1>";
	xml_data += "<end1>" + endd1 + "</end1>";
	xml_data += "<type>" + $(".type3 option:selected").val() + "</type>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data
	}
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/generate_report',
        success: function(data){
        	$(".generate3").removeClass('active');
    		$(".generate3").removeAttr('disabled');
        	location.href = baseUrl + "/admin/download-" + $(data).find("file").text();
        }
    });
}

function generate4(){
	$(".pedit").removeClass("active");
	$("#tabe4").addClass("active");
	var xml_data = "<root>";
	xml_data += "<r>4</r>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data
	}
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/generate_report',
        success: function(data){
        	$(".generate4").removeClass('active');
    		$(".generate4").removeAttr('disabled');
        	location.href = baseUrl + "/admin/download-" + $(data).find("file").text();
        }
    });
}

function generate5(){
	var xml_data = "<root>";
	xml_data += "<r>5</r>";
	xml_data += "<start>" + startd + "</start>";
	xml_data += "<end>" + endd + "</end>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data
	}
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/generate_report',
        success: function(data){
        	$(".generate5").removeClass('active');
    		$(".generate5").removeAttr('disabled');
        	location.href = baseUrl + "/admin/download-" + $(data).find("file").text();
        }
    });
}

function generate6(){
	$(".pedit").removeClass("active");
	$("#tabe6").addClass("active");
	var xml_data = "<root>";
	xml_data += "<r>6</r>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data
	}
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/generate_report',
        success: function(data){
        	$(".generate6").removeClass('active');
    		$(".generate6").removeAttr('disabled');
        	location.href = baseUrl + "/admin/download-" + $(data).find("file").text();
        }
    });
}

function generate7(){
	var xml_data = "<root>";
	xml_data += "<r>7</r>";
	xml_data += "<pid>" + $(".professional option:selected").val() + "</pid>";
	xml_data += "<nro>" + $(".nro_comp").val() + "</nro>";
	xml_data += "<start>" + startd + "</start>";
	xml_data += "<end>" + endd + "</end>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data
	}
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/generate_report',
        success: function(data){
        	$(".generate7").removeClass('active');
    		$(".generate7").removeAttr('disabled');
        	location.href = baseUrl + "/admin/download-" + $(data).find("file").text();
        }
    });
}

function generate8(){
	var xml_data = "<root>";
	xml_data += "<r>8</r>";
	xml_data += "<eid>" + $(".entitylist8 option:selected").val() + "</eid>";
	xml_data += "<start>" + startd + "</start>";
	xml_data += "<end>" + endd + "</end>";
	xml_data += "<start1>" + startd1 + "</start1>";
	xml_data += "<end1>" + endd1 + "</end1>";
	xml_data += "<start2>" + startd2 + "</start2>";
	xml_data += "<end2>" + endd2 + "</end2>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data
	}
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/generate_report',
        success: function(data){
        	$(".generate8").removeClass('active');
    		$(".generate8").removeAttr('disabled');
        	location.href = baseUrl + "/admin/download-" + $(data).find("file").text();
        }
    });
}

function getPlans(){
	var xml_data = "<root><eid>" + $(".entitylist").val() + "</eid></root>";
	var data = {
		xml_data: xml_data
	};
	 $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/getPlans',
        success: function(data){
        	var html = "<option value='*'>TODOS</option>";
        	$(data).find("plan").each(function() {
	            html += "<option value='" + $(this).find("id").text() + "'>" + $(this).find("name").text().toUpperCase() + "</option>";
        	});
            $(".entityplans").html(html);
        }
    });
}

function getPlans2(){
	var xml_data = "<root><eid>" + $(".entitylist2").val() + "</eid></root>";
	var data = {
		xml_data: xml_data
	};
	 $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/getPlans',
        success: function(data){
        	var html = "<option value='*'>TODOS</option>";
        	$(data).find("plan").each(function() {
	            html += "<option value='" + $(this).find("id").text() + "'>" + $(this).find("name").text().toUpperCase() + "</option>";
        	});
            $(".entityplans2").html(html);
        }
    });
}

function getPlans3(){
	var xml_data = "<root><eid>" + $(".entitylist3").val() + "</eid></root>";
	var data = {
		xml_data: xml_data
	};
	 $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/getPlans',
        success: function(data){
        	var html = "<option value='*'>TODOS</option>";
        	$(data).find("plan").each(function() {
	            html += "<option value='" + $(this).find("id").text() + "'>" + $(this).find("name").text().toUpperCase() + "</option>";
        	});
            $(".entityplans3").html(html);
        }
    });
}