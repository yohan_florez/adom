var baseUrl = window.location.protocol + "//" + window.location.host + "/web";
//var baseUrl = window.location.protocol + "//" + window.location.host;
var initDt = false;
$(document).ready(function() {
	$(".savep").click(savep);
	getHistoryPatients();
	$("body").on("click", ".vd", function(){
		patient = $(this).attr("id");
		$(".formnewentry").attr("style","display:none;");
		$("#modalingr").modal('show');
		$(".modal-title2").html("Lista de ingresos - " + $(this).attr("id2") + " - " +  $(this).attr("name"));
		getEntry(patient);
		getallentities();
	});
	$(".cancel").click(function(){
		$(".formnewentry").attr("style","display:none;");
	});
	$(".newentry").click(function(){
		$(".formnewentry").removeAttr("style");
	});
	$(".newentrybtn").click(saveentry);
	$("body").on("click",".entryd",function(){
		saveHistory($(this).attr("id"));
	});
	$(".entitylist").change(getPlans);
	$(".clear").click(function(){
		clear();
	});
	 $('.search_text').keydown(function(event) {
        if (event.keyCode == 13) {
        	search();
            return false;
         }
	 });
	 initCalendars();
	 $(".fn").change(calcularEdad);
});

function calcularEdad(){
    var fecha = $(".fn").val();
    var values=fecha.split("-");
    var dia = values[2];
    var mes = values[1];
    var ano = values[0];
    var fecha_hoy = new Date();
    var ahora_ano = fecha_hoy.getYear();
    var ahora_mes = fecha_hoy.getMonth()+1;
    var ahora_dia = fecha_hoy.getDate();
    var edad = (ahora_ano + 1900) - ano;
    if ( ahora_mes < mes )
        edad--;
    if ((mes == ahora_mes) && (ahora_dia < dia))
        edad--;
    if (edad > 1900)
        edad -= 1900;
    var meses=0;
    if(ahora_mes>mes)
        meses=ahora_mes-mes;
    if(ahora_mes<mes)
        meses=12-(mes-ahora_mes);
    if(ahora_mes==mes && dia>ahora_dia)
        meses=11;
    var dias=0;
    if(ahora_dia>dia)
        dias=ahora_dia-dia;
    if(ahora_dia<dia){
        ultimoDiaMes=new Date(ahora_ano, ahora_mes, 0);
        dias=ultimoDiaMes.getDate()-(dia-ahora_dia);
    }
    if(edad == 0){
    	$(".a").val(meses);
    	$('.ta option[valuw="M"]').attr("selected","selected");
    }else{
    	$(".a").val(edad);
    	$('.ta option[value="A"]').attr("selected","selected");
    }
    console.log("Tienes "+edad+" años, "+meses+" meses y "+dias+" días");
}

function initCalendars(){
	 $.datepicker.setDefaults($.datepicker.regional['es']);
    $(".date").datepicker({changeMonth:true,changeYear:true,dateFormat: 'yy-mm-dd'});
}

function initDataTables(){
	$('.data-tables').dataTable({
	    language: {
	        "sProcessing":     "Procesando...",
	        "sLengthMenu":     "Mostrar _MENU_ registros",
	        "sZeroRecords":    "No se encontraron resultados",
	        "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
	        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	        "sInfoPostFix":    "",
	        "sSearch":         "Buscar:  ",
	        "sUrl":            "",
	        "sInfoThousands":  ",",
	        "sLoadingRecords": "Cargando...",
	        "oPaginate": {
	            "sFirst":    "Primero",
	            "sLast":     "Último",
	            "sNext":     "Siguiente",
	            "sPrevious": "Anterior"
	        },
	        "oAria": {
	            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	        }
	    },
	    "paging":false,
	    "searching":false,
	    "info":false,
	    "scrollY": "200px",
	});
	initDt = true;
}

function saveHistory(id){
	var xml_data = "<root>";
	xml_data += "<entry>" + id + "</entry>";
	xml_data +="</root>";
	var data = {
		xml_data : xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/savehistory',
        success: function(data){
        	location.href = baseUrl + '/admin/details/entry-' + id;
        }
    });
}

function search(){
	var xml_data = "<root>";
	xml_data += "<search_txt>" + $(".search_text").val() + "</search_txt>";
	xml_data +="</root>";
	var data = {
		xml_data : xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/searchpatients',
        success: function(data){
        	var html = "";
        	$(data).find("user").each(function() {
	            html += "<tr>";
	            html += "<td>" + $(this).find("type_doc").text() + "</td>";
	            html += "<td>" + $(this).find("num_doc").text() + "</td>";
	            html += "<td>" + $(this).find("full_name").text() + "</td>";
	            html += "<td>" + $(this).find("phone").text() + "</td>";
	            html += "<td><a class='btn color-black btn-default btn-xs vd' id='" + $(this).find("id").text() + "' id2='" + $(this).find("num_doc").text() + "' name='" + $(this).find("full_name").text() + "'> Ver detalles </a></td>";
	            html += "</tr>";
        	});
            $(".tbody").html(html);
            if(!initDt)
            	initDataTables();
        }
    });
}

function getEntry(pid){
	var xml_data = "<root><patient>" + pid + "</patient></root>";
	var data = {
		xml_data : xml_data	
	};
	$.ajax({
        type: 'POST',
        data : data,
        url: baseUrl + '/admin/getEntrys',
        success: function(data){
        	var html = "";
        	$(".newentry").removeAttr("style");
        	if($(data).find("entry").length == 0){
        		html = "El paciente no tiene ingresos";
        	}else{
	        	$(data).find("entry").each(function() {
	        		if($(this).find("end").text() == "Activo"){
	        			html += '<div class="row curpoint entryd entry-active" id="' + $(this).find("entry_id").text() + '">';
	        			$(".newentry").attr("style", "display:none;");
	        		}else
	        			html += '<div class="row curpoint entryd entry-inactive" id="' + $(this).find("entry_id").text() + '" >';
		        	html += '<div class="col-md-5"><strong>Fecha de ingreso:</strong> ' + $(this).find("start").text() + '</div>';
		        	html += '<div class="col-md-7"><strong>Fecha de salida:</strong>  ' + $(this).find("end").text() + '</div>';
		        	html += '<div class="col-md-5"><strong>CIE10:</strong>  ' + $(this).find("cie10").text() + '</div>';
		        	html += '<div class="col-md-7"><strong>Descripci&oacute;n:</strong> ' + $(this).find("cie10_desc").text() + '</div>';
		        	html += '</div>';
	        	});
        	}
        	$(".listentrys").html(html);
        }
    });
}

function getHistoryPatients(){
	 $.ajax({
        type: 'POST',
        url: baseUrl + '/admin/getHistoryPatients',
        success: function(data){
        	var html = "";
        	$(data).find("user").each(function() {
	            html += "<tr>";
	            html += "<td>" + $(this).find("type_doc").text() + "</td>";
	            html += "<td>" + $(this).find("num_doc").text() + "</td>";
	            html += "<td>" + $(this).find("full_name").text() + "</td>";
	            html += "<td>" + $(this).find("phone").text() + "</td>";
	            html += "<td><a class='btn color-black btn-default btn-xs vd' id='" + $(this).find("id").text() + "' id2='" + $(this).find("num_doc").text() + "' name='" + $(this).find("full_name").text() + "'> Ver detalles </a></td>";
	            html += "</tr>";
        	});
            $(".tbody").html(html);
        }
    });
}

function getallentities(){
	 $.ajax({
       type: 'POST',
       url: baseUrl + '/admin/getallentities',
       success: function(data){
    	    var html = "";
		   	$(data).find("entity").each(function() {
		         html += "<option value='" + $(this).find("id").text() + "'>" + $(this).find("name").text().toUpperCase(); + "</td>";
		   	});
		    $(".entitylist").html(html);
		    getPlans();
       }
   });
}

function getPlans(){
	var xml_data = "<root><eid>" + $(".entitylist").val() + "</eid></root>";
	var data = {
		xml_data: xml_data
	};
	 $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/getPlans',
        success: function(data){
        	var html = "";
        	$(data).find("plan").each(function() {
        		if($(this).find("status").text() == "true")
        			html += "<option value='" + $(this).find("id").text() + "'>" + $(this).find("name").text().toUpperCase(); + "</option>";
        	});
            $(".entityPlan").html(html);
        }
    });
}

function savep(){
	var xml_data = "<root>";
	xml_data += "<type_doc>" + $(".td").val() + "</type_doc>";
	xml_data += "<num_doc>" + $(".nd").val() + "</num_doc>";
	xml_data += "<name1>" + $(".n1").val() + "</name1>";
	xml_data += "<name2>" + $(".n2").val() + "</name2>";
	xml_data += "<last1>" + $(".l1").val() + "</last1>";
	xml_data += "<last2>" + $(".l2").val() + "</last2>";
	xml_data += "<gender>" + $(".g").val() + "</gender>";
	xml_data += "<type_user>" + $(".tu").val() + "</type_user>";
	xml_data += "<age>" + $(".a").val() + "</age>";
	xml_data += "<fn>" + $(".fn").val() + "</fn>";
	xml_data += "<type_age>" + $(".ta").val() + "</type_age>";
	xml_data += "<distric>" + $(".d").val() + "</distric>";
	xml_data += "<address>" + $(".add").val() + "</address>";
	xml_data += "<zone>" + $(".z").val() + "</zone>";
	xml_data += "<phone1>" + $(".p1").val() + "</phone1>";
	xml_data += "<phone2>" + $(".p2").val() + "</phone2>";
	xml_data += "<email>" + $(".e").val() + "</email>";
	xml_data += "</root>";
    var data = {
		xml_data   : xml_data
    };
    $(".bas").toggleClass('active');
	$(".bas").attr('disabled','disabled');
	$(".close-model").attr('disabled','disabled');
    $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/registerpatient',
        success: function(data, responseText, arguments){
            var html = "<tr>";
            html += "<td>" + $(data).find("type_doc").text() + "</td>";
            html += "<td>" + $(data).find("num_doc").text() + "</td>";
            html += "<td>" + $(data).find("full_name").text() + "</td>";
            html += "<td>" + $(data).find("phone").text() + "</td>";
            html += "<td><a class='btn color-black btn-default btn-xs vd' id2='" + $(data).find("num_doc").text() + "' id='" + $(data).find("id").text() + "' name='" + $(data).find("full_name").text() + "'> Ver detalles </a></td>";
            html += "</tr>";
            $(".tbody").html(html);
            $("#myModal").modal("hide");
            $(".bas").removeAttr('disabled','disabled');
    		$(".close-model-assignservice").removeAttr('disabled','disabled');
    		$(".bas").removeClass('active');
            clear();
        }
    });
}

function clear(){
	$('input').each(function(){
		$(this).val("");
	});
	setDefaults();
}

function setDefaults(){
	$(".tu").val('5');
}

function saveentry(){
	var xml_data = "<root>";
	xml_data += "<patient>" + patient + "</patient>";
	xml_data += "<code>" + $(".cie10").val() + "</code>";
	xml_data += "<plan>" + $(".entityPlan").val() + "</plan>";
	xml_data += "<number>" + $(".number_c").val() + "</number>";
	xml_data += "<description>" + $(".cie10desc").val() + "</description>";
	xml_data += "</root>";
    var data = {
		xml_data   : xml_data
    };
    $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/saveentry',
        success: function(data, responseText, arguments){
        	$(".formnewentry").attr("style","display:none;");
        	getEntry(patient);
        	clear();
        }
    });
}