var baseUrl = window.location.protocol + "//" + window.location.host + "/web";
//var baseUrl = window.location.protocol + "//" + window.location.host;
var eid = "";
var dt = null;
var dt2 = null;
$(document).ready(function() {
	$(".savep").click(savep);
	$(".newplan").click(saveplan);
	$(".newtarserv").click(saverate);
	getallentities();
	$("body").on("click", ".tar", function(e){
		$(".tbl-tar").attr("style","display:none;");
		eid = $(this).attr("id");
		getPlans(eid);
		$("#modaltar").modal('show');
		$(".modal-title2").html("Planes y Tarifas : " + $(this).attr("name"));
	});
	$("body").on("click", ".edit", function(e){
		eid = $(this).attr("id");
		getentity(eid);
	});
	$("body").on("click", ".remove_rate", function(e){
		eid = $(this).attr("id");
		removerate(eid);
	});
	$(".plans").change(getRates);
	$(".clear").click(function(){
		clear();
	});
	$(".updatep").click(function(){
		updatep($(this).attr("id"));
	});
	$("body").on("click",".status-plan",function(){
		var id = $(".plans option:selected").val();
		var status = $(".plans option:selected").attr("id") == 'true' ? 'desactivar' : 'activar';
		swal({   
			title: "Est&aacute;s seguro de " + status + " este plan?",   
			text: "",   
			type: "warning",   
			confirmButtonText: "Si, " + status, 
			confirmButtonColor: "#428bca",
			cancelButtonText: "No, salir",
			showCancelButton: true,   
			closeOnConfirm: false,   
			showLoaderOnConfirm: true,
			html: true
		}, function(){   
			change_status(id, status);
		});
	});
});

function change_status(id, status){
	var xml_data = "<root><pid>" + id + "</pid></root>";
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/changestatusplan',
        success: function(data){
        	if($(data).find("code").text() == 100){
        		getPlans(eid,function(){
        			$(".plans option[value='" + id  + "']").attr('selected', 'selected');
        			if($(".plans option:selected").attr("id") == "true"){
        				$(".status-plan").html("DESACTIVAR"	);
        				$(".status-plan").removeClass("btn-success");
        				$(".status-plan").addClass("btn-danger");
        			}else{
        				$(".status-plan").html("ACTIVAR	");
        				$(".status-plan").removeClass("btn-danger");
        				$(".status-plan").addClass("btn-success");
        			}
        			swal({   
        				title: "Se han aplicado los cambios",   
        				text: "",   
        				type: "success",   
        				showCancelButton: false
        			});
        		});
        	}else{
        		swal({   
    				title: "Ocurrio un error, no se pudo " + status + " el usuario",   
    				text: "",   
    				type: "error",   
    				showCancelButton: false
    			});
        	}
        }
    });
}

function clear(){
	$('input').each(function(){
		$(this).val("");
	});
}

function getPlans(id, callback){
	var xml_data = "<root><eid>" + id + "</eid></root>";
	var data = {
		xml_data: xml_data
	};
	 $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/getPlans',
        success: function(data){
        	var html = "<option>Seleccione un plan</option>";
        	$(data).find("plan").each(function() {
	            html += "<option id='" + $(this).find("status").text() + "' value='" + $(this).find("id").text() + "'>" + $(this).find("name").text().toUpperCase(); + "</option>";
        	});
            $(".plans").html(html);
            if(callback)
            	callback();
        }
    });
}

function removerate(id){
	var xml_data = "<root><rid>" + id + "</rid></root>";
	var data = {
		xml_data: xml_data
	};
	 $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/removerate',
        success: function(data){
        	if($(data).find("code").text() == '100')
        		getRates();
        	else{
        		swal({   
    				title: "Ocurrio un error, no se pudo borrar la tarifa",   
    				text: "",   
    				type: "error",   
    				showCancelButton: false
    			});
        	}
        }
    });
}

function getRates(){
	if($(".plans option:selected").attr("id") == "true"){
		$(".status-plan").html("DESACTIVAR"	);
		$(".status-plan").removeClass("btn-success");
		$(".status-plan").addClass("btn-danger");
	}else{
		$(".status-plan").html("ACTIVAR	");
		$(".status-plan").removeClass("btn-danger");
		$(".status-plan").addClass("btn-success");
	}
	$(".meac").removeAttr("style");
	var xml_data = "<root><plan>" + $(".plans").val() + "</plan></root>";
	var data = {
		xml_data: xml_data
	};
	 $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/getRates',
        success: function(data){
        	$(".tbl-tar").removeAttr("style");
        	var html = "";
        	if(dt2 !=null)
        		dt2.fnClearTable();
        	$(data).find("rate").each(function() {
        		if(dt2 != null){
	        		dt2.fnAddData([
	                    $(this).find("service").text(),
	                    $(this).find("cost").text(),
	                    '<a id="' + $(this).find("id").text() + '" class="remove_rate btn menu-icon color-black btn-default btn-xs">Borrar</a></td>'
	                ]);
        		}else{
		            html += "<tr>";
		            html += "<td>" + $(this).find("service").text() + "</td>";
		            html += "<td>" + parseFloat($(this).find("cost").text()).toFixed(0) + "</td>";
		            html += '<td><a id="' + $(this).find("id").text() + '" class="remove_rate btn menu-icon color-black btn-default btn-xs">Borrar</a></td>';
		            html += "</tr>";
        		}
        	});
        	if(dt2 == null){
        		$(".tbody-2").html(html);
        		initDataTables_2();
        	}
            getAllservices();
            
        }
    });
}

function getAllservices(){
	 $.ajax({
      type: 'POST',
      url: baseUrl + '/admin/getallservices',
      success: function(data){
		var html = "<option>Seleccione un servicio</option>";
		$(data).find("service").each(function() {
	        html += "<option value='" + $(this).find("id").text() + "'>" + $(this).find("name").text().toUpperCase() + "</td>";
		});
		$(".lservicios").html(html);
      }
  });
}

function getallentities(){
	 $.ajax({
        type: 'POST',
        url: baseUrl + '/admin/getallentities',
        success: function(data){
        	var html = "";
        	if(dt !=null)
        		dt.fnClearTable();
        	$(data).find("entity").each(function() {
        		if(dt != null){
	        		dt.fnAddData([
	                    $(this).find("nit").text(),
	                    $(this).find("rs").text(),
	                    $(this).find("code").text(),
	                    $(this).find("name").text(),
	                    '<a name="' + $(this).find("rs").text() + '" id="' + $(this).find("id").text() + '" class="edit btn menu-icon color-black btn-default">Ver detalles</a>' +
	                    '<a name="' + $(this).find("rs").text() + '" id="' + $(this).find("id").text() + '" class="tar btn menu-icon color-black btn-default"><i class="fa fa-cog"> Planes y tarifas</i></a>'
	                ]);
        		}else{
		            html += "<tr>";
		            html += "<td>" + $(this).find("nit").text() + "</td>";
		            html += "<td>" + $(this).find("rs").text() + "</td>";
		            html += "<td>" + $(this).find("code").text() + "</td>";
		            html += "<td>" + $(this).find("name").text() + "</td>";
		            html += "<td class='menu-action'>";
		            html += '<a name="' + $(this).find("rs").text() + '" id="' + $(this).find("id").text() + '" class="edit btn menu-icon color-black btn-default">Ver detalles</a>';
		            html += '<a name="' + $(this).find("rs").text() + '" id="' + $(this).find("id").text() + '" class="tar btn menu-icon color-black btn-default"><i class="fa fa-cog"> Planes y tarifas</i></a>';
		            html += "</td>";
		            html += "</tr>";
        		}
        	});
        	if(dt ==null){
	            $(".tbody").html(html);
	            initDataTables();
        	}
        }
    });
}

function getentity(eid){
	var xml_data = "<root><eid>" + eid + "</eid></root>";
	var data = {
		xml_data : xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/getentity',
        success: function(data){
	    	$(".nit1").val($(data).find("nit").text());
	    	$(".rs1").val($(data).find("rs").text());
	    	$(".cod1").val($(data).find("code").text());
	    	$(".nam1").val($(data).find("name").text());
	    	$(".updatep").attr("id",$(data).find("id").text());
            $("#modal-update").modal('show');
        }
    });
}

function savep(){
	var xml_data = "<root>";
	xml_data += "<nit>" + $(".nit").val() + "</nit>";
	xml_data += "<rs>" + $(".rs").val() + "</rs>";
	xml_data += "<code>" + $(".cod").val() + "</code>";
	xml_data += "<name>" + $(".nam").val() + "</name>";
	xml_data += "</root>";
    var data = {
		xml_data   : xml_data
    };
    $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/registerentity',
        success: function(data, responseText, arguments){
        	getallentities();
            $("#myModal").modal("hide");
            clear();
        }
    });
}

function updatep(eid){
	var xml_data = "<root>";
	xml_data += "<eid>" + eid + "</eid>";
	xml_data += "<nit>" + $(".nit1").val() + "</nit>";
	xml_data += "<rs>" + $(".rs1").val() + "</rs>";
	xml_data += "<code>" + $(".cod1").val() + "</code>";
	xml_data += "<name>" + $(".nam1").val() + "</name>";
	xml_data += "</root>";
    var data = {
		xml_data   : xml_data
    };
    $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/update_entity',
        success: function(data, responseText, arguments){
        	getallentities();
            $("#modal-update").modal("hide");
            clear();
        }
    });
}

function saveplan(){
	var xml_data = "<root>";
	xml_data += "<name>" + $(".planname").val() + "</name>";
	xml_data += "<entity>" + eid + "</entity>";
	xml_data += "</root>";
    var data = {
		xml_data : xml_data
    };
    $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/saveplan',
        success: function(data, responseText, arguments){
        	if($(data).find("code").text() == "100"){
        		$(".msg-plan").html("El plan se ha agregado correctamente!");
        		$(".msg-plan").attr("style", "color:green;");
        		$(".planname").val("");
        	}else{
        		$(".msg-plan").html("Error: No se pudo crear el plan");
        		$(".msg-plan").attr("style", "color:red;");
        	}
        	getPlans(eid);
        	clear();
        }
    });
}

function saverate(){
	var xml_data = "<root>";
	xml_data += "<plan>" + $(".plans").val() + "</plan>";
	xml_data += "<service>" + $(".lservicios").val() + "</service>";
	xml_data += "<cost>" + $(".tarval").val() + "</cost>";
	xml_data += "</root>";
    var data = {
		xml_data   : xml_data
    };
    $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/saverate',
        success: function(data, responseText, arguments){
        	if($(data).find("code").text() == "100"){
        		$(".msg-tari").html("Se ha agregado correctamente!");
        		$(".msg-tari").attr("style", "color:green;");
        		$(".tarval").val("");
        	}else{
        		$(".msg-tari").html("Error: No se pudo asignar la tarifa");
        		$(".msg-tari").attr("style", "color:red;");
        	}
        	getRates();
        	clear();
        }
    });
}

function initDataTables(){
	dt = $('.data-tables').dataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:  ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "paging":false,
        "searching":true,
        "info":false
    });
}

function initDataTables_2(){
	dt2 = $('.data-tables-2').dataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:  ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "paging":false,
        "searching":false,
        "info":false,
        "scrollY": "150px",
        "scrollCollapse": false,
    });
}