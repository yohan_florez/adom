var baseUrl = window.location.protocol + "//" + window.location.host + "/web";
//var baseUrl = window.location.protocol + "//" + window.location.host;
$(document).ready(function() {
	$(".update-adom").click(updateAdom);
	$(".update-limits").click(updateLimits);
	$(".save-tmp1").click(savetempl);
	$(".save-tmp2").click(savetempl2);
	$(".save-tmp3").click(savetempl3);
	$(".save-tmp4").click(savetempl4);
	$(".save-tmp5").click(savetempl5);
	$(".save-tmp6").click(savetempl6);
	$(".change-pass").click(function(){
		update();
	});
	$(".change-pass-2").click(function(){
		update2();
	});
});

function savetempl(){
	var xml_data = "<root>";
	xml_data += "<option>1</option>";
	xml_data += "<content><![CDATA[" + CKEDITOR.instances.editor1.getData() + "]]></content>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/savemail',
        success: function(data){
        	$(".msg1").removeAttr("style");
        	$(".msg1").attr("style","color:green;");
        }
	});
}

function savetempl2(){
	var xml_data = "<root>";
	xml_data += "<option>2</option>";
	xml_data += "<content><![CDATA[" + CKEDITOR.instances.editor2.getData() + "]]></content>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/savemail',
        success: function(data){
        	$(".msg1").removeAttr("style");
        	$(".msg1").attr("style","color:green;");
        }
	});
}

function savetempl3(){
	var xml_data = "<root>";
	xml_data += "<option>3</option>";
	xml_data += "<content><![CDATA[" + CKEDITOR.instances.editor3.getData() + "]]></content>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/savemail',
        success: function(data){
        	$(".msg1").removeAttr("style");
        	$(".msg1").attr("style","color:green;");
        }
	});
}

function savetempl4(){
	var xml_data = "<root>";
	xml_data += "<option>4</option>";
	xml_data += "<content><![CDATA[" + CKEDITOR.instances.editor4.getData() + "]]></content>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/savemail',
        success: function(data){
        	$(".msg1").removeAttr("style");
        	$(".msg1").attr("style","color:green;");
        }
	});
}

function savetempl5(){
	var xml_data = "<root>";
	xml_data += "<option>5</option>";
	xml_data += "<content><![CDATA[" + CKEDITOR.instances.editor5.getData() + "]]></content>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/savemail',
        success: function(data){
        	$(".msg1").removeAttr("style");
        	$(".msg1").attr("style","color:green;");
        }
	});
}

function savetempl6(){
	var xml_data = "<root>";
	xml_data += "<option>6</option>";
	xml_data += "<content><![CDATA[" + CKEDITOR.instances.editor6.getData() + "]]></content>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/savemail',
        success: function(data){
        	$(".msg1").removeAttr("style");
        	$(".msg1").attr("style","color:green;");
        }
	});
}

function updateAdom(){
	var xml_data = "<root>";
	xml_data += "<nit>" + $(".nit").val() + "</nit>";
	xml_data += "<name>" + $(".name").val() + "</name>";
	xml_data += "<code>" + $(".cod_pre").val() + "</code>";
	xml_data += "<dpto>" + $(".cod_dpto").val() + "</dpto>";
	xml_data += "<mun>" + $(".cod_mun").val() + "</mun>";
	xml_data += "<zone>" + $(".zone").val() + "</zone>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/updateadom',
        success: function(data){
        	$(".div-msg").removeAttr("style");
        }
	});
}

function updateLimits(){
	var xml_data = "<root>";
	xml_data += "<limit>" + $(".limit").val() + "</limit>";
	xml_data += "<days>" + $(".limitday").val() + "</days>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/updatelimits',
        success: function(data){
        	$(".div-msg-limits").removeAttr("style");
        }
	});
}

function update(){
    if($(".p1").val() != "" && $(".p1").val() == $(".p2").val()) {
      if($(".p1").val().length < 8) {
        $(".msg").html("La contrase&ntilde;a debe contener al menos 8 caracteres");
        $(".type-alert").addClass("alert-danger");
        $(".div-msg").removeAttr("style");
        $(".p1").focus();
        return false;
      }
    } else {
      $(".msg").html("Las contrase&ntilde;as no coinciden");
      $(".type-alert").addClass("alert-danger");
      $(".div-msg").removeAttr("style");
      $(".p1").focus();
      return false;
    }
    $(".div-msg").attr("style","display:none;");
    $(".type-alert").removeClass("alert-danger");
    var xml_data = "<root>";
	xml_data += "<p>" + CryptoJS.MD5($(".p1").val()) + "</p>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/changepass',
        success: function(data){
        	if($(data).find("code").text() == 100){
	        	$(".msg").html("La contrase&ntilde;a ha sido actualizada");
	            $(".type-alert").addClass("alert-success");
	            $(".div-msg").removeAttr("style");
        	}else{
        		$(".msg").html("Ha ocurrido un error");
	            $(".type-alert").addClass("alert-danger");
	            $(".div-msg").removeAttr("style");
        	}
        }
	});
    return true;
}

function update2(){
    if($(".p1").val() != "" && $(".p1").val() == $(".p2").val()) {
      if($(".p1").val().length < 8) {
        $(".msg").html("La contrase&ntilde;a debe contener al menos 8 caracteres");
        $(".type-alert").addClass("alert-danger");
        $(".div-msg").removeAttr("style");
        $(".p1").focus();
        return false;
      }
    } else {
      $(".msg").html("Las contrase&ntilde;as no coinciden");
      $(".type-alert").addClass("alert-danger");
      $(".div-msg").removeAttr("style");
      $(".p1").focus();
      return false;
    }
    $(".div-msg").attr("style","display:none;");
    $(".type-alert").removeClass("alert-danger");
    var xml_data = "<root>";
	xml_data += "<p>" + CryptoJS.MD5($(".p1").val()) + "</p>";
	xml_data += "<uid>" + $(".change-pass-2").attr("id") + "</uid>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/changepass',
        success: function(data){
        	if($(data).find("code").text() == 100){
	        	$(".msg").html("La contrase&ntilde;a ha sido actualizada");
	            $(".type-alert").addClass("alert-success");
	            $(".div-msg").removeAttr("style");
        	}else{
        		$(".msg").html("Ha ocurrido un error");
	            $(".type-alert").addClass("alert-danger");
	            $(".div-msg").removeAttr("style");
        	}
        }
	});
    return true;
}