var baseUrl = window.location.protocol + "//" + window.location.host + "/web";
//var baseUrl = window.location.protocol + "//" + window.location.host;
var listInsumos = [];
var initDt = false;
var selprofessionals = "";
var entrys;
$(document).ready(function() {
	initDataTables();
	refreshServices($(".pkentryh").val(), function(){
		getallprofessionals(function(){
			refresh_Observations();
			getAllInsumos();
		});
	});
	$(".addinsu").click(addinsumo);
	$(".fn").change(calcularEdad);
	$(".update-p").click(function(){
		updatep($(this).attr("id"));
	});
	$("body").on("click", ".delin", function(){
		listInsumos = deleteArrayAdvanced(listInsumos, 'code', $(this).attr('id'));
		$("." + $(this).attr('id')).remove();
	});
	$(".datestart").change(calculatedDates);
	$(".fservices").change(calculatedDates);
	$(".qty").change(calculatedDates);
	$(".assignservice").click(assignservice);
	$(".btn-modal-newserv").click(function(){
		$("#myModal").modal('show');
	});
	$(".update-service_").click(function(){
		$("#myModal_edit").modal('show');
	});
	$("body").on("click",".trservice",function(){
		$('.trservice').removeClass('trselected');
		$(this).addClass("trselected");
		$(this).children("td").each(function (index2){
			$(this).removeClass("sorting_1");
		});
		entrys = $(this).attr("id");
		getdetails($(this).attr("id"));
	});
	$(".cancel-cit").click(function(){
		var itms = false;
		var xml_data = "<root><eid>" + $(this).attr("id") + "</eid>";
		$("input:checkbox:checked").each(function(){
			if($(this).is(':checked')){
				itms = true;
				xml_data += "<visit_id>" + $(this).attr("name") + "</visit_id>";
			}
		});
		xml_data += "</root>";
		if(itms)
			cancelVisits(xml_data);
		else
			swal({   
				title: "Debes seleccionar al menos un elemento de la lista",   
				text: "",   
				type: "error",   
				showCancelButton: false
			});
	});
	$("body").on("change",'.chk', function(){
		check($(this));
	});
	
	$("body").on("change",'#check-all', function(){
	    /*if($(this).is(':checked')){
	        $(".chk").prop('checked', true);
	        $(".even").addClass("trsel");
	        $(".odd").addClass("trsel");
	    } else{
	    	$(".chk").prop('checked', false);
	    	$(".even").removeClass("trsel"); 
	    	$(".odd").removeClass("trsel"); 
	    }*/
		if($(this).is(':checked')){
	        $(".chk").prop('checked', true);
	        $(".trselt").addClass("trsel");
	    } else{
	    	$(".chk").prop('checked', false);
	    	$(".trselt").removeClass("trsel"); 
	    }
	});
	$("body").on("change",".passign, .statusd", function(){
		$(".update-visit").removeClass("disabled");
	});
	$("body").on("change",".denf", function(){
		$(".update-visit").removeClass("disabled");
	});
	$(".update-visit").click(function(){
		updateVisits($(this));
	});
	$(".update_service_").click(function(){
		update_service_($(this).attr("id"));
	});
	$(".save").click(function(){
		updateVisit($(this));
	});
	$(".fpsel").change(function(){
		if($('option:selected', this).val() == "PIN"){
			$(".nropin").removeAttr("style");
		}else
			$(".nropin").attr("style","display:none;");
	});
	$(".infop").click(function(){
		openmpa($(this).attr("id"));
	});
	$(".close-entry").click(function(){
		closeentry($(this).attr("id"));
	});
	$(".close-entry-2").click(function(){
		closeentry_2($(this).attr("id"));
	});
	$(".close").click(function(){
		clear();
	});
	$(".close-model-assignservice").click(function(){
		clear();
	});
	initCalendars();
	$(".addobs").click(add_Observation);
	initDataTables_4();
	$(".addinsu2").click(add_insumo);
	$("body").on("click",".delin2", function(){
		deleteinsumo($(this).attr("id"));
	});
});

function updatep(uid){
	var xml_data = "<root>";
	xml_data += "<uid>" + uid + "</uid>";
	xml_data += "<type_doc>" + $(".td").val() + "</type_doc>";
	xml_data += "<num_doc>" + $(".nd").val() + "</num_doc>";
	xml_data += "<name1>" + $(".n1").val() + "</name1>";
	xml_data += "<name2>" + $(".n2").val() + "</name2>";
	xml_data += "<last1>" + $(".l1").val() + "</last1>";
	xml_data += "<last2>" + $(".l2").val() + "</last2>";
	xml_data += "<gender>" + $(".g").val() + "</gender>";
	xml_data += "<type_user>" + $(".tu").val() + "</type_user>";
	xml_data += "<age>" + $(".a").val() + "</age>";
	xml_data += "<fn>" + $(".fn").val() + "</fn>";
	xml_data += "<type_age>" + $(".ta").val() + "</type_age>";
	xml_data += "<distric>" + $(".d").val() + "</distric>";
	xml_data += "<address>" + $(".add").val() + "</address>";
	xml_data += "<zone>" + $(".z").val() + "</zone>";
	xml_data += "<phone1>" + $(".p1").val() + "</phone1>";
	xml_data += "<phone2>" + $(".p2").val() + "</phone2>";
	xml_data += "<email>" + $(".e").val() + "</email>";
	xml_data += "</root>";
    var data = {
		xml_data   : xml_data
    };
    $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/updatepatient',
        success: function(data, responseText, arguments){
            location.reload();
        }
    });
}

function calcularEdad(){
    var fecha = $(".fn").val();
    var values=fecha.split("-");
    var dia = values[2];
    var mes = values[1];
    var ano = values[0];
    var fecha_hoy = new Date();
    var ahora_ano = fecha_hoy.getYear();
    var ahora_mes = fecha_hoy.getMonth()+1;
    var ahora_dia = fecha_hoy.getDate();
    var edad = (ahora_ano + 1900) - ano;
    if ( ahora_mes < mes )
        edad--;
    if ((mes == ahora_mes) && (ahora_dia < dia))
        edad--;
    if (edad > 1900)
        edad -= 1900;
    var meses=0;
    if(ahora_mes>mes)
        meses=ahora_mes-mes;
    if(ahora_mes<mes)
        meses=12-(mes-ahora_mes);
    if(ahora_mes==mes && dia>ahora_dia)
        meses=11;
    var dias=0;
    if(ahora_dia>dia)
        dias=ahora_dia-dia;
    if(ahora_dia<dia){
        ultimoDiaMes=new Date(ahora_ano, ahora_mes, 0);
        dias=ultimoDiaMes.getDate()-(dia-ahora_dia);
    }
    if(edad == 0){
    	$(".a").val(meses);
    	$('.ta option[valuw="M"]').attr("selected","selected");
    }else{
    	$(".a").val(edad);
    	$('.ta option[value="A"]').attr("selected","selected");
    }
}

function add_Observation(){
	var xml_data = "<root><message>" + $(".rs").val() + "</message>";
	xml_data += "<eid>" + $(".pkentryh").val() + "</eid></root>";
	var data = {
		xml_data : xml_data
	};
	$.ajax({
      type: 'POST',
      url: baseUrl + '/admin/add_observation',
      data: data,
      success: function(data){
    	  refresh_Observations();
    	  $(".rs").val("");
      }
	});
}

function add_insumo(){
	var xml_data = "<root><entrys>" + entrys + "</entrys>";
	xml_data += "<invoiced>" + $(".facby2 option:selected").val() + "</invoiced>";
	xml_data += "<insumo>" + $(".insumo2 option:selected").val() + "</insumo>";
	xml_data += "<qty>" + $(".qtyinsumo2").val() + "</qty></root>";
	var data = {
		xml_data : xml_data
	};
	$.ajax({
      type: 'POST',
      url: baseUrl + '/admin/add_insumo',
      data: data,
      success: function(data){
    	  refreshInsumos();
    	  $(".qtyinsumo2").val("");
      }
	});
}

function deleteinsumo(eid){
	var xml_data = "<root><ei>" + eid + "</ei></root>";
	var data = {
		xml_data : xml_data
	};
	$.ajax({
      type: 'POST',
      url: baseUrl + '/admin/deleteinsumo',
      data: data,
      success: function(data){
    	  refreshInsumos();
      }
	});
}

function update_service_(eid){
	var xml_data = "<root>";
	xml_data += "<esid>" + eid + "</esid>";
	xml_data += "<nro_auth>" + $(".nro_auth_2").val() + "</nro_auth>";
	xml_data += "<copago>" + $(".cop_2").val() + "</copago>";
	xml_data += "<fcop>" + $(".fcopago_2 option:selected").val() + "</fcop>";
	xml_data += "</root>";
	var data = {
		xml_data : xml_data
	};
	$.ajax({
      type: 'POST',
      url: baseUrl + '/admin/updateservice_',
      data: data,
      success: function(data){
		  $(".lauth").html($(".nro_auth_2").val());
		  $(".lcop").html($(".cop_2").val());
		  var fcop = "";
		  if($(".fcopago_2 option:selected").val() == 0)
			  fcop = "OTRO";
		  else if($(".fcopago_2 option:selected").val() == 1)
			  fcop = "CADA SESI&Oacute;N";
		  else
			  fcop = "CADA 5 SESIONES";
		  $(".lfrecop").html(fcop);
		  $("#myModal_edit").modal("hide");
      }
	});
}

function refresh_Observations(eid){
	var xml_data = "<root><entry>" + $(".pkentryh").val() + "</entry></root>";
	var data = {
		xml_data : xml_data
	};
	$.ajax({
      type: 'POST',
      url: baseUrl + '/admin/get_observations',
      data: data,
      success: function(data){
    	  var html = "";
    	  $(".tbody-lob").html("");
    	 $(data).find('observation').each(function() {
	    	 html +='<tr class="trservice">'+
	          '<td><label style="color:blue;">' + $(this).find("date").text() + "</label> <label style='font-weigth:bold;'>" +$(this).find("user").text() + "</label> " + $(this).find("msg").text() + '</td>'+
	         '<tr>';
    	 });
    	 $(".tbody-lob").html(html);
      }
	});
}

function initCalendars(){
	 $.datepicker.setDefaults($.datepicker.regional['es']);
     $(".date").datepicker({changeMonth:true,changeYear:true,dateFormat: 'yy-mm-dd'});
}

function closeentry(eid){
	var xml_data = "<root><eid>" + eid + "</eid></root>";
	var data = {
		xml_data : xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/verifyClose',
        success: function(data){
        	if($(data).find("code").text() == '100' || $(data).find("code").text() == '200'){
        		var title = $(data).find("code").text() == '100' ? "Por favor confirma el alta del paciente" : "El paciente tiene servicios activos, confirmas que quieres dar de alta al paciente";
        		swal({   
        			title: title,   
        			text: "",   
        			type: "warning",   
        			confirmButtonText: "Si, dar de alta", 
        			confirmButtonColor: "#428bca",
        			cancelButtonText: "No, salir",
        			showCancelButton: true,   
        			closeOnConfirm: false,   
        			showLoaderOnConfirm: true,
        			html: true
        		}, function(){   
        			confirmCloseEntry(xml_data, function(){     
        				location.reload();
        			}); 
        		});
        	}else{
        		swal({
    				title:"Ocurrio un error", 
    				text:"", 
    				type:"error",
    				showCancelButton: false,   
    				closeOnConfirm: true,
    			});  
        	}
        }
    });
}

function closeentry_2(eid){
	var xml_data = "<root><eid>" + eid + "</eid></root>";
	var data = {
		xml_data : xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/verifyClose',
        success: function(data){
        	if($(data).find("code").text() == '100' || $(data).find("code").text() == '200'){
        		if($(data).find("code").text() == '100'){
	        		swal({   
	        			title: title,   
	        			text: "Confirmar dar de alta al paciente",   
	        			type: "warning",   
	        			confirmButtonText: "Si, dar de alta", 
	        			confirmButtonColor: "#428bca",
	        			cancelButtonText: "No, salir",
	        			showCancelButton: true,   
	        			closeOnConfirm: false,   
	        			showLoaderOnConfirm: true,
	        			html: true
	        		}, function(){   
	        			confirmCloseEntry(xml_data, function(){     
	        				location.reload();
	        			}); 
	        		});
        		}else{
        			swal({
        				title:"Error", 
        				text:"El paciente no puede ser dado de alta por que tiene servicios sin completarse", 
        				type:"error",
        				showCancelButton: false,   
        				closeOnConfirm: true,
        			}); 
        		}
        	}else{
        		swal({
    				title:"Ocurrio un error", 
    				text:"", 
    				type:"error",
    				showCancelButton: false,   
    				closeOnConfirm: true,
    			});  
        	}
        }
    });
}

function confirmCloseEntry(xml_data, callback){
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/closeentry',
        success: function(data){
        	callback();
        }
	});
}

function openmpa(pid){
	var xml_data = "<root><pid>" + pid + "</pid></root>";
	var data = {
		xml_data : xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/getpatient',
        success: function(data){
        	$(".td option[value='" + $(data).find("type_doc").text()  + "']").attr('selected', 'selected');
	    	$(".nd").val($(data).find("num_doc").text());
	    	$(".n1").val($(data).find("name1").text());
	    	$(".n2").val($(data).find("name2").text());
	    	$(".l1").val($(data).find("last1").text());
	    	$(".l2").val($(data).find("last2").text());
	    	$(".fn").val($(data).find("fn").text());
	    	$(".g option[value='" + $(data).find("gender").text()  + "']").attr('selected', 'selected');
	    	$(".tu").val($(data).find("type_user").text());
	    	$(".a").val($(data).find("age").text());
	    	$(".ta option[value='" + $(data).find("type_age").text()  + "']").attr('selected', 'selected');
	    	$(".d").val($(data).find("distric").text());
	    	$(".add").val($(data).find("address").text());
	    	$(".e").val($(data).find("email").text());
	    	$(".z").val($(data).find("zone").text());
	    	$(".p1").val($(data).find("phone1").text());
	    	$(".p2").val($(data).find("phone2").text());
	    	$(".update-p").attr("id",$(data).find("id").text());
            $("#modalp").modal('show');
        }
    });
}

function updateVisits(element){
	swal({   
		title: "Deseas aplicar los cambios?",   
		text: "",   
		type: "warning",   
		confirmButtonText: "Si, aplicar", 
		confirmButtonColor: "#428bca",
		cancelButtonText: "No, salir",
		showCancelButton: true,   
		closeOnConfirm: false,   
		showLoaderOnConfirm: true,
		html: true
	}, function(){   
		confirmUpdateVisits(element, function(){     
			swal({
				title:"Se han actualizado las visitas", 
				text:"", 
				type:"success",
				showCancelButton: false,   
				closeOnConfirm: true,
			});  
		}); 
	});
}

function updateVisit(element){
	var xml_data = "<root><esid>" + $(element).attr("id") + "</esid>";
	xml_data += "<date>" + $('.date-a' + $(element).attr("id")).val() + "</date>";
	xml_data += "<money>" + $('.valrec' + $(element).attr("id")).val() + "</money>";
	xml_data += "<othermoney>" + $('.other' + $(element).attr("id")).val() + "</othermoney>";
	xml_data += "<pay>" + $(".fp" + $(element).attr("id") + " option:selected").val() + "</pay>";
	xml_data += "<nro_pin>" + $(".nropin").val() + "</nro_pin>";
	xml_data += "</root>";
	var data = {
		xml_data : xml_data
	};
	$.ajax({
      type: 'POST',
      url: baseUrl + '/admin/updateVisit',
      data: data,
      success: function(data){
    	location.reload();
      }
	});
}

function refreshInsumos(){
	var data = {
		xml_data : "<root><entrys>" + entrys + "</entrys></root>"
	};
	$('.tbody-insumo').html("");
	$.ajax({
	      type: 'POST',
	      url: baseUrl + '/admin/get_insumos',
	      data: data,
	      success: function(data){
	    	
	    	var html = "";
	    	var c = 1;
	    	$(data).find('insumo_detail').each(function() {
	    		html += "<tr>";
	    		html += "<td>" + (c++) + "</td>";
	    		html += "<td>" + $(this).find("name").text() + "</td>";
	    		html += "<td>" + $(this).find("qty").text() + "</td>";
	    		html += "<td>" + $(this).find("fa").text() + "</td>";
	    		html += '<td><a id="' + $(this).find("id").text() + '" class="delin2 btn menu-icon color-black btn-default btn-xs" style="font-size: 9px;margin-top: -5px;"><i class="fa fa-close">Borrar</i></a></td>';
	    		html += "</tr>";
	    	});
	    	$(".tbody-insumo").html(html);
	      }
	  });
}

function refreshServices(eid, callback){
	var xml_data = "<root><eid>" + eid + "</eid></root>";
	var data = {
		xml_data : xml_data
	};
	$.ajax({
      type: 'POST',
      url: baseUrl + '/admin/getservicesbyentry',
      data: data,
      success: function(data){
    	  var html = "";
    	  $(".tbody-lservices").html("");
    	 $(data).find('service').each(function() {
	    	 html +='<tr id="' + $(this).find("id").text() + '" class="trservice">'+
	          '<td>' + $(this).find("name").text() + '</td>'+
	          '<td>' + $(this).find("qty_p").text() + '</td>'+
	          '<td>' + $(this).find("qty_r").text() + '</td>'+
	          '<td>' + $(this).find("frecuency").text() + '</td>'+
	          '<td>' + $(this).find("finicio").text() + '</td>'+
	          '<td style="color:' + $(this).find("color").text() + ';">' + $(this).find("status").text() + '</td>'+
	          '<td>' + $(this).find("invoiced").text() + '</td>'+
	         '<tr>';
    	 });
    	 $(".tbody-lservices").html(html);/*.promise().done(function(){
    		 console.log("Lo inserto!!");
    		 initDataTables_3();
    	 });*/
    	 if(callback)
    		 callback();
      }
	});
}

function confirmUpdateVisits(element, callback){
	var xml_data = "<root><eid>" + $(element).attr("id") + "</eid>";
	$('.passign').each(function(){
		xml_data += "<visit>";
		xml_data += "<visit_id>" + $(this).attr("id") + "</visit_id>";
		xml_data += "<professional_id>" + $("option:selected", this).val() + "</professional_id>";
		xml_data += "<date>" + $(".denf" + $(this).attr("id")).val() + "</date>";
		xml_data += "<status>" + $(".statusd" + $(this).attr("id") + " option:selected").val() + "</status>";
		xml_data += "</visit>";
	});
	$('.passign-complete').each(function(){
		xml_data += "<visit>";
		xml_data += "<visit_id>" + $(this).attr("id") + "</visit_id>";
		xml_data += "<professional_id>" + $(this).attr("name") + "</professional_id>";
		xml_data += "<date>" + $(".denf" + $(this).attr("id")).val() + "</date>";
		xml_data += "<especial>TRUE</especial>";
		xml_data += "</visit>";
	});
	xml_data += "</root>";
	var data = {
		xml_data : xml_data
	};
	$.ajax({
      type: 'POST',
      url: baseUrl + '/admin/updateVisits',
      data: data,
      success: function(data){
    	var html = "";
    	var c = 1;
    	if($(data).find("code").text() == "100"){
    		var c = 1;
    		$(".tbody-visit").html("");
    		refreshServices($(".pkentryh").val());
    		getdetails($(element).attr("id"));
			callback();
    	}else{
    		swal({
				title:"Hubo un error al aplicar los cambios", 
				text:"", 
				type:"danger",
				showCancelButton: false
			});
    	}
      }
	});
}

function check(element){
	if($(element).is(':checked')) {  
        $(".trsel" + $(element).attr("name")).addClass("trsel");
    } else {  
    	$(".trsel" + $(element).attr("name")).removeClass("trsel"); 
    } 
}

function cancelVisits(data_xml){
	swal({   
		title: "Deseas cancelar las visitas seleccionadas?",   
		text: "",   
		type: "warning",   
		confirmButtonText: "Si, cancelar", 
		confirmButtonColor: "#428bca",
		cancelButtonText: "No, salir",
		showCancelButton: true,   
		closeOnConfirm: false,   
		showLoaderOnConfirm: true,
		html: true
	}, function(){   
		var data = {
			xml_data : data_xml
		};
		$.ajax({
	      type: 'POST',
	      url: baseUrl + '/admin/cancelVisits',
	      data: data,
	      success: function(data){
	    	var html = "";
	    	var c = 1;
	    	if($(data).find("code").text() == "100"){
	    		swal({
					title:"Se han cancelado las visitas seleccionadas", 
					text:"", 
					type:"success",
					showCancelButton: false,   
					closeOnConfirm: true,
				});  
	    		getdetails($(data_xml).find("eid").text())
				initDt = true;
				refreshServices($(".pkentryh").val());
	    	}else{
	    		swal({
					title:"Hubo un error al cancelar las visitas", 
					text:"", 
					type:"danger",
					showCancelButton: false
				});
	    	}
	      }
		});
	});
}

function getdetails(id){
	var data = {
		xml_data : "<root><eid>" + id + "</eid></root>"
	};
	$('.tbody-visit').html("");
	$('.tbody-insumo').html("");
	$(".update_service_").attr("id",id);
	$.ajax({
	      type: 'POST',
	      url: baseUrl + '/admin/getEntryDetails',
	      data: data,
	      success: function(data){
	    	$(".lvig").html($(data).find("vigencia").text());
	    	$(".lauth").html($(data).find("nro_auth").text());
	    	$(".nro_auth_2").val($(data).find("nro_auth").text());
	    	$(".lfsol").html($(data).find("").text());
	    	$(".lsol").html($(data).find("name_s").text());
	    	$(".lfsol").html($(data).find("date_soli").text());
	    	$(".lserv").html($(data).find("service").text());
	    	$(".lstart").html($(data).find("start_date").text());
	    	$(".lend").html($(data).find("end_date").text());
	    	$(".lqty").html($(data).find("qty_v").text());
	    	$(".lfrec").html($(data).find("frecuency").text());
	    	$(".lcop").html(formatNumber(parseInt($(data).find("copago").text()).toFixed(0)));
	    	$(".cop_2").val(parseInt($(data).find("copago").text()));
	    	$(".lfrecop").html($(data).find("copago_frec").text());
	    	$('.fcopago_2 option[value="' + $(data).find("copago_frec_id").text() + '"]').attr("selected","selected");
	    	$(".lprofs").html($(data).find("professionals").text());
	    	$(".cancel-cit").attr("id",$(data).find("entry").text());
	    	$(".update-visit").attr("id",$(data).find("entry").text());
	    	var c = 1;
	    	$(".tbody-visit").html("");
	    	$(data).find('detail').each(function() {
	    		var html = "";
	    		if($(this).find("status_2").text() == "ALTA" ){
	    			html += "<tr><td></td>";
	    		}else{
		    		html += '<tr class="trselt trsel' + $(this).find("id_det").text() + '">';
		    		html += '<td><div class="form-group">';
		    				html += '<div class="col-sm-7 controls">'+
	                          '<div class="vd_checkbox">'+
	                            '<input type="checkbox" class="chk chk' + $(this).find("id_det").text() + '" id="lbloption' + $(this).find("id_det").text() + '" name="' + $(this).find("id_det").text() + '">'+
	                            '<label class="lblcheck" for="lbloption' + $(this).find("id_det").text() + '" id="' + $(this).find("id_det").text() + '"></label>'+
	                          '</div>'+
	                        '</div></div></td>';
	    		}
	    		html += "<td>" + (c++) + "</td>";
	    		if($(this).find("status_2").text() != "ALTA"){
		    		/*if($(data).find("service_type").text() == '1' && ($(this).find("status").text() == "PROGRAMADA" || $(this).find("status").text() == "COMPLETADA")){
		    			html += "<td><input type='text' value='" + $(this).find("date").text() + "' class='denf denf" + $(this).find("id_det").text() + "'></td>";
		    		}else
		    			html += "<td>" + $(this).find("date").text() + "</td>";*/
	    			html += "<td><input type='date' value='" + $(this).find("date").text() + "' class='denf denf" + $(this).find("id_det").text() + "'></td>";
	    		}else
	    			html += "<td>" + $(this).find("date").text() + "</td>";
	    		if($(this).find("status_2").text() != "ALTA"){
		    		/*if($(this).find("status").text() != "PROGRAMADA"){
		    			html += "<td class='passign-complete passign-complete" + $(this).find("id_det").text() + "' id='" + $(this).find("id_det").text() + "' name='" + $(this).find("professional_id").text() + "'>" + $(this).find("professional").text() + "</td>";
		    		}else{
		    			html += '<td><select class="passign chosen-select passign' + $(this).find("id_det").text() + '" id="' + $(this).find("id_det").text() + '" style="width:350px;" tabindex="2">' +
		    				'<option value="null">POR ASIGNAR</option>' +
			    			selprofessionals + 
			    		"</select></td>";
		    		}*/
	    			html += '<td><select class="passign chosen-select2 passign' + $(this).find("id_det").text() + '" id="' + $(this).find("id_det").text() + '" style="width:350px;" tabindex="2">' +
	    				'<option value="null">POR ASIGNAR</option>' +
		    			selprofessionals + 
		    		"</select></td>";
	    		}else
	    			html += "<td>" + $(this).find("professional").text() + "</td>";
	    		
	    		html += '<td><select class="statusd chosen-select2 statusd' + $(this).find("id_det").text() + '" id="' + $(this).find("id_det").text() + '" style="width:350px;" tabindex="2">' +
		    		'<option value="2">PROGRAMADA</option>' +
		    		'<option value="3">COMPLETADA</i></option>' +
		    		'<option value="4">CANCELADA</option>' +
	    		"</select></td>";
	    		/*if($(this).find("status").text() == "PROGRAMADA")
	    			html += "<td style='color:blue;font-weight: bold;'><i class='fa fa-clock-o'></i> " + $(this).find("status").text() + "</td>";
	    		else if($(this).find("status").text() == "COMPLETADA")
	    			html += "<td style='color:green;font-weight: bold;'><i class='fa fa-check'></i> " + $(this).find("status").text() + "</td>";
	    		else
	    			html += "<td style='color:red;font-weight: bold;'><i class='fa fa-times-circle'></i> " + $(this).find("status").text() + "</td>";
	    		html += "</tr>";*/
	    		
	    		$(".tbody-visit").append(html);
	    		$('.passign' + $(this).find("id_det").text() + ' option[value="' + $(this).find("professional_id").text() + '"]').attr("selected","selected");
	    		$('.statusd' + $(this).find("id_det").text() + ' option[value="' + $(this).find("id_sta").text() + '"]').attr("selected","selected");
	    		//----------- initialize calendars -----------
	    		$('.denf' + $(this).find("id_det").text()).datepicker({
	    	        dateFormat: 'yy-mm-dd',
	    	        showButtonPanel: true,
	    	        changeMonth: true,
	    	        changeYear: true,
	    	        minDate: new Date($(data).find("start_date").text()),
	    	        maxDate: '+30Y',
	    	        inline: true
	    	    });
	    	});
	    	var html = "";
	    	var c = 1;
	    	$(data).find('insumo_detail').each(function() {
	    		html += "<tr>";
	    		html += "<td>" + (c++) + "</td>";
	    		html += "<td>" + $(this).find("name").text() + "</td>";
	    		html += "<td>" + $(this).find("qty").text() + "</td>";
	    		html += "<td>" + $(this).find("fa").text() + "</td>";
	    		html += '<td><a id="' + $(this).find("id").text() + '" class="delin2 btn menu-icon color-black btn-default btn-xs" style="font-size: 9px;margin-top: -5px;"><i class="fa fa-close">Borrar</i></a></td>';
	    		html += "</tr>";
	    	});
	    	$(".tbody-insumo").html(html);
	    	$(".divdetail").removeAttr("style");
	    	if(!initDt){
	    		initDt = true;
	    		initDataTables_2();
	    	}
	    	$(".passign, .statusd").chosen2({
	    	    no_results_text: "",
	    	    width: "100%"
	    	});
	    	$("body").animate({ scrollTop: 450 }, "slow");
	      }
	  });
}


function formatNumber(x) {
    x = x.toString();
    var pattern = /(-?\d+)(\d{3})/;
    while (pattern.test(x))
        x = x.replace(pattern, "$1,$2");
    return x;
}

function assignservice(){
	var xml_data = "<root>";
	xml_data += "<entry>" + $(".btn-modal-newserv").attr("id") + "</entry>";
	xml_data += "<nro_auth>" + $(".nroaut").val() + "</nro_auth>";
	xml_data += "<name>" + $(".solicitante").val() + "</name>";
	xml_data += "<validity>" + $(".datevalidity").val() + "</validity>";
	xml_data += "<service>" + $(".lservices option:selected").val() + "</service>";
	xml_data += "<qty>" + $(".qty").val() + "</qty>";
	xml_data += "<start_date>" + $(".datestart").val() + "</start_date>";
	xml_data += "<end_date>" + $(".dateend").val() + "</end_date>";
	xml_data += "<frec_service>" + $(".fservices").val() + "</frec_service>";
	xml_data += "<professional>" + $(".professional").val() + "</professional>";
	xml_data += "<copago>" + $(".valcop").val() + "</copago>";
	xml_data += "<frec_copago>" + $(".fcopago option:selected").val() + "</frec_copago>";
	xml_data += "<consultation>" + $(".fincon").val() + "</consultation>";
	xml_data += "<external>" + $(".cauext").val() + "</external>";
	xml_data += "<insumos>";
	for(var i=0; i< listInsumos.length; i++){
		xml_data += "<insumo>";
		for (var key in listInsumos[i])
            xml_data += '<' + key + '>' + listInsumos[i][key] + '</' + key + ">";
		xml_data += "</insumo>";
    }
	xml_data += "</insumos>";
	xml_data += "</root>";
	var data = {
		xml_data : xml_data
	};
	confirmAssignService(data, function(){     
		swal({
			title:"El servicio se ha asignado correctamente", 
			text:"", 
			type:"success",
			showCancelButton: false,   
			closeOnConfirm: true,
		});  
		initDt = true;
		refreshServices($(".pkentryh").val());
		clear();
	}); 
}

function clear(){
	$('input').each(function(){
		$(this).val("");
	});
	setDefaults();
}

function setDefaults(){
	$(".tu").val('5');
}

function confirmAssignService(data, callback){
	$(".bas").toggleClass('active');
	$('#addserv').find('input, textarea, button, select').attr('disabled','disabled');
	$(".bas").attr('disabled','disabled');
	$(".close-model-assignservice").attr('disabled','disabled');
	$.ajax({
	      type: 'POST',
	      url: baseUrl + '/admin/assignservice',
	      data: data,
	      success: function(data){
	    	$('#addserv').find('input, textarea, button, select').removeAttr('disabled','disabled');
    		$(".bas").removeAttr('disabled','disabled');
    		$(".close-model-assignservice").removeAttr('disabled','disabled');
    		$(".bas").removeClass('active');
	    	var code = $(data).find("code").text();
			if(code != "100"){
				sweetAlert({
					title: "Error", 
					text: "No se pudo completar la operaci&oacute;n", 
					type: "error",
					html: true
				});
			}else{
				$("#myModal").modal('hide');
				deleteAllInsumos();
				callback();
			}
	      }
	  });
}

function clear(){
	$('input').each(function(){
		$(this).val("");
	});
	setDefaults();
}

function deleteAllInsumos(){
	for(var i=0; i< listInsumos.length; i++){
		listInsumos = deleteArrayAdvanced(listInsumos[0], 'code', $(this).attr('id'));
		$("." + $(this).attr('id')).remove();
	}
}

function setDefaults(){
	$(".fincon").val('10');
	$(".cauext").val('13');
	$(".qtyinsumo").val('0');
	$(".valcop").val('0');
	$(".qty").val('0');
}

function calculatedDates(){
	var start = $(".datestart").val();
    var end = new Date(start);
    
    var fdays = $(".fservices option:selected").attr("id");
    var vall  = $(".fservices option:selected").val();
    var qty = $(".qty").val();
    var add = 0;
    
    var d = 7 / fdays;
    add = d * qty;
    if(vall == 2 || vall == 3)
    	add = add + 1;
    
    end.setDate(end.getDate() + add);
    var dd = end.getDate();
    var mm = end.getMonth() + 1;
    var y = end.getFullYear();
    var endString = y + '-' + (mm <10 ? '0' + mm: mm) + '-' + (dd <10 ? '0' + dd : dd);
    $(".dateend").val(endString)
    $(".dateend").removeAttr("disabled");
}

function deleteArrayAdvanced(array, attribute, value){
    var array2 = [];
    for(var i=0;i<array.length;i++){
        if(array[i][attribute] != value)
            array2.push(array[i]);
    }
    return array2;
}

function getAllInsumos(){
	 $.ajax({
      type: 'POST',
      url: baseUrl + '/admin/getallinsumos',
      success: function(data){
		var html = "";
		$(data).find("insumo").each(function() {
	        html += "<option value='" + $(this).find("id").text() + "'>" + $(this).find("name").text() + "</option>";
		});
		$(".insumo").html(html);
		$(".insumo2").html(html);
      }
  });
}

function getallprofessionals(callback){
	 $.ajax({
       type: 'POST',
       url: baseUrl + '/admin/getallprofessionals',
       success: function(data){
       	var html = "";
       	
       	$(data).find("user").each(function() {
       		html += "<option value='" + $(this).find("id").text() + "'>" + $(this).find("full_name").text() +  ' - ' + $(this).find("num_doc").text() + "</option>";
       	});
       	selprofessionals = html;
       	if(callback)
       		callback();
       }
   });
}

function addinsumo(){
    var insumo = [];
    insumo['id'] = $(".insumo").val();
    insumo['qty'] = $(".qtyinsumo").val();
    insumo['fa'] = $(".facby").val();
    code = makeid();
    insumo['code'] = code;
    if(listInsumos.length == 0)
    	$(".tbody-3").html(gettr(code));
    else
    	$(".tbody-3").prepend(gettr(code));
    listInsumos.push(insumo);
}

function gettr(code){
    return '<tr class="' + code + '">'+
            '<td>' + $(".insumo option:selected").text() + '</td>'+
            '<td>' + $(".qtyinsumo").val() + '</td>'+
            '<td>' + $(".facby option:selected").text() + '</td>'+
            '<td><a id="' + code + '" class="delin btn menu-icon color-black btn-default btn-xs" style="font-size: 9px;margin-top: -5px;">'+
    		'<i class="fa fa-close">Borrar</i></a></td>'+
           '<tr>';
}

function makeid(){
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for(var i=0; i < 10; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

function initDataTables(){
    $('.data-tables').dataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:  ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "paging":false,
        "searching":false,
        "info":false
    });
}

function initDataTables_2(){
	$('.data-tables-2').dataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:  ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "paging":false,
        "searching":false,
        "info":false,
        "bSort": false,
        "scrollY": "250px",
    });
	initDt = true;
}

function initDataTables_3(){
	$('.data-tables-3').dataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:  ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "paging":false,
        "searching":false,
        "info":false,
        "scrollY": "250px",
    });
}

function initDataTables_4(){
	$('.data-tables-4').dataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:  ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "paging":false,
        "searching":false,
        "info":false,
        "scrollY": "270px",
        "scrollCollapse": false,
    });
}