var baseUrl = window.location.protocol + "//" + window.location.host + "/web";
//var baseUrl = window.location.protocol + "//" + window.location.host;
var eid = "";
var dt = null;
$(document).ready(function() {
	$('input:radio[name="tipo"]').change(
	    function(){
	        if($(this).val() == '2'){
	        	$(".hi").attr("style","display: none;");
	        	type = 2;
	        }else{
	        	$(".hi").removeAttr("style");
	        	type = 1;
	        }
	});
	$(".savep").click(savep);
	$(".savei").click(savei);
	initDataTables();
	$(".clear").click(function(){
		clear();
	});
	$("body").on("click", ".edit-serv", function(){
		eid = $(this).attr("id");
		getservice(eid);
	});
	$("body").on("click", ".edit-ins", function(){
		id = $(this).attr("id");
		getinsumo(id);
	});
	$("body").on("click",".updatep",function(){
		updatep($(this).attr("id"));
	});
	$("body").on("click",".updatei",function(){
		updatei($(this).attr("id"));
	});
});

function getservice(sid){
	var xml_data = "<root><sid>" + eid + "</sid></root>";
	var data = {
		xml_data : xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/getservice',
        success: function(data){
	    	$(".name1").val($(data).find("name").text());
	    	$(".value1").val(parseFloat($(data).find("value").text()).toFixed(0));
	    	$(".cod1").val($(data).find("code").text());
	    	$(".hours1").val($(data).find("hours").text());
	    	$(".updatep").attr("id",$(data).find("id").text());
	    	if($(data).find("type").text() == "1"){
	    		$(".tipo11").click();
	    	}else{
	    		$(".tipo21").click();
	    	}
	    	if($(data).find("type2").text() == "C"){
	    		$(".clasi1").click();
	    	}else{
	    		$(".clasi2").click();
	    	}
            $("#modal-edit").modal('show');
        }
    });
}

function getinsumo(id){
	var xml_data = "<root><id>" + id + "</id></root>";
	var data = {
		xml_data : xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/getinsumo',
        success: function(data){
	    	$(".name3").val($(data).find("name").text());
	    	$(".cod3").val($(data).find("code").text());
	    	$(".updatei").attr("id",$(data).find("id").text());
            $("#modal-edit-2").modal('show');
        }
    });
}

function updatep(sid){
	var type = $("#tipo11").is(':checked') ? 1: 2;
	var type2 = $("#clasi1").is(':checked') ? 'C': 'P';
	var xml_data = "<root>";
	xml_data += "<sid>" + eid + "</sid>";
	xml_data += "<name>" + $(".name1").val() + "</name>";
	xml_data += "<value>" + $(".value1").val() + "</value>";
	xml_data += "<code>" + $(".cod1").val() + "</code>";
	xml_data += "<hours>" + $(".hours1").val() + "</hours>";
	xml_data += "<type>" + type + "</type>";
	xml_data += "<type2>" + type2 + "</type2>";
	xml_data += "</root>";
    var data = {
		xml_data   : xml_data
    };
    $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/updateservice',
        success: function(data, responseText, arguments){
        	ices();
            $("#modal-edit").modal("hide");
            clear();
        }
    });
}

function updatei(id){
	var xml_data = "<root>";
	xml_data += "<id>" + id + "</id>";
	xml_data += "<name>" + $(".name3").val() + "</name>";
	xml_data += "<code>" + $(".cod3").val() + "</code>";
	xml_data += "</root>";
    var data = {
		xml_data   : xml_data
    };
    $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/updateinsumo',
        success: function(data, responseText, arguments){
        	getAllInsumos();
            $("#modal-edit-2").modal("hide");
            clear();
        }
    });
}

function clear(){
	$('input').each(function(){
		$(this).val("");
	});
}

function savei(){
	var xml_data = "<root>";
	xml_data += "<name>" + $(".name2").val() + "</name>";
	xml_data += "<code>" + $(".cod2").val() + "</code>";
	xml_data += "</root>";
    var data = {
		xml_data   : xml_data
    };
    $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/saveinsumo',
        success: function(data, responseText, arguments){
        	getAllInsumos();
            $("#myModal2").modal("hide");
        }
    });
}

function savep(){
	var type = $("#tipo1").is(':checked') ? 1: 2;
	var type2 = $("#clasi11").is(':checked') ? 'C': 'P';
	var xml_data = "<root>";
	xml_data += "<name>" + $(".name").val() + "</name>";
	xml_data += "<value>" + $(".value").val() + "</value>";
	xml_data += "<code>" + $(".cod").val() + "</code>";
	xml_data += "<hours>" + $(".hours").val() + "</hours>";
	xml_data += "<type>" + type + "</type>";
	xml_data += "<type2>" + type2 + "</type2>";
	xml_data += "</root>";
    var data = {
		xml_data   : xml_data
    };
    $.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/saveservice',
        success: function(data, responseText, arguments){
        	ices();
            $("#myModal").modal("hide");
        }
    });
}

function getAllInsumos(){
	 $.ajax({
       type: 'POST',
       url: baseUrl + '/admin/getallinsumos',
       success: function(data){
		var html = "";
		$(data).find("insumo").each(function() {
	        html += "<tr>";
	        html += "<td>" + $(this).find("name").text() + "</td>";
	        html += "<td>" + $(this).find("code").text() + "</td>";
	        html += "<td><a class='btn color-black btn-default btn-xs edit-ins mr-5' id='" + $(this).find("id").text() + "'> Ver detalle </a></td>";
	        html += "</tr>";
		});
		$(".tbody2").html(html);
       }
   });
}

function ices(){
	 $.ajax({
      type: 'POST',
      url: baseUrl + '/admin/getallservices',
      success: function(data){
    	  var html = "";
      	if(dt !=null)
      		dt.fnClearTable();
      	$(data).find("service").each(function() {
      		if(dt != null){
        		dt.fnAddData([
                    $(this).find("name").text(),
                    parseFloat($(this).find("value").text()).toFixed(0),
                    $(this).find("code").text(),
                    $(this).find("type").text(),
                    $(this).find("type2").text(),
                    '<a id="' + $(this).find("id").text() + '" class="edit-serv btn menu-icon color-black btn-default btn-xs">Ver detalles</a>'
                ]);
      		}else{
      			html += "<tr>";
     	        html += "<td>" + $(this).find("name").text() + "</td>";
     	        html += "<td>$ " + $(this).find("value").text() + "</td>";
     	        html += "<td>" + $(this).find("code").text() + "</td>";
     	        html += "<td>" + $(this).find("type").text() + "</td>";
     	        html += "<td>" + $(this).find("type2").text() + "</td>";
	            html += '<a id="' + $(this).find("id").text() + '" class="edit-serv btn menu-icon color-black btn-default btn-xs">Ver detalles</a>';
	            html += "</td>";
	            html += "</tr>";
      		}
      	});
      	if(dt ==null){
            $(".tbody").html(html);
      	}
      }
  });
}

function initDataTables(){
    dt = $('#data-tables').dataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:  ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "Último",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "paging":false,
        "searching":true,
        "info":false,
        "scrollY": "300px"
    });
}