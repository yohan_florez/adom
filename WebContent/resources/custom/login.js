var baseUrl = window.location.protocol + "//" + window.location.host + "/web";
//var baseUrl = window.location.protocol + "//" + window.location.host;
$(document).ready(function() {
	msg();
	$(".se-pass").click(function(){
		send_lost();
	});
});

function send_lost(){
	var xml_data = "<root>";
	xml_data += "<email>" + $("#email").val() + "</email>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/sendpass',
        success: function(data){
        	if($(data).find("code").text() == 100){
        		swal({   
    				title: "Una nueva contrase&ntilde;a se ha enviado al usuario",   
    				text: "",   
    				type: "success",   
    				showCancelButton: false,
    				html: true
    			});
        	}else{
        		swal({   
    				title: "La direcci&oacute;n de correo ingresada no ha sido encontrada",   
    				text: "",   
    				type: "error",   
    				showCancelButton: false,
    				html: true
    			});
        	}
        }
	});
}

function msg() {
	var font = "font-size:12px;";
	var display = "display:inline";
	var fCenter = "text-align:center;";
	var alertE = "alert alert-error";
	var alertL = "alert alert-success";
	var e = getUrlVars()["e"];
	if (e != null && e != '') {
		if (e == 1) {
			$("#msglogin").html("<label style='color:rgb(253, 144, 159);'>Datos no v&aacute;lidos, por favor intente de nuevo</label>");
			$("#msglogin").attr("style", display + font + fCenter);
			$("#msglogin").attr("class", alertE);
		}
	}
}

function getUrlVars() {
	var vars = [], hash;
	var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	for (var i = 0; i < hashes.length; i++) {
		hash = hashes[i].split('=');
		vars.push(hash[0]);
		vars[hash[0]] = hash[1];
	}
	return vars;
}