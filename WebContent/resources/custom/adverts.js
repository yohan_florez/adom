var baseUrl = window.location.protocol + "//" + window.location.host + "/web";
//var baseUrl = window.location.protocol + "//" + window.location.host;
var files = [];
var base64 ;
$(document).ready(function(){
	$(".new_event").click(function(){
		save();
	});
	$(".delete-advert").click(function(){
		delete_ad($(this).attr("id"));
	});
	$(".file").change(function(event) {
	  $.each(event.target.files, function(index, file) {
	    var reader = new FileReader();
	    reader.onload = function(event) {  
	      object = {};
	      object.filename = file.name;
	      object.data = event.target.result;
	      files.push(object);
	      base64 = object.data;
	      $(".img_load").attr("style","width: 300px;");
	      $(".img_load").attr("src",object.data);
	    };  
	    reader.readAsDataURL(file);
	  });
	});
});

function save(){
	var xml_data = "<root>";
	xml_data += "<title>" + $(".title_new").val() + "</title>";
	xml_data += "<image>" + base64 + "</image>";
	xml_data += "<content><![CDATA[" + CKEDITOR.instances.editor1.getData() + "]]></content>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/save_advert',
        success: function(data){
        	location.reload();
        }
	});
}

function delete_ad(id){
	var xml_data = "<root>";
	xml_data += "<id>" + id + "</id>";
	xml_data += "</root>";
	var data = {
		xml_data: xml_data	
	};
	$.ajax({
        type: 'POST',
        data: data,
        url: baseUrl + '/admin/delete_advert',
        success: function(data){
        	location.reload();
        }
	});
}