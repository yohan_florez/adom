  <%@include file="header.jsp" %>
    <!-- Middle Content Start -->
    
    <style>
    	.dataTables_wrapper{
    		height: 280px;
    	}
    </style>
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <!-- vd_head-section -->
          <!-- vd_title-section -->
          
          <div class="vd_content-section clearfix" style="min-height: 860px;">
			
			<div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
				<div class="panel-body-list pd-25">
	               <h4 class="mgtp-5"> <span class="font-semibold">Enfermer&iacute;a (Horas)</span></h4>
	               <div id="bar-placeholder" class="bar-chart" style="height:200px;"></div>
	             </div> 
			</div>
			<div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
				<div class="panel-body-list pd-25">
	               <h4 class="mgtp-5"> <span class="font-semibold">Terapias (Visitas)</span></h4>
	               <div id="bar-placeholder2" class="bar-chart" style="height:200px;"></div>
	             </div> 
			</div>
			<div class="col-lg-4 col-sm-6 col-md-6 col-xs-12">
				<div class="panel-body-list pd-25">
	               <h4 class="mgtp-5"> <span class="font-semibold">Pacientes sin profesional asignado</span></h4>
	               <div class="panel-body  table-responsive">
		               <table class="table hover data-tables-3">
	                      <thead>
	                        <tr>
	                          <th class="pointer">Paciente <i class="fa fa-sort"></i></th>
	                          <th>Servicio <i class="fa fa-sort"></i></th>
	                        </tr>
	                      </thead>
	                      <tbody class="tbody">
	                      	<c:forEach items="${data.list_np}" var="l">
							   <tr class="tr-1" id="${l.id}">
							   	<td>${l.name}</td>
							   	<td>${l.service}</td>
							   </tr>
							</c:forEach>
	                      </tbody>
	                   </table>
	                </div>
	             </div> 
			</div>
			<div class="col-md-12">
				<hr>
			</div>  
			<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
				<div class="panel-body-list pd-25">
	               <h4 class="mgtp-5"> <span class="font-semibold">Servicios irregulares</span> </h4>
	               <!-- <img style="width:21px;" class="imgload" src="${pageContext.request.contextPath}/resources/img/loading.gif"></h4> -->
	               <table class="table hover data-tables-3">
                      <thead>
                        <tr>
                          <th class="pointer">Paciente <i class="fa fa-sort"></i></th>
                          <th>Servicio <i class="fa fa-sort"></i></th>
                        </tr>
                      </thead>
                      <tbody class="tbody">
                      	<c:forEach items="${data.irregular}" var="l">
						   <tr class="tr-1" id="${l.adTentry.pkentry}">
						   	<td>
						   		${l.adTentry.adTusersByFkpatient.userName1} 
						   		${l.adTentry.adTusersByFkpatient.userName2} 
						   		${l.adTentry.adTusersByFkpatient.userSurname1} 
						   		${l.adTentry.adTusersByFkpatient.userSurname2}
						   	</td>
						   	<td>${l.adTservices.serviceName}</td>
						   </tr>
						</c:forEach>
                      </tbody>
                   </table>
	             </div> 
			</div> 
			<div class="col-lg-6 col-sm-6 col-md-6 col-xs-12">
				<div class="panel-body-list pd-25">
	               <h4 class="mgtp-5"> <span class="font-semibold">Profesionales - Copagos</span> </h4>
	               <!-- <img style="width:21px;" class="imgload" src="${pageContext.request.contextPath}/resources/img/loading.gif"></h4> -->
	               <table class="table hover data-tables-3">
                      <thead>
                        <tr>
                          <th class="pointer">Profesional <i class="fa fa-sort"></i></th>
                          <th>Recaudado <i class="fa fa-sort"></i></th>
                        </tr>
                      </thead>
                      <tbody class="tbody">
                      	<c:forEach items="${data.limits}" var="p">
						   <tr>
						   	<td>
						   		${p.get(0).userName1}
	                          	${p.get(0).userName2} 
	                          	${p.get(0).userSurname1} 
	                          	${p.get(0).userSurname2}
						   	</td>
						   	<td>$ ${p.get(1)}</td>
						   </tr>
						</c:forEach>
                      </tbody>
                   </table>
	             </div> 
			</div> 
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>

<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqprint.js"></script>  
<!--[if lt IE 9]>
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script> 
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/caroufredsel.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/custom/main2.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jlinq.js"></script> 

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/fullcalendar.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/lang/es.js'></script>

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/moment.min.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/daterangepicker.js'></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/prettyphoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/theme.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/raphael/raphael.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/morris/morris.min.js"></script>
 


<!-- Specific Page Scripts Put Here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/google-code-prettify/prettify.js"></script>
<script type="text/javascript">
$(window).load(function() 
	{
	"use strict";
		
	Morris.Bar({
	  element: 'bar-placeholder',
	  data: [
		{ y: 'Programadas', a: ${data.hours_p} },
		{ y: 'Realizadas', b: ${data.hours_r} },
	  ],
	  xkey: 'y',
	  ykeys: ['a', 'b'],
	  labels: ['Horas ', 'Horas '],
	  barColors: ["#23709E","#55F543"]
	});
	
	Morris.Bar({
		  element: 'bar-placeholder2',
		  data: [
			{ y: 'Programadas', a: ${data.tera_p} },
			{ y: 'Canceladas', b: ${data.tera_c} },
			{ y: 'Realizadas', c: ${data.tera_r} },
		  ],
		  xkey: 'y',
		  ykeys: ['a', 'b', 'c'],
		  labels: ['Visitas ', 'Visitas ', 'Visitas '],
		  barColors: ["#23709E","#F54349","#55F543"]
		});
});
</script>

</body>
</html>