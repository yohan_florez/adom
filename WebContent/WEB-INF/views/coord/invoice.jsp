  <%@include file="header.jsp" %>
    <!-- Middle Content Start -->
    
    <style>
    	select {
		    padding: 9px;
		}
    </style>
        
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <!-- vd_head-section -->
          <!-- vd_title-section -->
          
          <div class="vd_content-section clearfix" style="min-height: 660px;">

          <div class="row">
            <div class="col-xs-12">
              <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> 
                      <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> 
                      </span> FACTURACI&Oacute;N
                    </h3>
                  </div>
                  <div class="panel-body bordergray1">
                    <div class="col-xs-12">
		              <div class="panel widget light-widget mb-10">
		                  <div class="panel-heading">
		                    <h3 class="panel-title"> 
		                      BUSQUEDA Y FILTRO
		                    </h3>
		                  </div>
		                  <div class="panel-body bordergray1">
		                  	<div class="row mb-5 mt-10 ml-10 mr-10">
		                        <div class="col-md-2">
		                          Entidad: 
		                          <div class="controls mt-5">
		                            <select class="entitylist">
		                            	<option>Seleccione</option>
		                            	<c:forEach var="e" items="${data.entities}">
		                            		<option value="${e.pkentity}"><c:out value="${fn:toUpperCase(e.entityName)}"/></option>
		                            	</c:forEach>
		                            </select>
		                          </div>
			                    </div>
			                    <div class="col-md-2">
		                          Plan: 
		                          <div class="controls mt-5">
		                            <select class="entityplans">
		                            	<option>Seleccione</option>
		                            </select>
		                          </div>
			                    </div>
			                    <div class="col-md-2">
		                          Servicio: 
		                          <div class="controls mt-5">
		                            <select class="service">
		                            	<option value="1">ENFERMER&Iacute;A</option>
		                            	<option value="2">TERAPIA</option>
		                            </select>
		                          </div>
			                    </div>
			                    <div class="col-md-2">
		                          Fecha inicio: 
		                          <div class="controls mt-5">
		                            <input type="text" class="start range1">
		                          </div>
			                    </div>
			                    <div class="col-md-2">
		                          Fecha final: 
		                          <div class="controls mt-5">
		                            <input type="text" class="end range2">
		                          </div>
			                    </div>
			                    <div class="col-md-2 mt-28">
		                          <button class="btn btn-block btn-primary search has-spinner"> 
		                          		Buscar 
		                          		<span class="spinner"><i class="icon-spin icon-refresh" style="color:#FFFFFF"></i></span>
		                          </button>
			                    </div>
			                </div>
			                <div class="row mb-5 mt-10 ml-10 mr-10 mt-20">
			                	<center>
			                		<div class="col-md-12">
					                   <div class="controls">
					                     <div class="vd_radio radio-success">
					                      <input type="radio" name="tipo" id="tipo1" value="1" checked="true">
					                      <label for="tipo1"> Factura General </label>
					                      <input type="radio" name="tipo" id="tipo2" value="2">
					                      <label for="tipo2"> Factura Individual</label>
					                    </div>
					                   </div>
					                 </div>
			                	</center>
			                </div>
		                  </div>
		                </div>
		            </div>
		            <div class="row mb-15 ml-10 mr-10">
                		<div class="col-md-12">
                			<div class="col-md-2 col-sm-3 col-xs-6 menu_ind">
                			  Copago
	                          <div class="controls mt-5">
	                            <input type="text" placeholder="Copago" class="copago">
	                          </div>
		                    </div>
		                    <div class="col-md-2 col-sm-3 col-xs-6 menu_ind">
		                      Valor Neto
	                          <div class="controls mt-5">
	                            <input type="text" placeholder="Neto" class="neto">
	                          </div>
		                    </div>
		                    <div class="col-md-2 col-sm-3 col-xs-6 menu_ind">
		                      F. Factura
	                          <div class="controls mt-5">
	                            <input type="date" placeholder="Fecha Factura" class="datefact">
	                          </div>
		                    </div>
		                    <div class="col-md-2 col-sm-3 col-xs-6 menu_ind">
		                      N. Factura
	                          <div class="controls mt-5">
	                            <input type="number" placeholder="N&uacute;mero Factura" class="number">
	                          </div>
		                    </div>
		                    <div class="col-md-2 col-sm-12 col-xs-12">
	                          <div class="controls mt-28">
	                            <button class="btn btn-success btn-block generate">Generar RIPS</button>
	                          </div>
		                    </div>
                		</div>
			        </div>
		            <div class="row">
			            <div class="col-xs-12 ml-15 pr-45">
			              <div class="panel-body  table-responsive bordergray1">
		                    <table class="table hover data-tables">
		                      <thead>
		                        <tr>
		                          <th>
		                          	<div class="form-group ml-15">
			                          	<div class="controls">
				                          <div class="vd_checkbox checkbox-success">
				                            <input type="checkbox" value="1" id="check-all">
				                            <label for="check-all" class="checkall"></label>
				                          </div>
			                        	</div>
			                        </div>
		                          </th>
		                          <th>Paciente</th>
		                          <th>Identificaci&oacute;n</th>
		                          <th>Servicio</th>
		                          <th>N. Autorizaci&oacute;n</th>
		                          <th>Fecha Inicio</th>
		                          <th>Fecha Finalizaci&oacute;n</th>
		                          <th>Entidad</th>
		                          <th>Plan</th>
		                          <th>Nro. Factura</th>
		                        </tr>
		                      </thead>
		                      <tbody class="tbody">
		                        
		                      </tbody>
		                    </table>
		                  </div>
			            </div>
			        </div>
                  </div>
                </div>
            </div>
          </div>
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>

<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqprint.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/custom/invoiced.js"></script>
<!--[if lt IE 9]>
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script> 
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/caroufredsel.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jlinq.js"></script> 

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/fullcalendar.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/lang/es.js'></script>

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/moment.min.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/daterangepicker.js'></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/prettyphoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/theme.js"></script>
 


<!-- Specific Page Scripts Put Here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/google-code-prettify/prettify.js"></script>
<script>
    var start1 = new Date(), end1 = new Date(), start2 = new Date(), end2 = new Date();
    $('.range1').daterangepicker({
        locale: {
          format: 'YYY-MM-DD'
        },
        ranges: {
         'Hoy': [moment(), moment()],
         'Ayer': [moment().subtract('days', 1), moment().subtract('days', 1)],
         'Ultimos 7 d�as': [moment().subtract('days', 6), moment()],
         'Ultimos 30 d�as': [moment().subtract('days', 29), moment()],
         'Este mes': [moment().startOf('month'), moment().endOf('month')],
         'El mes pasado': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
        },
        startDate: start1,
        endDate: end1
      },
      function(start, end) {
        start1 = start;
        end1 = end;
      }
    );
    $('.range2').daterangepicker({
        locale: {
          format: 'YYY-MM-DD'
        },
        ranges: {
         'Hoy': [moment(), moment()],
         'Ayer': [moment().subtract('days', 1), moment().subtract('days', 1)],
         'Ultimos 7 d�as': [moment().subtract('days', 6), moment()],
         'Ultimos 30 d�as': [moment().subtract('days', 29), moment()],
         'Este mes': [moment().startOf('month'), moment().endOf('month')],
         'El mes pasado': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
        },
        startDate: start2,
        endDate: end2
      },
      function(start, end) {
        start2 = start;
        end2 = end;
      }
    );
</script>
<script>
var dt = $('.data-tables').dataTable({
    language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:  ",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "�ltimo",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    "paging":false,
    "searching":true,
    "info":false,
    "scrollY": "300px",
    "scrollCollapse": false,
    "scrollX": true 
});
</script>
</body>
</html>