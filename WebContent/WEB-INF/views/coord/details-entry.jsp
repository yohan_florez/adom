  <%@include file="header-2.jsp" %>
  <%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
  	<style>
  		input[type="text"]{
  			padding: 4px;
  		}
  		input[type="number"]{
  			padding: 4px;
  		}
  		select {
		    padding: 6px;
		}
		@media screen and (max-width:768px){
		 .eliminar_padding{padding-left:10px;
		}
  	</style>
    <!-- Middle Content Start -->
        
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <!-- vd_head-section -->
          <!-- vd_title-section -->
          
          <div class="modal fade" id="modalp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header btn-active">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                  <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user-plus"></i> Datos del paciente</h4>
                </div>
                <div class="modal-body"> 
                  <form id="frmreg" class="form form-horizontal">
                    <div class="row mb-5">
                  <div class="col-md-6">
                    <label class="control-label">Tipo Documento</label>
                    <div class="controls">
                      <select class="td">
                      	<c:forEach var="typedocs" items="${data.typedocs}">
                      		<option value="${typedocs.pktypedocument}"><c:out value="${typedocs.typedocumentName}"/></option>
                      	</c:forEach>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label class="control-label">Numero Documento</label>
                    <div class="controls">
                      <input type="text" class="nd"  id="nd" required disabled placeholder="Numero documento">
                    </div>
                  </div>
              </div>
              <div class="row mb-5">
                  <div class="col-md-6">
                    <label class="control-label">Primer Nombre</label>
                    <div class="controls">
                      <input type="text" class="n1" required autofocus placeholder="Primer Nombre">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label class="control-label">Segundo Nombre</label>
                    <div class="controls">
                      <input type="text" class="n2" placeholder="Segundo Nombre">
                    </div>
                  </div>
              </div>
              <div class="row mb-5">
                  <div class="col-md-6">
                    <label class="control-label">Primer Apellido</label>
                    <div class="controls">
                      <input type="text" class="l1" required placeholder="Primer Apellido">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label class="control-label">Segundo Apellido</label>
                    <div class="controls">
                      <input type="text" class="l2" placeholder="Segundo Apellido">
                    </div>
                  </div>
              </div>
              <div class="row mb-5">
                  <div class="col-md-6">
                    <label class="control-label">G&eacute;nero</label>
                    <div class="controls">
                      <select class="g">
                      	<option value="M">MASCULINO</option>
                      	<option value="F">FEMENINO</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label class="control-label">Tipo Usuario</label>
                    <div class="controls">
                      <input type="text" required class="tu" value="5">
                    </div>
                  </div>
              </div>
              <div class="row mb-5">
              	   <div class="col-md-6">
                    <label class="control-label">Fecha nacimiento</label>
                    <div class="controls">
                      <input type="text" class="fn date" placeholder="Fecha nacimiento">
                    </div>
                  </div>
                  <div class="col-md-6 mt-30">
                    <label class="control-label col-md-2">Edad</label>
                    <div class="controls col-md-5">
                      <input type="text" class="a" required placeholder="Edad">
                    </div>
                    <div class="controls col-md-5">
                      <select class="ta">
                      	<option value="A">A�OS</option>
                      	<option value="Meses">MESES</option>
                      </select>
                    </div>
                  </div>
              </div>
              <div class="row mb-5">
                  <div class="col-md-6">
                    <label class="control-label">Barrio</label>
                    <div class="controls">
                      <input type="text" class="d" placeholder="Barrio">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label class="control-label">Direcci&oacute;n</label>
                    <div class="controls">
                      <input type="text" class="add" placeholder="Direcci&oacute;n">
                    </div>
                  </div>
              </div>
              <div class="row mb-5">
                  <div class="col-md-6">
                    <label class="control-label">Zona</label>
                    <div class="controls">
                      <input type="text" class="z" placeholder="Zona">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label class="control-label">Email</label>
                    <div class="controls">
                      <input type="email" class="e" id="e" placeholder="Email">
                    </div>
                  </div>
              </div>
              <div class="row mb-5">
                  <div class="col-md-6">
                    <label class="control-label">T&eacute;lefono 1</label>
                    <div class="controls">
                      <input type="text" class="p1" required placeholder="T&eacute;lefono 1">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <label class="control-label">T&eacute;lefono 2</label>
                    <div class="controls">
                      <input type="text" class="p2" placeholder="T&eacute;lefono 2">
                    </div>
                  </div>
              </div>
                    <div class="form-group">
                      <a class="btn pull-right btn vd_btn btn-mommy mr20 mt30 update-p">Guardar</a>
                      <button type="button" class="pull-right btn vd_btn vd_bg-info mr5 mt30" data-dismiss="modal">Cancelar</button>
                    </div>
                  </form>
                </div>
              </div>
              <!-- /.modal-content --> 
            </div>
            <!-- /.modal-dialog --> 
          </div>
          
          <input type="hidden" value="${data.entry.pkentry}" class="pkentryh">
          
          <div class="vd_content-section clearfix" style="min-height: 660px;">
          <div class="modal fade" id="myModal_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header btn-active">
                  <button type="button" class="close clear" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                  <h4 class="modal-title" id="myModalLabel"> Editar Servicio </h4>
                </div>
                <div class="modal-body"> 
                  <form id="frmreg" class="form form-horizontal">
                    <div class="row mb-5">
                  <div class="col-md-6 col-sm-6 col-xs-6">
                    <label class="control-label">N&uacute;mero de autorizaci&oacute;n</label>
                    <div class="controls">
                      <input type="text" class="nro_auth_2" autofocus>
                    </div>
                  </div>
              </div>
              <div class="row mb-5">
                  <div class="col-md-6 col-sm-6 col-xs-6">
                    <label class="control-label">Copago</label>
                    <div class="controls">
                      <input type="number" class="cop_2" name="rs">
                    </div>
                  </div>
              </div>
              <div class="row mb-5">
                  <div class="col-md-6 col-sm-6 col-xs-6">
                    <label class="control-label">Frecuencia del Copago</label>
                    <div class="controls">
                       <select class="fcopago_2">
                       	<option value="1">CADA SESI&Oacute;N</option>
                       	<option value="5">CADA 5 SESI&Oacute;NES</option>
                       	<option value="0">OTRO</option>
                       </select>
                     </div>
                  </div>
              </div>
                    <div class="form-group">
                      <a class="btn pull-right btn vd_btn btn-mommy mr20 mt30 update_service_">Actualizar</a>
                      <button type="button" class="clear pull-right btn vd_btn vd_bg-info mr5 mt30" data-dismiss="modal">Cancelar</button>
                    </div>
                  </form>
                </div>
              </div>
              <!-- /.modal-content --> 
            </div>
            <!-- /.modal-dialog --> 
          </div>
          <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog mservice">
              <form id="addserv">
              <div class="modal-content">
                <div class="modal-header btn-active">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                  <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user-plus"></i> A�adir nuevo servicio</h4>
                </div>
                <div class="modal-body">
                	<div class="row row-title"><h5>Datos de la autorizaci&oacute;n</h5></div>
                	<div class="row mb-5 padd-15" style="border: 1px solid gray;">
                		<div class="col-md-3 col-sm-3 mt-5">
                          NRO. CONTRATO: <label class="control-label">${data.entry.entryNumber}</label>
                        </div>
                		<div class="col-md-3 col-sm-4 mt-5">
                          ENTIDAD: <label class="control-label">${data.entry.adTentityplan.adTentity.entityName}</label>
                        </div>
                        <div class="col-md-6 col-sm-5 mt-5">
                          PLAN: <label class="control-label">${data.entry.adTentityplan.entityplanName}</label>
	                    </div>
                        <div class="col-md-3 col-sm-4">
                          <div class="controls">
                            <input type="text" class="nroaut" name="nro" placeholder="Nro. autorizaci&oacute;n">
                          </div>
                        </div>
                        <div class="col-md-3 col-sm-4">
                          <div class="controls">
                            <input placeholder="Vigencia" class="datevalidity" type="text" onfocus="(this.type='date')"  name="vali">
                          </div>
                        </div>
                        <div class="col-md-6 col-sm-4">
                          <div class="controls">
                            <input type="text" class="solicitante" name="sol" placeholder="Nombre del solicitante">
                          </div>
                        </div>
	                </div>
	                <div class="row row-title mt-20"><h5>Informaci&oacute;n del servicio</h5></div>
                	<div class="row mb-5 padd-15" style="border: 1px solid gray;">
               		<div class="col-md-12">
                        <div class="col-md-3 col-xs-12 col-sm-4 col-lg-3">
                          <label class="control-label">Servicio</label>
                          <div class="controls">
                            <select class="lservices">
                            	<c:forEach var="service" items="${data.services}">
	                           		<option value="${service.pkservice}"><c:out value="${fn:toUpperCase(service.serviceName)}"/></option>
	                           	</c:forEach>
                            </select>
                          </div>
                        </div>
                        <div class="col-md-2 col-xs-2 col-sm-2 col-lg-1">
                          <label class="control-label">Cantidad</label>
                          <div class="controls">
                            <input type="text" class="qty" name="qty" placeholder="Cantidad" value="1">
                          </div>
                        </div>
                        <div class="col-md-2 col-sm-3 col-lg-2">
                          <label class="control-label">F. inicio</label>
                          <div class="controls">
                            <input type="date" name="fini" class="datestart">
                          </div>
                        </div>
                        <div class="col-md-2 col-sm-3 col-lg-2">
                          <label class="control-label">F. finalizaci&oacute;n</label>
                          <div class="controls">
                            <input type="date" disabled class="dateend">
                          </div>
                        </div>
                        <div class="col-md-2 col-lg-2 col-sm-12">
                          <label class="control-label">Frec. servicio</label>
                          <div class="controls">
                            <select class="fservices">
                            	<c:forEach var="frecuency" items="${data.frecuency}">
	                           		<option value="${frecuency.pkfrecuency}" id="${frecuency.frecuencyDaysxweek}">
	                           			<c:out value="${frecuency.frecuencyName}"/>
	                           		</option>
	                           	</c:forEach>
                            </select>
                          </div>
                        </div>
                       </div>
                        <div class="col-md-12 mt-5">
                        	<div class="col-md-5 col-sm-6">
	                          <label class="control-label">Profesional</label>
	                          <div class="controls ui-widget">
	                            <select class="professional chosen-select"  data-placeholder="Asigne un profesional" style="width:350px;" tabindex="2">
	                            	<option value="null" selected>POR ASIGNAR</option>
	                            	<c:forEach var="professional" items="${data.professionals}">
		                           		<option value="${professional.pkuser}">${fn:trim(professional.userName1)} ${professional.userName2} ${professional.userSurname1} ${professional.userSurname2} - ${professional.userDocument}</option>
		                           	</c:forEach>
	                            </select>
	                          </div>
	                        </div>
	                        <div class="col-md-2 col-sm-6">
	                          <label class="control-label">Valor Copago</label>
	                          <div class="controls">
	                            <input type="number" class="valcop" name="valcop" placeholder="Valor Copago">
	                          </div>
	                        </div>
	                        <div class="col-md-2 col-sm-6">
	                          <label class="control-label">Frec. copago</label>
	                          <div class="controls">
	                            <select class="fcopago">
	                            	<option value="1">CADA SESI&Oacute;N</option>
	                            	<option value="5">CADA 5 SESI&Oacute;NES</option>
	                            	<option value="0">OTRO</option>
	                            </select>
	                          </div>
	                        </div>
	                        <div class="col-md-1 col-sm-3">
	                          <label class="control-label">Consulta</label>
	                          <div class="controls">
	                            <input type="text" class="fincon" name="fincon" value="10">
	                          </div>
	                        </div>
	                        <div class="col-md-1 col-sm-3">
	                          <label class="control-label">Externa</label>
	                          <div class="controls">
	                            <input type="text" class="cauext" name="cauext" value="13">
	                          </div>
	                        </div>
                        </div>
                        <hr>
                        <div class="col-md-12 mt-10">
                       		<div class="col-md-12 mt-20">
                       			<div class="col-md-3">
		                          <label class="control-label">Insumo</label>
		                          <div class="controls">
		                            <select class="insumo"></select>
		                          </div>
		                        </div>
		                        <div class="col-md-2">
		                          <label class="control-label">Cantidad</label>
		                          <input type="text" class="qtyinsumo" value="1">
		                        </div>
		                        <div class="col-md-3">
		                          <label class="control-label">FACTURADO A</label>
		                          <div class="controls">
		                            <select class="facby">
		                            	<c:forEach var="invoiced" items="${data.invoiced}">
			                           		<option value="${invoiced.pkinvoiced}">
			                           			${invoiced.invoicedName}
			                           		</option>
			                           	</c:forEach>
		                            </select>
		                          </div>
		                        </div>
		                        <div class="col-md-4">
		                          <div class="controls">
		                            <a class="btn btn-primary mt-25 btn-block addinsu">Agregar</a>
		                          </div>
		                        </div>
                       		</div>
                        	<div class="panel widget light-widget mb-0 mt-5 col-md-12">
			                  <div class="panel-body bordergray1">
			                  	<table class="table hover data-tables">
			                      <thead>
			                        <tr>
			                          <th class="pointer">Nombre <i class="fa fa-sort"></i></th>
			                          <th>Cantidad <i class="fa fa-sort"></i></th>
			                          <th>Facturado a <i class="fa fa-sort"></i></th>
			                          <th></th>
			                        </tr>
			                      </thead>
			                      <tbody class="tbody-3">
			                      	
			                      </tbody>
			                    </table>
			                  </div>
		                	</div>
                        </div>
	                </div>
                </div>
                <div class="modal-footer btn-active">
                	<div class="form-group">
                      <button class="btn pull-right btn vd_btn btn-mommy mr20 bas has-spinner">
                      		Asignar el servicio 
                      		<span class="spinner"><i class="icon-spin icon-refresh" style="color:#23709E"></i></span>
                      </button>
                      <button type="button" class="pull-right btn btn-danger mr5 close-model-assignservice" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
              </div>
              </form>
              </div>
              <!-- /.modal-content --> 
            <!-- /.modal-dialog --> 
          </div>

          <div class="row">
            <div class="col-xs-12">
              <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> 
                      <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> 
                      </span> Detalles del ingreso
                    </h3>
                  </div>
                  <div class="panel-body bordergray1 pedit1">
                  	<div class="col-xs-12">
		              <div class="panel widget light-widget">
		                  <div class="panel-heading" style="margin-bottom:30px !important;">
		                  	<div class="col-md-9">
			                    <h3 class="panel-title"> 
			                       Informaci&oacute;n general del ingreso
			                    </h3>
			                </div>
			                <div class="col-md-3">
			                	<c:if test="${data.status}">
			                		<c:if test="${data.p6}">
									   <div class="">
				                  			<button class="btn  btn-block btn-primary close-entry-2" id="${data.entry.pkentry}">
				                  			 DAR DE ALTA
				                  			</button>
				                  		</div>
				                  	</c:if>
								</c:if>
			                </div>
		                  </div>
		                  <div class="panel-body bordergray1">
			                <div class="row mb-5 eliminar_padding">
		                  		<div class="col-lg-3 col-md-3 col-sm-4">
		                  			<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Paciente: 
			                          <label class="control-label">
			                          	${data.entry.adTusersByFkpatient.userName1} 
			                          	${data.entry.adTusersByFkpatient.userName2} 
			                          	${data.entry.adTusersByFkpatient.userSurname1} 
			                          	${data.entry.adTusersByFkpatient.userSurname2}
			                          </label>
				                    </div>
				                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Tipo Doc.: <label class="control-label">${data.entry.adTusersByFkpatient.adTtypeDocument.typedocumentShortname}</label>
				                    </div>
				                    
			                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                        	G�nero: <label class="control-label">
				                         	<c:choose>
											  <c:when test="${data.entry.adTusersByFkpatient.userGender == 'M'}">
											    MASCULINO
											  </c:when>
											  <c:otherwise>
											    FEMENINO
											  </c:otherwise>
											</c:choose>
				                         </label>
			                        </div>
			                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Tel&eacute;fono: <label class="control-label">${data.entry.adTusersByFkpatient.userPhone1}</label>
				                    </div>
		                  		</div>
		                  		<div class="col-lg-3 col-md-3 col-sm-4">
		                  			<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
				                    	N�mero Doc.: <label class="control-label">${data.entry.adTusersByFkpatient.userDocument}</label>
				                    </div>		                  			
				                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Edad: <label class="control-label">
				                          		${data.entry.adTusersByFkpatient.userAge}
				                          		<c:choose>
												  <c:when test="${data.entry.adTusersByFkpatient.userAgetype == 'A'}">
												    A�os
												  </c:when>
												  <c:otherwise>
												    Meses
												  </c:otherwise>
												</c:choose>
				                          	</label>
			                        </div>
		                        	<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Fecha ingreso: <label class="control-label">${fn:substring(data.entry.entryStartdate, 0, 10)}</label>
			                        </div>
			                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Fecha salida: <label class="control-label">
											<c:choose>
											  <c:when test="${data.entry.entryEnddate == null}">
											    ACTIVO
											  </c:when>
											  <c:otherwise>
											    ${data.entry.entryEnddate}
											  </c:otherwise>
											</c:choose>
											</label>
			                        </div>			                         
			                    </div>
			                    <div class="col-lg-6 col-md-6 col-sm-4">	
			                    	 <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Entidad: <label class="control-label eliminar_padding">${data.entry.adTentityplan.adTentity.entityName}</label>
			                        </div>    
			                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Plan: <label class="control-label eliminar_padding">${data.entry.adTentityplan.entityplanName}</label>
			                        </div>		                        
			                    	<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          CIE10: <label class="control-label">${data.entry.entryCie10}</label>
			                        </div>
			                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Descripci&oacute;n: <label class="control-label">${data.entry.entryCie10description}</label>
			                        </div>
			                    </div>
			                </div>
			                <div class="row mb-5 eliminar_padding">
			                	<div class="col-md-10 col-sm-10 col-lg-10 col-xs-6 eliminar_padding"></div>
			                	<div class="col-md-2 col-sm-2 col-lg-2 col-xs-6 eliminar_padding">
		                          <a class="btn-xs vd pointer infop" id="${data.entry.adTusersByFkpatient.pkuser}" style="cursor:pointer;text-transform: capitalize; !important;">Datos del paciente</a>
			                    </div>
			                </div>
		                  </div>
		                </div>
		            </div>
		            
		            <div class="col-xs-12">
		              <div class="panel widget light-widget">
		                  <div class="panel-heading">
		                    <h3 class="panel-title"> 
		                       Servicios del paciente
		                    </h3>
		                  </div>
		                  <div class="panel-body table-responsive bordergray1">
		                  	<c:if test="${data.status}">
		                  		<c:if test="${data.p2}">
			                  		<button class="btn btn-mommy vd_btn mb-15 mt-10 ml-10 btn-modal-newserv" 
			                  			id="${data.entry.pkentry}">A�ADIR SERVICIO</button>
			                  	</c:if>
		                  	</c:if>
		                  	<table class="table hover data-tables-3">
		                      <thead>
		                        <tr>
		                          <th class="pointer">Servicio <i class="fa fa-sort"></i></th>
		                          <th>Cant. Programada <i class="fa fa-sort"></i></th>
		                          <th>Cant. Realizada <i class="fa fa-sort"></i></th>
		                          <th>Frecuencia <i class="fa fa-sort"></i></th>
		                          <th>F. inicio <i class="fa fa-sort"></i></th>
		                           <th>Estado <i class="fa fa-sort"></i></th>
		                          <th>Facturado <i class="fa fa-sort"></i></th>
		                        </tr>
		                      </thead>
		                      <tbody class="tbody tbody-lservices">
		                      	
		                      </tbody>
		                    </table>
		                  </div>
		                </div>
		            </div>
		            <div class="col-xs-12 col-md-9 divdetail" style="display:none;">
		              <div class="panel widget light-widget">
		                  <div class="panel-heading">
		                    <h3 class="panel-title"> 
		                      Detalle de visitas
		                    </h3>
		                  </div>
		                  <div class="panel-body bordergray1 pb-5">
		                  	<table class="table hover data-tables-2">
		                      <thead>
		                        <tr>
		                          <th>
		                          	<c:if test="${data.status}">
			                          	<div class="form-group ml-15">
				                          	<div class="controls">
					                          <div class="vd_checkbox checkbox-success">
					                            <input type="checkbox" value="1" id="check-all">
					                            <label for="check-all" class="checkall"></label>
					                          </div>
				                        	</div>
				                        </div>
				                    </c:if>
		                          </th>
		                          <th class="pointer">Visita </th>
		                          <th>Fecha </th>
		                          <th>Profesional </th>
		                          <th>Estado </th>
		                        </tr>
		                      </thead>
		                      <tbody class="tbody-visit">
		                      </tbody>
		                    </table>
		                    <c:if test="${data.status}">
			                    <div class="row mt-30">
			                    	<c:if test="${data.p3}">
				                  		<div class="col-md-4 col-sm-4 col-lg-4 col-xs-12">
				                  			<a class="btn btn-danger btn-block cancel-cit">
				                  				<i class="fa fa-remove"></i> Cancelar citas
				                  			</a>
				                  		</div>
				                  	</c:if>
				                  	<c:if test="${data.p4}">
				                  		<div class="col-md-4 col-sm-4 col-lg-4 col-xs-12">
				                  			<a class="btn btn-success disabled btn-block update-visit">
				                  				 Guardar Cambios
				                  			</a>
				                  		</div>
				                  	</c:if>
				                  	<c:if test="${data.p11}">
					                  	<div class="col-md-4 col-sm-4 col-lg-4 col-xs-12">
				                  			<a class="btn btn-primary btn-block update-service_">
				                  				 Editar servicio
				                  			</a>
				                  		</div>
				                  	</c:if>
			                  	</div>
			                </c:if>
		                  </div>
		                </div>
		            </div>
		            <div class="col-xs-12 col-md-3 divdetail" style="display:none;">
		              <div class="panel widget light-widget">
		                  <div class="panel-heading">
		                    <h3 class="panel-title"> 
		                      Detalle del servicio
		                    </h3>
		                  </div>
		                  <div class="panel-body bordergray1 div-det">
		                  	<div class="row mb-5 det-serv">
		                  		<div class="col-md-12">
		                          Plan: <label class="control-label">${data.entry.adTentityplan.entityplanName}</label>
			                    </div>
			                    <div class="col-md-12">
		                          Contrato: <label class="control-label">${data.entry.entryNumber}</label>
			                    </div>
		                        <div class="col-md-12">
		                          N. Autorizaci&oacute;n: <label class="control-label lauth"></label>
			                    </div>
		                        <div class="col-md-12">
		                          Vigencia: <label class="control-label lvig"></label>
		                        </div>
		                        <div class="col-md-12">
		                          F. Solicitud: <label class="control-label lfsol"></label>
		                        </div>
		                        <div class="col-md-12">
		                          Solicitante: <label class="control-label lsol"></label>
		                        </div>
		                        <div class="col-md-12">
		                          Servicio: <label class="control-label lserv"></label>
			                    </div>
		                        <div class="col-md-12">
		                          Fecha inicio: <label class="control-label lstart"></label>
		                        </div>
		                        <div class="col-md-12">
		                          Finaliza: <label class="control-label lend"></label>
		                        </div>
		                        <div class="col-md-12">
		                          Cantidad: <label class="control-label lqty"></label>
			                    </div>
		                        <div class="col-md-12">
		                          Frecuencia: <label class="control-label lfrec"></label>
		                        </div>
		                        <div class="col-md-12">
		                          Copago: <label class="control-label lcop"></label>
		                        </div>
		                        <div class="col-md-12">
		                          Frec. Copago: <label class="control-label lfrecop"></label>
			                    </div>
			                </div>
		                  </div>
		                </div>
		            </div>
		            <div class="col-xs-12 col-sm-7 col-md-7 divdetail">
		              <div class="panel widget light-widget">
		                  <div class="panel-heading">
		                    <h3 class="panel-title"> 
		                      Observaciones
		                    </h3>
		                  </div>
		                  <div class="panel-body table-responsive bordergray1">
		                  	<table class="table hover data-tables-4">
		                  	  <thead>
		                  	  	<tr>
		                  	  		<th></th>
		                  	  	</tr>
		                  	  </thead>
		                      <tbody class="tbody tbody-lob">
		                      	
		                      </tbody>
		                    </table>
		                    <c:if test="${data.status}">
		                    	<c:if test="${data.p5}">
					                <div class="row mt-30 mb-5">
						             	<div class="col-md-8">
				                          <div class="controls">
				                            <input type="text" class="rs" placeholder="Escriba su mensaje">
				                          </div>
				                        </div>
				                        <div class="col-md-4">
				                          <div class="controls">
				                            <a type="text" class="btn btn-success btn-block addobs">Enviar</a>
				                          </div>
				                        </div>
				                     </div>
				                  </c:if>
			                </c:if>
		                  </div>
		                </div>
		            </div>
		            <div class="col-xs-12 col-md-5 col-sm-5 divdetail" style="display:none;">
		              <div class="panel widget light-widget">
		                  <div class="panel-heading">
		                    <h3 class="panel-title"> 
		                      Insumos
		                    </h3>
		                  </div>
		                  <div class="panel-body bordergray1">
		                  	<c:if test="${data.status}">
			                  	<div class="col-md-12 mt-20">
	                       			<div class="col-md-4">
			                          <label class="control-label">Insumo</label>
			                          <div class="controls">
			                            <select class="insumo2"></select>
			                          </div>
			                        </div>
			                        <div class="col-md-2">
			                          <label class="control-label">Cantidad</label>
			                          <input type="text" class="qtyinsumo2" value="1">
			                        </div>
			                        <div class="col-md-4">
			                          <label class="control-label">FACTURADO A</label>
			                          <div class="controls">
			                            <select class="facby2">
			                            	<c:forEach var="invoiced" items="${data.invoiced}">
				                           		<option value="${invoiced.pkinvoiced}">
				                           			${invoiced.invoicedName}
				                           		</option>
				                           	</c:forEach>
			                            </select>
			                          </div>
			                        </div>
			                        <div class="col-md-2">
			                          <div class="controls">
			                            <a class="btn btn-primary mt-25 btn-block addinsu2">+</a>
			                          </div>
			                        </div>
	                       		</div>
	                       	</c:if>
		                  	<table class="table hover data-tables-2">
		                      <thead>
		                        <tr>
		                        <th class="pointer">C&Oacute;digo <i class="fa fa-sort"></i></th>
		                          <th class="pointer">Nombre <i class="fa fa-sort"></i></th>
		                          <th>Cantidad <i class="fa fa-sort"></i></th>
		                          <th>Facturado a <i class="fa fa-sort"></i></th>
		                          <th></th>
		                        </tr>
		                      </thead>
		                      <tbody class="tbody-insumo">
		                      </tbody>
		                    </table>
		                  </div>
		                </div>
		            </div>
                  </div>
                </div>
            </div>
          </div>
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
  <!-- .container --> 
<!-- .content -->
  
  
  
<!-- Footer Start -->


<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/chosen.jquery.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/chosen2.jquery.js" type="text/javascript"></script>
<script type="text/javascript">
	$(".professional").chosen({
	    no_results_text: "",
	    width: "95%"
	});
</script>
<script src="${pageContext.request.contextPath}/resources/js/remote-list.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqprint.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/custom/details-2.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-validate.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-ui.js"></script>
<!--[if lt IE 9]>
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script> 
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/caroufredsel.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jlinq.js"></script> 

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/fullcalendar.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/lang/es.js'></script>

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/moment.min.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/daterangepicker.js'></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/prettyphoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/theme.js"></script>


<!-- Specific Page Scripts Put Here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/google-code-prettify/prettify.js"></script>
<script type="text/javascript">
$(document).ready(function() {  
    "use strict";
        var form_register_2 = $('#addserv');
        var error_register_2 = $('.alert-danger', form_register_2);
        var success_register_2 = $('.alert-success', form_register_2);
        
        var msg_required = "Este campo es requerido";
        var msg_minlength = "Necesitamos por lo menos {0} caracteres";
        var msg_maxlength = "Necesitamos m&aacute;ximo {0} caracteres";;
        var msg_number = "Debes ingresar un n&uacute;mero";
		var msg_nospace = "No se admiten espacios";
		var msg_email = "Ej: example@example.com";
		var msg_remote_email = "Este email ya se encuentra registrado";
		var msg_remote_document = "Este documento ya se encuentra registrado";
        
        form_register_2.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
            	nro:{
            		required:true,
            		minlength:4
            	},
                vali: {
                    required: true
                },
                sol: {
                    required: true,
                    minlength: 6
                },
                qty: {
                    required: true,
                    number: true
                },
                fini: {
                    required: true
                },
                valcop: {
                	required: true,
                    number: true
                },
                fincon: {
                	required: true,
                    number: true
                },
                cauext: {
                	required: true,
                    number: true
                }
        
            },
            messages: {
            	nro: {
                    required: msg_required,
                    minlength: msg_minlength
                },
                vali: {
                    required: msg_required
                },
                sol: {
                	required: msg_required,
                	minlength: msg_minlength
                },
                qty: {
                    required: msg_required,
                    number: msg_number
                },
                fini: {
                	required: msg_required
                },
                valcop: {
                    required: msg_required,
                    number: msg_number
                },
                fincon: {
                    required: msg_required,
                    number: msg_number
                }, 
                cauext: {
                    required: msg_required,
                    number: msg_number
                }
           },
           
           submitHandler: function() {
        	   assignservice();
           }
        });
  
});
</script>
</body>
</html>