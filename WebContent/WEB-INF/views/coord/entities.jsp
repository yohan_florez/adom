  <%@include file="header.jsp" %>
    <!-- Middle Content Start -->
        
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <!-- vd_head-section -->
          <!-- vd_title-section -->
          
          <div class="vd_content-section clearfix" style="min-height: 660px;">

          <div class="row">
            <div class="col-xs-12">
              <button class="btn btn-mommy vd_btn" data-toggle="modal" data-target="#myModal"> NUEVA ENTIDAD</button>
              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header btn-active">
                          <button type="button" class="close clear" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user-plus"></i> Registrar nueva entidad</h4>
                        </div>
                        <div class="modal-body"> 
                          <form id="frmreg" class="form form-horizontal">
                            <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">NIT</label>
		                          <div class="controls">
		                            <input type="text" class="nit" name="nit" autofocus placeholder="NIT">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-12">
		                          <label class="control-label">Raz&oacute;n social</label>
		                          <div class="controls">
		                            <input type="text" class="rs" name="rs" autofocus placeholder="Raz&oacute;n social">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">C&oacute;digo entidad administradora</label>
		                          <div class="controls">
		                            <input type="text" class="cod" name="cod" autofocus placeholder="C&oacute;digo entidad administradora">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-12">
		                          <label class="control-label">Nombre entidad administradora</label>
		                          <div class="controls">
		                            <input type="text" class="nam" name="name" autofocus placeholder="Nombre entidad administradora">
		                          </div>
		                        </div>
		                    </div>
                            <div class="form-group">
                              <button class="btn pull-right btn vd_btn btn-mommy mr20 mt30">Guardar</button>
                              <button type="button" class="clear pull-right btn vd_btn vd_bg-info mr5 mt30" data-dismiss="modal">Cancelar</button>
                            </div>
                          </form>
                        </div>
                      </div>
                      <!-- /.modal-content --> 
                    </div>
                    <!-- /.modal-dialog --> 
                  </div>
                  <div class="modal fade" id="modal-update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header btn-active">
                          <button type="button" class="close clear" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user-plus"></i> Editar entidad</h4>
                        </div>
                        <div class="modal-body"> 
                          <form id="frmedit" class="form form-horizontal">
                            <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">NIT</label>
		                          <div class="controls">
		                            <input type="text" class="nit1" autofocus placeholder="NIT">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-12">
		                          <label class="control-label">Raz&oacute;n social</label>
		                          <div class="controls">
		                            <input type="text" class="rs1" autofocus placeholder="Raz&oacute;n social">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">C&oacute;digo entidad administradora</label>
		                          <div class="controls">
		                            <input type="text" class="cod1" autofocus placeholder="C&oacute;digo entidad administradora">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-12">
		                          <label class="control-label">Nombre entidad administradora</label>
		                          <div class="controls">
		                            <input type="text" class="nam1" autofocus placeholder="Nombre entidad administradora">
		                          </div>
		                        </div>
		                    </div>
                            <div class="form-group">
                              <a class="btn pull-right btn vd_btn btn-mommy mr20 mt30 updatep">Guardar</a>
                              <button type="button" class="clear pull-right btn vd_btn vd_bg-info mr5 mt30" data-dismiss="modal">Cancelar</button>
                            </div>
                          </form>
                        </div>
                      </div>
                      <!-- /.modal-content --> 
                    </div>
                    <!-- /.modal-dialog --> 
                  </div>
                  <div class="modal fade" id="modaltar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header btn-active">
                          <button type="button" class="close clear" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                          <h4 class="modal-title2" id="myModalLabel"></h4>
                        </div>
                        <div class="modal-body"> 
                          <form id="frm" class="form form-horizontal">
                            <div class="row mb-5">
		                        <div class="col-md-9 col-xs-9 col-lg-9 col-sm-9">
		                          <label class="control-label">Nombre del plan</label>
		                          <div class="controls">
		                            <input type="text" class="planname" autofocus placeholder="Nombre del nuevo plan">
		                          </div>
		                        </div>
		                        <div class="col-md-3 col-xs-3 col-lg-3 col-sm-3">
		                          <label class="control-label"></label>
		                          <div class="controls mt-5">
		                            <a class="btn btn-success newplan">Crear</a>
		                          </div>
		                        </div>
		                        <div class="col-md-12">
		                          <label class="control-label msg-plan"></label>
		                        </div>
		                    </div>
		                    <div class="row mt-20">
		                    	<div class="col-md-9 col-sm-9 col-lg-9 col-xs-9">
		                          <label class="control-label">Lista de planes disponibles</label>
		                          <div class="controls mt-5">
		                            <select class="plans"></select>
		                          </div>
		                        </div>
		                        <div class="col-md-3 col-sm-3 col-lg-3 col-xs-3 meac" style="display:none;">
		                          <label class="control-label"></label>
		                           <div class="controls mt-5">
		                            <a class="btn btn-block status-plan">Activar</a>
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5 tbl-tar" style="display:none;">
		                        <div class="col-md-5 col-xs-5 col-lg-5 col-sm-5">
		                          <label class="control-label">Servicio</label>
		                          <div class="controls mt-5">
		                            <select class="lservicios"></select>
		                          </div>
		                        </div>
		                        <div class="col-md-4 col-xs-4 col-lg-4 col-sm-4">
		                          <label class="control-label">Tarifa</label>
		                          <div class="controls mt-5">
		                            <input type="text" class="tarval" autofocus placeholder="Tarifa">
		                          </div>
		                        </div>
		                        <div class="col-md-3 col-xs-3 col-lg-3 col-sm-3">
		                          <label class="control-label"></label>
		                          <div class="controls mt-5">
		                            <a class="btn btn-success btn-block newtarserv">A�adir</a>
		                          </div>
		                        </div>
		                        <div class="col-md-12">
		                          <label class="control-label msg-tari"></label>
		                        </div>
		                        <div class="col-md-12">
		                          <div class="controls mt-5">
		                            <table class="table hover data-tables-2">
				                      <thead>
				                        <tr>
				                          <th class="pointer">Servicio <i class="fa fa-sort"></i></th>
				                          <th>Tarifa <i class="fa fa-sort"></i></th>
				                          <th>Acciones</th>
				                        </tr>
				                      </thead>
				                      <tbody class="tbody-2"></tbody>
				                    </table>
		                          </div>
		                        </div>
		                    </div>
                          </form>
                        </div>
                      </div>
                      <!-- /.modal-content --> 
                    </div>
                    <!-- /.modal-dialog --> 
                  </div>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12">
              <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> 
                      <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> 
                      </span> Listado de entidades
                    </h3>
                  </div>
                  <div class="panel-body  table-responsive bordergray1">
                    <table class="table hover data-tables">
                      <thead>
                        <tr>
                          <th class="pointer">NIT <i class="fa fa-sort"></i></th>
                          <th>Raz&oacute;n social <i class="fa fa-sort"></i></th>
                          <th>C&oacute;digo <i class="fa fa-sort"></i></th>
                          <th>Nombre <i class="fa fa-sort"></i></th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody class="tbody">
                        
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
          </div>
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
  <!-- .container --> 
<!-- .content -->
  
  
  
<!-- Footer Start -->


<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqprint.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/custom/entities.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-validate.js"></script>
<!--[if lt IE 9]>
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script> 
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/caroufredsel.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jlinq.js"></script> 

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/fullcalendar.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/lang/es.js'></script>

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/moment.min.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/daterangepicker.js'></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/prettyphoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/theme.js"></script>
 


<!-- Specific Page Scripts Put Here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/google-code-prettify/prettify.js"></script>
<script type="text/javascript">
$(document).ready(function() {  
    "use strict";
        var form_register_2 = $('#frmreg');
        var error_register_2 = $('.alert-danger', form_register_2);
        var success_register_2 = $('.alert-success', form_register_2);
        
        var msg_required = "Este campo es requerido";
        var msg_minlength = "Necesitamos por lo menos {0} caracteres";
        var msg_maxlength = "Necesitamos m&aacute;ximo {0} caracteres";;
        var msg_number = "Debes ingresar un n&uacute;mero";
		var msg_nospace = "No se admiten espacios";
		var msg_email = "Ej: example@example.com";
		var msg_remote_email = "Este email ya se encuentra registrado";
		var msg_remote_document = "Este documento ya se encuentra registrado";
        
        form_register_2.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
            	nit:{
            		required:true,
            		minlength:7,
            		number:true,
            	},
                rs: {
                    required: true,
                    minlength: 3
                },
                cod: {
                	required: true,
                    minlength: 3
                },
                name: {
                    required: true,
                    minlength: 3
                },
            },
            messages: {
            	nit: {
                    required: msg_required,
                    number: msg_number,
                    minlength: msg_minlength,
                },
                rs: {
                    required: msg_required,
                    minlength: msg_minlength
                },
                cod: {
                	required: msg_required,
                    minlength: msg_minlength
                },
                name: {
                    required: msg_required,
                    minlength: msg_minlength
                },
           },
           
           submitHandler: function() {
        	   savep();
           }
        });
  
});
</script>
</body>
</html>