  <%@include file="header.jsp" %>
    <!-- Middle Content Start -->
        
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <!-- vd_head-section -->
          <!-- vd_title-section -->
          
          <div class="vd_content-section clearfix" style="min-height: 660px;">
          
          <div class="row">
          <div class="col-md-6 col-sm-6 col-xs-4">
		  		 <button class="btn btn-mommy vd_btn" data-toggle="modal" data-target="#myModal"> NUEVO PROFESIONAL</button>
		  	</div>
		  	<div class="col-md-6 col-sm-6 col-xs-8">
                  <div class="col-md-12 col-sm-12 col-lg-12">
                    <div class="controls">
                      <input type="text" placeholder="Busqueda de profesionales" class="search_text">
                    </div>
                  </div>
		  	</div>
            <div class="col-xs-12">
             
              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header btn-active">
                          <button type="button" class="close clear" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user-plus"></i> Registrar nuevo profesional</h4>
                        </div>
                        <div class="modal-body"> 
                          <form  id="frmreg" class="form-horizontal">
                            <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">Tipo Documento</label>
		                          <div class="controls">
		                            <select class="td">
		                            	<c:forEach var="typedocs" items="${data.typedocs}">
		                            		<option value="${typedocs.pktypedocument}"><c:out value="${fn:toUpperCase(typedocs.typedocumentName)}"/></option>
		                            	</c:forEach>
		                            </select>
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">N&uacute;mero Documento</label>
		                          <div class="controls">
		                            <input type="text" class="nd" name="nd" autofocus placeholder="Numero documento">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">Primer Nombre</label>
		                          <div class="controls">
		                            <input type="text" class="n1" name="n1" placeholder="Primer Nombre">
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">Segundo Nombre</label>
		                          <div class="controls">
		                            <input type="text" class="n2" name="n2" placeholder="Segundo Nombre">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">Primer Apellido</label>
		                          <div class="controls">
		                            <input type="text" class="l1" name="a1" placeholder="Primer Apellido">
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">Segundo Apellido</label>
		                          <div class="controls">
		                            <input type="text" class="l2" name="a2" placeholder="Segundo Apellido">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">G&eacute;nero</label>
		                          <div class="controls">
		                            <select class="g">
		                            	<option value="M">MASCULINO</option>
		                            	<option value="F">FEMENINO</option>
		                            </select>
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">Especialidad</label>
		                          <div class="controls">
		                            <select class="tu">
		                            	<c:forEach var="special" items="${data.specialtys}">
		                            		<option value="${special.pkspecialty}"><c:out value="${fn:toUpperCase(special.specialtyName)}"/></option>
		                            	</c:forEach>
		                            </select>
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                         <div class="col-md-6">
		                          <label class="control-label">F. Nacimiento</label>
		                          <div class="controls">
		                            <input type="date" class="fn" name="fn">
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">Barrio</label>
		                          <div class="controls">
		                            <input type="text" class="d" name="d" placeholder="Barrio">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-12">
		                          <label class="control-label">Direcci&oacute;n</label>
		                          <div class="controls">
		                            <input type="text" class="add" name="add" placeholder="Direcci&oacute;n">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">Zona</label>
		                          <div class="controls">
		                            <input type="text" class="z" name="z" placeholder="Zona">
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">Email</label>
		                          <div class="controls">
		                            <input type="email" class="e" name="e" placeholder="Email">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">T&eacute;lefono 1</label>
		                          <div class="controls">
		                            <input type="text" class="p1" name="p1" placeholder="T&eacute;lefono 1">
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">T&eacute;lefono 2</label>
		                          <div class="controls">
		                            <input type="text" class="p2" name="p2" placeholder="T&eacute;lefono 2">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                    	<div class="col-md-6">
		                          <label class="control-label">N&uacute;mero Cuenta</label>
		                          <div class="controls">
		                            <input type="text" class="nc" autofocus placeholder="Numero cuenta">
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">Tipo Cuenta</label>
		                          <div class="controls">
		                            <select class="tc">
		                            	<option value="AHORRO">AHORRO</option>
		                            	<option value=CORRIENTE">CORRIENTE</option>
		                            </select>
		                          </div>
		                        </div>
		                    </div>
		                     <div class="row mb-5">
		                    	<div class="col-md-6">
		                          <label class="control-label">C&oacute;digo Banco</label>
		                          <div class="controls">
		                            <input type="text" class="cb" autofocus placeholder="C&oacute;digo Banco">
		                          </div>
		                        </div>
		                    </div>
                            <div class="form-group">
                              <button class="btn pull-right btn vd_btn btn-mommy mr20 mt30">Guardar</button>
                              <button type="button" class="clear pull-right btn vd_btn vd_bg-info mr5 mt30" data-dismiss="modal">Cancelar</button>
                            </div>
                          </form>
                        </div>
                      </div>
                      <!-- /.modal-content --> 
                    </div>
                    <!-- /.modal-dialog --> 
                  </div>
                  
                  <div class="modal fade" id="modal-edit-p" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header btn-active">
                          <button type="button" class="close clear" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user-plus"></i> Editar profesional</h4>
                        </div>
                        <div class="modal-body"> 
                          <form  id="frmreg" class="form-horizontal">
                            <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">Tipo Documento</label>
		                          <div class="controls">
		                            <select class="td1" disabled>
		                            	<c:forEach var="typedocs" items="${data.typedocs}">
		                            		<option value="${typedocs.pktypedocument}"><c:out value="${fn:toUpperCase(typedocs.typedocumentName)}"/></option>
		                            	</c:forEach>
		                            </select>
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">N&uacute;mero Documento</label>
		                          <div class="controls">
		                            <input type="text" class="nd1" disabled autofocus placeholder="Numero documento">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">Primer Nombre</label>
		                          <div class="controls">
		                            <input type="text" class="n11" placeholder="Primer Nombre">
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">Segundo Nombre</label>
		                          <div class="controls">
		                            <input type="text" class="n21" placeholder="Segundo Nombre">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">Primer Apellido</label>
		                          <div class="controls">
		                            <input type="text" class="l11" placeholder="Primer Apellido">
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">Segundo Apellido</label>
		                          <div class="controls">
		                            <input type="text" class="l21" placeholder="Segundo Apellido">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">G&eacute;nero</label>
		                          <div class="controls">
		                            <select class="g1">
		                            	<option value="M">MASCULINO</option>
		                            	<option value="F">FEMENINO</option>
		                            </select>
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">Especialidad</label>
		                          <div class="controls">
		                            <select class="tu1">
		                            	<c:forEach var="special" items="${data.specialtys}">
		                            		<option value="${special.pkspecialty}"><c:out value="${fn:toUpperCase(special.specialtyName)}"/></option>
		                            	</c:forEach>
		                            </select>
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                         <div class="col-md-6">
		                          <label class="control-label">F. Nacimiento</label>
		                          <div class="controls">
		                            <input type="date" class="fn1">
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">Barrio</label>
		                          <div class="controls">
		                            <input type="text" class="d1" placeholder="Barrio">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-12">
		                          <label class="control-label">Direcci&oacute;n</label>
		                          <div class="controls">
		                            <input type="text" class="add1" placeholder="Direcci&oacute;n">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">Zona</label>
		                          <div class="controls">
		                            <input type="text" class="z1" placeholder="Zona">
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">Email</label>
		                          <div class="controls">
		                            <input type="email" class="e1" placeholder="Email">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">T&eacute;lefono 1</label>
		                          <div class="controls">
		                            <input type="text" class="p11" placeholder="T&eacute;lefono 1">
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">T&eacute;lefono 2</label>
		                          <div class="controls">
		                            <input type="text" class="p21" placeholder="T&eacute;lefono 2">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                    	<div class="col-md-6">
		                          <label class="control-label">N&uacute;mero Cuenta</label>
		                          <div class="controls">
		                            <input type="text" class="nc1" autofocus placeholder="Numero cuenta">
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">Tipo Cuenta</label>
		                          <div class="controls">
		                            <select class="tc1">
		                            	<option value="AHORRO">AHORRO</option>
		                            	<option value=CORRIENTE">CORRIENTE</option>
		                            </select>
		                          </div>
		                        </div>
		                    </div>
		                     <div class="row mb-5">
		                    	<div class="col-md-6">
		                          <label class="control-label">C&oacute;digo Banco</label>
		                          <div class="controls">
		                            <input type="text" class="cb1" autofocus placeholder="C&oacute;digo Banco">
		                          </div>
		                        </div>
		                    </div>
                            <div class="form-group">
                              <a class="btn pull-right btn vd_btn btn-mommy mr20 mt30 update-p">Guardar</a>
                              <button type="button" class="clear pull-right btn vd_btn vd_bg-info mr5 mt30" data-dismiss="modal">Cancelar</button>
                            </div>
                          </form>
                        </div>
                      </div>
                      <!-- /.modal-content --> 
                    </div>
                    <!-- /.modal-dialog --> 
                  </div>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-12">
              <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> 
                      <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> 
                      </span> Resultados de la busqueda
                    </h3>
                  </div>
                  <div class="panel-body  table-responsive bordergray1">
                    <table class="table pointer hover data-tables">
                      <thead>
                        <tr>
                          <th>Tipo Documento <i class="fa fa-sort"></th>
                          <th>Numero documento <i class="fa fa-sort"></th>
                          <th>Nombre Completo <i class="fa fa-sort"></th>
                          <th>Especialidad <i class="fa fa-sort"></th>
                          <th>Detalle</th>
                        </tr>
                      </thead>
                      <tbody class="tbody">
                        
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
          </div>
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>

<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqprint.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/custom/professionals.js"></script>
<!--[if lt IE 9]>
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script> 
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/caroufredsel.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jlinq.js"></script> 

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/fullcalendar.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/lang/es.js'></script>

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/moment.min.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/daterangepicker.js'></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/prettyphoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/theme.js"></script>
 


<!-- Specific Page Scripts Put Here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/google-code-prettify/prettify.js"></script>
<script type="text/javascript">
$(document).ready(function() {  
    "use strict";
        var form_register_2 = $('#frmreg');
        var error_register_2 = $('.alert-danger', form_register_2);
        var success_register_2 = $('.alert-success', form_register_2);
        
        var msg_required = "Este campo es requerido";
        var msg_minlength = "Necesitamos por lo menos {0} caracteres";
        var msg_maxlength = "Necesitamos m&aacute;ximo {0} caracteres";;
        var msg_number = "Debes ingresar un n&uacute;mero";
		var msg_nospace = "No se admiten espacios";
		var msg_email = "Ej: example@example.com";
		var msg_remote_email = "Este email ya se encuentra registrado";
		var msg_remote_document = "Este documento ya se encuentra registrado";
        
        form_register_2.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
            	nd:{
            		required:true,
            		minlength:5,
            		number:true,
            		remote: {
                        url: "checkDocument",
                        type: "post"
                    }
            	},
                n1: {
                    required: true,
                    minlength: 3
                },
                n2: {
                    minlength: 3
                },
                a1: {
                    required: true,
                    minlength: 3
                },
                d: {
                    required: true,
                    minlength: 3
                },
                a2: {
                    minlength: 3
                },
                e: {
                    required: true,
                    email: true,
                    remote: {
                        url: "checkEmail",
                        type: "post"
                    }
                }, 
                tu: {
                	required: true,
                	number:true,
                    minlength: 1,
                    maxlength: 1
                },
                fn: {
                	required: true,
                },
                add: {
                    required: true,
                    minlength: 6
                },
                z: {
                    required: true,
                    minlength: 3
                },
                p1: {
                    required: true,
                    number: true,
                    minlength: 7
                },
        
            },
            messages: {
            	nd: {
                    required: msg_required,
                    number: msg_number,
                    minlength: msg_minlength,
                    remote: msg_remote_document
                },
                n1: {
                    required: msg_required,
                    minlength: msg_minlength
                },
                n2: {
                    minlength: msg_minlength
                },
                a1: {
                    required: msg_required,
                    minlength: msg_minlength
                },
                a2: {
                    minlength: msg_minlength
                },
                tu: {
                    required: msg_required,
                    number: msg_number,
                    minlength: msg_minlength,
                    maxlength: msg_maxlength,
                },
                e: {
                    required: msg_required,
                    email: msg_email,
                    remote: msg_remote_email
                }, 
                fn: {
                    required: msg_required,
                },
                add: {
                    required: msg_required,
                    minlength: msg_minlength
                },
                z: {
                    required: msg_required,
                    minlength: msg_minlength
                },
                d: {
                    required: msg_required,
                    minlength: msg_minlength
                },
                p1: {
                    required: msg_required,
                    number: msg_number,
                    minlength: msg_minlength
                },
           },
           
           submitHandler: function() {
        	   savep();
           }
        });
  
});
</script>

</body>
</html>