  <%@include file="header.jsp" %>
    <!-- Middle Content Start -->
    <style>
    	select {
		    padding: 9px;
		}
		.chosen-container-single .chosen-single{
		    padding: 15px;
		    padding-top: 3px;
		    padding-bottom: 27px;
		}
		.prev_comp{
			/*width: 24%;*/
		}
    </style>
        
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <!-- vd_head-section -->
          <!-- vd_title-section -->
          
          <div class="vd_content-section clearfix" style="min-height: 660px;">

          <div class="row">
            <div class="col-xs-12">
              <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> 
                      <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> 
                      </span> ENTREGA DE COPAGOS
                    </h3>
                  </div>
                  <div class="panel-body bordergray1">
                    <div class="col-xs-12">
		              <div class="panel widget light-widget mb-10">
		                  <div class="panel-body bordergray1">
		                  	<div class="row mb-5 mt-10 ml-10 mr-10">
		                  		<input type="hidden" class="pid">
		                        <div class="col-md-4 col-lg-4 col-sm-5">
		                          Profesional: 
		                          <div class="controls mt-5">
		                            <select class="professional chosen-select"  data-placeholder="Seleccione un profesional" style="width:350px;" tabindex="2">
		                            	<option value="null" selected>PROFESIONAL</option>
		                            	<c:forEach var="professional" items="${data.professionals}">
			                           		<option value="${professional.pkuser}">${fn:trim(professional.userName1)} ${professional.userName2} ${professional.userSurname1} ${professional.userSurname2} - ${professional.userDocument}</option>
			                           	</c:forEach>
		                            </select>
		                          </div>
			                    </div>
			                    <div class="col-md-3 col-lg-3 col-sm-3">
		                          Estado Atenci&oacute;n: 
		                          <div class="controls mt-5">
		                            <select class="estado">
		                            	<!-- <option value="*">TODOS</option>  -->
		                            	<option value="3">COMPLETADO</option>
		                            	<option value="2">PROGRAMADO</option>
		                            	<!-- <option value="4">CANCELADO</option>  -->
		                            </select>
		                          </div>
			                    </div>
			                    <div class="col-md-3 col-lg-3 col-sm-3">
		                          Estado Copagos: 
		                          <div class="controls mt-5">
		                            <select class="estado2">
		                            	<option value="*">TODOS</option> 
		                            	<option value="1">ENTREGADOS</option>
		                            	<option value="0">SIN ENTREGAR</option>
		                            </select>
		                          </div>
			                    </div>
			                    <div class="col-md-2 col-lg-2 col-sm-12 mt-28">
		                          <button class="btn btn-block btn-primary search has-spinner"> 
		                          		Buscar 
		                          		<span class="spinner"><i class="icon-spin icon-refresh" style="color:#FFFFFF"></i></span>
		                          </button>
			                    </div>
			                </div>
		                  </div>
		                </div>
		            </div>
		            <div class="row">
			            <div class="col-xs-12 ml-15 pr-45">
			              <div class="panel-body  table-responsive bordergray1">
		                    <table class="table hover data-tables">
		                      <thead>
		                        <tr>
		                          <th>Paciente</th>
		                          <th>Documento</th>
		                          <th>Tipo Servicio</th>
		                          <th>Nro. Auth</th>
		                          <th>Prog.</th>
		                          <th>Comp.</th>
		                          <th>Estado</th>
		                          <th>Fecha Fin.</th>
		                          <th>Entidad</th>
		                          <th>Fre. Copago</th>
		                          <th>Valor</th>
		                          <th>Cop Rep.</th>
		                          <th>Estado copago</th>
		                          <th style="width:150px !important;">Copagos Recibidos &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
		                          <th style="width:150px !important;">Copagos Entregados</th>
		                          <th style="width:150px !important;">Descuento</th>
		                        </tr>
		                      </thead>
		                      <tbody class="tbody"></tbody>
		                    </table>
		                  </div>
			            </div>
			            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 hidden div-prev" style="margin-top:10px !important">
			            	<div class="col-lg-9 col-md-8 col-sm-8 col-xs-12 warning_msg" style="visibility: hidden">
			            		<div class="alert alert-warning alert-dismissable alert-condensed">
			            			<strong class="warning_msg_text"></strong>
			            			<span class="vd_alert-icon"><i class="fa fa-exclamation-triangle "></i></span>
			            		</div>
			            	</div>
			            	<a class="btn btn-success col-lg-3 col-md-4 col-sm-4 col-xs-12 preview has-spinner">
			            		VISUALIZAR COMPROBANTE 
			            		<span class="spinner"><i class="icon-spin icon-refresh" style="color:#FFFFFF"></i></span>
			            	</a>
			            </div>
			        </div>
                  </div>
                </div>
            </div>
          </div>
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
    <!-- Inicia Modal -->
    <div class="modal fade"  id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
		<div class="modal-dialog modal_comprobante">
		  <div class="modal-content">
		    <div class="modal-header vd_bg-blue vd_white">
		      <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
		      <center><h4 class="modal-title" id="myModalLabel">Vista previa - comprobante de copagos recibidos</h4></center>
		    </div>
		    <div class="modal-body"> 
		    	<form class="form-horizontal">
		    	<div class="row">
		    		<div class="col-md-3 col-sm-3 hidden-xs">
		    			<img src="${pageContext.request.contextPath}/resources/img/logo.png" class="logo_comp">
		    		</div>
		    		<div class="col-md-6 col-sm-7 col-xs-12">
		    			<h5 align="center" class="t20"><b>COMPROBANTE DE ENTREGA DE COPAGOS Y EFECTIVO</b></h5>
		    		</div>
		    		<div class="col-md-3 col-sm-2 col-xs-12">
		    			<h5 class="t20 tar hidden-xs"><b>N� 0000000</b></h5>
		    			<h5 class="t20 visible-xs"><b>N� 0000000</b></h5>
		    		</div>
		    	</div><!-- row end -->
		    	<div class="row" style="margin-top:20px;">
		    		<div class="col-lg-5 col-md-3 col-sm-4 col-xs-12 ">
		    			<label class="pname"></label>
		    		</div>
		    		<div class="col-lg-3 col-md-4 col-sm-5 col-xs-12">
		    			<label class="lbldoc"></label>
		    		</div>
		    		<div class="col-lg-3 col-md-4 ">
		    			<label class="lbldate"></label>
		    		</div>
		    	</div><!-- row end -->
		    	<div class="row">
		    		<div class="col-lg-12">
		    		<div style=" overflow: auto; position: relative; " class="scrollbar" id="#div_tbl"> 
		    			<table class="table-bordered tbl">
		    				<thead>
		    					<tr>
		    						<th>NOMBRE PACIENTE</th>
		    						<th>ENTIDAD</th>
		    						<th>SERVICIO</th>
		    						<th>VALOR SERVICIO</th>
		    						<th>CANTIDAD</th>
		    						<th>EFEC. RECIBIDO</th>
		    						<th>EFEC. ENTREGADO</th>
		    						<th>DESCUENTO</th>
		    						<th>VALOR A PAGAR</th>
		    					</tr>
		    				</thead>
		    				<tbody class="tbodyprev">
		    					
		    				</tbody>
		    			</table><!-- table end -->
		    			</div>
		    			<div class="row" style="margin-top:2S0px;">
		    				<div class="col-md-6">
		    					<label class="lblrec"></label>
		    				</div>
		    			</div>
		    		</div>
		    	</div><!-- row end -->
		    	
		      </form>
		    </div>
		    <div class="modal-footer background-login">
		      <a type="button" class="btn btn-primary" data-dismiss="modal">EDITAR</a>
		      <a type="button" class="btn btn-success generate">GUARDAR Y GENERAR PDF</a>
		    </div>
		  </div>
		</div>
	</div>
	<!-- Finaliza Modal -->

<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqprint.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/custom/copago.js"></script>
<!--[if lt IE 9]>
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script> 
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/caroufredsel.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jlinq.js"></script> 

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/fullcalendar.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/lang/es.js'></script>

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/moment.min.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/daterangepicker.js'></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/prettyphoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/theme.js"></script>
 
<!-- Specific Page Scripts Put Here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/google-code-prettify/prettify.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/chosen.jquery.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/chosen2.jquery.js" type="text/javascript"></script>
<script type="text/javascript">
	$(".professional").chosen({
	    no_results_text: "",
	    width: "95%"
	});
</script>
<script>
var dt = $('.data-tables').dataTable({
    language: {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:  ",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "�ltimo",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    },
    "paging":false,
    "searching":true,
    "info":false,
    "scrollY": "300px",
    "scrollCollapse": false,
    "scrollX": true
});
</script>
</body>
</html>