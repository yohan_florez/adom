  <%@include file="header.jsp" %>
    <!-- Middle Content Start -->
    
    <style>
    	.dataTables_wrapper{
    		height: 280px;
    	}
    	.content-list .list-wrapper>li:hover{
    		background-color: transparent !important;
    		color: #333 !important;
    	}
    </style>
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <!-- vd_head-section -->
          <!-- vd_title-section -->
          
          <div class="vd_content-section clearfix" style="min-height: 860px;">
				<div class="row">
				<!-- col-md-4 -->
              
              <div class="col-md-4">
                <div class="panel widget light-widget">
                  <div class="panel-heading no-title"> </div>
                  <div class="panel-body">
                    <h3 class="mgtp--5 mgbt-xs-20"> NUEVO ANUNCIO </h3>
                    <form role="form" class="form-horizontal">    
						<div class="form-group">
			              <label class="col-sm-2 control-label">T&iacute;tulo</label>
			              <div class="col-sm-10 controls">
			                <input type="text" placeholder="T&iacute;tulo" class="title_new">
			              </div>
			            </div> 
			            <div class="form-group">
	                        <label class="col-sm-2 control-label">Imagen</label>
	                        <div class="col-sm-10 controls">
	                          <input type="file" class="file">
	                        </div>
	                    </div>
	                    <div class="form-group">
	                        <label class="col-sm-2 control-label"></label>
	                        <div class="col-sm-10 controls">
	                          <img class="img_load">
	                        </div>
	                    </div>
			             <div class="form-group">
			              	  <label class="col-sm-2 control-label">Texto</label>              
			                  <div class="col-sm-10 controls">                                    
			                         <textarea name="editor1" data-rel="ckeditor" rows="3" ></textarea>                                                
			                  </div>                                 
			            </div>
			            <div class="form-group">
			              <div class="col-sm-2"></div>
						  <div class="col-sm-10 controls">
			                  <a type="submit" class="btn btn-mommy vd_btn btn-block new_event">Crear</i></a>
			              </div>
			            </div>               
			        </form>
                  </div>
                </div>
                <!-- Panel Widget --> 
                
              </div>
              <!-- col-md-4 --> 
              <div class="col-md-8">
                <div class="panel widget light-widget panel-bd-top">
                  <div class="panel-heading no-title"> </div>
                  <div class="panel-body">
                    <h3 class="mgtp--5"> Anuncios</h3>
                    <div class="content-list content-blog-small">
                      <ul class="list-wrapper">
                        <c:forEach var="e" items="${data.adverts}">
	                        <li>
	                          <c:if test="${not empty e.advertImage}">
		                          <div class="menu-icon"> 
		                          	<img src="data:image/png;base64,${e.advertImage}" style="width:200px;"> 
		                          </div>
		                      </c:if>
	                          <div class="menu-text">
	                            <h2 class="blog-title font-bold letter-xs">
	                            <a> ${e.advertTitle}</a></h2>
	                            <div class="menu-info">
	                              <div class="menu-date font-xs">${e.advertDate}</div>
	                            </div>
	                            ${e.advertBody}
	                          </div>
	                          <div>
	                          	<a class="btn btn-primary delete-advert" id="${e.pkadvert}"> Borrar </a>
	                          </div>
	                        </li>
	                   </c:forEach>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
            </div>     
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>

<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqprint.js"></script>  
<!--[if lt IE 9]>
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script> 
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/caroufredsel.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/custom/adverts.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jlinq.js"></script> 

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/fullcalendar.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/lang/es.js'></script>

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/moment.min.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/daterangepicker.js'></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/prettyphoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/ckeditor/ckeditor.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/ckeditor/adapters/jquery.js'></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/theme.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/raphael/raphael.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/morris/morris.min.js"></script>
 


<!-- Specific Page Scripts Put Here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/google-code-prettify/prettify.js"></script>
<script type="text/javascript" >
	"use strict";
	$(window).load(function() 
	{
		"use strict";	
		$( '[data-rel^="ckeditor"]' ).ckeditor();
		var availableTags = [
			"ActionScript",
			"AppleScript",
			"Asp",
			"BASIC",
			"C",
			"C++",
			"Clojure",
			"COBOL",
			"ColdFusion",
			"Erlang",
			"Fortran",
			"Groovy",
			"Haskell",
			"Java",
			"JavaScript",
			"Lisp",
			"Perl",
			"PHP",
			"Python",
			"Ruby",
			"Scala",
			"Scheme"
		];
		$( "#input-autocomplete" ).autocomplete({
			source: availableTags
		});
		//CKEDITOR.instances['editor1'].setData('${fn:trim(data.email1content)}');
	});
</script>

</body>
</html>