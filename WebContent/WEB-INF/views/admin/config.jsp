  <%@include file="header.jsp" %>
    <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <!-- vd_head-section -->
          <!-- vd_title-section -->
          
          <div class="vd_content-section clearfix" style="min-height: 860px;">
          	<div class="row">
				<div class="col-md-9">
					<ul class="nav nav-tabs nav-justified">
	                   <li class="active"><a href="#tab1" data-toggle="tab">ADOM</a></li>
	                   <li><a href="#tab2" data-toggle="tab">EMAIL</a></li>
	                   <li><a href="#tab3" data-toggle="tab">general</a></li>
	                 </ul>
				</div> 
				<div class="col-md-10">
					<div class="tab-content">
	                      <div class="tab-pane active" id="tab1">
	                      	<form id="frmadom" class="form form-horizontal mt-30">
	                            <div class="row mb-5">
	                            	<div class="col-md-2"></div>
			                        <div class="col-md-6">
			                          <label class="control-label">NIT</label>
			                          <div class="controls">
			                            <input type="number" class="nit" autofocus placeholder="NIT" value="${data.adom.adomNit}">
			                          </div>
			                        </div>
			                    </div>
			                    <div class="row mb-5">
			                    	<div class="col-md-2"></div>
			                        <div class="col-md-6">
			                          <label class="control-label">Raz&oacute;n social</label>
			                          <div class="controls">
			                            <input type="text" class="name" placeholder="Raz&oacute;n social" value="${data.adom.adomName}">
			                          </div>
			                        </div>
			                    </div>
			                    <div class="row mb-5">
			                    	<div class="col-md-2"></div>
			                        <div class="col-md-4">
			                          <label class="control-label">C&oacute;digo del prestador</label>
			                          <div class="controls">
			                            <input type="number" value="${data.adom.adomCode}" class="cod_pre" placeholder="C&oacute;digo del prestador">
			                          </div>
			                        </div>
			                    </div>
			                    <div class="row mb-5">
			                    	<div class="col-md-2"></div>
			                        <div class="col-md-4">
			                          <label class="control-label">C&oacute;digo del departamento</label>
			                          <div class="controls">
			                            <input type="number" value="${data.adom.adomDepartament}" class="cod_dpto" placeholder="C&oacute;digo del departamento">
			                          </div>
			                        </div>
			                    </div>
			                    <div class="row mb-5">
			                    	<div class="col-md-2"></div>
			                        <div class="col-md-4">
			                          <label class="control-label">C&oacute;digo del municipio</label>
			                          <div class="controls">
			                            <input type="number" value="${data.adom.adomTown}" class="cod_mun" placeholder="C&oacute;digo del municipio">
			                          </div>
			                        </div>
			                    </div>
			                    <div class="row mb-5">
			                    	<div class="col-md-2"></div>
			                        <div class="col-md-4">
			                          <label class="control-label">Zona residencial</label>
			                          <div class="controls">
			                            <input type="text" value="${data.adom.adomZone}" class="zone" placeholder="Zona residencial">
			                          </div>
			                        </div>
			                    </div>
			                    <div class="row mb-5 div-msg" style="display:none;">
			                    	<div class="col-md-2"></div>
			                    	<div class="col-md-6">
			                           <center><label style="color:green;" class="msg-adom">Los cambios se han guardado</label></center>
			                        </div>
		                        </div>
			                    <div class="row mb-5">
			                    	<div class="col-md-2"></div>
			                    	<div class="col-md-6">
			                            <div class="form-group">
			                              <a class="btn  btn vd_btn btn-mommy btn-block mt30 update-adom">Guardar</a>
			                            </div>
			                        </div>
		                        </div>
	                          </form>
	                      </div>
	                      <div class="tab-pane" id="tab2">
	                      	<div class="row">
		                      <div class="col-sm-3">
		                        <ul class="nav nav-pills nav-stacked mt-30">
		                          <li><a href="#tabe1" data-toggle="tab" class="btn btn-mommy vd_btn">Nuevo usuario</a></li>
		                          <li><a href="#tabe2" data-toggle="tab" class="btn btn-mommy vd_btn">Nueva contraseņa</a></li>
		                          <li><a href="#tabe3" data-toggle="tab" class="btn btn-mommy vd_btn">Asignar Servicios</a></li>
		                          <li><a href="#tabe4" data-toggle="tab" class="btn btn-mommy vd_btn">Cancelar Visitas</a></li>
		                          <li><a href="#tabe5" data-toggle="tab" class="btn btn-mommy vd_btn">Asignar Visitas</a></li>
		                          <li><a href="#tabe6" data-toggle="tab" class="btn btn-mommy vd_btn">Remover Visitas</a></li>
		                        </ul>
		                      </div>                      
		                      <div class="col-sm-9">
		                        <div class="tab-content  mgbt-xs-20">
		                          <div class="tab-pane" id="tabe1">
		                          	<form role="form" class="form-horizontal mt-30"> 
							             <div class="form-group">
							                  <div class="col-sm-10 controls">                                    
							                       <textarea name="editor1" data-rel="ckeditor" rows="5">
							                       	${data.email1content}
							                       </textarea>                                                
							                  </div>                                 
							           	 </div>
							           	 <div class="form-group">
							                  <div class="col-sm-10 controls">                                    
							                       <a class="btn btn-mommy btn-block vd_btn save-tmp1">Guardar</a> 
							                       <label class="col-sm-0 control-label msg1" style="display:none;">Cambios guardados</label>                                              
							                  </div>                                 
							           	 </div>
							         </form>
		                          </div>
		                          <div class="tab-pane" id="tabe2">
		                          	<form role="form" class="form-horizontal mt-30"> 
							             <div class="form-group">
							                  <div class="col-sm-10 controls">                                    
							                       <textarea name="editor2" data-rel="ckeditor" rows="5">
							                       	${data.email2content}
							                       </textarea>                                                
							                  </div>                                 
							           	 </div>
							           	 <div class="form-group">
							                  <div class="col-sm-10 controls">                                    
							                       <a class="btn btn-mommy btn-block vd_btn save-tmp2">Guardar</a> 
							                       <label class="col-sm-0 control-label msg1" style="display:none;">Cambios guardados</label>                                              
							                  </div>                                 
							           	 </div>
							         </form>
		                          </div>
		                          <div class="tab-pane" id="tabe3">
		                          	<form role="form" class="form-horizontal mt-30"> 
							             <div class="form-group">
							                  <div class="col-sm-10 controls">                                    
							                       <textarea name="editor3" data-rel="ckeditor" rows="5">
							                       	${data.email3content}
							                       </textarea>                                                
							                  </div>                                 
							           	 </div>
							           	 <div class="form-group">
							                  <div class="col-sm-10 controls">                                    
							                       <a class="btn btn-mommy btn-block vd_btn save-tmp3">Guardar</a> 
							                       <label class="col-sm-0 control-label msg1" style="display:none;">Cambios guardados</label>                                              
							                  </div>                                 
							           	 </div>
							         </form>
		                          </div>
		                          <div class="tab-pane" id="tabe4">
		                          	<form role="form" class="form-horizontal mt-30"> 
							             <div class="form-group">
							                  <div class="col-sm-10 controls">                                    
							                       <textarea name="editor4" data-rel="ckeditor" rows="5">
							                       	${data.email4content}
							                       </textarea>                                                
							                  </div>                                 
							           	 </div>
							           	 <div class="form-group">
							                  <div class="col-sm-10 controls">                                    
							                       <a class="btn btn-mommy btn-block vd_btn save-tmp4">Guardar</a> 
							                       <label class="col-sm-0 control-label msg1" style="display:none;">Cambios guardados</label>                                              
							                  </div>                                 
							           	 </div>
							         </form>
		                          </div>
		                           <div class="tab-pane" id="tabe5">
		                          	<form role="form" class="form-horizontal mt-30"> 
							             <div class="form-group">
							                  <div class="col-sm-10 controls">                                    
							                       <textarea name="editor5" data-rel="ckeditor" rows="5">
							                       	${data.email5content}
							                       </textarea>                                                
							                  </div>                                 
							           	 </div>
							           	 <div class="form-group">
							                  <div class="col-sm-10 controls">                                    
							                       <a class="btn btn-mommy btn-block vd_btn save-tmp5">Guardar</a> 
							                       <label class="col-sm-0 control-label msg1" style="display:none;">Cambios guardados</label>                                              
							                  </div>                                 
							           	 </div>
							         </form>
		                          </div>
		                          <div class="tab-pane" id="tabe6">
		                          	<form role="form" class="form-horizontal mt-30"> 
							             <div class="form-group">
							                  <div class="col-sm-10 controls">                                    
							                       <textarea name="editor6" data-rel="ckeditor" rows="5">
							                       	${data.email6content}
							                       </textarea>                                                
							                  </div>                                 
							           	 </div>
							           	 <div class="form-group">
							                  <div class="col-sm-10 controls">                                    
							                       <a class="btn btn-mommy btn-block vd_btn save-tmp6">Guardar</a> 
							                       <label class="col-sm-0 control-label msg1" style="display:none;">Cambios guardados</label>                                              
							                  </div>                                 
							           	 </div>
							         </form>
		                          </div>
		                        </div>
		                      </div>
		                    </div>
	                      </div>
	                      <div class="tab-pane" id="tab3">
	                      	<div class="row">
		                      <div class="col-sm-3">
		                        <ul class="nav nav-pills nav-stacked mt-30">
		                          <li><a href="#tabe12" data-toggle="tab" class="btn btn-mommy vd_btn">Administrador</a></li>
		                          <li><a href="#tabe11" data-toggle="tab" class="btn btn-mommy vd_btn">Copagos</a></li>
		                        </ul>
		                      </div>
		                      <div class="col-sm-9">
		                        <div class="tab-content">
		                          <div class="tab-pane" id="tabe11">
		                          	<form id="frmadom" class="form form-horizontal mt-30">
			                            <div class="row mb-5">
					                        <div class="col-md-3">
					                          <label class="control-label">Limite copago</label>
					                          <div class="controls">
					                            <input type="number" class="limit" autofocus placeholder="Limite" value="${data.adom.adomLimit}">
					                          </div>
					                        </div>
					                    </div>
					                    <div class="row mb-5">
					                        <div class="col-md-2">
					                          <label class="control-label">D&iacute;as</label>
					                          <div class="controls">
					                            <input type="number" class="limitday" placeholder="D&iacute;as" value="${data.adom.adomLimitdays}">
					                          </div>
					                        </div>
					                    </div>
					                    <div class="row mb-5 div-msg-limits" style="display:none;">
					                    	<div class="col-md-6">
					                           <label style="color:green;" class="msg-adom">Los cambios se han guardado</label>
					                        </div>
				                        </div>
					                    <div class="row mb-5">
					                    	<div class="col-md-3">
					                            <div class="form-group">
					                              <a class="btn  btn vd_btn btn-mommy btn-block mt30 update-limits">Guardar</a>
					                            </div>
					                        </div>
				                        </div>
					                </form>
		                          </div>
		                          <div class="tab-pane" id="tabe12">
		                          	<form name="frm_cp" class="form form-horizontal mt-30">
			                            <div class="row mb-5">
					                        <div class="col-md-4">
					                          <label class="control-label">Nueva contrase&ntilde;a</label>
					                          <div class="controls">
					                            <input type="password" name="pwd1" class="p1" autofocus placeholder="Ingrese contraseņa" >
					                          </div>
					                        </div>
					                    </div>
					                    <div class="row mb-5">
					                        <div class="col-md-4">
					                          <label class="control-label">Confirmar contrase&ntilde;a</label>
					                          <div class="controls">
					                            <input type="password" name="pwd2" class="p2" placeholder="Confirme la contraseņa">
					                          </div>
					                        </div>
					                    </div>
					                    <div class="row mb-5">
					                    	<div class="col-md-4">
					                            <div class="form-group">
					                              <a class="btn  btn vd_btn btn-mommy btn-block mt30 change-pass">Cambiar contraseņa</a>
					                            </div>
					                        </div>
				                        </div>
				                        <div class="row mb-5 div-msg" style="display:none">
					                    	<div class="col-md-6">
					                           <div class="type-alert alert alert-dismissable alert-condensed">
							                        <i class="fa fa-exclamation-circle append-icon"></i>
							                        <a class="msg"></a>
							                   </div>
					                        </div>
				                        </div>
					                </form>
		                          </div>
		                         </div>
		                       </div>
		                    </div>
	                      </div>
	                 </div>
				</div> 
			</div>
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>

  <!-- .container --> 
<!-- .content -->
  
  
  
<!-- Footer Start -->
  
<!-- Head menu search form ends -->  

<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqprint.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/custom/config.js"></script>  
<!--[if lt IE 9]>
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script> 
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/caroufredsel.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jlinq.js"></script> 
<!-- CK Editor  -->
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/ckeditor/ckeditor.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/ckeditor/adapters/jquery.js'></script>

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/fullcalendar.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/lang/es.js'></script>

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/moment.min.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/daterangepicker.js'></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/prettyphoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/theme.js"></script>
 


<!-- Specific Page Scripts Put Here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/google-code-prettify/prettify.js"></script>
<script type="text/javascript" >
	"use strict";
	$(window).load(function() 
	{
		"use strict";	
		$( '[data-rel^="ckeditor"]' ).ckeditor();
		var availableTags = [
			"ActionScript",
			"AppleScript",
			"Asp",
			"BASIC",
			"C",
			"C++",
			"Clojure",
			"COBOL",
			"ColdFusion",
			"Erlang",
			"Fortran",
			"Groovy",
			"Haskell",
			"Java",
			"JavaScript",
			"Lisp",
			"Perl",
			"PHP",
			"Python",
			"Ruby",
			"Scala",
			"Scheme"
		];
		$( "#input-autocomplete" ).autocomplete({
			source: availableTags
		});
		//CKEDITOR.instances['editor1'].setData('${fn:trim(data.email1content)}');
	});
</script>

</body>
</html>