  <%@include file="header.jsp" %>
    <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <!-- vd_head-section -->
          <!-- vd_title-section -->
          
          <div class="vd_content-section clearfix" style="min-height: 860px;">
          	<div class="row">
				<div class="col-md-12">
					<div class="tab-content">
	                      <div class="tab-pane active" id="tab2">
	                      	<div class="row">
		                      <div class="col-sm-3">
		                        <ul class="nav nav-pills nav-stacked mt-30">
		                          <li class="active"><a href="#tabe1" data-toggle="tab" class="btn btn-mommy vd_btn">Reporte terapias</a></li>
		                          <li><a href="#tabe2" data-toggle="tab" class="btn btn-mommy vd_btn">Reporte pagos</a></li>
		                          <li><a href="#tabe3" data-toggle="tab" class="btn btn-mommy vd_btn">Reporte facturaci&oacute;n</a></li>
		                          <li><a href="#tabe7" data-toggle="tab" class="btn btn-mommy vd_btn">Reporte copagos</a></li>
		                          <li>
		                          	<a href="#tabe4" data-toggle="tab" class="btn btn-mommy vd_btn generate4 has-spinner">
		                          		Reporte profesionales
		                          		<span class="spinner"><i class="icon-spin icon-refresh" style="color:#1053BF"></i></span>
		                          	</a>
		                          </li>
		                          <li><a href="#tabe5" data-toggle="tab" class="btn btn-mommy vd_btn">Reporte pacientes</a></li>
		                          <li>
		                          	<a href="#tabe6" data-toggle="tab" class="btn btn-mommy vd_btn generate6 has-spinner">
		                          		Reporte General
		                          		<span class="spinner"><i class="icon-spin icon-refresh" style="color:#1053BF"></i></span>
		                          	</a>
		                          </li>
		                          <li><a href="#tabe8" data-toggle="tab" class="btn btn-mommy vd_btn">Reporte especial</a></li>
		                        </ul>
		                      </div>                      
		                      <div class="col-sm-9">
		                        <div class="tab-content  mgbt-xs-20">
		                          <div class="tab-pane pedit active" id="tabe1">
		                          	<div class="col-xs-12">
						              <div class="panel widget light-widget mb-10">
						                  <div class="panel-heading">
						                    <h3 class="panel-title"> 
						                      GENERAR REPORTE DE TERAPIAS
						                    </h3>
						                  </div>
						                  <div class="panel-body bordergray1">
						                  	<div class="row mb-5 mt-10 ml-10 mr-10">
						                  		<div class="col-md-3 col-sm-3 col-lg-3 col-xs-6">
						                          Fecha atenci&oacute;n: 
						                          <div class="controls mt-5">
						                            <input type="text" class="start range1">
						                          </div>
							                    </div>
						                        <div class="col-md-3 col-sm-3 col-lg-3 col-xs-6">
						                          Entidad: 
						                          <div class="controls mt-5">
						                            <select class="entitylist">
						                            	<option value="*">TODAS</option>
						                            	<c:forEach var="e" items="${data.entities}">
						                            		<option value="${e.pkentity}"><c:out value="${fn:toUpperCase(e.entityName)}"/></option>
						                            	</c:forEach>
						                            </select>
						                          </div>
							                    </div>
							                    <div class="col-md-3 col-sm-3 col-lg-3 col-xs-6">
						                          Plan: 
						                          <div class="controls mt-5">
						                            <select class="entityplans">
						                            	<option value="*">TODOS</option>
						                            </select>
						                          </div>
							                    </div>
							                    <div class="col-md-3 col-sm-3 col-lg-3 col-xs-6 mt-28">
							                      <button class="btn btn-block btn-primary generate has-spinner"> 
						                          		Generar 
						                          		<span class="spinner"><i class="icon-spin icon-refresh" style="color:#FFFFFF"></i></span>
						                          </button>
							                    </div>
							                </div>
						                  </div>
						                </div>
						            </div>
		                          </div>
		                          <div class="tab-pane pedit" id="tabe2">
		                          	<div class="col-xs-12">
						              <div class="panel widget light-widget mb-10">
						                  <div class="panel-heading">
						                    <h3 class="panel-title"> 
						                      GENERAR REPORTE DE PAGOS
						                    </h3>
						                  </div>
						                  <div class="panel-body bordergray1">
						                  	<div class="row mb-5 mt-10 ml-10 mr-10">
						                  		<div class="col-md-3 col-sm-3 col-lg-3 col-xs-6">
						                          Fecha atenci&oacute;n: 
						                          <div class="controls mt-5">
						                            <input type="text" class="start range1">
						                          </div>
							                    </div>
						                        <div class="col-md-3 col-sm-3 col-lg-3 col-xs-6">
						                          Entidad: 
						                          <div class="controls mt-5">
						                            <select class="entitylist2">
						                            	<option value="*">TODAS</option>
						                            	<c:forEach var="e" items="${data.entities}">
						                            		<option value="${e.pkentity}"><c:out value="${fn:toUpperCase(e.entityName)}"/></option>
						                            	</c:forEach>
						                            </select>
						                          </div>
							                    </div>
							                    <div class="col-md-3 col-sm-3 col-lg-3 col-xs-6">
						                          Plan: 
						                          <div class="controls mt-5">
						                            <select class="entityplans2">
						                            	<option value="*">TODOS</option>
						                            </select>
						                          </div>
							                    </div>
							                    <div class="col-md-3 col-sm-3 col-lg-3 col-xs-6">
						                          Servicio: 
						                          <div class="controls mt-5">
						                            <select class="type2">
						                            	<option value="*">TODOS</option>
						                            	<option value="2">TERAPIAS</option>
						                            	<option value="1">ENFERMERIA</option>
						                            </select>
						                          </div>
							                    </div>
							                    <div class="col-offset-md-3 col-offset-sm-3 col-offset-lg-3 col-xs-6 mt-28">
						                          <button class="btn btn-block btn-primary generate2 has-spinner"> 
						                          		Generar 
						                          		<span class="spinner"><i class="icon-spin icon-refresh" style="color:#FFFFFF"></i></span>
						                          </button>
							                    </div>
							                </div>
						                  </div>
						                </div>
						            </div>
		                          </div>
		                          <div class="tab-pane pedit" id="tabe3">
		                          	<div class="col-xs-12">
						              <div class="panel widget light-widget mb-10">
						                  <div class="panel-heading">
						                    <h3 class="panel-title"> 
						                      GENERAR REPORTE FACTURACI&Oacute;N Y ESTADO
						                    </h3>
						                  </div>
						                  <div class="panel-body bordergray1">
						                  	<div class="row mb-5 mt-10 ml-10 mr-10">
						                        <div class="col-md-4 col-sm-4 col-lg-4 col-xs-6">
						                          Entidad: 
						                          <div class="controls mt-5">
						                            <select class="entitylist3">
						                            	<option value="*">TODAS</option>
						                            	<c:forEach var="e" items="${data.entities}">
						                            		<option value="${e.pkentity}"><c:out value="${fn:toUpperCase(e.entityName)}"/></option>
						                            	</c:forEach>
						                            </select>
						                          </div>
							                    </div>
							                    <div class="col-md-4 col-sm-4 col-lg-4 col-xs-6">
						                          Plan: 
						                          <div class="controls mt-5">
						                            <select class="entityplans3">
						                            	<option value="*">TODOS</option>
						                            </select>
						                          </div>
							                    </div>
							                    <div class="col-md-4 col-sm-4 col-lg-4 col-xs-6">
						                          Servicio: 
						                          <div class="controls mt-5">
						                            <select class="type3">
						                            	<option value="*">TODOS</option>
						                            	<option value="2">TERAPIAS</option>
						                            	<option value="1">ENFERMERIA</option>
						                            </select>
						                          </div>
							                    </div>
							                    <div class="col-md-4 col-sm-4 col-lg-4 col-xs-6">
						                          Fecha Inicio: 
						                          <div class="controls mt-5">
						                            <input type="text" class="start range1">
						                          </div>
							                    </div>
							                    <div class="col-md-4 col-sm-4 col-lg-4 col-xs-6">
						                          Fecha Solicitud: 
						                          <div class="controls mt-5">
						                            <input type="text" class="start range2">
						                          </div>
							                    </div>
							                    <div class="col-md-4 col-sm-4 col-lg-4 col-xs-6 mt-28">
							                      <button class="btn btn-block btn-primary generate3 has-spinner"> 
						                          		Generar 
						                          		<span class="spinner"><i class="icon-spin icon-refresh" style="color:#FFFFFF"></i></span>
						                          </button>
							                    </div>
							                </div>
						                  </div>
						                </div>
						            </div>
		                           <div class="tab-pane" id="tabe4"><div>
		                          </div>
		                        </div>
		                      </div>
		                      <div class="tab-pane pedit" id="tabe5">
		                          	<div class="col-xs-12">
						              <div class="panel widget light-widget mb-10">
						                  <div class="panel-heading">
						                    <h3 class="panel-title"> 
						                      GENERAR REPORTE DE PACIENTES
						                    </h3>
						                  </div>
						                  <div class="panel-body bordergray1">
						                  	<div class="row mb-5 mt-10 ml-10 mr-10">
							                    <div class="col-md-3 col-sm-3 col-lg-3 col-xs-6">
						                          Fecha atenci&oacute;n: 
						                          <div class="controls mt-5">
						                            <input type="text" class="start range1">
						                          </div>
							                    </div>
							                    <div class="col-md-3 col-sm-3 col-lg-3 col-xs-6 mt-28">
						                          <button class="btn btn-block btn-primary generate5 has-spinner"> 
						                          		Generar 
						                          		<span class="spinner"><i class="icon-spin icon-refresh" style="color:#FFFFFF"></i></span>
						                          </button>
							                    </div>
							                </div>
						                  </div>
						                </div>
						            </div>
		                          </div>
		                          <div class="tab-pane pedit" id="tabe7">
		                          	<div class="col-xs-12">
						              <div class="panel widget light-widget mb-10">
						                  <div class="panel-heading">
						                    <h3 class="panel-title"> 
						                      GENERAR REPORTE DE COPAGOS
						                    </h3>
						                  </div>
						                  <div class="panel-body bordergray1">
						                  	<div class="row mb-5 mt-10 ml-10 mr-10">
						                  		<div class="col-md-3 col-sm-3 col-lg-3 col-xs-6">
						                          Fecha entrega: 
						                          <div class="controls mt-5">
						                            <input type="text" class="start7 range1">
						                          </div>
							                    </div>
						                        <div class="col-md-4 col-sm-4 col-lg-4 col-xs-6">
						                          Profesional: 
						                          <div class="controls mt-5">
						                            <select class="professional chosen-select"  data-placeholder="Seleccione un profesional" tabindex="2">
						                            	<option value="*" selected>Seleccione un profesional</option>
						                            	<c:forEach var="professional" items="${data.professionals}">
							                           		<option value="${professional.pkuser}">${fn:trim(professional.userName1)} ${professional.userName2} ${professional.userSurname1} ${professional.userSurname2} - ${professional.userDocument}</option>
							                           	</c:forEach>
						                            </select>
						                          </div>
							                    </div>
							                    <div class="col-md-2 col-sm-2 col-lg-2 col-xs-6">
						                          N. Comp : 
						                          <div class="controls mt-5">
						                            <input type="text" class="nro_comp">
						                          </div>
							                    </div>
							                    <div class="col-md-3 col-sm-3 col-lg-3 col-xs-6 mt-28">
							                      <button class="btn btn-block btn-primary generate7 has-spinner"> 
						                          		Generar 
						                          		<span class="spinner"><i class="icon-spin icon-refresh" style="color:#FFFFFF"></i></span>
						                          </button>
							                    </div>
							                </div>
						                  </div>
						                </div>
						            </div>
		                          </div>
		                          <div class="tab-pane pedit" id="tabe8">
		                          	<div class="col-xs-12">
						              <div class="panel widget light-widget mb-10">
						                  <div class="panel-heading">
						                    <h3 class="panel-title"> 
						                      GENERAR REPORTE ESPECIAL
						                    </h3>
						                  </div>
						                  <div class="panel-body bordergray1">
						                  	<div class="row mb-5 mt-10 ml-10 mr-10">
						                        <div class="col-md-3 col-sm-3 col-lg-3 col-xs-6">
						                          Entidad: 
						                          <div class="controls mt-5">
						                            <select class="entitylist8">
						                            	<c:forEach var="e" items="${data.entities}">
						                            		<option value="${e.pkentity}"><c:out value="${fn:toUpperCase(e.entityName)}"/></option>
						                            	</c:forEach>
						                            </select>
						                          </div>
							                    </div>
							                    <div class="col-md-3 col-sm-3 col-lg-3 col-xs-6">
						                          Fecha Inicio: 
						                          <div class="controls mt-5">
						                            <input type="text" class="start range1">
						                          </div>
							                    </div>
							                    <div class="col-md-3 col-sm-3 col-lg-3 col-xs-6">
						                          Fecha Solicitud: 
						                          <div class="controls mt-5">
						                            <input type="text" class="start range2">
						                          </div>
							                    </div>
							                    <div class="col-md-3 col-sm-3 col-lg-3 col-xs-6">
						                          Fecha Atenci�n: 
						                          <div class="controls mt-5">
						                            <input type="text" class="start range3">
						                          </div>
							                    </div>
							                    <div class="col-md-3 col-sm-3 col-lg-3 col-xs-6 mt-28">
							                      <button class="btn btn-block btn-primary generate8 has-spinner"> 
						                          		Generar 
						                          		<span class="spinner"><i class="icon-spin icon-refresh" style="color:#FFFFFF"></i></span>
						                          </button>
							                    </div>
							                </div>
						                  </div>
						                </div>
						            </div>
		                           <div class="tab-pane" id="tabe4"><div>
		                          </div>
		                        </div>
		                      </div>
		                    </div>
	                      </div>
	                 </div>
				</div> 
			</div>
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>

  <!-- .container --> 
<!-- .content -->
  
  
  
<!-- Footer Start -->
  
<!-- Head menu search form ends -->  

<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqprint.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/custom/reports.js"></script>  
<!--[if lt IE 9]>
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script> 
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/caroufredsel.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jlinq.js"></script> 
<!-- CK Editor  -->
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/ckeditor/ckeditor.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/ckeditor/adapters/jquery.js'></script>

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/fullcalendar.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/lang/es.js'></script>

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/moment.min.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/daterangepicker.js'></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/prettyphoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/theme.js"></script>
 
<script src="${pageContext.request.contextPath}/resources/js/chosen.jquery.js" type="text/javascript"></script>
<script src="${pageContext.request.contextPath}/resources/js/chosen2.jquery.js" type="text/javascript"></script>
<script type="text/javascript">
	$(".professional").chosen({
	    no_results_text: "",
	    width: "95%"
	});
</script>
<!-- Specific Page Scripts Put Here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/google-code-prettify/prettify.js"></script>
<script>
    var startd = "", endd = "", startd1 = "", endd1 = "", startd2 = "", endd2 = "";
    $('.range1').daterangepicker({
        locale: {
          format: 'YYY-MM-DD'
        },
        ranges: {
	        'Hoy': [moment(), moment()],
	        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	        'Ultimos 7 d�as': [moment().subtract(7, 'days'), moment()],
	        'Ultimos 30 d�as': [moment().subtract(30, 'days'), moment()],
	        'Este mes': [moment().startOf('month'), moment().endOf('month')],
	        'El mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
      },
      function(start, end) {
        startd = start;
        endd = end;
      }
    );
    $('.range2').daterangepicker({
        locale: {
          format: 'YYY-MM-DD'
        },
        ranges: {
        	'Hoy': [moment(), moment()],
	        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	        'Ultimos 7 d�as': [moment().subtract(6, 'days'), moment()],
	        'Ultimos 30 d�as': [moment().subtract(29, 'days'), moment()],
	        'Este mes': [moment().startOf('month'), moment().endOf('month')],
	        'El mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
      },
      function(start, end) {
        startd1 = start;
        endd1 = end;
      }
    );
    $('.range3').daterangepicker({
        locale: {
          format: 'YYY-MM-DD'
        },
        ranges: {
        	'Hoy': [moment(), moment()],
	        'Ayer': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
	        'Ultimos 7 d�as': [moment().subtract(6, 'days'), moment()],
	        'Ultimos 30 d�as': [moment().subtract(29, 'days'), moment()],
	        'Este mes': [moment().startOf('month'), moment().endOf('month')],
	        'El mes pasado': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
      },
      function(start, end) {
        startd2 = start;
        endd2 = end;
      }
    );
</script>
   
<script type="text/javascript" >
	"use strict";
	$(window).load(function() 
	{
		"use strict";	
		$( '[data-rel^="ckeditor"]' ).ckeditor();
		var availableTags = [
			"ActionScript",
			"AppleScript",
			"Asp",
			"BASIC",
			"C",
			"C++",
			"Clojure",
			"COBOL",
			"ColdFusion",
			"Erlang",
			"Fortran",
			"Groovy",
			"Haskell",
			"Java",
			"JavaScript",
			"Lisp",
			"Perl",
			"PHP",
			"Python",
			"Ruby",
			"Scala",
			"Scheme"
		];
		$( "#input-autocomplete" ).autocomplete({
			source: availableTags
		});
		//CKEDITOR.instances['editor1'].setData('${fn:trim(data.email1content)}');
	});
</script>

</body>
</html>