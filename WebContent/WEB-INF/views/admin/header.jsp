<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="java.util.List" %>
<%@ page import="com.adom.ecreativos.vo.AdTusers" %>
<%@ page import="com.adom.ecreativos.util.*" %>
<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html><!--<![endif]-->

<!-- Specific Page Data -->

<!-- End of Data -->

<head>
    <meta charset="utf-8" />
    <title>.:ADOM:. - Zona de administraci&oacute;n</title> 
    <meta name="viewport" content="width=device-width, initial-scale=1.0">  
    
    <link rel="shortcut icon" href="img/favicon.png">
    
    <!-- Bootstrap & FontAwesome & Entypo CSS -->
    <script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/moment/moment.min.js'></script>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!--[if IE 7]><link type="text/css" rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/font-awesome-ie7.min.css"><![endif]-->
    <link href="${pageContext.request.contextPath}/resources/css/font-entypo.css" rel="stylesheet" type="text/css"> 
    
        <link href="${pageContext.request.contextPath}/resources/css/chosen.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/css/chosen2.css" rel="stylesheet" type="text/css">   

    <link href="${pageContext.request.contextPath}/resources/plugins/fullcalendar/fullcalendar.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/plugins/fullcalendar/fullcalendar.print.css" rel="stylesheet" type="text/css">    
     
    <!-- Fonts CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/fonts.css"  rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css">  
               
    <!-- Plugin CSS -->
    <link href="${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery-ui.custom.min.css" rel="stylesheet" type="text/css">    
    <link href="${pageContext.request.contextPath}/resources/plugins/prettyphoto-plugin/css/prettyPhoto.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/plugins/isotope/css/isotope.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/plugins/pnotify/css/jquery.pnotify.css" media="screen" rel="stylesheet" type="text/css">    
	<link href="${pageContext.request.contextPath}/resources/plugins/google-code-prettify/prettify.css" rel="stylesheet" type="text/css"> 
   
         
    <link href="${pageContext.request.contextPath}/resources/plugins/mCustomScrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/plugins/tagsInput/jquery.tagsinput.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/plugins/bootstrap-switch/bootstrap-switch.css" rel="stylesheet" type="text/css">    
    <link href="${pageContext.request.contextPath}/resources/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css">    
    <link href="${pageContext.request.contextPath}/resources/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/plugins/colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css">            

	<!-- Specific CSS -->

  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.js"></script> 
  <script src="http://crypto-js.googlecode.com/svn/tags/3.1.2/build/rollups/md5.js"></script>
	    
     
    <!-- Theme CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/theme.min.css" rel="stylesheet" type="text/css">
    <!--[if IE]> <link href="${pageContext.request.contextPath}/resources/css/ie.css" rel="stylesheet" > <![endif]-->
    <link href="${pageContext.request.contextPath}/resources/css/chrome.css" rel="stylesheet" type="text/chrome"> <!-- chrome only css -->    

    <!-- Responsive CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/theme-responsive.min.css" rel="stylesheet" type="text/css"> 

    <link href="${pageContext.request.contextPath}/resources/custom/custom.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" type="text/css" href="http://t4t5.github.io/sweetalert/dist/sweetalert.css">  
    <script src="http://t4t5.github.io/sweetalert/dist/sweetalert-dev.js"></script>  
 

    <!-- Head SCRIPTS -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script> 
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/mobile-detect.min.js"></script> 
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/mobile-detect-modernizr.js"></script> 
 
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/html5shiv.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/respond.min.js"></script>     
    <![endif]-->
 <link href="http://netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet">
</head>    
<!-- <body id="nav" class="full-layout  nav-right-hide nav-right-start-hide  nav-top-fixed  nav-left-medium    responsive    clearfix" data-active="nav "  data-smooth-scrolling="1"> -->
<body id="nav" class="full-layout  nav-right-hide nav-right-start-hide  nav-top-fixed  responsive    clearfix" data-active="nav "  data-smooth-scrolling="1">       
<div class="vd_body">
<!-- Header Start -->
  <header class="header-1" id="header">
      <div class="vd_top-menu-wrapper">
        <div class="container ">
          <div class="vd_top-nav vd_nav-width  ">
          <div class="vd_panel-header">
            <div class="logo">
                <a href="admin"><img alt="ADOM" class="logo" src="${pageContext.request.contextPath}/resources/img/logo.png"></a>
            </div>
            <!-- logo -->
            <div class="vd_panel-menu hidden-md hidden-sm hidden-xs hidden-lg" data-intro="<strong>Minimize Left Navigation</strong><br/>Toggle navigation size to medium or small size. You can set both button or one button only. See full option at documentation." data-step=1>
                <span class="nav-medium-button menu" data-toggle="tooltip" data-placement="bottom"  data-action="nav-left-medium">
                    <i class="fa fa-bars"></i>
                </span>                                
                <span class="nav-small-button menu" data-toggle="tooltip" data-placement="bottom" data-action="nav-left-small">
                    <i class="fa fa-ellipsis-v"></i>
                </span>             
            </div>
            <div class="vd_panel-menu left-pos visible-sm visible-xs">
                <span class="menu" data-action="toggle-navbar-left">
                    <i class="fa fa-ellipsis-v"></i>
                </span>      
            </div>
            <div class="vd_panel-menu visible-xs emm">
                <span class="menu visible-xs" data-action="submenu">
                    <i class="fa fa-bars"></i>
                </span>   
            </div>                                     
            <!-- vd_panel-menu -->
          </div>
          <!-- vd_panel-header -->
            
          </div>    
          <div class="vd_container">
            <div class="row">
                <div class="col-sm-5 col-xs-12">
                </div>
                <div class="col-sm-7 col-xs-12">
                    <div class="vd_mega-menu-wrapper">
                        <div class="vd_mega-menu pull-right">
                            <ul class="mega-ul">   
     
    <li id="top-menu-profile" class="profile mega-li"> 
        <a href="#" class="mega-link"  data-action="click-trigger"> 
            <span class="mega-name">
                Hola, Administrador <i class="fa fa-caret-down fa-fw"></i>
            </span>
        </a> 
      <div class="vd_mega-menu-content  width-xs-2  left-xs left-sm" data-action="click-target">
        <div class="child-menu"> 
            <div class="content-list content-menu">
                <ul class="list-wrapper pd-lr-10">
                    <li> <a href="${pageContext.request.contextPath}/j_spring_security_logout"> <div class="menu-icon"><i class=" fa fa-sign-out"></i></div>  <div class="menu-text">Salir</div> </a> </li>           
                </ul>
            </div> 
        </div> 
      </div>     
  
    </li> 
    </ul>
<!-- Head menu search form ends -->                         
                        </div>
                    </div>
                </div>

            </div>
          </div>
        </div>
        <!-- container --> 
      </div>
      <!-- vd_primary-menu-wrapper --> 

  </header>
  <!-- Header Ends --> 
  <div class="content">
  <div class="container">
    <div class="vd_navbar vd_nav-width vd_navbar-no-tab vd_navbar-left  ">
    <div class="navbar-tabs-menu clearfix" style="height: 20px;">
            <span class="expand-menu" data-action="expand-navbar-tabs-menu">
                               
            </span>                                                  
    </div>
  <div class="navbar-menu clearfix">
        <!-- <div class="vd_panel-menu hidden-xs">
            <span data-original-title="Expand All" data-toggle="tooltip" data-placement="bottom" data-action="expand-all" class="menu">
                <i class="fa fa-sort-amount-asc"></i>
            </span>                   
        </div>  -->
      <!-- <h3 class="menu-title hide-nav-medium hide-nav-small">UI Features</h3>  -->
 <div class=" vd_menu">
   <ul> 
    <li class="<c:out value="${data.option eq 1 ? 'btn-active': 'btn-mommy'}"/>">
      <a href="${pageContext.request.contextPath}/admin/dashboard">
          <span class="menu-icon"><i class="fa fa-home"></i></span> 
            <span class="menu-text">Inicio</span>  
        </a> 
    </li>    
    <li class="<c:out value="${data.option eq 2 ? 'btn-active': 'btn-mommy'}"/>">
      <a href="${pageContext.request.contextPath}/admin/patients">
          <span class="menu-icon"><i class="fa fa-users"></i></span> 
            <span class="menu-text">Pacientes</span>  
        </a> 
    </li>
    <li class="<c:out value="${data.option eq 3 ? 'btn-active': 'btn-mommy'}"/>">
      <a href="${pageContext.request.contextPath}/admin/professionals">
        <span class="menu-icon"><i class="fa fa-user-md"> </i></span> 
          <span class="menu-text">Profesionales</span>  
          <span class="menu-badge"><span class="badge vd_bg-red">
            <c:out value="${data.count_pr}"></c:out>
            </span></span>
      </a>
    </li>
    <li class="<c:out value="${data.option eq 5 ? 'btn-active': 'btn-mommy'}"/>">
      <a href="${pageContext.request.contextPath}/admin/coordinators">
        <span class="menu-icon"><i class="append-icon fa fa-user"> </i></span> 
          <span class="menu-text">Coordinadores</span>  
          <span class="menu-badge"><span class="badge vd_bg-red">
            <c:out value="${data.count_co}"></c:out>
            </span></span>
      </a>
    </li>
    <li class="<c:out value="${data.option eq 4 ? 'btn-active': 'btn-mommy'}"/>">
      <a href="${pageContext.request.contextPath}/admin/entities">
        <span class="menu-icon"><i class="append-icon fa fa-building"> </i></span> 
          <span class="menu-text">Entidades</span> 
          <span class="menu-badge"><span class="badge vd_bg-red">
            <c:out value="${data.count_en}"></c:out>
            </span></span> 
      </a>
    </li> 
    <li class="<c:out value="${data.option eq 8 ? 'btn-active': 'btn-mommy'}"/>">
      <a href="${pageContext.request.contextPath}/admin/services">
        <span class="menu-icon"><i class="append-icon fa fa-stethoscope"> </i></span> 
          <span class="menu-text">Servicios e Insumos</span>
      </a>
    </li>  
    <li class="<c:out value="${data.option eq 6 ? 'btn-active': 'btn-mommy'}"/>">
      <a href="${pageContext.request.contextPath}/admin/billing">
        <span class="menu-icon"><i class="fa fa-file-text-o"> </i></span> 
          <span class="menu-text">Facturaci&oacute;n</span>  
      </a>
    </li>
    <li class="<c:out value="${data.option eq 12 ? 'btn-active': 'btn-mommy'}"/>">
      <a href="${pageContext.request.contextPath}/admin/copago">
        <span class="menu-icon"><i class="fa fa-dollar"> </i></span> 
          <span class="menu-text">Copagos</span>  
      </a>
    </li>
    <li class="<c:out value="${data.option eq 11 ? 'btn-active': 'btn-mommy'}"/>">
      <a href="${pageContext.request.contextPath}/admin/reports">
        <span class="menu-icon"><i class="fa fa-file-text-o"></i></span> 
          <span class="menu-text">Reportes</span>  
      </a>
    </li> 
    <li class="<c:out value="${data.option eq 9 ? 'btn-active': 'btn-mommy'}"/>">
      <a href="${pageContext.request.contextPath}/admin/adverts">
        <span class="menu-icon"><i class="fa fa-bullhorn"> </i></span> 
          <span class="menu-text">Anuncios</span>  
      </a>
    </li> 
    <li class="<c:out value="${data.option eq 7 ? 'btn-active': 'btn-mommy'}"/>">
      <a href="${pageContext.request.contextPath}/admin/configuration">
        <span class="menu-icon"><i class="append-icon fa fa-cogs"> </i></span> 
          <span class="menu-text">Configuraci&oacute;n</span>  
      </a>
    </li> 
    </ul>
<!-- Head menu search form ends -->         </div>             
    </div>
    <div class="navbar-spacing clearfix">
    </div>    
</div> 