  <%@include file="header.jsp" %>
    <!-- Middle Content Start -->
    
    <style>
    	select {
		    padding: 9px;
		}
    </style>
        
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <!-- vd_head-section -->
          <!-- vd_title-section -->
          
          <div class="vd_content-section clearfix" style="min-height: 660px;">

          <div class="row">
            <div class="col-xs-12">
              
              <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog mrp">
                      <div class="modal-content">
                        <div class="modal-header btn-active">
                          <button type="button" class="close clear" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                          <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user-plus"></i> Registrar nuevo paciente</h4>
                        </div>
                        <div class="modal-body"> 
                          <form id="frmreg" class="form form-horizontal">
                            <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">Tipo Documento</label>
		                          <div class="controls">
		                            <select class="td">
		                            	<c:forEach var="typedocs" items="${data.typedocs}">
		                            		<option value="${typedocs.pktypedocument}"><c:out value="${fn:toUpperCase(typedocs.typedocumentName)}"/></option>
		                            	</c:forEach>
		                            </select>
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">Numero Documento</label>
		                          <div class="controls">
		                            <input type="text" class="nd" id="nd" name="nd" required autofocus placeholder="Numero documento">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">Primer Nombre</label>
		                          <div class="controls">
		                            <input type="text" class="n1" name="n1" required placeholder="Primer Nombre">
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">Segundo Nombre</label>
		                          <div class="controls">
		                            <input type="text" class="n2" name="n2" placeholder="Segundo Nombre">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">Primer Apellido</label>
		                          <div class="controls">
		                            <input type="text" class="l1" name="a1" required placeholder="Primer Apellido">
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">Segundo Apellido</label>
		                          <div class="controls">
		                            <input type="text" class="l2" name="a2" placeholder="Segundo Apellido">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">G&eacute;nero</label>
		                          <div class="controls">
		                            <select class="g">
		                            	<option value="M">MASCULINO</option>
		                            	<option value="F">FEMENINO</option>
		                            </select>
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">Tipo Usuario</label>
		                          <div class="controls">
		                            <input type="text" required class="tu" name="tu" value="5">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">Fecha nacimiento</label>
		                          <div class="controls">
		                            <input type="date" class="fn" placeholder="Fecha nacimiento">
		                          </div>
		                        </div>
		                        <div class="col-md-6 mt-30">
		                          <label class="control-label col-md-2">Edad</label>
		                          <div class="controls col-md-5">
		                            <input type="text" class="a" name="edad" required placeholder="Edad">
		                          </div>
		                          <div class="controls col-md-5">
		                            <select class="ta">
		                            	<option value="A">A�OS</option>
		                            	<option valuw="M">MESES</option>
		                            </select>
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
			                    <div class="col-md-6">
		                          <label class="control-label">Barrio</label>
		                          <div class="controls">
		                            <input type="text" class="d" name="d" placeholder="Barrio">
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">Direcci&oacute;n</label>
		                          <div class="controls">
		                            <input type="text" class="add" name="add" placeholder="Direcci&oacute;n">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">Zona</label>
		                          <div class="controls">
		                            <input type="text" class="z" name="z" placeholder="Zona">
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">Email</label>
		                          <div class="controls">
		                            <input type="email" class="e" id="e" name="e" placeholder="Email">
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-6">
		                          <label class="control-label">T&eacute;lefono 1</label>
		                          <div class="controls">
		                            <input type="text" class="p1" name="p1" required placeholder="T&eacute;lefono 1">
		                          </div>
		                        </div>
		                        <div class="col-md-6">
		                          <label class="control-label">T&eacute;lefono 2</label>
		                          <div class="controls">
		                            <input type="text" class="p2" name="p2" placeholder="T&eacute;lefono 2">
		                          </div>
		                        </div>
		                    </div>
                            <div class="form-group">
                              <button type="submit" class="btn pull-right btn vd_btn btn-mommy mr20 mt30 bas has-spinner">
                              	Guardar
                              	<span class="spinner"><i class="icon-spin icon-refresh" style="color:#23709E"></i></span>
                              </button>
                              <button type="button" class="clear pull-right btn vd_btn vd_bg-info mr5 mt30 close-model" data-dismiss="modal">Cancelar</button>
                            </div>
                          </form>
                        </div>
                      </div>
                      <!-- /.modal-content --> 
                    </div>
                    <!-- /.modal-dialog --> 
                  </div>
                  
                  <div class="modal fade" id="modalingr" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                      <div class="modal-content">
                        <div class="modal-header btn-active">
                          <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
                          <h4 class="modal-title2" id="myModalLabel"><i class="fa fa-user-plus"></i> Lista de ingresos</h4>
                        </div>
                        <div class="modal-body"> 
                          <form id="frm_ni" class="form form-horizontal">
                            <div class="row mb-5">
		                        <div class="col-md-6">
		                          <a class="btn btn-default newentry">Nuevo ingreso</a>
		                        </div>
		                        <div class="col-md-12 formnewentry" style="display:none;">
		                          <div class="row" style="border: 3px solid gray; margin: 5px; padding:10px;">
		                          	<div class="col-md-6">
			                          <label class="control-label">Entidad</label>
			                          <div class="controls">
			                            <select class="entitylist"></select>
			                          </div>
			                        </div>
			                        <div class="col-md-6">
			                          <label class="control-label">Plan</label>
			                          <div class="controls">
			                            <select class="entityPlan"></select>
			                          </div>
			                        </div>
			                        <div class="col-md-4">
			                          <label class="control-label">C&oacute;digo CIE10</label>
			                          <div class="controls">
			                            <input type="text" class="cie10" name="cie10" placeholder="C&oacute;digo CIE10">
			                          </div>
			                        </div>
			                        <div class="col-md-8">
			                          <label class="control-label">Descripci&oacute;n</label>
			                          <div class="controls">
			                            <input type="text" class="cie10desc" name="cie10desc" placeholder="Descripci&oacute;n">
			                          </div>
			                        </div>
			                        <div class="col-md-7 mb-15">
			                          <label class="control-label">N&uacute;mero de contrato</label>
			                          <div class="controls">
			                            <input type="text" class="number_c" placeholder="N&uacute;mero de contrato">
			                          </div>
			                        </div>
			                        <div class="col-md-6 mt-5">
			                          <div class="controls">
			                            <a class="btn btn-default btn-block cancel"> Cancelar</a>
			                          </div>
			                        </div>
			                        <div class="col-md-6 mt-5">
			                          <div class="controls">
			                            <button type="submit" class="btn btn-success btn-block bas has-spinner">
			                            	Crear ingreso 
			                            	<span class="spinner"><i class="icon-spin icon-refresh" style="color:#23709E"></i></span>
			                            </button>
			                          </div>
			                        </div>
		                          </div>
		                        </div>
		                    </div>
		                    <div class="row mb-5">
		                        <div class="col-md-12 listentrys">
		                        </div>
		                    </div>
                          </form>
                        </div>
                      </div>
                      <!-- /.modal-content --> 
                    </div>
                    <!-- /.modal-dialog --> 
                  </div>
            </div>
          </div>
		  <div class="row">
		  	<div class="col-md-6 col-sm-6 col-xs-4">
		  		<button class="btn btn-mommy vd_btn" data-toggle="modal" data-target="#myModal"> NUEVO PACIENTE</button>
		  	</div>
		  	<div class="col-md-6 col-sm-6 col-xs-8">
                  <div class="col-md-12 col-sm-12 col-lg-12">
                    <div class="controls">
                      <input type="text" placeholder="Busqueda de pacientes" class="search_text">
                    </div>
                  </div>
		  	</div>
		  </div>
          <div class="row">
            <div class="col-xs-12">
              <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> 
                      <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> 
                      </span> Resultados de la busqueda
                    </h3>
                  </div>
                  <div class="panel-body  table-responsive bordergray1">
                    <table class="table hover data-tables">
                      <thead>
                        <tr>
                          <th>Tipo Documento <i class="fa fa-sort"></i></th>
                          <th>Numero documento <i class="fa fa-sort"></i></th>
                          <th>Nombre Completo <i class="fa fa-sort"></i></th>
                          <th>Tel&eacute;fono <i class="fa fa-sort"></i></th>
                          <th>Detalle</th>
                        </tr>
                      </thead>
                      <tbody class="tbody">
                        
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
          </div>
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
  <!-- .container --> 
<!-- .content -->
  
  
  
<!-- Footer Start -->


<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqprint.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/custom/admin.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-validate.js"></script>
<!--[if lt IE 9]>
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script> 
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/caroufredsel.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jlinq.js"></script> 

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/fullcalendar.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/lang/es.js'></script>

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/moment.min.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/daterangepicker.js'></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/prettyphoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/theme.js"></script>
 


<!-- Specific Page Scripts Put Here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/google-code-prettify/prettify.js"></script>
<script type="text/javascript">
$(document).ready(function() {  
    "use strict";
        var form_register_2 = $('#frmreg');
        var error_register_2 = $('.alert-danger', form_register_2);
        var success_register_2 = $('.alert-success', form_register_2);
        
        var msg_required = "Este campo es requerido";
        var msg_minlength = "Necesitamos por lo menos {0} caracteres";
        var msg_maxlength = "Necesitamos m&aacute;ximo {0} caracteres";;
        var msg_number = "Debes ingresar un n&uacute;mero";
		var msg_nospace = "No se admiten espacios";
		var msg_remote_document = "Este documento ya se encuentra registrado";
        
        form_register_2.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
            	nd:{
            		required:true,
            		minlength:5,
            		number:true,
            		remote: {
                        url: "checkDocument",
                        type: "post"
                    }
            	},
                n1: {
                    required: true,
                    minlength: 3
                },
                n2: {
                    minlength: 3
                },
                a1: {
                    required: true,
                    minlength: 3
                },
                a2: {
                    minlength: 3
                },
                tu: {
                	required: true,
                	number:true,
                    minlength: 1,
                    maxlength: 1
                },
                edad: {
                	required: true,
                	number: true,
                    minlength: 1,
                    maxlength: 3
                },
                add: {
                    required: true,
                    minlength: 6
                },
                z: {
                    required: true,
                    minlength: 3
                },
                p1: {
                    required: true,
                    number: true,
                    minlength: 7
                },
        
            },
            messages: {
            	nd: {
                    required: msg_required,
                    number: msg_number,
                    minlength: msg_minlength,
                    remote: msg_remote_document
                },
                n1: {
                    required: msg_required,
                    minlength: msg_minlength
                },
                n2: {
                    minlength: msg_minlength
                },
                a1: {
                    required: msg_required,
                    minlength: msg_minlength
                },
                a2: {
                    minlength: msg_minlength
                },
                tu: {
                    required: msg_required,
                    number: msg_number,
                    minlength: msg_minlength,
                    maxlength: msg_maxlength,
                },
                edad: {
                    required: msg_required,
                    number: msg_number,
                    minlength: msg_minlength,
                    maxlength: msg_maxlength,
                },
                add: {
                    required: msg_required,
                    minlength: msg_minlength
                },
                z: {
                    required: msg_required,
                    minlength: msg_minlength
                },
                p1: {
                    required: msg_required,
                    number: msg_number,
                    minlength: msg_minlength
                },
           },
           
           submitHandler: function() {
        	   savep();
           }
        });
        
        var form_register_3 = $('#frm_ni');
        
        form_register_3.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
            	cie10:{
            		required:true,
            		minlength:4,
            		maxlength:5,
            	},
            	cie10desc: {
                    required: true,
                    minlength: 3
                },
            },
            messages: {
            	cie10: {
                    required: msg_required,
                    minlength: msg_minlength,
                    maxlength: msg_maxlength
                },
                cie10desc: {
                    required: msg_required,
                    minlength: msg_minlength
                },
           },
           submitHandler: function() {
        	   saveentry();
           }
        });
  
});
</script>
</body>
</html>