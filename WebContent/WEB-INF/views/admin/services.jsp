  <%@include file="header.jsp" %>
    <!-- Middle Content Start -->
        
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <!-- vd_head-section -->
          <!-- vd_title-section -->
          
          <div class="vd_content-section clearfix" style="min-height: 660px;">
			
		  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	           <div class="modal-dialog">
	             <div class="modal-content">
	               <div class="modal-header btn-active">
	                 <button type="button" class="close clear" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
	                 <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user-plus"></i> Registrar nuevo servicio</h4>
	               </div>
	               <div class="modal-body"> 
	                 <form id="frmreg_s" class="form form-horizontal">
	                   <div class="row mb-5">
	                 <div class="col-md-12">
	                   <label class="control-label">Nombre del servicio</label>
	                   <div class="controls">
	                     <input type="text" class="name" name="name" autofocus placeholder="Nombre del servicio">
	                   </div>
	                 </div>
	             </div>
	             <div class="row mb-5">
	                 <div class="col-md-7">
	                   <label class="control-label">Valor a pagar al profesional</label>
	                   <div class="controls">
	                     <input type="text" class="value" name="val" autofocus placeholder="Valor a pagar">
	                   </div>
	                 </div>
	             </div>
	             <div class="row mb-5">
	                 <div class="col-md-7">
	                   <label class="control-label">C&oacute;digo del servicio (CUPS)</label>
	                   <div class="controls">
	                     <input type="text" class="cod" name="cod" autofocus placeholder="C&oacute;digo del servicio (CUPS)">
	                   </div>
	                 </div>
	             </div>
	             <div class="row mb-5">
	                 <div class="col-md-7">
	                   <label class="control-label">Clasificaci&oacute;n</label>
	                   <div class="controls">
	                     <div class="vd_radio radio-success">
	                      <input type="radio" name="clasi" id="clasi11" value="C" checked="true">
	                      <label for="clasi11" class="clasi11"> Consulta </label>
	                      <input type="radio" name="clasi" id="clasi21" value="P">
	                      <label for="clasi21" class="clasi21"> Procedimiento </label>
	                    </div>
	                   </div>
	                 </div>
	             </div>
	             <div class="row mb-5">
	                 <div class="col-md-7">
	                   <label class="control-label">Tipo de servicio</label>
	                   <div class="controls">
	                     <div class="vd_radio radio-success">
	                      <input type="radio" name="tipo" id="tipo1" value="1" checked="true">
	                      <label for="tipo1"> Enfermer&iacute;a </label>
	                      <input type="radio" name="tipo" id="tipo2" value="2">
	                      <label for="tipo2"> Terapias </label>
	                    </div>
	                   </div>
	                 </div>
	             </div>
	             <div class="row mb-5 hi">
	                 <div class="col-md-3">
	                   <label class="control-label">Horas a invertir</label>
	                   <div class="controls">
	                     <input type="number" value="0" class="hours" autofocus placeholder="Horas a invertir">
	                   </div>
	                 </div>
	             </div>
	                   <div class="form-group">
	                     <button class="btn pull-right btn vd_btn btn-mommy mr20 mt30">Guardar</button>
	                     <button type="button" class="clear pull-right btn vd_btn vd_bg-info mr5 mt30" data-dismiss="modal">Cancelar</button>
	                   </div>
	                 </form>
	               </div>
	             </div>
	             <!-- /.modal-content --> 
	           </div>
	           <!-- /.modal-dialog --> 
	         </div>	
	         <div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	           <div class="modal-dialog">
	             <div class="modal-content">
	               <div class="modal-header btn-active">
	                 <button type="button" class="close clear" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
	                 <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user-plus"></i> Editar servicio</h4>
	               </div>
	               <div class="modal-body"> 
	                 <form id="frmreg" class="form form-horizontal">
	                   <div class="row mb-5">
	                 <div class="col-md-12">
	                   <label class="control-label">Nombre del servicio</label>
	                   <div class="controls">
	                     <input type="text" class="name1" autofocus placeholder="Nombre del servicio">
	                   </div>
	                 </div>
	             </div>
	             <div class="row mb-5">
	                 <div class="col-md-7">
	                   <label class="control-label">Valor a pagar al profesional</label>
	                   <div class="controls">
	                     <input type="text" class="value1" autofocus placeholder="Valor a pagar">
	                   </div>
	                 </div>
	             </div>
	             <div class="row mb-5">
	                 <div class="col-md-7">
	                   <label class="control-label">C&oacute;digo del servicio (CUPS)</label>
	                   <div class="controls">
	                     <input type="text" class="cod1" autofocus placeholder="C&oacute;digo del servicio (CUPS)">
	                   </div>
	                 </div>
	             </div>
	             <div class="row mb-5">
	                 <div class="col-md-7">
	                   <label class="control-label">Clasificaci&oacute;n</label>
	                   <div class="controls">
	                     <div class="vd_radio radio-success">
	                      <input type="radio" name="clasi2" id="clasi1" value="C">
	                      <label for="clasi1" class="clasi1"> Consulta </label>
	                      <input type="radio" name="clasi2" id="clasi2" value="P">
	                      <label for="clasi2" class="clasi2"> Procedimiento </label>
	                    </div>
	                   </div>
	                 </div>
	             </div>
	             <div class="row mb-5">
	                 <div class="col-md-7">
	                   <label class="control-label">Tipo de servicio</label>
	                   <div class="controls">
	                     <div class="vd_radio radio-success">
	                      <input type="radio" name="tipo" id="tipo11" value="1">
	                      <label for="tipo11" class="tipo11"> Enfermer&iacute;a </label>
	                      <input type="radio" name="tipo" id="tipo21" value="2">
	                      <label for="tipo21" class="tipo21"> Terapias </label>
	                    </div>
	                   </div>
	                 </div>
	             </div>
	             <div class="row mb-5 hi">
	                 <div class="col-md-3">
	                   <label class="control-label">Horas a invertir</label>
	                   <div class="controls">
	                     <input type="number" value="0" class="hours1" autofocus placeholder="Horas a invertir">
	                   </div>
	                 </div>
	             </div>
	                   <div class="form-group">
	                     <a class="btn pull-right btn vd_btn btn-mommy mr20 mt30 updatep">Guardar</a>
	                     <button type="button" class="clear pull-right btn vd_btn vd_bg-info mr5 mt30" data-dismiss="modal">Cancelar</button>
	                   </div>
	                 </form>
	               </div>
	             </div>
	             <!-- /.modal-content --> 
	           </div>
	           <!-- /.modal-dialog --> 
	         </div>	
	         
	         <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	           <div class="modal-dialog">
	             <div class="modal-content">
	               <div class="modal-header btn-active">
	                 <button type="button" class="close clear" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
	                 <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user-plus"></i> Registrar nuevo insumo</h4>
	               </div>
	               <div class="modal-body"> 
	                 <form id="frmreg_i" class="form form-horizontal">
	                   <div class="row mb-5">
		                 <div class="col-md-12">
		                   <label class="control-label">Nombre del insumo</label>
		                   <div class="controls">
		                     <input type="text" class="name2" name="name2" autofocus placeholder="Nombre del insumo">
		                   </div>
		                 </div>
		               </div>
		               <div class="row mb-5">
		                 <div class="col-md-7">
		                   <label class="control-label">C&oacute;digo del insumo</label>
		                   <div class="controls">
		                     <input type="text" class="cod2" name="cod2" autofocus placeholder="C&oacute;digo del insumo">
		                   </div>
		                 </div>
		               </div>
		                   <div class="form-group">
		                     <button class="btn pull-right btn vd_btn btn-mommy mr20 mt30">Guardar</button>
		                     <button type="button" class="clear pull-right btn vd_btn vd_bg-info mr5 mt30" data-dismiss="modal">Cancelar</button>
		               </div>
	                 </form>
	               </div>
	             </div>
	             <!-- /.modal-content --> 
	           </div>
	           <!-- /.modal-dialog --> 
	         </div>	
	         
	         <div class="modal fade" id="modal-edit-2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	           <div class="modal-dialog">
	             <div class="modal-content">
	               <div class="modal-header btn-active">
	                 <button type="button" class="close clear" data-dismiss="modal" aria-hidden="true"><i class="fa fa-times"></i></button>
	                 <h4 class="modal-title" id="myModalLabel"><i class="fa fa-user-plus"></i> Editar insumo</h4>
	               </div>
	               <div class="modal-body"> 
	                 <form id="frmreg" class="form form-horizontal">
	                   <div class="row mb-5">
		                 <div class="col-md-12">
		                   <label class="control-label">Nombre del insumo</label>
		                   <div class="controls">
		                     <input type="text" class="name3" autofocus placeholder="Nombre del insumo">
		                   </div>
		                 </div>
		               </div>
		               <div class="row mb-5">
		                 <div class="col-md-7">
		                   <label class="control-label">C&oacute;digo del insumo</label>
		                   <div class="controls">
		                     <input type="text" class="cod3" autofocus placeholder="C&oacute;digo del insumo">
		                   </div>
		                 </div>
		               </div>
		                   <div class="form-group">
		                     <a class="btn pull-right btn vd_btn btn-mommy mr20 mt30 updatei">Guardar</a>
		                     <button type="button" class="clear pull-right btn vd_btn vd_bg-info mr5 mt30" data-dismiss="modal">Cancelar</button>
		               </div>
	                 </form>
	               </div>
	             </div>
	             <!-- /.modal-content --> 
	           </div>
	           <!-- /.modal-dialog --> 
	         </div>	
			
          <div class="row">
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
              <button class="btn btn-mommy vd_btn" data-toggle="modal" data-target="#myModal"> NUEVO SERVICIO</button>
            </div>
            <div class="col-xs-4 col-md-4 col-sm-4 col-lg-4">
              <button class="btn btn-mommy vd_btn" data-toggle="modal" data-target="#myModal2"> NUEVO INSUMO</button>
            </div>
          </div>

          <div class="row">
            <div class="col-xs-8">
              <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> 
                      <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> 
                      </span> Listado de servicios
                    </h3>
                  </div>
                  <div class="panel-body  table-responsive bordergray1">
                    <table class="table hover" id="data-tables">
                      <thead>
                        <tr>
                          <th class="pointer">Nombre <i class="fa fa-sort"></i></th>
                          <th>Valor a pagar <i class="fa fa-sort"></i></th>
                          <th>C&oacute;digo <i class="fa fa-sort"></i></th>
                          <th>Tipo Servicio <i class="fa fa-sort"></i></th>
                          <th>Clasificaci&oacute;n <i class="fa fa-sort"></i></th>
                          <th>Acciones </i></th>
                        </tr>
                      </thead>
                      <tbody class="tbody">
                        <c:forEach var="service" items="${data.services}">
                        	<tr>
                        		<td><c:out value="${service.serviceName}"/></td>
                        		<td>
                        			$ <fmt:formatNumber type="number" pattern="###.###" value="${service.serviceValue}" />
                        		</td>
                        		<td><c:out value="${service.serviceCode}"/></td>
                        		<td>
                        			<c:choose>
									  <c:when test="${service.serviceType == 1}">
									    Enfermer&iacute;a
									  </c:when>
									  <c:otherwise>
									    Terapias
									  </c:otherwise>
									</c:choose>
                        		</td>
                        		<td>
                        			<c:choose>
									  <c:when test="${service.serviceType2 == 'C'}">
									    CONSULTA
									  </c:when>
									  <c:otherwise>
									    PROCEDIMIENTO
									  </c:otherwise>
									</c:choose>
                        		</td>
                        		<td>
                        			<a class='btn color-black btn-default btn-xs edit-serv mr-5' id="${service.pkservice}"> Ver detalle </a>
                        		</td>
                        	</tr>
                       	</c:forEach>
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
            <div class="col-xs-4">
              <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> 
                      <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> 
                      </span> Listado de insumos
                    </h3>
                  </div>
                  <div class="panel-body  table-responsive bordergray1">
                    <table class="table hover" id="data-tables">
                      <thead>
                        <tr>
                          <th class="pointer">Nombre <i class="fa fa-sort"></i></th>
                          <th>C&oacute;digo <i class="fa fa-sort"></i></th>
                          <th>Acciones </th>
                        </tr>
                      </thead>
                      <tbody class="tbody2">
                        <c:forEach var="insumo" items="${data.insumos}">
                        	<tr>
                        		<td><c:out value="${insumo.insumoName}"/></td>
                        		<td><c:out value="${insumo.insumoCode}"/></td>
                        		<td>
                        			<a class='btn color-black btn-default btn-xs edit-ins mr-5' id="${insumo.pkinsumo}"> Ver detalle </a>
                        		</td>
                        	</tr>
                       	</c:forEach>
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
          </div>
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
  <!-- .container --> 
<!-- .content -->
  
  
  
<!-- Footer Start -->


<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqprint.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/custom/services.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-validate.js"></script>
<!--[if lt IE 9]>
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script> 
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/caroufredsel.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jlinq.js"></script> 

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/fullcalendar.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/lang/es.js'></script>

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/moment.min.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/daterangepicker.js'></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/prettyphoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/theme.js"></script>
 


<!-- Specific Page Scripts Put Here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/google-code-prettify/prettify.js"></script>
<script type="text/javascript">
$(document).ready(function() {  
    "use strict";
        var form_register_2 = $('#frmreg_s');
        var error_register_2 = $('.alert-danger', form_register_2);
        var success_register_2 = $('.alert-success', form_register_2);
        
        var msg_required = "Este campo es requerido";
        var msg_minlength = "Necesitamos por lo menos {0} caracteres";
        var msg_maxlength = "Necesitamos m&aacute;ximo {0} caracteres";;
        var msg_number = "Debes ingresar un n&uacute;mero";
		var msg_nospace = "No se admiten espacios";
		var msg_email = "Ej: example@example.com";
		var msg_remote_email = "Este email ya se encuentra registrado";
		var msg_remote_document = "Este documento ya se encuentra registrado";
        
        form_register_2.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
            	name:{
            		required:true,
            		minlength:3
            	},
                val: {
                    required: true,
                    number: true
                },
                cod: {
                	required: true,
                    minlength: 3
                }
            },
            messages: {
            	name: {
                    required: msg_required,
                    minlength: msg_minlength,
                },
                val: {
                    required: msg_required,
                    number: msg_number
                },
                cod: {
                	required: msg_required,
                    minlength: msg_minlength
                }
           },
           
           submitHandler: function() {
        	   savep();
           }
        });
        
        $("#frmreg_i").validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
            	name2:{
            		required:true,
            		minlength:3,
            	},
                cod2: {
                    required: true,
                    minlength: 3
                },
            },
            messages: {
            	name2: {
                    required: msg_required,
                    minlength: msg_minlength,
                },
                cod2: {
                    required: msg_required,
                    minlength: msg_minlength
                },
           },
           
           submitHandler: function() {
        	   savei();
           }
        });
  
});
</script>

</body>
</html>