<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="org.springframework.security.core.userdetails.User"%>
<%@ page import="org.springframework.security.core.context.SecurityContextHolder"%>
<%@ page import="org.springframework.security.core.GrantedAuthority"%>
<%@ page import="java.util.Collection"%>

<%
boolean sesion = false;
String tipo="",url;
try {
    User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    sesion = true;
    System.out.println("Sesi�n iniciada");
    Collection<GrantedAuthority> role= user.getAuthorities();
      for(GrantedAuthority rol : role)
       tipo =rol.getAuthority().toString();
      System.out.println("rol: " + tipo);
      if(tipo.equals("professional"))
              url="user/dashboard";
      else
          if(tipo.equals("administrator"))
                 url="admin/dashboard";
          else
         	 if(tipo.equals("coordinator"))
                  url="coordinator/dashboard";
	                     else
	                         url=null;
          
} catch (Exception e) {
        System.out.println("Sesi�n Invalida");
        url = null;
}
        if(url != null)
                response.sendRedirect(url);        
       
%>

<!DOCTYPE html>
<!--[if IE 8]>			<html class="ie ie8"> <![endif]-->
<!--[if IE 9]>			<html class="ie ie9"> <![endif]-->
<!--[if gt IE 9]><!-->	<html><!--<![endif]-->

<!-- Specific Page Data -->

<!-- End of Data -->

<head>
    <meta charset="utf-8" />
    <title>.:ADOM:.</title>
    <meta name="keywords" content="HTML5 Template, CSS3, All Purpose Admin Template, Vendroid" />
    <meta name="description" content="Login Pages - Responsive Admin HTML Template">
    <meta name="author" content="Venmond">
    
    <!-- Set the viewport width to device width for mobile -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">    
    
    
    <!-- Fav and touch icons -->
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.png">
    
    
    <!-- CSS -->
       
    <!-- Bootstrap & FontAwesome & Entypo CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!--[if IE 7]><link type="text/css" rel="stylesheet" href="css/font-awesome-ie7.min.css"><![endif]-->
    <link href="${pageContext.request.contextPath}/resources/css/font-entypo.css" rel="stylesheet" type="text/css">    

    <!-- Fonts CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/fonts.css"  rel="stylesheet" type="text/css">
               
    <!-- Plugin CSS -->
    <link href="${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery-ui.custom.min.css" rel="stylesheet" type="text/css">    
    <link href="${pageContext.request.contextPath}/resources/plugins/prettyphoto-plugin/css/prettyPhoto.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/plugins/isotope/css/isotope.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/plugins/pnotify/css/jquery.pnotify.css" media="screen" rel="stylesheet" type="text/css">    
	<link href="${pageContext.request.contextPath}/resources/plugins/google-code-prettify/prettify.css" rel="stylesheet" type="text/css"> 
   
         
    <link href="${pageContext.request.contextPath}/resources/plugins/mCustomScrollbar/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/plugins/tagsInput/jquery.tagsinput.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/plugins/bootstrap-switch/bootstrap-switch.css" rel="stylesheet" type="text/css">    
    <link href="${pageContext.request.contextPath}/resources/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css">    
    <link href="${pageContext.request.contextPath}/resources/plugins/bootstrap-timepicker/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.request.contextPath}/resources/plugins/colorpicker/css/colorpicker.css" rel="stylesheet" type="text/css">            

	<!-- Specific CSS -->
	
	<link rel="stylesheet" type="text/css" href="http://t4t5.github.io/sweetalert/dist/sweetalert.css">  
    <script src="http://t4t5.github.io/sweetalert/dist/sweetalert-dev.js"></script> 
	    
     
    <!-- Theme CSS -->
    <link href="${pageContext.request.contextPath}/resources/css/theme.css" rel="stylesheet" type="text/css">
    <!--[if IE]> <link href="css/ie.css" rel="stylesheet" > <![endif]-->
    <link href="${pageContext.request.contextPath}/resources/css/chrome.css" rel="stylesheet" type="text/chrome"> <!-- chrome only css -->    


        
    <!-- Responsive CSS -->
        	<link href="${pageContext.request.contextPath}/resources/css/theme-responsive.css" rel="stylesheet" type="text/css"> 

        	<link href="${pageContext.request.contextPath}/resources/css/_style.css" rel="stylesheet" type="text/css"> 

	  
 
 
    <!-- for specific page in style css -->
        
    <!-- for specific page responsive in style css -->
        
    
    <!-- Custom CSS -->
    <link href="${pageContext.request.contextPath}/resources/custom/custom.css" rel="stylesheet" type="text/css">



    <!-- Head SCRIPTS -->
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/modernizr.js"></script> 
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/mobile-detect.min.js"></script> 
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/mobile-detect-modernizr.js"></script> 
 
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/html5shiv.js"></script>
      <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/respond.min.js"></script>     
    <![endif]-->
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.js"></script>
    
    <style>
		.se-pass{
			height: 28px;
		    border-radius: 1px;
		    padding-top: 3px;
		}
	</style>
</head>    

<body id="pages" class="body_login full-layout no-nav-left no-nav-right no-transform  nav-top-fixed  vbackground-login     responsive remove-navbar login-layout   clearfix" data-active="pages "  data-smooth-scrolling="1">     
<script type="text/javascript">
	var frm = localStorage.getItem("frm");
	localStorage.removeItem("frm");
	if(frm == "pf")
		location.href = "${pageContext.request.contextPath}/clientes/recaudo?e=1";
</script>
<div class="vd_body" >
<!-- Header Start -->

<!-- Header Ends --> 
<div class="content">
  <div class="container bgbody" id="body_js"> 
    
    <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <div class="vd_content-section clearfix" style="min-height: 720px;">
            <div class="vd_login-page">
              <div class="heading clearfix">
                <div class="logo2">
                  <h2 class="mgbt-xs-5"><img src="${pageContext.request.contextPath}/resources/img/logo.png" class="l-bottom"></h2>
                </div>
              </div>
              <div class="panel widget">
                <div class="panel-body">
                	<div id="msglogin" style = "display:none;"></div>
                  <form class="form-horizontal" id="login-form">
                  <div class="alert alert-danger vd_hidden">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                    <span class="vd_alert-icon"><i class="fa fa-exclamation-circle vd_red"></i></span><strong></strong>Completa todos los campos</div>
                  <div class="alert alert-success vd_hidden">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="icon-cross"></i></button>
                    <span class="vd_alert-icon"><i class="fa fa-check-circle vd_green"></i></span><strong>Procesando!</strong>. </div>                  
                    <div class="form-group  mgbt-xs-20 frm_l">
                       <div class="form-group mb-0">
	                        <label class="col-sm-4 control-label az f17">Email</label>
	                        <div class="col-sm-8 controls frm_login">
	                          <input type="email" id="email" class="required" required>
	                        </div>
	                   </div>
	                   <div class="form-group">
	                        <label class="col-sm-4 control-label az pa f17"></label>
	                        <div class="col-sm-8 controls frm_login">
	                          <a  class="btn btn-primary btn-block se-pass">Enviar contrase�a</a>
	                        </div>
	                   </div>
                    </div>
                  </form>
                </div>
              </div>
              <!-- Panel Widget -->
            </div>
            <!-- vd_login-page --> 
          </div>
          <!-- .vd_content-section --> 
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
    <!-- .vd_content-wrapper --> 
    <!-- Middle Content End --> 
  </div>
  <!-- .container --> 
</div>
<!-- .content -->

<!-- Footer Start -->

<!-- Footer END -->

</div>

<script>
$(document).ready(function() {
    $('#email').keydown(function(event) {
        if (event.keyCode == 13) {
            this.form.submit();
            return false;
         }
    });
    $('#password').keydown(function(event) {
        if (event.keyCode == 13) {
            this.form.submit();
            return false;
         }
    });
});
</script>
<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>
<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
 
<!--[if lt IE 9]>
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script> 
<script type="text/javascript" src='resources/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/caroufredsel.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/prettyphoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/theme.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/custom/custom.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/custom/login.js"></script>
 
<!-- Specific Page Scripts Put Here -->
<script type="text/javascript">
$(document).ready(function() {
	
		"use strict";
	
        var form_register_2 = $('#login-formv');
        var error_register_2 = $('.alert-danger', form_register_2);
        var success_register_2 = $('.alert-success', form_register_2);

        form_register_2.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
				
                email: {
                    required: true,
                    email: true
                },				
                password: {
                    required: true,
					minlength: 6
                },
				
            },
			
			errorPlacement: function(error, element) {
				if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
					element.parent().append(error);
				} else if (element.parent().hasClass("vd_input-wrapper")){
					error.insertAfter(element.parent());
				}else {
					error.insertAfter(element);
				}
			}, 
			
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register_2.hide();
                error_register_2.show();


            },

            highlight: function (element) { // hightlight error inputs
		
				$(element).addClass('vd_bd-red');
				$(element).parent().siblings('.help-inline').removeClass('help-inline hidden');
				if ($(element).parent().hasClass("vd_checkbox") || $(element).parent().hasClass("vd_radio")) {
					$(element).siblings('.help-inline').removeClass('help-inline hidden');
				}

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline hidden') // mark the current input as valid and display OK icon
                	.closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
				$(element).removeClass('vd_bd-red');

					
            },

            submitHandler: function (form) {
				$(form).find('#login-submit').prepend('<i class="fa fa-spinner fa-spin mgr-10"></i>')/*.addClass('disabled').attr('disabled')*/;					
                success_register_2.show();
                error_register_2.hide();
				$(form).attr('disabled','disabled');	
				$(form).submit();		
            }
        });	
	
	
});
</script>
<!-- Specific Page Scripts END -->




<!-- Google Analytics: Change UA-XXXXX-X to be your site's ID. Go to http://www.google.com/analytics/ for more information. -->

<script>
    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-XXXXX-X']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();
</script> 

</body>
</html>