  <%@include file="header.jsp" %>
    <!-- Middle Content Start -->
        
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <!-- vd_head-section -->
          <!-- vd_title-section -->
          
          <div class="vd_content-section clearfix" style="min-height: 660px;">

          <div class="row">
            <div class="col-xs-12">
              <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> 
                      <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> 
                      </span>mis pacientes activos
                    </h3>
                  </div>
                  <div class="panel-body  table-responsive bordergray1">
                    <table class="table hover data-table">
                      <thead>
                        <tr>
                          <th>Nombre paciente</th>
                          <th>Identificaci&oacute;n</th>
                          <th>Nombre servicio</th>
                          <th>F. Inicio</th>
                          <th>Frecuencia</th>
                          <th>Estado</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody class="tbody">
                        <c:forEach var="service" items="${data.services_act}">
                       		<tr>
                       			<td>
                       				<c:out value="${service.es.adTentry.adTusersByFkpatient.userName1}"/> 
                           			<c:out value="${service.es.adTentry.adTusersByFkpatient.userName2}"/> 
                           			<c:out value="${service.es.adTentry.adTusersByFkpatient.userSurname1}"/> 
                           			<c:out value="${service.es.adTentry.adTusersByFkpatient.userSurname2}"/>
                       			</td>
                       			<td>
                       				${service.es.adTentry.adTusersByFkpatient.adTtypeDocument.typedocumentShortname} - 
                       				${service.es.adTentry.adTusersByFkpatient.userDocument}
                       			</td>
                       			<td>${service.es.adTservices.serviceName}</td>
                       			<td>${service.es.entryserviceStartdate}</td>
                       			<td>${service.es.adTfrencuency.frecuencyName}</td>
                       			<td style="color:${service.status.get(1)};">${service.status.get(0)}</td>
                       			<td>
                       				<a href="${pageContext.request.contextPath}/user/details/service-${service.es.pkentryservices}"
                       				 class="dt btn menu-icon color-black btn-default btn-xs">
                       					<i class="fa fa-cog"> ver detalle</i>
                       				</a>
                       			</td>
                       		</tr>
                       	</c:forEach>
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
          </div>
          
          <div class="row">
            <div class="col-xs-12">
              <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> 
                      <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> 
                      </span>Pacientes completados - Estos pacientes desaparecer&aacute;n despu&eacute;s de 7 d&iacute;as de completados
                    </h3>
                  </div>
                  <div class="panel-body  table-responsive bordergray1">
                    <table class="table hover data-table">
                      <thead>
                        <tr>
                          <th>Nombre paciente</th>
                          <th>Identificaci&oacute;n</th>
                          <th>Nombre servicio</th>
                          <th>F. Inicio</th>
                          <th>Frecuencia</th>
                          <th>Estado</th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody class="tbody-2">
                        <c:forEach var="service" items="${data.services_noact}">
                       		<tr>
                       			<td>
                       				<c:out value="${service.es.adTentry.adTusersByFkpatient.userName1}"/> 
                           			<c:out value="${service.es.adTentry.adTusersByFkpatient.userName2}"/> 
                           			<c:out value="${service.es.adTentry.adTusersByFkpatient.userSurname1}"/> 
                           			<c:out value="${service.es.adTentry.adTusersByFkpatient.userSurname2}"/>
                       			</td>
                       			<td>
                       				${service.es.adTentry.adTusersByFkpatient.adTtypeDocument.typedocumentShortname} - 
                       				${service.es.adTentry.adTusersByFkpatient.userDocument}
                       			</td>
                       			<td>${service.es.adTservices.serviceName}</td>
                       			<td>${service.es.entryserviceStartdate}</td>
                       			<td>${service.es.adTfrencuency.frecuencyName}</td>
                       			<td style="color:${service.status.get(1)};">${service.status.get(0)}</td>
                       			<td>
                       				<a href="${pageContext.request.contextPath}/user/details/service-${service.es.pkentryservices}"
                       				 class="dt btn menu-icon color-black btn-default btn-xs">
                       					<i class="fa fa-cog"> ver detalle</i>
                       				</a>
                       			</td>
                       		</tr>
                       	</c:forEach>
                      </tbody>
                    </table>
                  </div>
                </div>
            </div>
          </div>
            
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
  <!-- .container --> 
<!-- .content -->
  
  
  
<!-- Footer Start -->


<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqprint.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-validate.js"></script>
<!--[if lt IE 9]>
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script> 
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/caroufredsel.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jlinq.js"></script> 

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/fullcalendar.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/lang/es.js'></script>

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/moment.min.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/daterangepicker.js'></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/prettyphoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/theme.js"></script>
 


<!-- Specific Page Scripts Put Here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/google-code-prettify/prettify.js"></script>
<script type="text/javascript" >
	$('.data-table').dataTable({
	    language: {
	        "sProcessing":     "Procesando...",
	        "sLengthMenu":     "Mostrar _MENU_ registros",
	        "sZeroRecords":    "No se encontraron resultados",
	        "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
	        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
	        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
	        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
	        "sInfoPostFix":    "",
	        "sSearch":         "Buscar:  ",
	        "sUrl":            "",
	        "sInfoThousands":  ",",
	        "sLoadingRecords": "Cargando...",
	        "oPaginate": {
	            "sFirst":    "Primero",
	            "sLast":     "�ltimo",
	            "sNext":     "Siguiente",
	            "sPrevious": "Anterior"
	        },
	        "oAria": {
	            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
	            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
	        }
	    },
	    "paging":false,
	    "searching":false,
	    "info":false,
	    "scrollY": "200px",
	    "scrollCollapse": false,
	});
</script>

</body>
</html>