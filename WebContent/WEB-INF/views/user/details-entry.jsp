  <%@include file="header.jsp" %>
  	<style>
  		input[type="text"]{
  			padding: 4px;
  		}
  		input[type="number"]{
  			padding: 7px;
  		}
  		select {
		    padding: 6px;
		}
		@media screen and (max-width:768px){
			 .fpsel{
			 	width: 100px !important;
			 }
		 	 .nropin_div{
		 	 	padding-left: 0px !important;
		 	 	padding-right: 0px !important;
		 	 }
		}
  	</style>
    <!-- Middle Content Start -->
        
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <!-- vd_head-section -->
          <!-- vd_title-section -->
          
          <div class="vd_content-section clearfix" style="min-height: 660px;">
          
          <div class="row">
            <div class="col-xs-12">
              <div class="panel widget">
                  <div class="panel-heading vd_bg-grey">
                    <h3 class="panel-title"> 
                      <span class="menu-icon"> <i class="fa fa-dot-circle-o"></i> 
                      </span> Detalles del servicio
                    </h3>
                  </div>
                  <div class="panel-body bordergray1 pedit1">
		            <div class="col-xs-12">
		              <div class="panel widget light-widget">
		                  <div class="panel-heading" style="margin-bottom:20px !important;">
		                  	<div class="col-md-10">
			                    <h3 class="panel-title"> 
			                      </span> Informaci&oacute;n general del ingreso
			                    </h3>
			                </div>
			                <div class="col-md-2">
			                	<c:if test="${data.status}">
								   <div class="">
			                  			<button class="btn  btn-block btn-primary close-entry" id="${data.entry.pkentry}">
			                  			 DAR DE ALTA
			                  			</button>
			                  		</div>
								</c:if>
			                </div>
		                  </div>
		                  <div class="panel-body bordergray1">
		                  	<div class="row mb-5 eliminar_padding">
		                  		<div class="col-lg-5 col-md-5 col-sm-6">
		                  			<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Paciente: 
			                          <label class="control-label">
			                          	${data.entry.adTusersByFkpatient.userName1} 
			                          	${data.entry.adTusersByFkpatient.userName2} 
			                          	${data.entry.adTusersByFkpatient.userSurname1} 
			                          	${data.entry.adTusersByFkpatient.userSurname2}
			                          </label>
				                    </div>
				                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Edad: <label class="control-label">
			                          		${data.entry.adTusersByFkpatient.userAge}
			                          		<c:choose>
											  <c:when test="${data.entry.adTusersByFkpatient.userAgetype == 'A'}">
											    A�os
											  </c:when>
											  <c:otherwise>
											    Meses
											  </c:otherwise>
											</c:choose>
			                          	</label>
			                        </div>
			                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Tel&eacute;fono 1: <label class="control-label">${data.entry.adTusersByFkpatient.userPhone1}</label>
				                    </div>
				                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Tel&eacute;fono 2: <label class="control-label">${data.entry.adTusersByFkpatient.userPhone2}</label>
				                    </div>
				                    <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Direcci&oacute;n: <label class="control-label eliminar_padding">${data.entry.adTusersByFkpatient.userAddress}</label>
			                        </div>
			                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Entidad: <label class="control-label">${data.entry.adTentityplan.adTentity.entityName}</label>
			                        </div>
			                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Diagnostico: <label class="control-label">${data.entry.entryCie10description}</label>
			                        </div> 
		                  		</div>
		                        <div class="col-lg-7 col-md-7 col-sm-6">
		                        	<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Autorizaci&oacute;n: <label class="control-label">${data.entryservice.entryserviceNumerauth} </label>
			                        </div>
			                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Servicio: <label class="control-label">${data.entryservice.adTservices.serviceName}</label>
			                        </div>
			                         <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Cantidad: <label class="control-label">${data.qty_v}</label>
			                        </div>
			                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          F. Inicio: <label class="control-label">${data.entryservice.entryserviceStartdate}</label>
			                        </div>
			                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Frecuencia: <label class="control-label">${data.entryservice.adTfrencuency.frecuencyName}</label>
			                        </div>
			                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Copago: <label class="control-label">
			                          	 $<fmt:formatNumber type="number" pattern="#,##0" value="${data.entryservice.entryserviceCopago}" /></label>
			                        </div>
			                        <div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			                          Frecuencia Copago: 
			                          <label class="control-label">
			                          	<c:choose>
										  <c:when test="${data.entryservice.entryserviceFecuencycopago == '0'}">
										    OTRO
										  </c:when>
										  <c:when test="${data.entryservice.entryserviceFecuencycopago == '5'}">
										    CADA 5 SESI&Oacute;NES
										  </c:when>
										  <c:otherwise>
										    CADA SESI&Oacute;N
										  </c:otherwise>
										</c:choose>
			                          </label>
			                        </div>
		                        </div>
			                </div>
		                  </div>
		                </div>
		            </div>
			          <div class="col-xs-12">
			              <div class="panel widget light-widget">
			                  <div class="panel-heading">
			                    <h3 class="panel-title"> 
			                      </span> Detalle de visitas
			                    </h3>
			                  </div>
			                  <div class="panel-body table-responsive bordergray1">
			                  	<table class="table hover data-tables-3">
			                      <thead>
			                        <tr>
			                          <th class="pointer">Visita <i class="fa fa-sort"></i></th>
			                          <th>Servicio <i class="fa fa-sort"></i></th>
			                          <th>Fecha Atenci&oacute;n <i class="fa fa-sort"></i></th>
			                          <th>Valor recibido <i class="fa fa-sort"></i></th>
			                          <th>Otro Valor <i class="fa fa-sort"></i></th>
			                          <th>Forma pago <i class="fa fa-sort"></th>
			                          <th>Estado <i class="fa fa-sort"></th>
			                          <th></th>
			                        </tr>
			                      </thead>
			                      <tbody class="tbody">
			                      	<c:set var="count" value="0" scope="page" />
			                      	<c:set var="first" value="false" scope="page" />
			                      	<c:set var="verify" value="true" scope="page" />
			                      	<c:forEach var="detail" items="${data.details}">
			                      	  <c:choose>
									  	<c:when test="${(detail.detailDate == null) && (verify == true)}">
									  		<c:set var="first" value="${true}" scope="page"/>
									  		<c:set var="verify" value="${false}" scope="page"/>
									  	</c:when>
									  </c:choose>
									  <c:choose>
									  	<c:when test="${detail.adTstatus.pkstatus == 4}">
									  		<c:set var="first" value="${false}" scope="page"/>
									  		<c:set var="verify" value="${true}" scope="page"/>
									  	</c:when>
									  </c:choose>
										<c:set var="count" value="${count + 1}" scope="page"/>
										<tr>
				                        	<td>${count}</td>
				                        	<td>${detail.adTentryservices.adTservices.serviceName}</td>
				                        	<td>
					                          <div class="controls">
					                          	<c:choose>
												  <c:when test="${detail.detailDate != null}">
												  	${fn:substring(detail.detailDate, 0, 10)}
												  </c:when>
												  <c:when test="${first == true}">
												  	<input type="text" class="date date-a${detail.pkdetail}">
												  </c:when>
												  <c:otherwise>
												  </c:otherwise>
												</c:choose>
					                          </div>
				                        	</td>
				                        	<td>
				                        	  <div class="controls">
				                        	  	<c:choose>
												  <c:when test="${detail.detailDate != null}">
												  	<fmt:setLocale value="es_MX"/>
													$ <fmt:formatNumber value="${detail.detailMoney}" pattern="#,##0" />
												  </c:when>
												  <c:when test="${first == true}">
												  	<input type="number" placeholder="0" class="valrec${detail.pkdetail}">
												  </c:when>
												  <c:otherwise>
												  </c:otherwise>
												</c:choose>
					                          </div>
				                        	</td>
				                        	<td>
				                        	  <div class="controls">
				                        	  	<c:choose>
												  <c:when test="${detail.detailDate != null}">
												  	<fmt:setLocale value="es_MX"/>
													$ <fmt:formatNumber value="${detail.detailOthermoney}" pattern="#,##0" />
												  </c:when>
												  <c:when test="${first == true}">
												  	<input type="number" placeholder="0" class="other${detail.pkdetail}">
												  </c:when>
												  <c:otherwise>
												  </c:otherwise>
												</c:choose>
					                          </div>
				                        	</td>
				                        	<td>
				                        		<c:choose>
												  <c:when test="${detail.detailDate != null}">
												  	<c:choose>
													  	<c:when test="${detail.detailPay == 'PIN'}">
														  	${detail.detailPay} - ${detail.detailPin}
														</c:when>
														<c:otherwise>
														  	${detail.detailPay}
													  	</c:otherwise>
													  </c:choose>
												  </c:when>
												  <c:when test="${first == true}">
												  	<select class="col-md-6 col-sm-6 col-xs-12 fpsel fp${detail.pkdetail}">
					                        			<option value="EFECTIVO">EFECTIVO</option>
					                        			<option value="PIN">PIN</option>
					                        			<option value="OTRO">OTRO</option>
					                        	   </select>
					                        	   <div class="col-md-6 col-sm-6 col-xs-12 nropin_div"><input type="text" class="nropin" placeholder="Ingrese PIN" style="display:none;"></div>
												  </c:when>
												  <c:otherwise>
												  </c:otherwise>
												</c:choose>
				                        	</td>
				                        	<c:choose>
											  <c:when test="${detail.adTstatus.pkstatus == 2}">
											    <td style='color:blue;font-weight: bold;'><i class='fa fa-clock-o'></i> ${detail.adTstatus.statusName}</td>
											  </c:when>
											  <c:when test="${detail.adTstatus.pkstatus == 3}">
											    <td style='color:green;font-weight: bold;'><i class='fa fa-check'></i> ${detail.adTstatus.statusName}</td>
											  </c:when>
											  <c:otherwise>
											    <td style='color:red;font-weight: bold;'><i class='fa fa-times-circle'></i> ${detail.adTstatus.statusName}</td>
											  </c:otherwise>
											</c:choose>
				                        	<td>
				                        		<c:if test="${first == true}">
					                        		<a class="btn btn-success save" id="${detail.pkdetail}">Guardar</a>
					                        	</c:if>
				                        	</td>
				                        	<c:if test="${detail.adTstatus.pkstatus == 2}">
											   <c:set var="first" value="${false}" scope="page"/>
											</c:if>
				                        </tr>
				                     </c:forEach>
			                      </tbody>
			                    </table>
			                  </div>
			                </div>
			            </div>
                  </div>
                </div>
            </div>
          </div>
          </div>
          <!-- .vd_content-section --> 
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>
  <!-- .container --> 
<!-- .content -->
<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/chosen.jquery.js" type="text/javascript"></script>

<script src="${pageContext.request.contextPath}/resources/js/remote-list.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqprint.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/custom/details.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-validate.js"></script>
<!--[if lt IE 9]>
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script> 
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/caroufredsel.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jlinq.js"></script> 

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/fullcalendar.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/lang/es.js'></script>

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/moment.min.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/daterangepicker.js'></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/prettyphoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/theme.js"></script>

<style>
	.date{
		height: 31px;
	}
	.fpsel{
		padding-left: 2px;
		padding-right: 2px;
	}
	.nropin{
		height: 31px;
	}
</style>
<script type="text/javascript">
	$.datepicker.setDefaults($.datepicker.regional['es']);
    $(".date").datepicker({
    	changeMonth:true,
    	changeYear:true,
    	dateFormat: 'yy-mm-dd',
    	maxDate: 'today'
    });
    $('.data-tables-3').dataTable({
        language: {
            "sProcessing":     "Procesando...",
            "sLengthMenu":     "Mostrar _MENU_ registros",
            "sZeroRecords":    "No se encontraron resultados",
            "sEmptyTable":     "Ning&uacute;n dato disponible en esta tabla",
            "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
            "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
            "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix":    "",
            "sSearch":         "Buscar:  ",
            "sUrl":            "",
            "sInfoThousands":  ",",
            "sLoadingRecords": "Cargando...",
            "oPaginate": {
                "sFirst":    "Primero",
                "sLast":     "�ltimo",
                "sNext":     "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        },
        "paging":false,
        "searching":false,
        "info":false,
        "scrollY": "270px",
        "scrollCollapse": false,
        "scrollX": true,
        "autoWidth": false
    });
</script>

<!-- Specific Page Scripts Put Here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/google-code-prettify/prettify.js"></script>
</body>
</html>