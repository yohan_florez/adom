  <%@include file="header.jsp" %>
    <!-- Middle Content Start -->
    
    <style>
    	.content-list .list-wrapper>li:hover{
    		background-color: transparent !important;
    		color: #333 !important;
    	}
    </style>
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <!-- vd_head-section -->
          <!-- vd_title-section -->
          
          <div class="vd_content-section clearfix" style="min-height: 860px;">
          <div class="col-md-8">
                <div class="panel widget light-widget panel-bd-top">
                  <div class="panel-heading no-title"> </div>
                  <div class="panel-body">
                    <h3 class="mgtp--5">Anuncios</h3>
                    <div class="content-list content-blog-small">
                      <ul class="list-wrapper">
                        <c:forEach var="e" items="${data.adverts}">
	                        <li>
	                          <c:if test="${not empty e.advertImage}">
		                          <div class="menu-icon"> 
		                          	<img src="data:image/png;base64,${e.advertImage}" style="width:200px;"> 
		                          </div>
		                      </c:if>
	                          <div class="menu-text">
	                            <h2 class="blog-title font-bold letter-xs">
	                            <a> ${e.advertTitle}</a></h2>
	                            <div class="menu-info">
	                              <div class="menu-date font-xs">${e.advertDate}</div>
	                            </div>
	                            ${e.advertBody}
	                          </div>
	                        </li>
	                   </c:forEach>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>

    <?php if(isset($title) && strlen($title)>0) { ?>
      <script type="text/javascript">
        swal({ 
          title: "<?php echo $title ?>",
          text: "<?php echo $text ?>",
          type: "<?php echo $type ?>",
          confirmButtonText: "Cerrar" 
        });
      </script>
    <?php }?>

    <script type="text/javascript">
      $(".btn-delete").click(function(e){
        var id = $(this).attr("name");
        swal({
          title: "¡Esta operación no se puede deshacer!",   
          text: "¿Estas seguro de continuar?",   
          type: "warning",   
          showCancelButton: true,   
          confirmButtonColor: "#DD6B55",   
          confirmButtonText: "Si, eliminar",   
          closeOnConfirm: false 
        }, function(){
          var params = {'id' : id};
          send('/mommy/admin/eliminar', params);
        });
      });

      function send(page, params) {
        var body = document.body;
        form = document.createElement('form');
        form.method = 'POST';
        form.action = window.location.protocol + "//" + window.location.host + page;
        form.name = 'jsform';
        for (index in params) {
          var input = document.createElement('input');
          input.type = 'hidden';
          input.name = index;
          input.id = index;
          input.value = params[index];
          form.appendChild(input);
        }
        body.appendChild(form);
        form.submit();
      }
    </script>

    <script type="text/javascript">
$(document).ready(function() {

  $("#name").keyup(function () {
        var value = $(this).val().toLowerCase();
        value = value.replace(" ", ".");
        $("#user").val(value);
  });
  
    "use strict";
  
        var form_register_2 = $('#frmreg');
        var error_register_2 = $('.alert-danger', form_register_2);
        var success_register_2 = $('.alert-success', form_register_2);

        form_register_2.validate({
            errorElement: 'div', //default input error message container
            errorClass: 'vd_red', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                name: {
                    required: true,
                    minlength: 6
                },
                user: {
                    required: true,
                    minlength: 6
                },
                 email: {
                    required: true,
                    email: true,
                    remote: {
                        url: "checkEmail",
                        type: "post"
                    }
                }, 
                user: {
                    required: true,
                    minlength: 6,
                    remote: {
                        url: "checkUser",
                        type: "post"
                    }
                },             
                p1: {
                    required: true,
                    minlength: 6
                },
                p2: {
                  equalTo: "#p1"
                }
        
            },

            messages: {
               email: {
                   required: "Debes ingresar tu email",
                   email: "Ingresa una direcci&oacute;n de email valida",
                   remote: "Este email ya se encuentra registrado"
               },
               name: {
                   required: "Ingresa el nombre de la cl&iacute;nica",
                   minlength: $.format("Necesitamos por lo menos {0} caracteres")
               },
               user: {
                   required: "Ingresa el nombre de usuario",
                   minlength: $.format("Necesitamos por lo menos {0} caracteres")
               },
               user: {
                   required: "Ingresa el nombre de usuario",
                   minlength: $.format("Necesitamos por lo menos {0} caracteres"),
                   remote: "Nombre de usuario no disponible"
               },
               p1: {
                   required: "Debes ingresar tu contraseña",
                   minlength: $.format("Necesitamos por lo menos {0} caracteres"),
                   equalTo: "Las contraseñas deben coincidir",
               },
               p2: {
                  required: "Debes ingresar tu contraseña",
                  equalTo: "Las contraseñas deben coincidir",
               }
           },
      
      errorPlacement: function(error, element) {
        if (element.parent().hasClass("vd_checkbox") || element.parent().hasClass("vd_radio")){
          element.parent().append(error);
        } else if (element.parent().hasClass("vd_input-wrapper")){
          error.insertAfter(element.parent());
        }else {
          error.insertAfter(element);
        }
      }, 
      
            invalidHandler: function (event, validator) { //display error alert on form submit              
                success_register_2.hide();
                error_register_2.show();


            },

            highlight: function (element) { // hightlight error inputs
    
        $(element).addClass('vd_bd-red');
        $(element).parent().siblings('.help-inline').removeClass('help-inline hidden');
        if ($(element).parent().hasClass("vd_checkbox") || $(element).parent().hasClass("vd_radio")) {
          $(element).siblings('.help-inline').removeClass('help-inline hidden');
        }

            },

            unhighlight: function (element) { // revert the change dony by hightlight
                $(element)
                    .closest('.control-group').removeClass('error'); // set error class to the control group
            },

            success: function (label, element) {
                label
                    .addClass('valid').addClass('help-inline hidden') // mark the current input as valid and display OK icon
                  .closest('.control-group').removeClass('error').addClass('success'); // set success class to the control group
        $(element).removeClass('vd_bd-red');

          
            },
        }); 
  
  
});
</script>
    

    
  </div>
  <!-- .container --> 
</div>
<!-- .content -->
  
  
  
<!-- Footer Start -->
  
</ul>
<!-- Head menu search form ends -->  
          </div>   
      </div>      
  </div>

</div>

<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqprint.js"></script>  
<!--[if lt IE 9]>
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script> 
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/caroufredsel.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jlinq.js"></script> 

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/fullcalendar.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/lang/es.js'></script>

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/moment.min.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/daterangepicker.js'></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/prettyphoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/theme.js"></script>
 


<!-- Specific Page Scripts Put Here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/google-code-prettify/prettify.js"></script>
<script type="text/javascript" >
	"use strict";
	jQuery(document).ready(function($){prettyPrint();});
</script>

</body>
</html>