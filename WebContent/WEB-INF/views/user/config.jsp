  <%@include file="header.jsp" %>
    <!-- Middle Content Start -->
    
    <div class="vd_content-wrapper">
      <div class="vd_container">
        <div class="vd_content clearfix">
          <!-- vd_head-section -->
          <!-- vd_title-section -->
          
          <div class="vd_content-section clearfix" style="min-height: 860px;">
          	<div class="row">
				<div class="col-md-2">
					<ul class="nav nav-tabs nav-justified">
	                   <li class="active"><a href="#tab1" data-toggle="tab">Cambiar contraseņa</a></li>
	                 </ul>
				</div> 
				<div class="col-md-10 mt30">
					<form name="frm_cp" class="form form-horizontal mt-30">
	                          <div class="row mb-5">
	                        <div class="col-md-4">
	                          <label class="control-label">Nueva contrase&ntilde;a</label>
	                          <div class="controls">
	                            <input type="password" name="pwd1" class="p1" autofocus placeholder="Ingrese contraseņa" >
	                          </div>
	                        </div>
	                    </div>
	                    <div class="row mb-5">
	                        <div class="col-md-4">
	                          <label class="control-label">Confirmar contrase&ntilde;a</label>
	                          <div class="controls">
	                            <input type="password" name="pwd2" class="p2" placeholder="Confirme la contraseņa">
	                          </div>
	                        </div>
	                    </div>
	                    <div class="row mb-5">
	                    	<div class="col-md-4">
	                            <div class="form-group">
	                              <a class="btn  btn vd_btn btn-mommy btn-block mt30 change-pass-2" id="${data.active.pkuser}">Cambiar contraseņa</a>
	                            </div>
	                        </div>
	                       </div>
	                       <div class="row mb-5 div-msg" style="display:none">
	                    	<div class="col-md-6">
	                           <div class="type-alert alert alert-dismissable alert-condensed">
			                        <i class="fa fa-exclamation-circle append-icon"></i>
			                        <a class="msg"></a>
			                   </div>
	                        </div>
	                       </div>
	                </form>
				</div> 
			</div>
          </div>
          <!-- .vd_content-section --> 
          
        </div>
        <!-- .vd_content --> 
      </div>
      <!-- .vd_container --> 
    </div>

  <!-- .container --> 
<!-- .content -->
  
  
  
<!-- Footer Start -->
  
<!-- Head menu search form ends -->  

<!-- .vd_body END  -->
<a id="back-top" href="#" data-action="backtop" class="vd_back-top visible"> <i class="fa  fa-angle-up"> </i> </a>

<!--
<a class="back-top" href="#" id="back-top"> <i class="icon-chevron-up icon-white"> </i> </a> -->

<!-- Javascript =============================================== --> 
<!-- Placed at the end of the document so the pages load faster --> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jqprint.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/custom/config.js"></script>  
<!--[if lt IE 9]>
  <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/excanvas.js"></script>      
<![endif]-->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script> 
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/jquery-ui/jquery-ui.custom.min.js'></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/caroufredsel.js"></script> 
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/plugins.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jlinq.js"></script> 
<!-- CK Editor  -->
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/ckeditor/ckeditor.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/ckeditor/adapters/jquery.js'></script>

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/fullcalendar.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/fullcalendar/lang/es.js'></script>

<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/moment.min.js'></script>
<script type="text/javascript" src='${pageContext.request.contextPath}/resources/plugins/daterangepicker/daterangepicker.js'></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/breakpoints/breakpoints.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/prettyphoto-plugin/js/jquery.prettyPhoto.js"></script> 

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/mCustomScrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/tagsInput/jquery.tagsinput.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/bootstrap-switch/bootstrap-switch.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/blockUI/jquery.blockUI.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/pnotify/js/jquery.pnotify.min.js"></script>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/theme.js"></script>
 


<!-- Specific Page Scripts Put Here -->
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/plugins/google-code-prettify/prettify.js"></script>
<script type="text/javascript" >
	"use strict";
	$(window).load(function() 
	{
		"use strict";	
		$( '[data-rel^="ckeditor"]' ).ckeditor();
		var availableTags = [
			"ActionScript",
			"AppleScript",
			"Asp",
			"BASIC",
			"C",
			"C++",
			"Clojure",
			"COBOL",
			"ColdFusion",
			"Erlang",
			"Fortran",
			"Groovy",
			"Haskell",
			"Java",
			"JavaScript",
			"Lisp",
			"Perl",
			"PHP",
			"Python",
			"Ruby",
			"Scala",
			"Scheme"
		];
		$( "#input-autocomplete" ).autocomplete({
			source: availableTags
		});
		//CKEDITOR.instances['editor1'].setData('${fn:trim(data.email1content)}');
	});
</script>

</body>
</html>