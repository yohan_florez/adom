package com.adom.ecreativos.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

import com.adom.ecreativos.dao.Dao;
import com.adom.ecreativos.vo.AdTusers;

public class Util {
	
	public static Dao dao;
	public static String ERROR_01_LOADFILE = "Error al cargar el fichero";
	
	public static String getSessionStatus(){
		String xml_data = "";
		try {
			String rol = null;
			User user=(User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			Collection<GrantedAuthority> roles = user.getAuthorities();
			for (GrantedAuthority role : roles)
				rol = role.getAuthority().toString();
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			if (auth != null && !auth.getName().equals("anonymousUser") && auth.isAuthenticated()) 
				xml_data = "<root><code>200</code><message>Session OK!</message><role>" + rol + "</role></root>";
			else 
				xml_data = "<root><code>100</code><message>Invalid Session</message></root>";
		} catch (Exception e) {
			e.printStackTrace();
			xml_data = "<root><code>100</code><message>Invalid Session</message></root>";
		}
		return xml_data;
	}
	
	public static Date fechaActual(){
		return new Date();
	}
	public static void startSendReport(){
		final Runnable tarea = new Runnable() {
			  public void run() {
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS");
				String file = "/Users/YohanFlorez/Documents/Exports_Adom/8EJ6W0UMCUVGVRX42J62.xls";
				List<String> properties = new ArrayList<>();
				properties.add("mail.adomsalud.com");
				properties.add("25");
				properties.add("noreply@adomsalud.com");
				properties.add("#zsyg6412");
				Date today = new Date();
				new Mail2("Report " + formatter.format(today), "", "yflorezr@gmail.com", file, "8EJ6W0UMCUVGVRX42J62.xls", properties).run();
				File file_tmp = new File(file);
				/*if(file_tmp.delete())
					System.out.println("Eliminado!");
				else
					System.out.println("No se pudo eliminar el archivo " + file);*/
			  }
			};
		java.util.concurrent.ScheduledExecutorService timer = Executors.newSingleThreadScheduledExecutor();
		timer.scheduleAtFixedRate(tarea, 1, 1, TimeUnit.MINUTES);
	}
	
	public static boolean SaveDateInFile(String message){
		try{
			File file = new File(System.getProperty("catalina.base") + "/test/test_dates.txt");
			FileWriter TextOut = new FileWriter(file, true);
			Date now = new Date();
			TextOut.write(now.toString() + " - " + message);
			TextOut.close();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	public static Date AddDaysToDate(Date date, int increment){
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, increment);
		return calendar.getTime();
	}
	
	public static java.sql.Timestamp getTimeStampFromString(String timeString) {
	    java.sql.Timestamp ts = null;
	    try {
	    ts = Timestamp.valueOf(timeString);
	    } catch(Exception ex) {
	        System.out.println("Error parsing time:  " + ex.getMessage());
	    }
	    return ts;
	}
	
	public static String getCode(int longitud) {
		String cadenaAleatoria = "";
		long milis = new java.util.GregorianCalendar().getTimeInMillis();
		Random r = new Random(milis);
		int i = 0;
		while (i < longitud) {
			char c = (char) r.nextInt(255);
			if ((c >= '0' && c <= '9') || (c >= 'A' && c <= 'Z')) {
				cadenaAleatoria += c;
				i++;
			}
		}
		return cadenaAleatoria;
	}
	
	public static String encriptar(String encript){
		final char[] CONSTS_HEX = { '0','1','2','3','4','5','6','7','8','9','a','b','c','d','e','f' };
        try{
           MessageDigest msgd = MessageDigest.getInstance("MD5");
           byte[] bytes = msgd.digest(encript.getBytes());
           StringBuilder strbCadenaMD5 = new StringBuilder(2 * bytes.length);
           for (int i = 0; i < bytes.length; i++){
               int bajo = (int)(bytes[i] & 0x0f);
               int alto = (int)((bytes[i] & 0xf0) >> 4);
               strbCadenaMD5.append(CONSTS_HEX[alto]);
               strbCadenaMD5.append(CONSTS_HEX[bajo]);
           }
           	return strbCadenaMD5.toString();
        }catch (NoSuchAlgorithmException e) {
           return null;
        }
    }
	
	public static AdTusers getUserActive(){
		try{
			AdTusers usuario;
			User user=(User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			usuario = (AdTusers) dao.findByQuery("from AdTusers u where u.userEmail = '" + user.getUsername() + "'").get(0);
			return usuario;
		}catch(Exception e){
			return null;
		}
	}
	
	static public String loadHTMLFile(String pathname) {
		String content = "";
		File f = null;
		BufferedReader in = null;
		try {
			f = new File(System.getProperty("catalina.base") + "/" + pathname);
			//f = new File("C:/" + pathname);
			if (f.exists()) {
				long len_bytes = f.length();
				trazas("loadHTMLFile", "pathname:" + System.getProperty("catalina.base") + "/" + pathname + ", len:" + len_bytes);
			}
			in = new BufferedReader(new FileReader(f));
			String str;
			while ((str = in.readLine()) != null) {
				// process(str);
				str = str.trim();
				content = content + str;
			}
			in.close();
			return content;
		} catch (Exception e) {
			
		} finally {
			if (in != null)
				try {
					in.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		}
		return content;
	}
	
	private static void trazas(String metodo, String mensaje) {
		System.out.println("[HTMLMail][" + metodo+ "]:[" + mensaje + "]");
	}
	
}