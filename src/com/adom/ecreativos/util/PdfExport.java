package com.adom.ecreativos.util;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.common.PDRectangle;
import org.apache.pdfbox.pdmodel.edit.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDFont;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDPixelMap;
import org.apache.pdfbox.pdmodel.graphics.xobject.PDXObjectImage;
 
public class PdfExport {
	
	public static boolean generate_copago(String outputFileName, List<List<PdfData>> listData,
			String professional, String professionalDocument, String date, String copago) {
		try{
			//outputFileName = "/home/yflorezr/Documentos/Exports_Adom/Simple.pdf";
	        PDDocument document = new PDDocument();
	        PDFont fontPlain = PDType1Font.HELVETICA;
	 
	        PDPage page1 = new PDPage(PDPage.PAGE_SIZE_A4);
	        page1.setRotation(90);
	        document.addPage(page1);
	        PDPageContentStream cos = new PDPageContentStream(document, page1);
	        PDRectangle pageSize = page1.findMediaBox();
	        float pageWidth = pageSize.getWidth();
	        cos.concatenate2CTM(0, 1, -1, 0, pageWidth, 0);
	        cos.beginText();
	        cos.setFont(fontPlain, 8);
	        cos.moveTextPositionByAmount(300, 515);
	        cos.drawString("COMPROBANTE DE ENTREGA DE COPAGOS Y EFECTIVO");
	        cos.endText();
	        
	        int pos = 10 - copago.length();
	        String compNumber = "0000000000".substring(0, pos);
	        compNumber += "" + copago;
	        
	        cos.beginText();
	        cos.setFont(fontPlain, 8);
	        cos.moveTextPositionByAmount(740,515);
	        cos.drawString("No " + compNumber);
	        cos.endText();
	        
	        cos.beginText();
	        cos.setFont(fontPlain, 8);
	        cos.moveTextPositionByAmount(70,460);
	        cos.drawString("PROFESIONAL: " + professional.toUpperCase());
	        cos.endText();
	        
	        cos.beginText();
	        cos.setFont(fontPlain, 8);
	        cos.moveTextPositionByAmount(400,460);
	        cos.drawString("DOCUMENTO: " + professionalDocument);
	        cos.endText();
	        
	        cos.beginText();
	        cos.setFont(fontPlain, 8);
	        cos.moveTextPositionByAmount(600,460);
	        cos.drawString("FECHA DE ENTREGA: " + date);
	        cos.endText();
	        
	        List<PdfData> titles = new ArrayList<PdfData>();
	        titles.add(new PdfData("VALOR A PAGAR", -45));
	        //titles.add(new PdfData("NETO", -58));
	        titles.add(new PdfData("DESCUENTO", -58));
	        titles.add(new PdfData("EF. ENTREGADO", -48));
	        titles.add(new PdfData("EF. RECIBIDO", -40));
	        titles.add(new PdfData("CANTIDAD", -52));
	        titles.add(new PdfData("VLR. SERVICIO", -90));
	        titles.add(new PdfData("SERVICIO", -140));
	        titles.add(new PdfData("ENTIDAD", -170));
	        titles.add(new PdfData("NOMBRE PACIENTE", -100));
	        
	        drawTable(page1, cos, 440, 580, titles, listData);
	        
	        try {
	            //BufferedImage awtImage = ImageIO.read(new File("/home/yflorezr/Documentos/Exports_Adom/logo.png"));
	            BufferedImage awtImage = ImageIO.read(new File(System.getProperty("catalina.base") + "/pdf/logo.png"));
	            PDXObjectImage ximage = new PDPixelMap(document, awtImage);
	            cos.drawXObject(ximage, 60, 500, 150, 40);
	        } catch (Exception e) {
	            System.out.println("No image for you");
	           // Util.SaveDateInFile("Error: " + e.getMessage());
				//Util.SaveDateInFile("- Localized: " + e.getLocalizedMessage());
	        }
	        cos.close();
	        document.save(outputFileName);
	        document.close();
	        return true;
		}catch(Exception e){
			e.printStackTrace();
			//Util.SaveDateInFile("Error: " + e.getMessage());
			//Util.SaveDateInFile("- Localized: " + e.getLocalizedMessage());
			return false;
		}
	}
    
    public static void drawTable(PDPage page, PDPageContentStream contentStream,
            float y, float margin, List<PdfData> titles, List<List<PdfData>> listData){
    		try{
				final int rows = 1;
				final int cols = titles.size();
				final float rowHeight = 20f;
				final float tableHeight = rowHeight * rows;
		
				// draw the rows
				float nexty = y;
				for (int i = 0; i <= rows; i++) {
					contentStream.drawLine(790, nexty, 50, nexty);
					nexty -= rowHeight;
				}
				
				List<Integer> size = new ArrayList<Integer>();
				size.add(-57);
				size.add(-47);
				size.add(-54);
				size.add(-50);
				size.add(-40);
				size.add(-50);
				size.add(-145);
				size.add(-140);
				size.add(-157);
				size.add(-50);
		
				// draw the columns
				float nextx = 790;
				for (int i = 0; i <= cols; i++) {
					contentStream.drawLine(nextx, y, nextx, y - tableHeight);
					nextx += size.get(i);
				}
				
				contentStream.setFont(PDType1Font.HELVETICA, 6);
		
				float textx = 737;
				float texty = y - 15;
				
				for(PdfData d: titles){
					String text = d.getText();
					contentStream.beginText();
					contentStream.moveTextPositionByAmount(textx, texty);
					contentStream.drawString(text);
					contentStream.endText();
					textx += d.getLongitud();
				}
				
				boolean first = true;
				for(List<PdfData> data : listData){
					textx = 748;
					if(first){
						texty -= 15;
						first = false;
					}else
						texty -= 10;
					for(PdfData d: data){
						if(d.getBold())
							contentStream.setFont(PDType1Font.HELVETICA_BOLD, 6);
						String text = d.getText();
						contentStream.beginText();
						contentStream.moveTextPositionByAmount(textx, texty);
						contentStream.drawString(text);
						contentStream.endText();
						textx += d.getLongitud();
					}
				}
    		}catch(Exception e){
    			e.printStackTrace();
    			//Util.SaveDateInFile("Error: " + e.getMessage());
    			//Util.SaveDateInFile("- Localized: " + e.getLocalizedMessage());
    		}
	}
}