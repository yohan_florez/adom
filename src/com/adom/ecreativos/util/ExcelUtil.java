package com.adom.ecreativos.util;

import java.util.List;

import com.adom.ecreativos.vo.AdTsheet;
import com.adom.ecreativos.vo.AdTsheetcolumns;

import jxl.write.WritableSheet;
import jxl.write.biff.RowsExceededException;

public class ExcelUtil {
	
	public static ExcelExport createBook(){
		String fileName = Util.getCode(20) + ".xls";
		return generateSheets(new ExcelExport(fileName));
	}
	
	public static ExcelExport createBook_nocolumns(){
		String fileName = Util.getCode(20) + ".xls";
		ExcelExport e = new ExcelExport(fileName);
		return e;
	}
	
	public static ExcelExport generateSheets(ExcelExport excel){
		List<AdTsheet> lsheets = Util.dao.findAll(AdTsheet.class);
		int p = 0;
		for(AdTsheet s: lsheets){
			WritableSheet sheet = excel.createSheet(s.getSheetName(), p++);
			try {
				sheet.setRowView(0, 26*20);
			} catch (RowsExceededException e1) {
				e1.printStackTrace();
			}
			int c = 0;
			List<AdTsheetcolumns> lcolumns = Util.dao.findByQuery("from AdTsheetcolumns as a where a.adTsheet.pksheet = " + s.getPksheet() + " order by a.pkcolumn ASC");
			for(AdTsheetcolumns sc : lcolumns){
				try {
					sheet.addCell(excel.newLabelTitle(c++, 0, sc.getColumnName()));
					sheet.setColumnView(c, sc.getColumnName().length() + 10);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return excel;
	}

}
