package com.adom.ecreativos.util;

import java.util.List;

import com.adom.ecreativos.vo.AdTentryservices;

public class UserDash {
	
	public AdTentryservices es;
	public List<String> status;
	
	public AdTentryservices getEs() {
		return es;
	}
	public void setEs(AdTentryservices es) {
		this.es = es;
	}
	public List<String> getStatus() {
		return status;
	}
	public void setStatus(List<String> status) {
		this.status = status;
	}

}
