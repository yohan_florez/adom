package com.adom.ecreativos.util;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.CharacterData;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class XmlUtilities {

	public static String clearXmlForm(String xml) {
		try {
			if (xml.contains("<root>") && xml.contains("</root>")) {
				int posInit = xml.indexOf("<root>");
				int posEnd = xml.indexOf("</root>") + 7;
				xml = xml.substring(posInit, posEnd);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return xml;
	}

	public static String readInputStreamAsString(InputStream in) {
		BufferedInputStream bis = new BufferedInputStream(in);
		ByteArrayOutputStream buf = new ByteArrayOutputStream();
		try {
			int result = bis.read();
			while (result != -1) {
				byte b = (byte) result;
				buf.write(b);
				result = bis.read();
			}
		} catch (Exception e) {

		}
		return buf.toString();
	}

	public static String getValueTag(String xml, String tag) {
		String value = null;
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xml));

			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("root");

			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);

				NodeList name = element.getElementsByTagName(tag);
				Element line = (Element) name.item(0);
				value = getCharacterDataFromElement(line);
			}
			return value;
		} catch (Exception ex) {
			ex.printStackTrace();
			value = null;
			return value;
		}
	}

	public static List<String> getCollectionValuesTag(String xml, String tag) {
		List<String> list = new ArrayList<String>();
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xml));

			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName("root");

			for (int i = 0; i < nodes.getLength(); i++) {
				Element element = (Element) nodes.item(i);
				NodeList nodes2 = element.getElementsByTagName(tag);
				for (int j = 0; j < nodes2.getLength(); j++) {
					Element line = (Element) nodes2.item(j);
					list.add(getCharacterDataFromElement(line));
				}
			}
			return list;
		} catch (Exception ex) {
			list = null;
			return list;
		}
	}

	public static List<List<String>> convertToList(String xml_data, String tag) {
		List<List<String>> listData = new ArrayList<List<String>>();
		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(xml_data));

			Document doc = db.parse(is);
			NodeList nodes = doc.getElementsByTagName(tag);

			for (int i = 0; i < nodes.getLength(); i++) {
				try {
					Element element = (Element) nodes.item(i);
					List<String> list = new ArrayList<String>();

					NodeList tags = element.getChildNodes();
					for (int numTag = 0; numTag < tags.getLength(); numTag++) {
						Element line = (Element) tags.item(numTag);
						list.add(getCharacterDataFromElement(line));
					}

					listData.add(list);
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
			}
		} catch (Exception ex) {
			listData = null;
		}
		return listData;
	}

	private static String getCharacterDataFromElement(Element e) {
		Node child = e.getFirstChild();
		if (child instanceof CharacterData) {
			CharacterData cd = (CharacterData) child;
			return cd.getData();
		}
		return "";
	}

}
