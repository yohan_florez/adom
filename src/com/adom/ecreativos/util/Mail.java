package com.adom.ecreativos.util;

import com.goebl.david.Webb;

public class Mail extends Thread{
	
	private String subject;
	private String messageSend;
	private String recipients;
	
	public static String ERROR_01_LOADFILE = "Error al cargar el fichero";
	public static String ERROR_02_SENDMAIL = "Error al enviar el mail";
	
	public Mail(String subject, String messageSend, String recipients) {
		this.subject = subject;
		this.messageSend = messageSend;
		this.recipients = recipients;
	}
	
	@Override
	public void run() {
        try{        
        	//String url = "http://servicios.adomsalud.com/index.php";
        	String url = "http://servicios.yohanflores.com/mail/adom/index.php";
            Webb webb = Webb.create();
            webb.post(url)
            .param("to", recipients)
            .param("subject", subject)
            .param("body", messageSend)
            .ensureSuccess()
            .asString()
            .getBody();
        }
        catch (Exception e) {
        	e.printStackTrace();
            System.out.println("Error enviando las notificaciones de correo electronico. " + e.getMessage());
        }
	}

}
