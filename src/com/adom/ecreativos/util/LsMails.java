package com.adom.ecreativos.util;

import com.adom.ecreativos.vo.AdTusers;

public class LsMails {
	
	private AdTusers user;
	private int qty;
	
	public LsMails(AdTusers user, int qty) {
		this.user = user;
		this.qty = qty;
	}
	
	public AdTusers getUser() {
		return user;
	}
	public void setUser(AdTusers user) {
		this.user = user;
	}
	public int getQty() {
		return qty;
	}
	public void setQty(int qty) {
		this.qty = qty;
	}

}
