package com.adom.ecreativos.util;

import java.io.File;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

public class ExcelExport {
	
	private File worksheetFile;
	private WritableWorkbook workbook = null;
	private String fileName;
	
	public String getFileName() {
		return fileName;
	}
	
	public File getFile() {
		return worksheetFile;
	}

	public ExcelExport(String fileName) {
		try {
			this.fileName = fileName;
			//worksheetFile = new File("/Users/YohanFlorez/Documents/Exports_Adom/" + fileName);
			worksheetFile = new File(System.getProperty("catalina.base") + "/excel/" + fileName);
			System.out.println("File - " + worksheetFile.getAbsolutePath());
			workbook = Workbook.createWorkbook(worksheetFile);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public WritableSheet createSheet(String name, int position){
		return workbook.createSheet(name, position);
	}
	
	public WritableSheet getSheet(String sheetName){
		return workbook.getSheet(sheetName);
	}
	
	public Label newLabelTitle(int column, int row, String value){
		Label label = null;
		try {
			WritableFont fontTitle = new WritableFont(WritableFont.ARIAL);
			fontTitle.setBoldStyle(WritableFont.BOLD);
			WritableCellFormat cellFormat = new WritableCellFormat(fontTitle);
			label = new Label(column,row,value,cellFormat);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return label;
	}
	
	public Label newLabel(int column, int row, String value){
		Label label = null;
		try {
			label = new Label(column,row,value);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return label;
	}
	
	public void addLabel(WritableSheet sheet, Label label, char type){
		try {
			switch (type) {
			case 'S':
				sheet.addCell(label);
				break;
			case 'D':
				WritableCellFormat twodblfmt = new WritableCellFormat(NumberFormats.FORMAT1);
				Double valDouble = Double.parseDouble(label.getContents());
				Number numberdbl = new Number(label.getColumn(), label.getRow(), valDouble, twodblfmt);
				sheet.addCell(numberdbl);
				break;
			case 'I':
				WritableCellFormat intfmt = new WritableCellFormat(NumberFormats.INTEGER);
				Integer valInt = Integer.parseInt(label.getContents());
				Number numberint = new Number(label.getColumn(), label.getRow(), valInt, intfmt);
				sheet.addCell(numberint);
				break;
			case 'T':
				sheet.addCell(label);
				break;
			}
			int cellTitleSize = sheet.getCell(label.getColumn(), 0).getContents().length();
			int cellContentSize =  sheet.getCell(label.getColumn(), label.getRow()).getContents().length();
			sheet.setColumnView(label.getColumn(), cellTitleSize > cellContentSize ? cellTitleSize : cellContentSize + 5);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public int getNumbersOfSheets(){
		return workbook.getNumberOfSheets();
	}
	
	public void write(){
		try {
			workbook.write();
			workbook.close();
			System.out.println("Excel Path: " + worksheetFile.getAbsolutePath());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
