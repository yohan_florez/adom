package com.adom.ecreativos.util;

public class PdfData {
	
	private String text;
	private int longitud;
	private boolean bold;
	
	public PdfData(String text, int longitud) {
		this.text = text;
		this.longitud = longitud;
		bold = false;
	}
	
	public PdfData(String text, int longitud, boolean bold) {
		this.text = text;
		this.longitud = longitud;
		this.bold = bold;
	}
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public int getLongitud() {
		return longitud;
	}
	public void setLongitud(int longitud) {
		this.longitud = longitud;
	}
	public boolean getBold() {
		return this.bold;
	}
	public void setBold(boolean bold) {
		this.bold = bold;
	}

}
