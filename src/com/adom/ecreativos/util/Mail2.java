package com.adom.ecreativos.util;

import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class Mail2 extends Thread{
	
	private String subject;
	private String messageSend;
	private String recipients;
	private List<String> properties;
	private String filePath;
	private String fileName;
	
	public static String ERROR_01_LOADFILE = "Error al cargar el fichero";
	public static String ERROR_02_SENDMAIL = "Error al enviar el mail";
	
	public Mail2(String subject, String messageSend, String recipients, String filePath, String fileName, List<String>properties) {
		this.subject = subject;
		this.messageSend = messageSend;
		this.recipients = recipients;
		this.properties = properties;
		this.filePath = filePath;
		this.fileName = fileName;
	}
	
	@Override
	public void run() {
		Properties props = new Properties();
        String destinatarios[];
        try{        
        	//-----------------------------------------------------------
        	props.put("mail.smtp.host", properties.get(0)); 
            props.put("mail.smtp.starttls.enable", "true"); 
            props.put("mail.smtp.port", properties.get(1)); 
            props.put("mail.smtp.user", properties.get(2)); 
            props.put("mail.smtp.user.password", properties.get(3));
            props.put("mail.smtp.user.recipients", recipients);
            props.put("mail.smtp.auth", "true");
            props.put("mail.debug", "true");
            
	        Session session = Session.getDefaultInstance(props, null);
	        session.setDebug(true);
	        destinatarios = props.getProperty("mail.smtp.user.recipients").toString().split(";");
	        
            BodyPart texto = new MimeBodyPart(); 
            texto.setText(messageSend);  
            
            BodyPart adjunto = new MimeBodyPart();
            adjunto.setDataHandler(new DataHandler(new FileDataSource(filePath)));
            adjunto.setFileName(fileName);

            MimeMultipart multiParte = new MimeMultipart();
            multiParte.addBodyPart(texto);
            multiParte.addBodyPart(adjunto);
	        
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(props.getProperty("mail.smtp.user").toString()));
            
            for(int i = 0; i<destinatarios.length; i ++)
            	message.addRecipient(Message.RecipientType.TO,new InternetAddress(destinatarios[i].trim()));      
            message.setSubject(subject); 
            message.setContent(multiParte);
            //message.setText(messageSend, "utf-8", "html");

            Transport t = session.getTransport("smtp");
            t.connect(props.getProperty("mail.smtp.user").toString(), props.getProperty("mail.smtp.user.password").toString());
            t.sendMessage(message, message.getAllRecipients());

            t.close();
            System.out.println("Correo enviado con exito");
        }
        catch (Exception e) {
        	e.printStackTrace();
            System.out.println("Error enviando las notificaciones de correo electronico. " + e.getMessage());
        }
	}

}
