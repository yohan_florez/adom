package com.adom.ecreativos.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;

@Repository
@SuppressWarnings("unchecked")
public class DaoClass extends HibernateDaoSupport implements Dao {

	@Autowired
	public DaoClass(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	public void saveOrUpdate(Object entity) {
		getHibernateTemplate().saveOrUpdate(entity);
		getHibernateTemplate().flush();
	}

	public void saveOrUpdateAll(Object[] entities) {
		for (int i = 0; i < entities.length; i++) {
			saveOrUpdate(entities[i]);
		}
	}

	public <T> List<T> findAll(Class<T> entityClass) {
		final List<T> entities = getHibernateTemplate().loadAll(entityClass);
		return entities;
	}

	public <T> T findById(Class<T> entityClass, Serializable id) {
		getHibernateTemplate().initialize(null);
		final T entity = getHibernateTemplate().get(entityClass, id);
		return entity;
	}

	public <T> List<T> findByExample(Class<T> entity) {
		final List<T> entities = getHibernateTemplate().findByExample(entity);
		return entities;
	}

	public <T> List<T> findByQuery(String hql) {
		final List<T> entities = getHibernateTemplate().find(hql);
		return entities;
	}

	public <T> List<T> findByQueryWithPatams(String hql, Object[] values) {
		final List<T> entities = getHibernateTemplate().find(hql, values);
		return entities;
	}

	public void delete(Object entity) {
		getHibernateTemplate().delete(entity);
		getHibernateTemplate().flush();
	}

	public void delete(Object[] entities) {
		for (int i = 0; i < entities.length; i++) {
			delete(entities[i]);
		}
	}
	
	public BigInteger postgrstSeqNextVal(String esquema, String seqName){
		BigInteger nextVal = (BigInteger) getSession().createSQLQuery("SELECT nextval('"+esquema+"."+seqName+"')").uniqueResult();
		return nextVal;
	}
	
	public List<Object[]> executeQuery(String hql){
		Query query = getSession().createQuery(hql);
		List<Object[]> results = query.list();
		return results;
	}

	@Override
	public BigDecimal executeQueryForSum(String hql) {
		Query query = getSession().createQuery(hql);
		Object resp = query.uniqueResult();
		resp = resp == null? '0' : resp;
		BigDecimal sum = new BigDecimal(resp.toString());
		return sum;
	}
	
	@Override
	public long count(String entity) throws DataAccessException {
	   @SuppressWarnings("rawtypes")
	   List result = getHibernateTemplate().find("select count(*) from " + entity);
	   return ((Long)result.get(0)).longValue();
	}
	
	@Override
	public long sumByHql(String hql) throws DataAccessException {
	   @SuppressWarnings("rawtypes")
	   List result = getHibernateTemplate().find(hql);
	   if(result.get(0) == null)
		   return 0;
	   else
		   return ((Long)result.get(0)).longValue();
	}
	
	@Override
	public int countByHql(String hql) throws DataAccessException {
	   int r = -1;
	   @SuppressWarnings("rawtypes")
	   List result = getHibernateTemplate().find(hql);
	   try {
		   r = Integer.parseInt(result.get(0).toString());
		} catch (Exception e) {
			e.printStackTrace();
		}
	   return r;
	}
	
}
