package com.adom.ecreativos.dao;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;

public interface Dao {  
  
    /**
     * Guarda o actualiza una entidad.
     * @param entity entidad a guardar o actualizar.
     */
    public void saveOrUpdate(Object entity);  
  
    /**
     * Guarda o actualiza unas entidades.
     * @param entities entidades a guradar o actualizar.
     */
    public void saveOrUpdateAll(Object[] entities);  
  
    /**
     * Busca todas las entidades de una determinada clase.
     * @param entityClass clase de entidad a bsucar.
     * @return lista de entidades de la clase buscada.
     */
    public <T> List<T> findAll(Class<T> entityClass); 
    
    /**
     * Busca una entidad con determinado identificador.
     * @param entityClass clase de entidad a bsucar
     * @param id identificador de la entidad a buscar.
     * @return Entidad encontrada.
     */
    public <T> T findById(Class<T> entityClass, Serializable id);  
  
    
    /**
     * Busca deacuerdo al objeto entidad dado.
     * @param entity entidad ejemplo para la busqueda.
     * @return Lista con 0 o mas instancias encontradas.
     */
    public <T> List<T> findByExample(Class<T> entity); 
    
    /**
     * Busca de acuerdo a la consulta ingresada.
     * @param hql consulta.
     * @return Lista con los resultados de la consulta.
     */
    public <T> List<T> findByQuery(String hql);
  
    /**
     * Busca de acuerdo a la consulta con los aprametros dados. 
     * @param hql consulta.
     * @param values parametros para asociar a la consulta
     * @return Lista con los resultados de la consulta.
     */
    public <T> List<T> findByQueryWithPatams(String hql, Object[] values);
    
    /**
     * Elimina la entidad dada.
     * @param entity entidad a eliminar.
     */
    public void delete(Object entity);
    
    /**
     * Elimina el grupo de entidades dadas del mismo tipo.
     * @param entities Grupo de entidades.
     */
    public void delete(Object[] entities);
    
    /**
     * Obtiene el siguiente valor de la secuencia. 
     * @param seqName nombre de la secuencia.
     */
    public BigInteger postgrstSeqNextVal(String esquema, String seqName);
    
    /**
     * Ejecuta la consulta.
     * @param hql consulta a ejecutar
     * @return resultado de la consulta en una lista.
     */
    public List<Object[]> executeQuery(String hql);
    
    /**
     * Ejecuta la consulta.
     * @param hql consulta a ejecutar
     * @return resultado de la consulta en una lista.
     */
    public BigDecimal executeQueryForSum(String hql);
   
    /**
     * Ejecuta la consulta.
     * @param entity entidad a consultar
     * @return resultado de la consulta en un valor n�merico.
     */
    public long count(String entity);
    
    /**
     * Ejecuta la consulta.
     * @param hql consulta a ejecutar
     * @return resultado de la consulta en un valor n�merico.
     */
    public int countByHql(String hql);
    
    public long sumByHql(String hql);
} 