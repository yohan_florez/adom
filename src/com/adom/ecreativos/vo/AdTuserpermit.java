package com.adom.ecreativos.vo;

// Generated 1/10/2015 12:27:18 AM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * AdTuserpermit generated by hbm2java
 */
@Entity
@Table(name = "ad_tuserpermit", catalog = "adomsalu_adom_s1")
public class AdTuserpermit implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7398025640789962969L;
	private Long pkuserpermit;
	private AdTpermits adTpermits;
	private AdTusers adTusers;
	private Integer userpermitValue;

	public AdTuserpermit() {
	}

	public AdTuserpermit(AdTpermits adTpermits, AdTusers adTusers,
			Integer userpermitValue) {
		this.adTpermits = adTpermits;
		this.adTusers = adTusers;
		this.userpermitValue = userpermitValue;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "pkuserpermit", unique = true, nullable = false)
	public Long getPkuserpermit() {
		return this.pkuserpermit;
	}

	public void setPkuserpermit(Long pkuserpermit) {
		this.pkuserpermit = pkuserpermit;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkpermit")
	public AdTpermits getAdTpermits() {
		return this.adTpermits;
	}

	public void setAdTpermits(AdTpermits adTpermits) {
		this.adTpermits = adTpermits;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkuser")
	public AdTusers getAdTusers() {
		return this.adTusers;
	}

	public void setAdTusers(AdTusers adTusers) {
		this.adTusers = adTusers;
	}

	@Column(name = "userpermit_value")
	public Integer getUserpermitValue() {
		return this.userpermitValue;
	}

	public void setUserpermitValue(Integer userpermitValue) {
		this.userpermitValue = userpermitValue;
	}

}
