package com.adom.ecreativos.vo;

// Generated 1/10/2015 12:27:18 AM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * AdTup generated by hbm2java
 */
@Entity
@Table(name = "ad_tup", catalog = "adomsalu_adom_s1")
public class AdTup implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5416715849486513420L;
	private Integer pkup;
	private AdTusers adTusersByFkprofessional;
	private AdTusers adTusersByFkpatient;

	public AdTup() {
	}

	public AdTup(AdTusers adTusersByFkprofessional, AdTusers adTusersByFkpatient) {
		this.adTusersByFkprofessional = adTusersByFkprofessional;
		this.adTusersByFkpatient = adTusersByFkpatient;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "pkup", unique = true, nullable = false)
	public Integer getPkup() {
		return this.pkup;
	}

	public void setPkup(Integer pkup) {
		this.pkup = pkup;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkprofessional")
	public AdTusers getAdTusersByFkprofessional() {
		return this.adTusersByFkprofessional;
	}

	public void setAdTusersByFkprofessional(AdTusers adTusersByFkprofessional) {
		this.adTusersByFkprofessional = adTusersByFkprofessional;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkpatient")
	public AdTusers getAdTusersByFkpatient() {
		return this.adTusersByFkpatient;
	}

	public void setAdTusersByFkpatient(AdTusers adTusersByFkpatient) {
		this.adTusersByFkpatient = adTusersByFkpatient;
	}

}
