package com.adom.ecreativos.vo;

// Generated 13/10/2015 09:02:38 AM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * AdTadverts generated by hbm2java
 */
@Entity
@Table(name = "ad_tadverts", catalog = "adomsalu_adom_s1")
public class AdTadverts implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8911581673523206607L;
	private Integer pkadvert;
	private AdTusers adTusers;
	private String advertTitle;
	private String advertBody;
	private String advertImage;
	private String advertDate;

	public AdTadverts() {
	}

	public AdTadverts(AdTusers adTusers, String advertTitle, String advertBody,
			String advertImage, String advertDate) {
		this.adTusers = adTusers;
		this.advertTitle = advertTitle;
		this.advertBody = advertBody;
		this.advertImage = advertImage;
		this.advertDate = advertDate;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "pkadvert", unique = true, nullable = false)
	public Integer getPkadvert() {
		return this.pkadvert;
	}

	public void setPkadvert(Integer pkadvert) {
		this.pkadvert = pkadvert;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "advert_createby")
	public AdTusers getAdTusers() {
		return this.adTusers;
	}

	public void setAdTusers(AdTusers adTusers) {
		this.adTusers = adTusers;
	}

	@Column(name = "advert_title", length = 100)
	public String getAdvertTitle() {
		return this.advertTitle;
	}

	public void setAdvertTitle(String advertTitle) {
		this.advertTitle = advertTitle;
	}

	@Column(name = "advert_body")
	public String getAdvertBody() {
		return this.advertBody;
	}

	public void setAdvertBody(String advertBody) {
		this.advertBody = advertBody;
	}

	@Column(name = "advert_image")
	public String getAdvertImage() {
		return this.advertImage;
	}

	public void setAdvertImage(String advertImage) {
		this.advertImage = advertImage;
	}

	@Column(name = "advert_date")
	public String getAdvertDate() {
		return this.advertDate;
	}

	public void setAdvertDate(String advertDate) {
		this.advertDate = advertDate;
	}

}
