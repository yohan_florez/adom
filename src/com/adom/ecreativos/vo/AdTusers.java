package com.adom.ecreativos.vo;

// Generated 6/10/2015 02:30:40 AM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * AdTusers generated by hbm2java
 */
@Entity
@Table(name = "ad_tusers", catalog = "adomsalu_adom_s1")
public class AdTusers implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9167295348576742437L;
	private Long pkuser;
	private AdAccounttype adAccounttype;
	private AdTtypeDocument adTtypeDocument;
	private AdTspecialty adTspecialty;
	private String userName1;
	private String userName2;
	private String userSurname1;
	private String userSurname2;
	private String userPhone1;
	private String userPhone2;
	private String userEmail;
	private String userAddress;
	private String userGender;
	private Integer userTyperips;
	private Integer userAge;
	private String userAgetype;
	private String userDistrict;
	private String userZone;
	private Integer userType;
	private String userNumberaccount;
	private String userCodebank;
	private String userDocument;
	private Date userBirthdate;
	private Boolean userEnabled;
	private Set<AdTentry> adTentriesForFkcreateby = new HashSet<AdTentry>(0);
	private Set<AdTuserpermit> adTuserpermits = new HashSet<AdTuserpermit>(0);
	private Set<AdTentry> adTentriesForFkpatient = new HashSet<AdTentry>(0);
	private Set<AdTlogin> adTlogins = new HashSet<AdTlogin>(0);
	private Set<AdTup> adTupsForFkprofessional = new HashSet<AdTup>(0);
	private Set<AdTobservations> adTobservationses = new HashSet<AdTobservations>(
			0);
	private Set<AdTdetails> adTdetailses = new HashSet<AdTdetails>(0);
	private Set<AdTup> adTupsForFkpatient = new HashSet<AdTup>(0);

	public AdTusers() {
	}

	public AdTusers(String userName1, String userSurname1) {
		this.userName1 = userName1;
		this.userSurname1 = userSurname1;
	}

	public AdTusers(AdAccounttype adAccounttype,
			AdTtypeDocument adTtypeDocument, AdTspecialty adTspecialty,
			String userName1, String userName2, String userSurname1,
			String userSurname2, String userPhone1, String userPhone2,
			String userEmail, String userAddress, String userGender,
			Integer userTyperips, Integer userAge, String userAgetype,
			String userDistrict, String userZone, Integer userType,
			String userNumberaccount, String userCodebank, String userDocument,
			Date userBirthdate, Boolean userEnabled,
			Set<AdTentry> adTentriesForFkcreateby,
			Set<AdTuserpermit> adTuserpermits,
			Set<AdTentry> adTentriesForFkpatient, Set<AdTlogin> adTlogins,
			Set<AdTup> adTupsForFkprofessional,
			Set<AdTobservations> adTobservationses,
			Set<AdTdetails> adTdetailses, Set<AdTup> adTupsForFkpatient) {
		this.adAccounttype = adAccounttype;
		this.adTtypeDocument = adTtypeDocument;
		this.adTspecialty = adTspecialty;
		this.userName1 = userName1;
		this.userName2 = userName2;
		this.userSurname1 = userSurname1;
		this.userSurname2 = userSurname2;
		this.userPhone1 = userPhone1;
		this.userPhone2 = userPhone2;
		this.userEmail = userEmail;
		this.userAddress = userAddress;
		this.userGender = userGender;
		this.userTyperips = userTyperips;
		this.userAge = userAge;
		this.userAgetype = userAgetype;
		this.userDistrict = userDistrict;
		this.userZone = userZone;
		this.userType = userType;
		this.userNumberaccount = userNumberaccount;
		this.userCodebank = userCodebank;
		this.userDocument = userDocument;
		this.userBirthdate = userBirthdate;
		this.userEnabled = userEnabled;
		this.adTentriesForFkcreateby = adTentriesForFkcreateby;
		this.adTuserpermits = adTuserpermits;
		this.adTentriesForFkpatient = adTentriesForFkpatient;
		this.adTlogins = adTlogins;
		this.adTupsForFkprofessional = adTupsForFkprofessional;
		this.adTobservationses = adTobservationses;
		this.adTdetailses = adTdetailses;
		this.adTupsForFkpatient = adTupsForFkpatient;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "pkuser", unique = true, nullable = false)
	public Long getPkuser() {
		return this.pkuser;
	}

	public void setPkuser(Long pkuser) {
		this.pkuser = pkuser;
	}

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fkaccounttype")
	public AdAccounttype getAdAccounttype() {
		return this.adAccounttype;
	}

	public void setAdAccounttype(AdAccounttype adAccounttype) {
		this.adAccounttype = adAccounttype;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fktypedocument")
	public AdTtypeDocument getAdTtypeDocument() {
		return this.adTtypeDocument;
	}

	public void setAdTtypeDocument(AdTtypeDocument adTtypeDocument) {
		this.adTtypeDocument = adTtypeDocument;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkspecialty")
	public AdTspecialty getAdTspecialty() {
		return this.adTspecialty;
	}

	public void setAdTspecialty(AdTspecialty adTspecialty) {
		this.adTspecialty = adTspecialty;
	}

	@Column(name = "user_name1", nullable = false)
	public String getUserName1() {
		return this.userName1;
	}

	public void setUserName1(String userName1) {
		this.userName1 = userName1;
	}

	@Column(name = "user_name2")
	public String getUserName2() {
		return this.userName2;
	}

	public void setUserName2(String userName2) {
		this.userName2 = userName2;
	}

	@Column(name = "user_surname1", nullable = false)
	public String getUserSurname1() {
		return this.userSurname1;
	}

	public void setUserSurname1(String userSurname1) {
		this.userSurname1 = userSurname1;
	}

	@Column(name = "user_surname2")
	public String getUserSurname2() {
		return this.userSurname2;
	}

	public void setUserSurname2(String userSurname2) {
		this.userSurname2 = userSurname2;
	}

	@Column(name = "user_phone1")
	public String getUserPhone1() {
		return this.userPhone1;
	}

	public void setUserPhone1(String userPhone1) {
		this.userPhone1 = userPhone1;
	}

	@Column(name = "user_phone2")
	public String getUserPhone2() {
		return this.userPhone2;
	}

	public void setUserPhone2(String userPhone2) {
		this.userPhone2 = userPhone2;
	}

	@Column(name = "user_email")
	public String getUserEmail() {
		return this.userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	@Column(name = "user_address")
	public String getUserAddress() {
		return this.userAddress;
	}

	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}

	@Column(name = "user_gender")
	public String getUserGender() {
		return this.userGender;
	}

	public void setUserGender(String userGender) {
		this.userGender = userGender;
	}

	@Column(name = "user_typerips")
	public Integer getUserTyperips() {
		return this.userTyperips;
	}

	public void setUserTyperips(Integer userTyperips) {
		this.userTyperips = userTyperips;
	}

	@Column(name = "user_age")
	public Integer getUserAge() {
		return this.userAge;
	}

	public void setUserAge(Integer userAge) {
		this.userAge = userAge;
	}

	@Column(name = "user_agetype")
	public String getUserAgetype() {
		return this.userAgetype;
	}

	public void setUserAgetype(String userAgetype) {
		this.userAgetype = userAgetype;
	}

	@Column(name = "user_district")
	public String getUserDistrict() {
		return this.userDistrict;
	}

	public void setUserDistrict(String userDistrict) {
		this.userDistrict = userDistrict;
	}

	@Column(name = "user_zone")
	public String getUserZone() {
		return this.userZone;
	}

	public void setUserZone(String userZone) {
		this.userZone = userZone;
	}

	@Column(name = "user_type")
	public Integer getUserType() {
		return this.userType;
	}

	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	@Column(name = "user_numberaccount")
	public String getUserNumberaccount() {
		return this.userNumberaccount;
	}

	public void setUserNumberaccount(String userNumberaccount) {
		this.userNumberaccount = userNumberaccount;
	}

	@Column(name = "user_codebank")
	public String getUserCodebank() {
		return this.userCodebank;
	}

	public void setUserCodebank(String userCodebank) {
		this.userCodebank = userCodebank;
	}

	@Column(name = "user_document", length = 100)
	public String getUserDocument() {
		return this.userDocument;
	}

	public void setUserDocument(String userDocument) {
		this.userDocument = userDocument;
	}

	@Temporal(TemporalType.DATE)
	@Column(name = "user_birthdate", length = 10)
	public Date getUserBirthdate() {
		return this.userBirthdate;
	}

	public void setUserBirthdate(Date userBirthdate) {
		this.userBirthdate = userBirthdate;
	}

	@Column(name = "user_enabled")
	public Boolean getUserEnabled() {
		return this.userEnabled;
	}

	public void setUserEnabled(Boolean userEnabled) {
		this.userEnabled = userEnabled;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "adTusersByFkcreateby")
	public Set<AdTentry> getAdTentriesForFkcreateby() {
		return this.adTentriesForFkcreateby;
	}

	public void setAdTentriesForFkcreateby(Set<AdTentry> adTentriesForFkcreateby) {
		this.adTentriesForFkcreateby = adTentriesForFkcreateby;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "adTusers")
	public Set<AdTuserpermit> getAdTuserpermits() {
		return this.adTuserpermits;
	}

	public void setAdTuserpermits(Set<AdTuserpermit> adTuserpermits) {
		this.adTuserpermits = adTuserpermits;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "adTusersByFkpatient")
	public Set<AdTentry> getAdTentriesForFkpatient() {
		return this.adTentriesForFkpatient;
	}

	public void setAdTentriesForFkpatient(Set<AdTentry> adTentriesForFkpatient) {
		this.adTentriesForFkpatient = adTentriesForFkpatient;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "adTusers")
	public Set<AdTlogin> getAdTlogins() {
		return this.adTlogins;
	}

	public void setAdTlogins(Set<AdTlogin> adTlogins) {
		this.adTlogins = adTlogins;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "adTusersByFkprofessional")
	public Set<AdTup> getAdTupsForFkprofessional() {
		return this.adTupsForFkprofessional;
	}

	public void setAdTupsForFkprofessional(Set<AdTup> adTupsForFkprofessional) {
		this.adTupsForFkprofessional = adTupsForFkprofessional;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "adTusers")
	public Set<AdTobservations> getAdTobservationses() {
		return this.adTobservationses;
	}

	public void setAdTobservationses(Set<AdTobservations> adTobservationses) {
		this.adTobservationses = adTobservationses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "adTusers")
	public Set<AdTdetails> getAdTdetailses() {
		return this.adTdetailses;
	}

	public void setAdTdetailses(Set<AdTdetails> adTdetailses) {
		this.adTdetailses = adTdetailses;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "adTusersByFkpatient")
	public Set<AdTup> getAdTupsForFkpatient() {
		return this.adTupsForFkpatient;
	}

	public void setAdTupsForFkpatient(Set<AdTup> adTupsForFkpatient) {
		this.adTupsForFkpatient = adTupsForFkpatient;
	}

}
