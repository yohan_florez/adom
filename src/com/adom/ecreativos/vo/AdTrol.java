package com.adom.ecreativos.vo;

// Generated 24/09/2015 10:49:00 AM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * AdTrol generated by hbm2java
 */
@Entity
@Table(name = "ad_trol", catalog = "adomsalu_adom_s1")
public class AdTrol implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2407713220864632168L;
	private Integer pkrol;
	private String rolName;
	private Set<AdTlogin> adTlogins = new HashSet<AdTlogin>(0);

	public AdTrol() {
	}

	public AdTrol(String rolName) {
		this.rolName = rolName;
	}

	public AdTrol(String rolName, Set<AdTlogin> adTlogins) {
		this.rolName = rolName;
		this.adTlogins = adTlogins;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "pkrol", unique = true, nullable = false)
	public Integer getPkrol() {
		return this.pkrol;
	}

	public void setPkrol(Integer pkrol) {
		this.pkrol = pkrol;
	}

	@Column(name = "rol_name", nullable = false)
	public String getRolName() {
		return this.rolName;
	}

	public void setRolName(String rolName) {
		this.rolName = rolName;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "adTrol")
	public Set<AdTlogin> getAdTlogins() {
		return this.adTlogins;
	}

	public void setAdTlogins(Set<AdTlogin> adTlogins) {
		this.adTlogins = adTlogins;
	}

}
