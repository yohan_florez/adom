package com.adom.ecreativos.vo;

// Generated 24/09/2015 10:49:00 AM by Hibernate Tools 3.4.0.CR1

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * AdTspecialty generated by hbm2java
 */
@Entity
@Table(name = "ad_tspecialty", catalog = "adomsalu_adom_s1")
public class AdTspecialty implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4431670306227727474L;
	private Integer pkspecialty;
	private String specialtyName;
	private Set<AdTusers> adTuserses = new HashSet<AdTusers>(0);

	public AdTspecialty() {
	}

	public AdTspecialty(String specialtyName, Set<AdTusers> adTuserses) {
		this.specialtyName = specialtyName;
		this.adTuserses = adTuserses;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "pkspecialty", unique = true, nullable = false)
	public Integer getPkspecialty() {
		return this.pkspecialty;
	}

	public void setPkspecialty(Integer pkspecialty) {
		this.pkspecialty = pkspecialty;
	}

	@Column(name = "specialty_name")
	public String getSpecialtyName() {
		return this.specialtyName;
	}

	public void setSpecialtyName(String specialtyName) {
		this.specialtyName = specialtyName;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "adTspecialty")
	public Set<AdTusers> getAdTuserses() {
		return this.adTuserses;
	}

	public void setAdTuserses(Set<AdTusers> adTuserses) {
		this.adTuserses = adTuserses;
	}

}
