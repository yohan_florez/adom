package com.adom.ecreativos.vo;
// Generated 25-feb-2016 9:41:16 by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * AdTcopagodetailDetail generated by hbm2java
 */
@Entity
@Table(name = "ad_tcopagodetail_detail", catalog = "adomsalu_adom_s1")
public class AdTcopagodetailDetail implements java.io.Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7272702327889223823L;
	private long pkcopagodetaildetail;
	private AdTdetails adTdetails;
	private AdTcopagoDetail adTcopagoDetail;

	public AdTcopagodetailDetail() {
	}

	public AdTcopagodetailDetail(long pkcopagodetaildetail) {
		this.pkcopagodetaildetail = pkcopagodetaildetail;
	}

	public AdTcopagodetailDetail(long pkcopagodetaildetail, AdTdetails adTdetails, AdTcopagoDetail adTcopagoDetail) {
		this.pkcopagodetaildetail = pkcopagodetaildetail;
		this.adTdetails = adTdetails;
		this.adTcopagoDetail = adTcopagoDetail;
	}

	@Id

	@Column(name = "pkcopagodetaildetail", unique = true, nullable = false)
	public long getPkcopagodetaildetail() {
		return this.pkcopagodetaildetail;
	}

	public void setPkcopagodetaildetail(long pkcopagodetaildetail) {
		this.pkcopagodetaildetail = pkcopagodetaildetail;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkdetail")
	public AdTdetails getAdTdetails() {
		return this.adTdetails;
	}

	public void setAdTdetails(AdTdetails adTdetails) {
		this.adTdetails = adTdetails;
	}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "fkcopagodetail")
	public AdTcopagoDetail getAdTcopagoDetail() {
		return this.adTcopagoDetail;
	}

	public void setAdTcopagoDetail(AdTcopagoDetail adTcopagoDetail) {
		this.adTcopagoDetail = adTcopagoDetail;
	}

}
