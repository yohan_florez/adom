package com.adom.ecreativos.web;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.adom.ecreativos.dao.Dao;
import com.adom.ecreativos.services.Adom;
import com.adom.ecreativos.services.AdomClass;
import com.adom.ecreativos.services.Coordinator;
import com.adom.ecreativos.services.CoordinatorClass;
import com.adom.ecreativos.services.Copago;
import com.adom.ecreativos.services.CopagoClass;
import com.adom.ecreativos.services.Entity;
import com.adom.ecreativos.services.EntityClass;
import com.adom.ecreativos.services.Entry;
import com.adom.ecreativos.services.EntryClass;
import com.adom.ecreativos.services.Frecuency;
import com.adom.ecreativos.services.FrecuencyClass;
import com.adom.ecreativos.services.Patient;
import com.adom.ecreativos.services.PatientClass;
import com.adom.ecreativos.services.Professional;
import com.adom.ecreativos.services.ProfessionalClass;
import com.adom.ecreativos.services.Service;
import com.adom.ecreativos.services.ServiceClass;
import com.adom.ecreativos.util.Base64;
import com.adom.ecreativos.util.Util;
import com.adom.ecreativos.util.XmlUtilities;
import com.adom.ecreativos.vo.AdTadom;
import com.adom.ecreativos.vo.AdTadverts;
import com.adom.ecreativos.vo.AdTcopagodetailDetail;
import com.adom.ecreativos.vo.AdTdetails;
import com.adom.ecreativos.vo.AdTentity;
import com.adom.ecreativos.vo.AdTentityplan;
import com.adom.ecreativos.vo.AdTentry;
import com.adom.ecreativos.vo.AdTentryserviceinsumo;
import com.adom.ecreativos.vo.AdTentryservices;
import com.adom.ecreativos.vo.AdTinsumo;
import com.adom.ecreativos.vo.AdTobservations;
import com.adom.ecreativos.vo.AdTrate;
import com.adom.ecreativos.vo.AdTservices;
import com.adom.ecreativos.vo.AdTspecialty;
import com.adom.ecreativos.vo.AdTtypeDocument;
import com.adom.ecreativos.vo.AdTuserpermit;
import com.adom.ecreativos.vo.AdTusers;

@org.springframework.stereotype.Controller
public class AdminController {
	
	private String r;
	
	@Resource
	Dao dao;
	
	@RequestMapping("admin/dashboard")
    public ModelAndView admin_dash() {
		Map<String, Object> model = new HashMap<String, Object>();
		Util.dao = dao;
		Adom a = new AdomClass();
		Entry e = new EntryClass();
		model.put("option", "1");
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		model.put("hours_p", a.getHoursProg());
		model.put("hours_r", a.getHoursReal());
		model.put("tera_p", a.getTeraProg());
		model.put("tera_c", a.getTeraCancel());
		model.put("tera_r", a.getTeraReal());
		model.put("list_np", e.getNotProfessional());
		model.put("notinvoiced", e.getServicesNotInvoiced());
		model.put("irregular", e.getIrregularServices());
		model.put("limits", e.getCopagoLimits());
		return new ModelAndView("/admin/main", "data", model);
    }
	
	@RequestMapping("admin/adverts")
    public ModelAndView adverts() {
		Map<String, Object> model = new HashMap<String, Object>();
		Adom a = new AdomClass();
		Util.dao = dao;
		model.put("option", "9");
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		List<AdTadverts> ladv = new ArrayList<>();
		for(AdTadverts adv: a.getalladverts()){
			FileInputStream mFileInputStream;
			try {
				if(adv.getAdvertImage() != null){
					mFileInputStream = new FileInputStream(adv.getAdvertImage());
					ByteArrayOutputStream bos = new ByteArrayOutputStream();
		            byte[] b = new byte[1024];
		            int bytesRead = 0;
		            while ((bytesRead = mFileInputStream.read(b)) != -1) {
		               bos.write(b, 0, bytesRead);
		            }
		            byte[] ba = bos.toByteArray();
		            String encoded = Base64.encodeBytes(ba);
		            adv.setAdvertImage(encoded);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
            ladv.add(adv);
		}
		model.put("adverts", ladv);
		return new ModelAndView("/admin/adverts", "data", model);
    }
	
	@RequestMapping("admin/reports")
    public ModelAndView reports() {
		Map<String, Object> model = new HashMap<String, Object>();
		Util.dao = dao;
		Entity e = new EntityClass();
		Professional professional = new ProfessionalClass();
		model.put("option", "11");
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		model.put("entities", e.getAllEntities());
		model.put("professionals", professional.getAllProfessionals());
		return new ModelAndView("/admin/reports", "data", model);
    }
	
	@RequestMapping("admin/billing")
    public ModelAndView billing() {
		Map<String, Object> model = new HashMap<String, Object>();
		Util.dao = dao;
		Entity e = new EntityClass();
		model.put("option", "6");
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		model.put("entities", e.getAllEntities());
		return new ModelAndView("/admin/invoice", "data", model);
    }
	
	@RequestMapping("admin/copago")
    public ModelAndView copago() {
		Map<String, Object> model = new HashMap<String, Object>();
		Util.dao = dao;
		Entity e = new EntityClass();
		Professional professional = new ProfessionalClass();
		model.put("option", "12");
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		model.put("entities", e.getAllEntities());
		model.put("professionals", professional.getAllProfessionals());
		return new ModelAndView("/admin/copago", "data", model);
    }
	
	@RequestMapping("admin/savehistory")
    public @ResponseBody String savehistory(String xml_data) {
		Util.dao = dao;
		Entry e = new EntryClass();
		e.saveHistory(xml_data);
		return "OK";
    }
	
	@RequestMapping("admin/services")
    public ModelAndView services() {
		Service s = new ServiceClass();
		Util.dao = dao;
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("option", "8");
		model.put("services", s.getAllServices());
		model.put("insumos", s.getAllInsumos());
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		return new ModelAndView("/admin/services", "data", model);
    }
	
	@RequestMapping("admin/patients")
	public ModelAndView admin_patients(){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("option", "2");
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		model.put("typedocs", dao.findAll(AdTtypeDocument.class));
		return new ModelAndView("/admin/patients", "data", model);
	}
	
	@RequestMapping("admin/configuration")
	public ModelAndView configuration(){
		Util.dao = dao;
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("option", "7");
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		model.put("adom", dao.findByQuery("from AdTadom as a where a.pkadom = 1").get(0));
		model.put("email1content", Util.loadHTMLFile("templates/new.jsp"));
		model.put("email2content", Util.loadHTMLFile("templates/update.jsp"));
		model.put("email3content", Util.loadHTMLFile("templates/assign_s.jsp"));
		model.put("email4content", Util.loadHTMLFile("templates/cancel_s.jsp"));
		model.put("email5content", Util.loadHTMLFile("templates/assign_v.jsp"));
		model.put("email6content", Util.loadHTMLFile("templates/cancel_v.jsp"));
		return new ModelAndView("/admin/config", "data", model);
	}
	
	@RequestMapping(value={"admin/details/entry-{entryid}"}, method=RequestMethod.GET)
	public ModelAndView detail(@PathVariable(value="entryid") int eid){
		Map<String, Object> model = new HashMap<String, Object>();
		Util.dao = dao;
		Entry entry = new EntryClass();
		Service service = new ServiceClass();
		Professional professional = new ProfessionalClass();
		Frecuency frencuency = new FrecuencyClass();
		AdTentry entryv = entry.getEntry(eid);
		model.put("entry", entryv);
		model.put("services", service.getServicesByEntry(eid));
		model.put("invoiced", entry.getAllInvoiced());
		model.put("professionals", professional.getAllProfessionals());
		model.put("frecuency", frencuency.getAllFrencuency());
		model.put("entryservices", entry.getEntryServices(eid));
		model.put("typedocs", dao.findAll(AdTtypeDocument.class));
		model.put("status", entryv.getEntryEnddate() == null ? true: false);
		return new ModelAndView("/admin/details-entry", "data", model);
	}
	
	@RequestMapping("admin/professionals")
	public ModelAndView admin_professionals(){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("typedocs", dao.findAll(AdTtypeDocument.class));
		model.put("specialtys", dao.findAll(AdTspecialty.class));
		model.put("option", "3");
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		return new ModelAndView("/admin/professionals", "data", model);
	}
	
	@RequestMapping("admin/coordinators")
	public ModelAndView admin_coordinators(){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("typedocs", dao.findAll(AdTtypeDocument.class));
		model.put("option", "5");
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		return new ModelAndView("/admin/coordinators", "data", model);
	}
	
	@RequestMapping("admin/entities")
    public ModelAndView entities() {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("option", "4");
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		return new ModelAndView("/admin/entities", "data", model);
    }
	
	@RequestMapping("admin/getHistoryPatients")
    public @ResponseBody String getallpatients() {
		try{
			Patient p = new PatientClass();
			Util.dao = dao;
			List<AdTusers> patients = p.getHistoryPatients();
			r = "<root>";
			for(AdTusers u : patients){
				r += "<user>";
				r += "<id>" + u.getPkuser() + "</id>";
				r += "<type_doc>" + u.getAdTtypeDocument().getTypedocumentName() + "</type_doc>";
				r += "<num_doc>" + u.getUserDocument() + "</num_doc>";
				r += "<full_name>" + u.getUserName1() + " " + u.getUserName2() + " " + u.getUserSurname1() + " " + u.getUserSurname2() + "</full_name>";
				r += "<phone>" + u.getUserPhone1() + "</phone>";
				r += "</user>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/get_observations")
    public @ResponseBody String get_observations(String xml_data) {
		try{
			Entry e = new EntryClass();
			Util.dao = dao;
			List<AdTobservations> ob = e.get_observation(xml_data);
			r = "<root>";
			for(AdTobservations o : ob){
				r += "<observation>";
				r += "<date>" + o.getObservationDate() + "</date>";
				r += "<user>" + o.getAdTusers().getUserName1() + " " +  o.getAdTusers().getUserName2() + " " +  o.getAdTusers().getUserSurname1() + " " +  o.getAdTusers().getUserSurname2() + "</user>";
				r += "<msg>" + o.getObservationText() + "</msg>";
				r += "</observation>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/get_insumos")
    public @ResponseBody String get_insumos(String xml_data) {
		try{
			Entry e = new EntryClass();
			Util.dao = dao;
			List<AdTentryserviceinsumo> esi = e.get_insumos(xml_data);
			r = "<root>";
			for(AdTentryserviceinsumo i : esi){
				r += "<insumo_detail>";
				r +="<id>" + i.getPkentryserviceinsumo() + "</id>";
				r +="<name>" + i.getAdTinsumo().getInsumoName() + "</name>";
				r +="<qty>" + i.getEntryserviceinsumoQty() + "</qty>";
				r +="<fa>" + i.getAdTinvoiced().getInvoicedName() + "</fa>";
				r += "</insumo_detail>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/searchpatients")
    public @ResponseBody String searchpatients(String xml_data) {
		try{
			Patient p = new PatientClass();
			Util.dao = dao;
			List<AdTusers> patients = p.search(xml_data);
			r = "<root>";
			for(AdTusers u : patients){
				r += "<user>";
				r += "<id>" + u.getPkuser() + "</id>";
				r += "<type_doc>" + u.getAdTtypeDocument().getTypedocumentName() + "</type_doc>";
				r += "<num_doc>" + u.getUserDocument() + "</num_doc>";
				r += "<full_name>" + u.getUserName1() + " " + u.getUserName2() + " " + u.getUserSurname1() + " " + u.getUserSurname2() + "</full_name>";
				r += "<phone>" + u.getUserPhone1() + "</phone>";
				r += "</user>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/searchbilling")
    public @ResponseBody String searchbilling(String xml_data) {
		try{
			Adom a = new AdomClass();
			Entry e = new EntryClass();
			Util.dao = dao;
			List<AdTentryservices> services = a.search(xml_data);
			r = "<root>";
			for(AdTentryservices s : services){
				List<String> status = e.getEntryServicesStatusByServices(s.getAdTdetailses());
				if(status.get(0).equals("COMPLETADA")){
					AdTusers u = s.getAdTentry().getAdTusersByFkpatient();
					r += "<service>";
					r += "<id>" + s.getPkentryservices() + "</id>";
					r += "<full_name>" + u.getUserName1() + " " + u.getUserName2() + " " + u.getUserSurname1() + " " + u.getUserSurname2() + "</full_name>";
					r += "<identity>" + u.getAdTtypeDocument().getTypedocumentShortname() + " " + u.getUserDocument() + "</identity>";
					r += "<service_name>" + s.getAdTservices().getServiceName() + "</service_name>";
					r += "<start_date>" + s.getEntryserviceStartdate() + "</start_date>";
					r += "<end_date>" + s.getEntryserviceEnddate() + "</end_date>";
					r += "<entity>" + s.getAdTentry().getAdTentityplan().getAdTentity().getEntityName() + "</entity>";
					r += "<plan>" + s.getAdTentry().getAdTentityplan().getEntityplanName() + "</plan>";
					r += "<invoice_number>" + (s.getEntryserviceInvoicenumber() == null ? "" : s.getEntryserviceInvoicenumber()) + "</invoice_number>";
					r += "<invoice_date>" + s.getEntryserviceInvoicedate() + "</invoice_date>";
					r += "<invoice_value>" + s.getEntryserviceInvoicevalue() + "</invoice_value>";
					r += "<invoice_copago>" + s.getEntryserviceInvoicecopago() + "</invoice_copago>";
					r += "<nro_auth>" + s.getEntryserviceNumerauth() + "</nro_auth>";
					r += "<status>" + status.get(0) + "</status>";
					r += "<color>" + status.get(1) + "</color>";
					r += "</service>";
				}
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/searchcopago")
    public @ResponseBody String searchCopago(String xml_data) {
		try{
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			Copago c = new CopagoClass();
			Util.dao = dao;
			List<AdTdetails> details = c.search(xml_data);
			r = "<root>";
			for(AdTdetails d :  details){
				String copago_f = "";
				AdTentryservices entryservice = d.getAdTentryservices();
				Set<AdTdetails> ldetails = entryservice.getAdTdetailses();
				int status = CopagoClass.getEntryServicesStatusInList(ldetails, XmlUtilities.getValueTag(xml_data, "pid"));
				String status1 = XmlUtilities.getValueTag(xml_data, "e1");
				if(status1.equals("*") || Integer.parseInt(status1) == status){
					if(entryservice.getEntryserviceFecuencycopago() == 1)
						copago_f = "CADA SESI&Oacute;N";
					else if(entryservice.getEntryserviceFecuencycopago() == 5)
						copago_f = "CADA 5 SESIONES";
					else copago_f = "OTRO";
					AdTusers u = entryservice.getAdTentry().getAdTusersByFkpatient();
					String color = status == 4 ? "red" : "";
					color = status == 3 ? "#08D808" : color;
					color = status == 2 ? "blue" : color;
					String status_name = status == 4 ? "CANCELADO" : "";
					status_name = status == 3 ? "COMPLETADO" : status_name;
					status_name = status == 2 ? "PROGRAMADO" : status_name;
					String status_copago = d.getDetailCopago() == 0 ? "SE" : "E";
					String color_copago = d.getDetailCopago() == 0 ? "red" : "#08D808";
					String cr = "";
					String ce = "";
					String dto = "";
					if(d.getDetailCopago() == 1){
						Set<AdTcopagodetailDetail> detailCopagos = d.getAdTcopagodetailDetails();
						for(AdTcopagodetailDetail detail: detailCopagos){
							ce = detail.getAdTcopagoDetail().getCopagodetailCashdelivered().toString();
							cr = detail.getAdTcopagoDetail().getCopagodetailCashreceived().toString();
							dto = detail.getAdTcopagoDetail().getCopagodetailDto().toString();
						}
					}
					r += "<copago>";
						r += "<id>" + entryservice.getPkentryservices() + "</id>";
						r += "<full_name>" + u.getUserName1() + " " + u.getUserName2() + " " + u.getUserSurname1() + " " + u.getUserSurname2() + "</full_name>";
						r += "<document>" + u.getUserDocument() + "</document>";
						r += "<service>" + entryservice.getAdTservices().getServiceName() + "</service>";
						r += "<auth>" + entryservice.getEntryserviceNumerauth().toUpperCase() + "</auth>";
						r += "<total>" + CopagoClass.totalByProfessional(ldetails, XmlUtilities.getValueTag(xml_data, "pid")) + "</total>";
						r += "<complete>" + CopagoClass.statusInListByProfessional(entryservice.getAdTdetailses(), 3, Long.parseLong(XmlUtilities.getValueTag(xml_data, "pid"))) + "</complete>";
						r += "<status>" + status_name + "</status>";
						r += "<color>" + color + "</color>";
						r += "<status_c>" + status_copago + "</status_c>";
						r += "<color_c>" + color_copago + "</color_c>";
						r += "<received>" + cr + "</received>";
						r += "<delivered>" + ce + "</delivered>";
						r += "<dto>" + dto + "</dto>";
						r += "<date_end>" + formatter.format(entryservice.getEntryserviceEnddate()) + "</date_end>";
						r += "<entity>" + entryservice.getAdTentry().getAdTentityplan().getAdTentity().getEntityName() + "</entity>";
						r += "<copago_>" + entryservice.getEntryserviceCopago().intValue() + "</copago_>";
						r += "<frecuency>" + copago_f + "</frecuency>";
						r += "<total_copagos>" + CopagoClass.totalCopago(entryservice.getAdTdetailses()) + "</total_copagos>";
						r += "<total_other>" + CopagoClass.totalOthersCopagos(entryservice.getAdTdetailses()) + "</total_other>";
					r += "</copago>";
				}
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			Util.SaveDateInFile("2: " + e.getMessage());
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/searchcopagobyservices")
    public @ResponseBody String searchcopagobyservices(String xml_data) {
		try{
			Copago c = new CopagoClass();
			Util.dao = dao;
			List<AdTdetails> details = c.searchByServicios(xml_data);
			r = "<root>";
			boolean first = false;
			for(AdTdetails d : details){
				AdTentryservices entryservice = d.getAdTentryservices();
				AdTusers u = entryservice.getAdTentry().getAdTusersByFkpatient();
				AdTusers p = d.getAdTusers();
				AdTusers re = Util.getUserActive();
				if(!first){
					r += "<professional>" + p.getUserName1() + " " + p.getUserName2() + " " + p.getUserSurname1() + " " + p.getUserSurname2() + "</professional>";
					r += "<document>" + p.getUserDocument() + "</document>";
					r += "<by>" + re.getUserName1() + " " + re.getUserName2() + " " + re.getUserSurname1() + " " + re.getUserSurname2() + "</by>";
					first = true;
				}
				r += "<copago>";
					r += "<id>" + entryservice.getPkentryservices() + "</id>";
					r += "<full_name>" + u.getUserName1() + " " + u.getUserName2() + " " + u.getUserSurname1() + " " + u.getUserSurname2() + "</full_name>";
					r += "<service>" + entryservice.getAdTservices().getServiceName() + "</service>";
					r += "<entity>" + entryservice.getAdTentry().getAdTentityplan().getAdTentity().getEntityName() + "</entity>";
					r += "<service_value>" + entryservice.getAdTservices().getServiceValue().intValue() + "</service_value>";
					r += "<complete>" + CopagoClass.statusInListByProfessional(entryservice.getAdTdetailses(), 3, d.getAdTusers().getPkuser()) + "</complete>";
					r += "<ids>" + CopagoClass.getIds(entryservice.getAdTdetailses(), 3, d.getAdTusers().getPkuser().longValue()) + "</ids>";
					r += "<total>" + 
							(CopagoClass.totalCopago(entryservice.getAdTdetailses()).intValue() 
							+ CopagoClass.totalOthersCopagos(entryservice.getAdTdetailses()).intValue())
					+ "</total>";
				r += "</copago>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/searchprofessionals")
    public @ResponseBody String searchprofessionals(String xml_data) {
		try{
			Professional p = new ProfessionalClass();
			Util.dao = dao;
			List<AdTusers> patients = p.search(xml_data);
			r = "<root>";
			for(AdTusers u : patients){
				r += "<user>";
				r += "<id>" + u.getPkuser() + "</id>";
				r += "<type_doc>" + u.getAdTtypeDocument().getTypedocumentName() + "</type_doc>";
				r += "<num_doc>" + u.getUserDocument() + "</num_doc>";
				r += "<full_name>" + u.getUserName1() + " " + u.getUserName2() + " " + u.getUserSurname1() + " " + u.getUserSurname2() + "</full_name>";
				r += "<espec>" + u.getAdTspecialty().getSpecialtyName() + "</espec>";
				r += "<status>" + (u.getUserEnabled() ? 1 : 0) + "</status>";
				r += "</user>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/searchcoord")
    public @ResponseBody String searchcoord(String xml_data) {
		try{
			Coordinator p = new CoordinatorClass();
			Util.dao = dao;
			List<AdTusers> patients = p.search(xml_data);
			r = "<root>";
			for(AdTusers u : patients){
				r += "<user>";
				r += "<id>" + u.getPkuser() + "</id>";
				r += "<type_doc>" + u.getAdTtypeDocument().getTypedocumentName() + "</type_doc>";
				r += "<num_doc>" + u.getUserDocument() + "</num_doc>";
				r += "<full_name>" + u.getUserName1() + " " + u.getUserName2() + " " + u.getUserSurname1() + " " + u.getUserSurname2() + "</full_name>";
				r += "<email>" + u.getUserEmail() + "</email>";
				r += "<status>" + (u.getUserEnabled() ? 1 : 0) + "</status>";
				r += "<permissions>";
				List<AdTuserpermit> lp = Util.dao.findByQuery("from AdTuserpermit as p where p.adTusers.pkuser = " + u.getPkuser());
				for(AdTuserpermit up: lp){
					r += "<permit>";
					r += "<id_permit>" + up.getPkuserpermit() + "</id_permit>";
					r += "<name>" + up.getAdTpermits().getPermitName() + "</name>";
					r += "<value>" + (up.getUserpermitValue() == 1 ? "checked" : "") + "</value>";
					r += "</permit>";
				}
				r += "</permissions>";
				r += "</user>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/getpatient")
    public @ResponseBody String getpatient(String xml_data) {
		try{
			Patient p = new PatientClass();
			Util.dao = dao;
			AdTusers u = p.getPatient(Long.parseLong(XmlUtilities.getValueTag(xml_data, "pid")));
			r = "<root>";
			if(u != null){
				r += "<id>" + u.getPkuser() + "</id>";
				r += "<type_doc>" + u.getAdTtypeDocument().getPktypedocument() + "</type_doc>";
				r += "<num_doc>" + u.getUserDocument() + "</num_doc>";
				r += "<name1>" + u.getUserName1() + "</name1>";
				r += "<name2>" + u.getUserName2() + "</name2>";
				r += "<last1>" + u.getUserSurname1() + "</last1>";
				r += "<last2>" + u.getUserSurname2() + "</last2>";
				r += "<gender>" + u.getUserGender() + "</gender>";
				r += "<type_user>" + u.getUserTyperips() + "</type_user>";
				r += "<age>" + u.getUserAge() + "</age>";
				r += "<type_age>" + u.getUserAgetype() + "</type_age>";
				r += "<fn>" + (u.getUserBirthdate() == null ? "" : u.getUserBirthdate()) + "</fn>";
				r += "<distric>" + u.getUserDistrict() + "</distric>";
				r += "<address>" + u.getUserAddress() + "</address>";
				r += "<zone>" + u.getUserZone() + "</zone>";
				r += "<phone1>" + u.getUserPhone1() + "</phone1>";
				r += "<phone2>" + u.getUserPhone2() + "</phone2>";
				r += "<email>" + u.getUserEmail() + "</email>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/getentity")
    public @ResponseBody String getentity(String xml_data) {
		try{
			Entity p = new EntityClass();
			Util.dao = dao;
			AdTentity e = p.getEntity(xml_data);
			r = "<root>";
			if(e != null){
				r += "<id>" + e.getPkentity() + "</id>";
				r += "<nit>" + e.getEntityNit() + "</nit>";
				r += "<rs>" + e.getEntityRz() + "</rs>";
				r += "<code>" + e.getEntityCode() + "</code>";
				r += "<name>" + e.getEntityName() + "</name>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/getservice")
    public @ResponseBody String getservice(String xml_data) {
		try{
			Service p = new ServiceClass();
			Util.dao = dao;
			AdTservices e = p.getService(xml_data);
			r = "<root>";
			if(e != null){
				r += "<id>" + e.getPkservice() + "</id>";
				r += "<name>" + e.getServiceName() + "</name>";
				r += "<value>" + e.getServiceValue() + "</value>";
				r += "<code>" + e.getServiceCode() + "</code>";
				r += "<type>" + e.getServiceType() + "</type>";
				r += "<type2>" + e.getServiceType2() + "</type2>";
				r += "<hours>" + e.getServiceHours() + "</hours>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/getinsumo")
    public @ResponseBody String getinsumo(String xml_data) {
		try{
			Service p = new ServiceClass();
			Util.dao = dao;
			AdTinsumo e = p.getInsumo(xml_data);
			r = "<root>";
			if(e != null){
				r += "<id>" + e.getPkinsumo() + "</id>";
				r += "<name>" + e.getInsumoName() + "</name>";
				r += "<code>" + e.getInsumoCode() + "</code>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/getcoordinator")
    public @ResponseBody String getcoordinator(String xml_data) {
		try{
			Coordinator c = new CoordinatorClass();
			Util.dao = dao;
			AdTusers u = c.getCoordinator(Long.parseLong(XmlUtilities.getValueTag(xml_data, "cid")));
			r = "<root>";
			if(u != null){
				r += "<id>" + u.getPkuser() + "</id>";
				r += "<type_doc>" + u.getAdTtypeDocument().getPktypedocument() + "</type_doc>";
				r += "<num_doc>" + u.getUserDocument() + "</num_doc>";
				r += "<name1>" + u.getUserName1() + "</name1>";
				r += "<name2>" + u.getUserName2() + "</name2>";
				r += "<last1>" + u.getUserSurname1() + "</last1>";
				r += "<last2>" + u.getUserSurname2() + "</last2>";
				r += "<gender>" + u.getUserGender() + "</gender>";
				r += "<fn>" + (u.getUserBirthdate() == null ? "" : u.getUserBirthdate()) + "</fn>";
				r += "<phone1>" + u.getUserPhone1() + "</phone1>";
				r += "<phone2>" + u.getUserPhone2() + "</phone2>";
				r += "<email>" + u.getUserEmail() + "</email>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/getoneprofessional")
    public @ResponseBody String getprofessional(String xml_data) {
		try{
			Util.dao = dao;
			AdTusers u = dao.findById(AdTusers.class, Long.parseLong(XmlUtilities.getValueTag(xml_data, "pid")));
			r = "<root>";
			if(u != null){
				r += "<id>" + u.getPkuser() + "</id>";
				r += "<type_doc>" + u.getAdTtypeDocument().getPktypedocument() + "</type_doc>";
				r += "<num_doc>" + u.getUserDocument() + "</num_doc>";
				r += "<name1>" + u.getUserName1() + "</name1>";
				r += "<name2>" + u.getUserName2() + "</name2>";
				r += "<last1>" + u.getUserSurname1() + "</last1>";
				r += "<last2>" + u.getUserSurname2() + "</last2>";
				r += "<gender>" + u.getUserGender() + "</gender>";
				r += "<espec>" + u.getAdTspecialty().getPkspecialty() + "</espec>";
				r += "<fn>" + (u.getUserBirthdate() == null ? "" : u.getUserBirthdate()) + "</fn>";
				r += "<district>" + u.getUserDistrict() + "</district>";
				r += "<address>" + u.getUserAddress() + "</address>";
				r += "<zone>" + u.getUserZone() + "</zone>";
				r += "<phone1>" + u.getUserPhone1() + "</phone1>";
				r += "<phone2>" + u.getUserPhone2() + "</phone2>";
				r += "<email>" + u.getUserEmail() + "</email>";
				r += "<number>" + u.getUserNumberaccount() == null ? "" : u.getUserNumberaccount() + "</number>";
				r += "<codbank>" + u.getUserCodebank() == null ? "" : u.getUserCodebank() + "</codbank>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/getPlans")
    public @ResponseBody String getPlans(String xml_data) {
		try{
			Entity e = new EntityClass();
			Util.dao = dao;
			List<AdTentityplan> plans = e.getPlans(Long.parseLong(XmlUtilities.getValueTag(xml_data, "eid")));
			r = "<root>";
			for(AdTentityplan p : plans){
				r += "<plan>";
				r += "<id>" + p.getPkentityplan() + "</id>";
				r += "<name>" + p.getEntityplanName() + "</name>";
				r += "<status>" + p.getEntityplanStatus() + "</status>";
				r += "</plan>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/getEntrys")
    public @ResponseBody String getEntrys(String xml_data) {
		try{
			Entry en = new EntryClass();
			Util.dao = dao;
			List<AdTentry> entry = en.getEntrys(Long.parseLong(XmlUtilities.getValueTag(xml_data, "patient")));
			r = "<root>";
			for(AdTentry e : entry){
				r += "<entry>";
				r += "<entry_id>" + e.getPkentry() + "</entry_id>";
				r += "<patient>" + e.getAdTusersByFkpatient().getPkuser() + "</patient>";
				r += "<start>" + ("" + e.getEntryStartdate()).substring(0, 10) + "</start>";
				r += "<end>" + (e.getEntryEnddate() == null ? "Activo" : e.getEntryEnddate()) + "</end>";
				r += "<cie10>" + e.getEntryCie10() + "</cie10>";
				r += "<cie10_desc>" + e.getEntryCie10description() + "</cie10_desc>";
				r += "</entry>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/getallprofessionals")
    public @ResponseBody String getallprofessionals(String xml_data) {
		try{
			Professional p = new ProfessionalClass();
			Util.dao = dao;
			List<AdTusers> patients = p.getAllProfessionals();
			r = "<root>";
			for(AdTusers u : patients){
				r += "<user>";
				r += "<id>" + u.getPkuser() + "</id>";
				r += "<type_doc>" + u.getAdTtypeDocument().getTypedocumentName() + "</type_doc>";
				r += "<num_doc>" + u.getUserDocument() + "</num_doc>";
				r += "<full_name>" + u.getUserName1() + " " + u.getUserName2() + " " + u.getUserSurname1() + " " + u.getUserSurname2() + "</full_name>";
				r += "<espec>" + u.getAdTspecialty().getSpecialtyName() + "</espec>";
				r += "<status>" + (u.getUserEnabled() ? 1 : 0) + "</status>";
				r += "</user>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/getallcoordinators")
    public @ResponseBody String getallcoordinators(String xml_data) {
		try{
			Coordinator p = new CoordinatorClass();
			Util.dao = dao;
			List<AdTusers> coordinators = p.getAllCoordinators();
			r = "<root>";
			for(AdTusers u : coordinators){
				r += "<user>";
				r += "<id>" + u.getPkuser() + "</id>";
				r += "<type_doc>" + u.getAdTtypeDocument().getTypedocumentName() + "</type_doc>";
				r += "<num_doc>" + u.getUserDocument() + "</num_doc>";
				r += "<full_name>" + u.getUserName1() + " " + u.getUserName2() + " " + u.getUserSurname1() + " " + u.getUserSurname2() + "</full_name>";
				r += "<email>" + u.getUserEmail() + "</email>";
				r += "<status>" + (u.getUserEnabled() ? 1 : 0) + "</status>";
				r += "<permissions>";
				List<AdTuserpermit> lp = Util.dao.findByQuery("from AdTuserpermit as p where p.adTusers.pkuser = " + u.getPkuser());
				for(AdTuserpermit up: lp){
					r += "<permit>";
					r += "<id_permit>" + up.getPkuserpermit() + "</id_permit>";
					r += "<name>" + up.getAdTpermits().getPermitName() + "</name>";
					r += "<value>" + (up.getUserpermitValue() == 1 ? "checked" : "") + "</value>";
					r += "</permit>";
				}
				r += "</permissions>";
				r += "</user>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/getpermissions")
    public @ResponseBody String getpermissions(String xml_data) {
		try{
			Util.dao = dao;
			AdTusers u = Util.dao.findById(AdTusers.class, Long.parseLong(XmlUtilities.getValueTag(xml_data, "uid")));
			r = "<root>";
			if(u != null){
				r += "<permissions>";
				List<AdTuserpermit> lp = Util.dao.findByQuery("from AdTuserpermit as p where p.adTusers.pkuser = " + u.getPkuser());
				for(AdTuserpermit up: lp){
					r += "<permit>";
					r += "<id_permit>" + up.getPkuserpermit() + "</id_permit>";
					r += "<name>" + up.getAdTpermits().getPermitName() + "</name>";
					r += "<value>" + (up.getUserpermitValue() == 1 ? "checked" : "") + "</value>";
					r += "</permit>";
				}
				r += "</permissions>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/updateservice_")
    public @ResponseBody String updateservice_(String xml_data) {
		try{
			Util.dao = dao;
			AdTentryservices es = Util.dao.findById(AdTentryservices.class, Long.parseLong(XmlUtilities.getValueTag(xml_data, "esid")));
			es.setEntryserviceNumerauth(XmlUtilities.getValueTag(xml_data, "nro_auth"));
			es.setEntryserviceCopago(Double.parseDouble(XmlUtilities.getValueTag(xml_data, "copago")));
			es.setEntryserviceFecuencycopago(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "fcop")));
			Util.dao.saveOrUpdate(es);
			r = "<root><code>100</code><message>OK</message></root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/changePermit")
    public @ResponseBody String changePermit(String xml_data) {
		try{
			Adom a = new AdomClass();
			Util.dao = dao;
			boolean up = a.changePermit(xml_data);
			if(up){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/getEntryDetails")
    public @ResponseBody String getEntryDetails(String xml_data) {
		try{
			Entry e = new EntryClass();
			Util.dao = dao;
			String detail = e.getEntryDetails(Long.parseLong(XmlUtilities.getValueTag(xml_data, "eid")));
			if(detail != null)
				return detail;
			else
				return null;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/getallentities")
    public @ResponseBody String getallentities() {
		try{
			Entity en = new EntityClass();
			Util.dao = dao;
			List<AdTentity> entities = en.getAllEntities();
			r = "<root>";
			for(AdTentity e : entities){
				r += "<entity>";
				r += "<id>" + e.getPkentity() + "</id>";
				r += "<nit>" + e.getEntityNit() + "</nit>";
				r += "<rs>" + e.getEntityRz() + "</rs>";
				r += "<code>" + e.getEntityCode() + "</code>";
				r += "<name>" + e.getEntityName() + "</name>";
				r += "</entity>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/getallservices")
    public @ResponseBody String getallservices() {
		try{
			Service se = new ServiceClass();
			Util.dao = dao;
			List<AdTservices> services = se.getAllServices();
			r = "<root>";
			for(AdTservices s : services){
				r += "<service>";
				r += "<id>" + s.getPkservice() + "</id>";
				r += "<name>" + s.getServiceName() + "</name>";
				r += "<value>" + s.getServiceValue() + "</value>";
				r += "<code>" + s.getServiceCode() + "</code>";
				r += "<type>" + (s.getServiceType() == 1 ? "Enfermer&iacute;a" : "Teparia") + "</type>";
				r += "<type2>" + (s.getServiceType2().equals("C") ? "CONSULTA" : "PROCEDIMIENTO") + "</type2>";
				r += "</service>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/getservicesbyentry")
    public @ResponseBody String getservicesbyEntry(String xml_data) {
		try{
			Entry e = new EntryClass();
			Util.dao = dao;
			int eid = Integer.parseInt(XmlUtilities.getValueTag(xml_data, "eid"));
			List<AdTentryservices> lentrys = e.getEntryServices(eid);
			r = "<root>";
			for(AdTentryservices s : lentrys){
				List<String> status = e.getEntryServicesStatus(s.getPkentryservices());
				r += "<service>";
				r += "<id>" + s.getPkentryservices() + "</id>";
				r += "<name>" + s.getAdTservices().getServiceName() + "</name>";
				r += "<qty_p>" + e.getEntryServicesQtyByEntry(s.getPkentryservices()) + "</qty_p>";
				r += "<qty_r>" + e.getEntryServicesRealizadosQtyByEntry(s.getPkentryservices()) + "</qty_r>";
				r += "<frecuency>" + s.getAdTfrencuency().getFrecuencyName() + "</frecuency>";
				r += "<finicio>" + s.getEntryserviceStartdate() + "</finicio>";
				r += "<status>" + status.get(0) + "</status>";
				r += "<color>" + status.get(1) + "</color>";
				r += "<invoiced>" + (s.getEntryserviceInvoicenumber() == null ? "NO" : "SI") + "</invoiced>";
				r += "</service>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/getallinsumos")
    public @ResponseBody String getallinsumos() {
		try{
			Service se = new ServiceClass();
			Util.dao = dao;
			List<AdTinsumo> insumos = se.getAllInsumos();
			r = "<root>";
			for(AdTinsumo i : insumos){
				r += "<insumo>";
				r += "<id>" + i.getPkinsumo() + "</id>";
				r += "<name>" + i.getInsumoName().toUpperCase() + "</name>";
				r += "<code>" + i.getInsumoCode() + "</code>";
				r += "</insumo>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/getRates")
    public @ResponseBody String getRates(String xml_data) {
		try{
			Entity en = new EntityClass();
			Util.dao = dao;
			List<AdTrate> rates = en.getRate(Long.parseLong(XmlUtilities.getValueTag(xml_data, "plan")));
			r = "<root>";
			for(AdTrate e : rates){
				r += "<rate>";
				r += "<id>" + e.getPkrate()+ "</id>";
				r += "<cost>" + e.getRateCost() + "</cost>";
				r += "<service>" + e.getAdTservices().getServiceName() + "</service>";
				r += "</rate>";
			}
			r += "</root>";
			return r;
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
	}
	
	@RequestMapping("admin/assignservice")
    public @ResponseBody String assignservice(String xml_data) {
		try{
			Entry e = new EntryClass();
			Util.dao = dao;
			AdTentryservices service = e.saveEntryService(xml_data);
			if(service != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/registerpatient")
    public @ResponseBody String registerpatient(String xml_data) {
		try{
			Patient p = new PatientClass();
			Util.dao = dao;
			AdTusers patient = p.savePatient(xml_data);
			if(patient != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "<id>" + patient.getPkuser() + "</id>";
				r += "<type_doc>" + patient.getAdTtypeDocument().getTypedocumentName() + "</type_doc>";
				r += "<num_doc>" + patient.getUserDocument() + "</num_doc>";
				r += "<full_name>" + patient.getUserName1() + " " + patient.getUserName2() + " " + patient.getUserSurname1() + " " + patient.getUserSurname2() + "</full_name>";
				r += "<phone>" + patient.getUserPhone1() + "</phone>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/updatepatient")
    public @ResponseBody String updatepatient(String xml_data) {
		try{
			Patient p = new PatientClass();
			Util.dao = dao;
			AdTusers patient = p.updatePatient(xml_data);
			if(patient != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "<id>" + patient.getPkuser() + "</id>";
				r += "<type_doc>" + patient.getAdTtypeDocument().getTypedocumentName() + "</type_doc>";
				r += "<num_doc>" + patient.getUserDocument() + "</num_doc>";
				r += "<full_name>" + patient.getUserName1() + " " + patient.getUserName2() + " " + patient.getUserSurname1() + " " + patient.getUserSurname2() + "</full_name>";
				r += "<phone>" + patient.getUserPhone1() + "</phone>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/updateservice")
    public @ResponseBody String updateservice(String xml_data) {
		try{
			Service s = new ServiceClass();
			Util.dao = dao;
			AdTservices se = s.updateService(xml_data);
			if(se != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/updateinsumo")
    public @ResponseBody String updateinsumo(String xml_data) {
		try{
			Service s = new ServiceClass();
			Util.dao = dao;
			AdTinsumo se = s.updateInsumo(xml_data);
			if(se != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/updatecoordinator")
    public @ResponseBody String updatecoordinator(String xml_data) {
		try{
			Coordinator c = new CoordinatorClass();
			Util.dao = dao;
			AdTusers coor = c.updateCoordinator(xml_data);
			if(coor != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/updateprofessional")
    public @ResponseBody String updateprofessional(String xml_data) {
		try{
			Professional c = new ProfessionalClass();
			Util.dao = dao;
			AdTusers coor = c.updateProfessional(xml_data);
			if(coor != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/cancelVisits")
	public @ResponseBody String cancelVisits(String xml_data) {
		try{
			Entry e = new EntryClass();
			Util.dao = dao;
			String xml = e.cancelVisits(xml_data);
			return xml;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/updateVisits")
	public @ResponseBody String updateVisits(String xml_data) {
		try{
			Entry e = new EntryClass();
			Util.dao = dao;
			String xml = e.updateVisits(xml_data);
			return xml;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/updateVisitsC")
	public @ResponseBody String updateVisitsc(String xml_data) {
		try{
			Entry e = new EntryClass();
			Util.dao = dao;
			String xml = e.updateVisitsCoordinator(xml_data);
			return xml;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/verifyClose")
	public @ResponseBody String verifyClose(String xml_data) {
		try{
			Entry e = new EntryClass();
			Util.dao = dao;
			String xml = e.verifyClose(Long.parseLong(XmlUtilities.getValueTag(xml_data, "eid")));
			return xml;
		}catch(Exception e){
			return "<root><code>300</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/verifycomplete")
	public @ResponseBody String verifycomplete(String xml_data) {
		try{
			Entry e = new EntryClass();
			Util.dao = dao;
			List<String> lservices = XmlUtilities.getCollectionValuesTag(xml_data, "service");
			String xml = e.verify_complete(lservices);
			return xml;
		}catch(Exception e){
			return "<root><code>300</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/closeentry")
	public @ResponseBody String closeentry(String xml_data) {
		try{
			Entry e = new EntryClass();
			Util.dao = dao;
			Object o = e.close_entry(Long.parseLong(XmlUtilities.getValueTag(xml_data, "eid")));
			if(o != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/saveservice")
    public @ResponseBody String saveservice(String xml_data) {
		try{
			Service se = new ServiceClass();
			Util.dao = dao;
			AdTservices service = se.saveService(xml_data);
			if(service != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/saveinsumo")
    public @ResponseBody String saveinsumo(String xml_data) {
		try{
			Service se = new ServiceClass();
			Util.dao = dao;
			AdTinsumo insumo = se.saveInsumo(xml_data);
			if(insumo != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }

	@RequestMapping("admin/saverate")
    public @ResponseBody String saverate(String xml_data) {
		try{
			Entity en = new EntityClass();
			Util.dao = dao;
			AdTrate rate = en.saveRate(xml_data);
			if(rate != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/removerate")
    public @ResponseBody String removerate(String xml_data) {
		try{
			Entity en = new EntityClass();
			Util.dao = dao;
			boolean rate = en.removeRate(xml_data);
			if(rate){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/saveplan")
    public @ResponseBody String saveplan(String xml_data) {
		try{
			Entity en = new EntityClass();
			Util.dao = dao;
			String plan = en.savePlan(xml_data);
			if(plan.equals("OK")){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>" + plan + "</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/generate_password")
    public @ResponseBody String generate_password(String xml_data) {
		try{
			Adom a = new AdomClass();
			Util.dao = dao;
			AdTusers u = a.generate_password(xml_data);
			if(u != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/change_status")
    public @ResponseBody String change_status(String xml_data) {
		try{
			Adom a = new AdomClass();
			Util.dao = dao;
			AdTusers u = a.change_status_user(xml_data);
			if(u != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/changestatusplan")
    public @ResponseBody String changestatusplan(String xml_data) {
		try{
			Adom a = new AdomClass();
			Util.dao = dao;
			AdTentityplan p = a.change_status_plan(xml_data);
			if(p != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/saveentry")
    public @ResponseBody String saveentry(String xml_data) {
		try{
			Entry en = new EntryClass();
			Util.dao = dao;
			AdTentry entry = en.saveEntry(xml_data);
			if(entry != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/add_observation")
    public @ResponseBody String add_observation(String xml_data) {
		try{
			Entry en = new EntryClass();
			Util.dao = dao;
			boolean entry = en.add_observation(xml_data);
			if(entry){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/add_insumo")
    public @ResponseBody String add_insumo(String xml_data) {
		try{
			Entry en = new EntryClass();
			Util.dao = dao;
			boolean insumo = en.add_insumo(xml_data);
			if(insumo){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/deleteinsumo")
    public @ResponseBody String deleteinsumo(String xml_data) {
		try{
			Entry en = new EntryClass();
			Util.dao = dao;
			boolean insumo = en.deleteinsumo(xml_data);
			if(insumo){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/registerentity")
    public @ResponseBody String registerentity(String xml_data) {
		try{
			Entity en = new EntityClass();
			Util.dao = dao;
			AdTentity entity = en.saveEntity(xml_data);
			if(entity != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/update_entity")
    public @ResponseBody String update_entity(String xml_data) {
		try{
			Entity en = new EntityClass();
			Util.dao = dao;
			AdTentity entity = en.updateEntity(xml_data);
			if(entity != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/registerprofessional")
    public @ResponseBody String registerprofessional(String xml_data) {
		try{
			Professional p = new ProfessionalClass();
			Util.dao = dao;
			AdTusers professional = p.saveProfessional(xml_data);
			if(professional != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "<id>" + professional.getPkuser() + "</id>";
				r += "<type_doc>" + professional.getAdTtypeDocument().getTypedocumentName() + "</type_doc>";
				r += "<num_doc>" + professional.getUserDocument() + "</num_doc>";
				r += "<full_name>" + professional.getUserName1() + " " + professional.getUserName2() + " " + professional.getUserSurname1() + " " + professional.getUserSurname2() + "</full_name>";
				r += "<espec>" + professional.getAdTspecialty().getSpecialtyName() + "</espec>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/registercoordinator")
    public @ResponseBody String registercoordinator(String xml_data) {
		try{
			Coordinator p = new CoordinatorClass();
			Util.dao = dao;
			AdTusers professional = p.saveCoordinator(xml_data);
			if(professional != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/updateadom")
    public @ResponseBody String updateadom(String xml_data) {
		try{
			Adom a = new AdomClass();
			Util.dao = dao;
			AdTadom plan = a.updateadom(xml_data);
			if(plan != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/updatelimits")
    public @ResponseBody String updatelimits(String xml_data) {
		try{
			Adom a = new AdomClass();
			Util.dao = dao;
			AdTadom plan = a.updatelimits(xml_data);
			if(plan != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/savemail")
    public @ResponseBody String savemail(String xml_data) {
		try{
			Adom a = new AdomClass();
			Util.dao = dao;
			boolean re = a.saveEmail1(xml_data);
			if(re){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/save_advert")
    public @ResponseBody String save_advert(String xml_data) {
		try{
			Adom a = new AdomClass();
			Util.dao = dao;
			AdTadverts re = a.saveadvert(xml_data);
			if(re != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/delete_advert")
    public @ResponseBody String delete_advert(String xml_data) {
		try{
			Adom a = new AdomClass();
			Util.dao = dao;
			AdTadverts re = a.deleteadvert(xml_data);
			if(re != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping(value="admin/checkEmail", method = RequestMethod.POST)
    public @ResponseBody String checkEmail(@RequestParam("e") String email) {
		try{
			int count = dao.countByHql("select count(*) from AdTusers as u where u.userEmail = '" + email + "'");
			if(count == 0){
				return "true";
			}else{
				return "false";
			}
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping(value="admin/checkDocument", method = RequestMethod.POST)
    public @ResponseBody String checkDocument(@RequestParam("nd") String nd) {
		try{
			int count = dao.countByHql("select count(*) from AdTusers as u where u.userDocument = '" + nd + "'");
			if(count == 0){
				return "true";
			}else{
				return "false";
			}
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("admin/changepass")
    public @ResponseBody String changepass(String xml_data) {
		try{
			Adom a = new AdomClass();
			Util.dao = dao;
			boolean re = a.changepass(xml_data);
			if(re){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping(value = "/admin/generate")
    public @ResponseBody String generate(String xml_data) {
		try{
			Adom a = new AdomClass();
			Util.dao = dao;
			int type = Integer.parseInt(XmlUtilities.getValueTag(xml_data, "type_gen"));
			List<AdTentryservices> re = type == 1 ? a.invoiced(xml_data) : a.invoiced2(xml_data);
			if(re.size() > 0){
				List<String> response = a.generate_rip(re, type);
				if(response != null){
					r = "<root>";
					r += "<code>100</code>";
					r += "<message>Exito</message>";
					r += "<file>" + response.get(1) + "</file>";
					r += "<url>" + response.get(0) + "</url>";
					r += "</root>";
				}else{
					r = "<root>";
					r += "<code>200</code>";
					r += "<message>Error: response is null</message>";
					r += "</root>";
				}
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error: response is zero</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping(value = "/admin/generate_report")
    public @ResponseBody String generate_report(String xml_data) {
		try{
			Adom a = new AdomClass();
			Util.dao = dao;
			int type = Integer.parseInt(XmlUtilities.getValueTag(xml_data, "r"));
			List<String> response = null;
			if(type == 1)
				response = a.generate_report_terapias(xml_data);
			if(type == 2)
				response = a.generate_report_pagos(xml_data);
			if(type == 3)
				response = a.generate_report_factura(xml_data);
			if(type == 4)
				response = a.generate_report_professionals();
			if(type == 5)
				response = a.generate_report_patients(xml_data);
			if(type == 6)
				response = a.generate_report_general();
			if(type == 7)
				response = a.generate_report_copago(xml_data);
			if(type == 8)
				response = a.generate_report_general2(xml_data);
			if(response != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "<file>" + response.get(1) + "</file>";
				r += "<url>" + response.get(0) + "</url>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping(value = "/admin/generate_copago")
    public @ResponseBody String generate_copago(String xml_data) {
		try{
			Adom a = new AdomClass();
			Util.dao = dao;
			List<String> response = null;
			response = a.generate_copago(xml_data);
			if(response != null){
				r = "<root>";
				r += "<code>100</code>";
				r += "<message>Exito</message>";
				r += "<file>" + response.get(0) + "</file>";
				r += "</root>";
			}else{
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}
			return r;
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping(value = "/admin/download-{filename}", method = RequestMethod.GET)
    @ResponseBody
    public void download(@PathVariable(value="filename") String file, HttpServletResponse response) throws IOException{
		try{
			//byte[] array = Files.readAllBytes(new File("/Users/YohanFlorez/Documents/Exports_Adom/" + file + ".xls").toPath());
			byte[] array = Files.readAllBytes(new File(System.getProperty("catalina.base") + "/excel/" + file + ".xls").toPath());
		    response.setHeader("Content-Disposition", "attachment; filename=" + file + ".xls");
		    response.getOutputStream().write(array);
		    response.flushBuffer();
		}catch(Exception e){
			response.getOutputStream().print("File Path: " + System.getProperty("catalina.base") + "/excel/" + file + ".xls");
			e.printStackTrace();
		}
    }
	
	@RequestMapping(value = "/admin/downloadpdf-{filename}", method = RequestMethod.GET)
    @ResponseBody
    public void downloadpdf(@PathVariable(value="filename") String file, HttpServletResponse response) throws IOException{
		try{
			//byte[] array = Files.readAllBytes(new File("/home/yflorezr/Documentos/Exports_Adom/" + file + ".pdf").toPath());
			byte[] array = Files.readAllBytes(new File(System.getProperty("catalina.base") + "/pdf/" + file + ".pdf").toPath());
		    response.setHeader("Content-Disposition", "attachment; filename=" + file + ".pdf");
		    response.getOutputStream().write(array);
		    response.flushBuffer();
		}catch(Exception e){
			response.getOutputStream().print("File Path: " + System.getProperty("catalina.base") + "/pdf/" + file + ".pdf");
			e.printStackTrace();
		}
    }
	
	public class AuthTokenInterceptor extends HandlerInterceptorAdapter {
	        public boolean preHandle(HttpServletRequest request,
	            HttpServletResponse response, Object handler) throws Exception {
	        System.out.println("Inside preHandle");
	        if(null == request.getSession(false)){   
	        	System.out.println("Inside if");
	            response.sendRedirect("login.html");
	            return false;
	        }
	        return true;
	    }
	}

}
