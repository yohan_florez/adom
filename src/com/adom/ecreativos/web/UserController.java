package com.adom.ecreativos.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.adom.ecreativos.dao.Dao;
import com.adom.ecreativos.services.Adom;
import com.adom.ecreativos.services.AdomClass;
import com.adom.ecreativos.services.Entry;
import com.adom.ecreativos.services.EntryClass;
import com.adom.ecreativos.util.UserDash;
import com.adom.ecreativos.util.Util;
import com.adom.ecreativos.vo.AdTdetails;
import com.adom.ecreativos.vo.AdTentryservices;

@org.springframework.stereotype.Controller
public class UserController {
	
	@Resource
	Dao dao;
	
	@RequestMapping("user/dashboard")
    public ModelAndView coordinator_dash() {
		Util.dao = dao;
		Map<String, Object> model = new HashMap<String, Object>();
		Adom a = new AdomClass();
		model.put("option", "1");
		model.put("user", Util.getUserActive());
		model.put("adverts", a.getalladverts());
		return new ModelAndView("/user/main", "data", model);
    }
	
	@RequestMapping("user/configuration")
	public ModelAndView configuration(){
		Util.dao = dao;
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("option", "10");
		model.put("active", Util.getUserActive());
		model.put("user", Util.getUserActive());
		return new ModelAndView("/user/config", "data", model);
	}
	
	@RequestMapping("user/services")
    public ModelAndView services() {
		Entry e = new EntryClass();
		Util.dao = dao;
		Map<String, Object> model = new HashMap<String, Object>();
		List<UserDash> lact = new ArrayList<UserDash>();
		List<UserDash> lother = new ArrayList<UserDash>();
		List<Object[]> data = e.getActiveServicesByProfessional(Util.getUserActive().getPkuser());
		for(Object[] item: data){
			List<String> status = new ArrayList<String>();
			status.add("EN PROCESO");
			status.add("blue");
			AdTentryservices service = (AdTentryservices) item[0];
			UserDash u = new UserDash();
			u.setEs(service);
			u.setStatus(status);
			lact.add(u);
		}
		model.put("option", "2");
		model.put("services_act", lact);
		model.put("services_noact", lother);
		model.put("user", Util.getUserActive());
		return new ModelAndView("/user/services", "data", model);
    }
	
	@RequestMapping(value={"user/details/service-{serviceid}"}, method=RequestMethod.GET)
	public ModelAndView detail(@PathVariable(value="serviceid") Long esid){
		Map<String, Object> model = new HashMap<String, Object>();
		Util.dao = dao;
		Entry entry = new EntryClass();
		model.put("option", "2");
		AdTentryservices entrys = entry.getEntryService(esid);
		String hql = "from AdTdetails d where d.adTentryservices.pkentryservices = " + entrys.getPkentryservices();
		hql += " and d.adTusers.pkuser = " + Util.getUserActive().getPkuser();
		hql += " order by d.pkdetail ASC";
		List<AdTdetails> ldetails = Util.dao.findByQuery(hql);
		hql = "from AdTdetails as d where d.adTentryservices.pkentryservices = " + entrys.getPkentryservices();
		hql += " and (d.adTstatus.pkstatus = 2 or d.adTstatus.pkstatus = 3)";
		List<AdTdetails> ldetails2 = Util.dao.findByQuery(hql);
		model.put("entryservice", entrys);
		model.put("entry", entrys.getAdTentry());
		model.put("details",ldetails);
		model.put("qty_v",ldetails2.size());
		model.put("user", Util.getUserActive());
		return new ModelAndView("/user/details-entry", "data", model);
	}
	
	@RequestMapping("admin/updateVisit")
	public @ResponseBody String updateVisit(String xml_data) {
		try{
			Entry e = new EntryClass();
			Util.dao = dao;
			String xml = e.updateVisit(xml_data);
			return xml;
		}catch(Exception e){
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }

}