package com.adom.ecreativos.web;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.adom.ecreativos.dao.Dao;
import com.adom.ecreativos.services.Adom;
import com.adom.ecreativos.services.AdomClass;
import com.adom.ecreativos.util.Util;
import com.adom.ecreativos.util.XmlUtilities;
import com.adom.ecreativos.vo.AdTusers;

@org.springframework.stereotype.Controller
public class Controller {
	
	private String r;
	
	@Resource
	Dao dao;
	
	@RequestMapping("/confirm")
    public String confirmar_rol() {
       return "util/confirm_rol";
    }
	
	@RequestMapping("/exect")
	public String exect(){
		Util.startSendReport();
		return "OK";
	}
	
	@RequestMapping("/forgot")
    public String forgot() {
       return "forgot";
    }
	
	@RequestMapping("/")
	public String raiz(){
		return "index";
	}
	
	@RequestMapping("/sendpass")
    public @ResponseBody String sendpass(String xml_data) {
		try{
			Adom a = new AdomClass();
			Util.dao = dao;
			AdTusers u;
			try{
				String email = XmlUtilities.getValueTag(xml_data, "email");
				u = (AdTusers) Util.dao.findByQuery("from AdTusers as a where a.userEmail ='" + email + "'").get(0);
			}catch(Exception e){
				e.printStackTrace();
				u = null;
			}
			if(u == null){
				r = "<root>";
				r += "<code>200</code>";
				r += "<message>Error</message>";
				r += "</root>";
			}else{
				xml_data = "<root><uid>" + u.getPkuser() + "</uid></root>";
				AdTusers re = a.generate_password(xml_data);
				if(re != null){
					r = "<root>";
					r += "<code>100</code>";
					r += "<message>Exito</message>";
					r += "</root>";
				}else{
					r = "<root>";
					r += "<code>200</code>";
					r += "<message>Error</message>";
					r += "</root>";
				}
			}
			return r;
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping(value = "/403", method = RequestMethod.GET)
	public String accesssDenied() {
		return "util/403";
	}

}
