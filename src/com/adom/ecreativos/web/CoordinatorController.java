package com.adom.ecreativos.web;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.annotation.Resource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.adom.ecreativos.dao.Dao;
import com.adom.ecreativos.services.Adom;
import com.adom.ecreativos.services.AdomClass;
import com.adom.ecreativos.services.Entity;
import com.adom.ecreativos.services.EntityClass;
import com.adom.ecreativos.services.Entry;
import com.adom.ecreativos.services.EntryClass;
import com.adom.ecreativos.services.Frecuency;
import com.adom.ecreativos.services.FrecuencyClass;
import com.adom.ecreativos.services.Professional;
import com.adom.ecreativos.services.ProfessionalClass;
import com.adom.ecreativos.services.Service;
import com.adom.ecreativos.services.ServiceClass;
import com.adom.ecreativos.util.Base64;
import com.adom.ecreativos.util.Util;
import com.adom.ecreativos.vo.AdTadverts;
import com.adom.ecreativos.vo.AdTentry;
import com.adom.ecreativos.vo.AdTspecialty;
import com.adom.ecreativos.vo.AdTtypeDocument;
import com.adom.ecreativos.vo.AdTuserpermit;
import com.sun.mail.util.MailSSLSocketFactory;

@org.springframework.stereotype.Controller
public class CoordinatorController {
	
	@Resource
	Dao dao;
	
	@RequestMapping("coordinator/dashboard")
    public ModelAndView coordinator_dash() {
		Util.dao = dao;
		SimpleDateFormat dt = new SimpleDateFormat("yyyyy-mm-dd hh:mm:ss"); 
		Adom a = new AdomClass();
		Entry e = new EntryClass();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("option", "1");
		model.put("count_p", dao.count("AdTusers p where p.userType = 1"));
		model.put("user", Util.getUserActive());
		model.put("hours_p", a.getHoursProg());
		model.put("hours_r", a.getHoursReal());
		model.put("tera_p", a.getTeraProg());
		model.put("tera_c", a.getTeraCancel());
		model.put("tera_r", a.getTeraReal());
		model.put("list_np", e.getNotProfessional());
		model.put("count_p", dao.count("AdTusers p where p.userType = 1"));
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		model.put("notinvoiced", e.getServicesNotInvoiced());
		model.put("limits", e.getCopagoLimits());
		System.out.println("14: " + dt.format(new Date()));
		model.put("irregular", e.getIrregularServices());
		System.out.println("15: " + dt.format(new Date()));
		List<AdTuserpermit> lp = a.getPermissions(Util.getUserActive().getPkuser());
		for(AdTuserpermit up: lp)
			model.put("p" + up.getAdTpermits().getPkpermit(), up.getUserpermitValue() == 1 ? true : false);
		return new ModelAndView("/coord/main", "data", model);
    }
	
	@RequestMapping("coordinator/services")
    public ModelAndView services() {
		Service s = new ServiceClass();
		Util.dao = dao;
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("option", "8");
		model.put("services", s.getAllServices());
		model.put("insumos", s.getAllInsumos());
		model.put("count_p", dao.count("AdTusers p where p.userType = 1"));
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		model.put("user", Util.getUserActive());
		Adom a = new AdomClass();
		List<AdTuserpermit> lp = a.getPermissions(Util.getUserActive().getPkuser());
		for(AdTuserpermit up: lp)
			model.put("p" + up.getAdTpermits().getPkpermit(), up.getUserpermitValue() == 1 ? true : false);
		return new ModelAndView("/coord/services", "data", model);
    }
	
	@RequestMapping("coordinator/professionals")
	public ModelAndView admin_professionals(){
		Util.dao = dao;
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("typedocs", dao.findAll(AdTtypeDocument.class));
		model.put("specialtys", dao.findAll(AdTspecialty.class));
		model.put("option", "3");
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		model.put("user", Util.getUserActive());
		Adom a = new AdomClass();
		List<AdTuserpermit> lp = a.getPermissions(Util.getUserActive().getPkuser());
		for(AdTuserpermit up: lp)
			model.put("p" + up.getAdTpermits().getPkpermit(), up.getUserpermitValue() == 1 ? true : false);
		return new ModelAndView("/coord/professionals", "data", model);
	}
	
	@RequestMapping("coordinator/entities")
    public ModelAndView entities() {
		Util.dao = dao;
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("option", "4");
		model.put("count_p", dao.count("AdTusers p where p.userType = 1"));
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("user", Util.getUserActive());
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		Adom a = new AdomClass();
		List<AdTuserpermit> lp = a.getPermissions(Util.getUserActive().getPkuser());
		for(AdTuserpermit up: lp)
			model.put("p" + up.getAdTpermits().getPkpermit(), up.getUserpermitValue() == 1 ? true : false);
		return new ModelAndView("/coord/entities", "data", model);
    }
	
	@RequestMapping("coordinator/adverts")
    public ModelAndView adverts() {
		Map<String, Object> model = new HashMap<String, Object>();
		Adom a = new AdomClass();
		Util.dao = dao;
		model.put("option", "9");
		model.put("count_p", dao.count("AdTusers p where p.userType = 1"));
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		model.put("user", Util.getUserActive());
		List<AdTuserpermit> lp = a.getPermissions(Util.getUserActive().getPkuser());
		for(AdTuserpermit up: lp)
			model.put("p" + up.getAdTpermits().getPkpermit(), up.getUserpermitValue() == 1 ? true : false);
		List<AdTadverts> ladv = new ArrayList<>();
		for(AdTadverts adv: a.getalladverts()){
			FileInputStream mFileInputStream;
			try {
				mFileInputStream = new FileInputStream(adv.getAdvertImage());
				ByteArrayOutputStream bos = new ByteArrayOutputStream();
	            byte[] b = new byte[1024];
	            int bytesRead = 0;
	            while ((bytesRead = mFileInputStream.read(b)) != -1) {
	               bos.write(b, 0, bytesRead);
	            }
	            byte[] ba = bos.toByteArray();
	            String encoded = Base64.encodeBytes(ba);
	            adv.setAdvertImage(encoded);
			} catch (Exception e) {
				e.printStackTrace();
			}
            ladv.add(adv);
		}
		model.put("adverts", ladv);
		return new ModelAndView("/coord/adverts", "data", model);
    }
	
	@RequestMapping("coordinator/reports")
    public ModelAndView reports() {
		Map<String, Object> model = new HashMap<String, Object>();
		Util.dao = dao;
		Entity e = new EntityClass();
		Professional professional = new ProfessionalClass();
		model.put("option", "11");
		model.put("count_p", dao.count("AdTusers p where p.userType = 1"));
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		model.put("entities", e.getAllEntities());
		model.put("user", Util.getUserActive());
		model.put("professionals", professional.getAllProfessionals());
		Adom a = new AdomClass();
		List<AdTuserpermit> lp = a.getPermissions(Util.getUserActive().getPkuser());
		for(AdTuserpermit up: lp)
			model.put("p" + up.getAdTpermits().getPkpermit(), up.getUserpermitValue() == 1 ? true : false);
		return new ModelAndView("/coord/reports", "data", model);
    }
	
	@RequestMapping("coordinator/copago")
    public ModelAndView copago() {
		Map<String, Object> model = new HashMap<String, Object>();
		Util.dao = dao;
		Entity e = new EntityClass();
		Professional professional = new ProfessionalClass();
		model.put("option", "12");
		model.put("count_p", dao.count("AdTusers p where p.userType = 1"));
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		model.put("entities", e.getAllEntities());
		model.put("user", Util.getUserActive());
		model.put("professionals", professional.getAllProfessionals());
		Adom a = new AdomClass();
		List<AdTuserpermit> lp = a.getPermissions(Util.getUserActive().getPkuser());
		for(AdTuserpermit up: lp)
			model.put("p" + up.getAdTpermits().getPkpermit(), up.getUserpermitValue() == 1 ? true : false);
		return new ModelAndView("/coord/copago", "data", model);
    }
	
	@RequestMapping("coordinator/billing")
    public ModelAndView billing() {
		Map<String, Object> model = new HashMap<String, Object>();
		Util.dao = dao;
		Entity e = new EntityClass();
		model.put("option", "6");
		model.put("count_p", dao.count("AdTusers p where p.userType = 1"));
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		model.put("entities", e.getAllEntities());
		model.put("user", Util.getUserActive());
		Adom a = new AdomClass();
		List<AdTuserpermit> lp = a.getPermissions(Util.getUserActive().getPkuser());
		for(AdTuserpermit up: lp)
			model.put("p" + up.getAdTpermits().getPkpermit(), up.getUserpermitValue() == 1 ? true : false);
		return new ModelAndView("/coord/invoice", "data", model);
    }
	
	@RequestMapping(value="coordinator/checkEmail", method = RequestMethod.POST)
    public @ResponseBody String checkEmail(@RequestParam("e") String email) {
		try{
			int count = dao.countByHql("select count(*) from AdTusers as u where u.userEmail = '" + email + "'");
			if(count == 0){
				return "true";
			}else{
				return "false";
			}
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping(value="coordinator/checkDocument", method = RequestMethod.POST)
    public @ResponseBody String checkDocument(@RequestParam("nd") String nd) {
		try{
			int count = dao.countByHql("select count(*) from AdTusers as u where u.userDocument = '" + nd + "'");
			if(count == 0){
				return "true";
			}else{
				return "false";
			}
		}catch(Exception e){
			e.printStackTrace();
			return "<root><code>200</code><message>Error: " + e.getMessage() + "</message></root>";
		}
    }
	
	@RequestMapping("coordinator/patients")
	public ModelAndView admin_patients(){
		Util.dao = dao;
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("option", "2");
		model.put("count_p", dao.count("AdTusers p where p.userType = 1"));
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		model.put("typedocs", dao.findAll(AdTtypeDocument.class));
		model.put("user", Util.getUserActive());
		Adom a = new AdomClass();
		List<AdTuserpermit> lp = a.getPermissions(Util.getUserActive().getPkuser());
		for(AdTuserpermit up: lp)
			model.put("p" + up.getAdTpermits().getPkpermit(), up.getUserpermitValue() == 1 ? true : false);
		return new ModelAndView("/coord/patients", "data", model);
	}
	
	@RequestMapping(value={"coordinator/details/entry-{entryid}"}, method=RequestMethod.GET)
	public ModelAndView detail(@PathVariable(value="entryid") int eid){
		Map<String, Object> model = new HashMap<String, Object>();
		Util.dao = dao;
		Entry entry = new EntryClass();
		Adom a = new AdomClass();
		Service service = new ServiceClass();
		Professional professional = new ProfessionalClass();
		Frecuency frencuency = new FrecuencyClass();
		List<AdTuserpermit> lp = a.getPermissions(Util.getUserActive().getPkuser());
		AdTentry entryv = entry.getEntry(eid);
		model.put("entry", entryv);
		model.put("services", service.getServicesByEntry(eid));
		model.put("invoiced", entry.getAllInvoiced());
		model.put("professionals", professional.getAllProfessionals());
		model.put("frecuency", frencuency.getAllFrencuency());
		model.put("entryservices", entry.getEntryServices(eid));
		model.put("user", Util.getUserActive());
		model.put("typedocs", dao.findAll(AdTtypeDocument.class));
		model.put("status", entryv.getEntryEnddate() == null ? true: false);
		for(AdTuserpermit up: lp)
			model.put("p" + up.getAdTpermits().getPkpermit(), up.getUserpermitValue() == 1 ? true : false);
		return new ModelAndView("/coord/details-entry", "data", model);
	}
	
	@RequestMapping("coordinator/configuration")
	public ModelAndView configuration(){
		Util.dao = dao;
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("option", "10");
		model.put("count_pr", dao.count("AdTusers p where p.userType = 2"));
		model.put("count_en", dao.count("AdTentity"));
		model.put("count_co", dao.count("AdTusers p where p.userType = 3"));
		model.put("active", Util.getUserActive());
		model.put("user", Util.getUserActive());
		Adom a = new AdomClass();
		List<AdTuserpermit> lp = a.getPermissions(Util.getUserActive().getPkuser());
		for(AdTuserpermit up: lp)
			model.put("p" + up.getAdTpermits().getPkpermit(), up.getUserpermitValue() == 1 ? true : false);
		return new ModelAndView("/coord/config", "data", model);
	}

}
