package com.adom.ecreativos.services;

import java.util.List;

import com.adom.ecreativos.vo.AdTusers;

public interface Patient {
	
	public AdTusers savePatient(String xml_data);
	public AdTusers getPatient(long pkpatient);
	public List<AdTusers> getAllPatient();
	public List<AdTusers> getHistoryPatients();
	public List<AdTusers> search(String xml_data);
	public AdTusers updatePatient(String xml_data);

}
