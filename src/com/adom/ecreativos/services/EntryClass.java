package com.adom.ecreativos.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import com.adom.ecreativos.util.LsMails;
import com.adom.ecreativos.util.Mail;
import com.adom.ecreativos.util.NoP;
import com.adom.ecreativos.util.Util;
import com.adom.ecreativos.util.XmlUtilities;
import com.adom.ecreativos.vo.AdTadom;
import com.adom.ecreativos.vo.AdTdetails;
import com.adom.ecreativos.vo.AdTentityplan;
import com.adom.ecreativos.vo.AdTentry;
import com.adom.ecreativos.vo.AdTentryserviceinsumo;
import com.adom.ecreativos.vo.AdTentryservices;
import com.adom.ecreativos.vo.AdTfrencuency;
import com.adom.ecreativos.vo.AdTinsumo;
import com.adom.ecreativos.vo.AdTinvoiced;
import com.adom.ecreativos.vo.AdTobservations;
import com.adom.ecreativos.vo.AdTrate;
import com.adom.ecreativos.vo.AdTservices;
import com.adom.ecreativos.vo.AdTstatus;
import com.adom.ecreativos.vo.AdTup;
import com.adom.ecreativos.vo.AdTuserpermit;
import com.adom.ecreativos.vo.AdTusers;

public class EntryClass implements Entry{

	@Override
	public AdTentry saveEntry(String xml_data) {
		try {
			AdTentry entry = new AdTentry();
			AdTusers patient = new AdTusers();
			AdTentityplan plan = new AdTentityplan();
			plan.setPkentityplan(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "plan")));
			patient.setPkuser(Long.parseLong(XmlUtilities.getValueTag(xml_data, "patient")));
			entry.setEntryCie10(XmlUtilities.getValueTag(xml_data, "code"));
			entry.setEntryCie10description(XmlUtilities.getValueTag(xml_data, "description"));
			entry.setEntryNumber(XmlUtilities.getValueTag(xml_data, "number"));
			entry.setAdTusersByFkpatient(patient);
			entry.setAdTentityplan(plan);
			entry.setEntryStartdate(new Date());
			Util.dao.saveOrUpdate(entry);
			return Util.dao.findById(AdTentry.class, entry.getPkentry());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public AdTentryservices saveEntryService(String xml_data) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			AdTentryservices entryservice = new AdTentryservices();
			AdTentry entry = Util.dao.findById(AdTentry.class, Integer.parseInt(XmlUtilities.getValueTag(xml_data, "entry")));
			AdTstatus status = new AdTstatus();
			AdTservices service = Util.dao.findById(AdTservices.class, Long.parseLong(XmlUtilities.getValueTag(xml_data, "service")));
			status.setPkstatus(1);
			AdTfrencuency frecuency = Util.dao.findById(AdTfrencuency.class, Integer.parseInt(XmlUtilities.getValueTag(xml_data, "frec_service")));
			
			String hql = "from AdTrate as r where r.adTentityplan.pkentityplan = " + entry.getAdTentityplan().getPkentityplan();
			hql += " and r.adTservices.pkservice = " + service.getPkservice();
			AdTrate r = (AdTrate) Util.dao.findByQuery(hql).get(0);
			
			entryservice.setAdTentry(entry);
			entryservice.setAdTstatus(status);
			entryservice.setAdTfrencuency(frecuency);
			entryservice.setAdTservices(service);
			entryservice.setEntryserviceStartdate(formatter.parse(XmlUtilities.getValueTag(xml_data, "start_date")));
			entryservice.setEntryserviceEnddate(formatter.parse(XmlUtilities.getValueTag(xml_data, "end_date")));
			entryservice.setEntryserviceCopago(Double.parseDouble(XmlUtilities.getValueTag(xml_data, "copago")));
			entryservice.setEntryserviceFecuencycopago(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "frec_copago")));
			entryservice.setEntryserviceNumerauth(XmlUtilities.getValueTag(xml_data, "nro_auth"));
			entryservice.setEntryserviceAuthvalidity(formatter.parse(XmlUtilities.getValueTag(xml_data, "validity")));
			entryservice.setEntryserviceName(XmlUtilities.getValueTag(xml_data, "name"));
			entryservice.setEntryserviceConsultation(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "consultation")));
			entryservice.setEntryserviceExternal(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "external")));
			entryservice.setEntryserviceRequestdate(new Date());
			entryservice.setAdTusers(Util.getUserActive());
			entryservice.setEntryserviceInvoicetarifa(r.getRateCost().longValue());
			Util.dao.saveOrUpdate(entryservice);
			List<List<String>> linsumos = XmlUtilities.convertToList(xml_data, "insumo");
			for(List<String> insumo : linsumos){
				AdTentryserviceinsumo serin = new AdTentryserviceinsumo();
				AdTinsumo i = new AdTinsumo();
				AdTinvoiced invoiced = new AdTinvoiced();
				invoiced.setPkinvoiced(Integer.parseInt(insumo.get(2)));
				i.setPkinsumo(Long.parseLong(insumo.get(0)));
				serin.setAdTinsumo(i);
				serin.setAdTentryservices(entryservice);
				serin.setEntryserviceinsumoQty(Integer.parseInt(insumo.get(1)));
				serin.setAdTinvoiced(invoiced);
				Util.dao.saveOrUpdate(serin);
			}
			service = Util.dao.findById(AdTservices.class, service.getPkservice());
			boolean enfermeria = false;
			Date start = formatter.parse(XmlUtilities.getValueTag(xml_data, "start_date"));
			Calendar c = Calendar.getInstance();
			c.setTime(start);
			if(service.getServiceType() == 1)
				enfermeria = true;
			AdTusers p = null;
			int qty = Integer.parseInt(XmlUtilities.getValueTag(xml_data, "qty"));
			try{
				p = Util.dao.findById(AdTusers.class, Long.parseLong(XmlUtilities.getValueTag(xml_data, "professional")));
			}catch(Exception e){
				p = null;
			}
			for(int i=0; i<qty; i++){
				AdTdetails detail = new AdTdetails();
				AdTstatus status2 = new AdTstatus();
				status2.setPkstatus(2);
				detail.setAdTentryservices(entryservice);
				detail.setAdTstatus(status2);
				detail.setAdTusers(p);
				detail.setDetailCopago(0);
				if(enfermeria){
					detail.setDetailDate(c.getTime());
					c.add(Calendar.DATE, 7/ frecuency.getFrecuencyDaysxweek());
				}
				Util.dao.saveOrUpdate(detail);
			}
			try {
				if(p != null){
					String copago_f = "";
					if(entryservice.getEntryserviceFecuencycopago() == 1)
						copago_f = "CADA SESI&Oacute;N";
					else if(entryservice.getEntryserviceFecuencycopago() == 5)
						copago_f = "CADA 5 SESIONES";
					else copago_f = "OTRO";
					String content = Util.loadHTMLFile("templates/assign_s.jsp");
					AdTusers patient = entry.getAdTusersByFkpatient();
					String proname = p.getUserName1() + " " + p.getUserName2() + " " + p.getUserSurname1() + " " + p.getUserSurname2();
					String patname = patient.getUserName1() + " " + patient.getUserName2() + " " + patient.getUserSurname1() + " " + patient.getUserSurname2();
					content = content.replace("{@name}", proname.toUpperCase());
					content = content.replace("{@patient_id}", patient.getAdTtypeDocument().getTypedocumentShortname() + " - " + patient.getUserDocument());
					content = content.replace("{@patient_name}", patname.toUpperCase());
					content = content.replace("{@patient_address}", patient.getUserAddress().toUpperCase());
					content = content.replace("{@patient_phone}", patient.getUserPhone1());
					content = content.replace("{@service_nro_auto}", entryservice.getEntryserviceNumerauth());
					content = content.replace("{@service_auto}", entryservice.getEntryserviceNumerauth());
					content = content.replace("{@service_name}", service.getServiceName().toUpperCase());
					content = content.replace("{@service_qty}", "" + qty);
					content = content.replace("{@service_startdate}",formatter.format(entryservice.getEntryserviceStartdate()));
					content = content.replace("{@service_enddate}", formatter.format(entryservice.getEntryserviceEnddate()));
					content = content.replace("{@service_frecuency}", entryservice.getAdTfrencuency().getFrecuencyName());
					content = content.replace("{@service_copago}", "$" + entryservice.getEntryserviceCopago().intValue());
					content = content.replace("{@service_copago_frecuency}", copago_f);
					if(service.getServiceType() == 2){
						new Mail("Nuevo servicio asignado - ADOM", content, "" + p.getUserEmail() + ",adomsalud1978@gmail.com").start();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return Util.dao.findById(AdTentryservices.class, entryservice.getPkentryservices());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<AdTentry> getEntrys(Long pid) {
		try {
			return Util.dao.findByQuery("from AdTentry e where e.adTusersByFkpatient.pkuser = " + pid + " order by e.pkentry desc");
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public AdTentry getEntry(int pid) {
		try {
			return Util.dao.findById(AdTentry.class, pid);
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public List<AdTinvoiced> getAllInvoiced() {
		try {
			return Util.dao.findAll(AdTinvoiced.class);
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public List<AdTentryservices> getEntryServices(int eid) {
		try {
			return Util.dao.findByQuery("from AdTentryservices as es where es.adTentry.pkentry = " + eid);
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public AdTentryservices getEntryService(Long esid) {
		try {
			return Util.dao.findById(AdTentryservices.class, esid);
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public List<Object[]> getActiveServicesByProfessional(Long pid) {
		try {
			String hql = "from AdTentryservices as s inner join s.adTdetailses as d ";
			hql += "where d.adTstatus.pkstatus = 2 and d.adTusers.pkuser = " + pid;
			hql += " group by s.pkentryservices";
			return Util.dao.findByQuery(hql);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public AdTentry close_entry(Long eid) {
		try {
			AdTentry e = Util.dao.findById(AdTentry.class, eid.intValue());
			e.setEntryEnddate(new Date());
			Util.dao.saveOrUpdate(e);
			return e;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getEntryDetails(Long eid) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try{
			String hql = "from AdTdetails as d where d.adTentryservices.pkentryservices = " + eid;
			hql += " and (d.adTstatus.pkstatus = 2 or d.adTstatus.pkstatus = 3)";
			List<AdTdetails> ldetails = Util.dao.findByQuery(hql);
			AdTentryservices entrys = Util.dao.findById(AdTentryservices.class, eid);
			String cfrec = "";
			if(entrys.getEntryserviceFecuencycopago() == 1)
				cfrec = "CADA SESI&Oacute;N";
			if(entrys.getEntryserviceFecuencycopago() == 5)
				cfrec = "CADA 5 SESIONES";
			if(entrys.getEntryserviceFecuencycopago() == 0)
				cfrec = "OTRO";
			hql = "from AdTuserpermit as p where p.adTusers.pkuser = " + Util.getUserActive().getPkuser();
			hql += " and p.adTpermits.pkpermit = 12";
			AdTuserpermit up = null;
			try{
				up = (AdTuserpermit) Util.dao.findByQuery(hql).get(0);
			}catch(Exception e){
				System.out.println(e.getMessage());
			}
			String xml_data = "<root>";
			xml_data +="<service_detail>";
			xml_data +="<entry>" + eid + "</entry>";
			xml_data +="<code>100</code>";
			xml_data +="<nro_auth>" + entrys.getEntryserviceNumerauth() + "</nro_auth>";
			xml_data +="<vigencia>" + entrys.getEntryserviceAuthvalidity() + "</vigencia>";
			xml_data +="<date_soli>" + (entrys.getEntryserviceRequestdate() == null ? "N/A" : formatter.format(entrys.getEntryserviceRequestdate())) + "</date_soli>";
			xml_data +="<name_s>" + entrys.getEntryserviceName() + "</name_s>";
			xml_data +="<service>" + entrys.getAdTservices().getServiceName() + "</service>";
			xml_data +="<service_type>" + entrys.getAdTservices().getServiceType() + "</service_type>";
			xml_data +="<start_date>" + entrys.getEntryserviceStartdate() + "</start_date>";
			xml_data +="<end_date>" + entrys.getEntryserviceEnddate() + "</end_date>";
			xml_data +="<qty_v>" + ldetails.size() + "</qty_v>";
			xml_data +="<frecuency>" + entrys.getAdTfrencuency().getFrecuencyName() + "</frecuency>";
			xml_data +="<copago>" + entrys.getEntryserviceCopago() + "</copago>";
			xml_data +="<copago_frec>" + cfrec + "</copago_frec>";
			xml_data +="<copago_frec_id>" + (entrys.getEntryserviceInvoicecopago() == null ? "0" : entrys.getEntryserviceInvoicecopago()) + "</copago_frec_id>";
			xml_data +="<professionals></professionals>";
			xml_data +="<edit_v>" + (up != null && up.getUserpermitValue() == 1 ? "true" : "false") + "</edit_v>";
			xml_data +="</service_detail>";
			xml_data +="<visit_details>";
			hql = "from AdTdetails d where d.adTentryservices.pkentryservices = " + entrys.getPkentryservices();
			hql += " order by d.pkdetail ASC";
			ldetails = Util.dao.findByQuery(hql);
			for(AdTdetails d: ldetails){
				String status = d.getAdTentryservices().getAdTentry().getEntryEnddate() != null ? "ALTA" : "";
				String pkprof = "";
				String name = "";
				if(d.getAdTusers() == null){
					pkprof = "null";
					name = "SIN ASIGNAR";
				}else{
					pkprof = "" + d.getAdTusers().getPkuser();
					name = d.getAdTusers().getUserName1() + ' ' +
							d.getAdTusers().getUserName2() + ' ' +
							d.getAdTusers().getUserSurname1() + ' ' +
							d.getAdTusers().getUserSurname2();
				}
				xml_data += "<detail>";
				xml_data +="<id_det>" + d.getPkdetail() + "</id_det>";
				xml_data +="<date>" + (d.getDetailDate() == null ? "N/A" : formatter.format(d.getDetailDate())) + "</date>";
				xml_data +="<professional>" + name + "</professional>";
				xml_data +="<professional_id>" + pkprof + "</professional_id>";
				xml_data +="<status>" + d.getAdTstatus().getStatusName() + "</status>";
				xml_data +="<id_sta>" + d.getAdTstatus().getPkstatus() + "</id_sta>";
				xml_data +="<status_2>" + status + "</status_2>";
				xml_data += "</detail>";
			}
			xml_data +="</visit_details>";
			xml_data +="<insumo_details>";
			entrys = Util.dao.findById(AdTentryservices.class, eid);
			for(AdTentryserviceinsumo i: entrys.getAdTentryserviceinsumos()){
				xml_data += "<insumo_detail>";
				xml_data +="<id>" + i.getPkentryserviceinsumo() + "</id>";
				xml_data +="<name>" + i.getAdTinsumo().getInsumoName() + "</name>";
				xml_data +="<qty>" + i.getEntryserviceinsumoQty() + "</qty>";
				xml_data +="<fa>" + i.getAdTinvoiced().getInvoicedName() + "</fa>";
				xml_data += "</insumo_detail>";
			}
			xml_data +="</insumo_details>";
			xml_data += "</root>";
			return xml_data;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public String cancelVisits(String xml_data){
		try{
			AdTstatus status = new AdTstatus();
			status.setPkstatus(4);
			List<String> eids = new ArrayList<>();
			List<String> eids2 = XmlUtilities.getCollectionValuesTag(xml_data, "visit_id");
			for(String tmp : eids2){
				if(!tmp.equals("undefined"))
					eids.add(tmp);
			}
			List<AdTusers> lprofs = new ArrayList<AdTusers>();
			List<LsMails> lmails = new ArrayList<LsMails>();
			AdTentryservices entrys = null;
			if(eids.size() == 1){
				AdTdetails detail = Util.dao.findById(AdTdetails.class, Long.parseLong(eids.get(0)));
				entrys = detail.getAdTentryservices();
				detail.setAdTstatus(status);
				detail.setAdTusersByFkusercancelled(Util.getUserActive());
				detail.setDetailDatecancel(new Date());
				Util.dao.saveOrUpdate(detail);
				lmails.add(new LsMails(detail.getAdTusers(), 1));
			}else{
				for(String eid : eids){
					AdTdetails detail = Util.dao.findById(AdTdetails.class, Long.parseLong(eid));
					entrys = detail.getAdTentryservices();
					int pos = 0;
					boolean exist = false;
					for(LsMails u: lmails){
						pos++;
						if(u.getUser() != null && detail.getAdTusers() != null && u.getUser().getPkuser().longValue() == detail.getAdTusers().getPkuser().longValue()){
							exist = true;
							break;
						}
					}
					if(exist){
						LsMails mail = lmails.get(pos - 1);
						mail.setQty(mail.getQty() + 1);
						lmails.set(pos - 1, mail);
					}else{
						lmails.add(new LsMails(detail.getAdTusers(), 1));
					}
					lprofs.add(detail.getAdTusers());
					detail.setAdTstatus(status);
					detail.setAdTusersByFkusercancelled(Util.getUserActive());
					detail.setDetailDatecancel(new Date());
					Util.dao.saveOrUpdate(detail);
				}
			}
			try {
				for(LsMails m: lmails){
					AdTusers p = m.getUser();
					if(p != null){
						String content = Util.loadHTMLFile("templates/cancel_s.jsp");
						AdTusers patient = entrys.getAdTentry().getAdTusersByFkpatient();
						String proname = p.getUserName1() + " " + p.getUserName2() + " " + p.getUserSurname1() + " " + p.getUserSurname2();
						String patname = patient.getUserName1() + " " + patient.getUserName2() + " " + patient.getUserSurname1() + " " + patient.getUserSurname2();
						content = content.replace("{@name}", proname.toUpperCase());
						content = content.replace("{@patient_id}", patient.getAdTtypeDocument().getTypedocumentShortname() + " - " + patient.getUserDocument());
						content = content.replace("{@patient_name}", patname.toUpperCase());
						content = content.replace("{@service_name}", entrys.getAdTservices().getServiceName().toUpperCase());
						content = content.replace("{@service_qty_canceled}", "" + m.getQty());
						if(entrys.getAdTservices().getServiceType() == 2){
							new Mail("Cancelaci\u00f3n de visitas - ADOM", content, "" + p.getUserEmail() + ",adomsalud1978@gmail.com").start();
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return getEntryDetails(Long.parseLong(XmlUtilities.getValueTag(xml_data, "eid")));
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public String updateVisitsCoordinator(String xml_data){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try{
			List<List<String>> lvisits = XmlUtilities.convertToList(xml_data, "visit");
			List<LsMails> lmails1 = new ArrayList<LsMails>();
			List<LsMails> lmails2 = new ArrayList<LsMails>();
			AdTentryservices entrys = null;
			for(List<String> visit : lvisits){
				AdTdetails detail = Util.dao.findById(AdTdetails.class, Long.parseLong(visit.get(0)));
				AdTusers pactive = detail.getAdTusers();
				entrys = detail.getAdTentryservices();
				int pos = 0;
				boolean exist = false;
				Long uservisit = null;
				boolean distint = false;
				try{
					uservisit = Long.parseLong(visit.get(1));
					if(pactive == null)
						distint = true;
					else
						if(pactive.getPkuser().longValue() == uservisit)
							distint = false;
						else 
							distint = true;
				}catch(Exception e){
					uservisit = null;
					if(pactive == null)
						distint = false;
					else
						distint = true;
				}
				if(pactive != null && distint){
					for(LsMails u: lmails1){
						pos++;
						if(u.getUser().getPkuser().longValue() == pactive.getPkuser().longValue()){
							LsMails mail = lmails1.get(pos - 1);
							mail.setQty(mail.getQty() + 1);
							lmails1.set(pos - 1, mail);
							exist = true;
							break;
						}
					}
					if(!exist){
						lmails1.add(new LsMails(pactive, 1));
					}
				} 
				AdTusers np = null;
				boolean distint2 = false;
				try {
					np = Util.dao.findById(AdTusers.class, Long.parseLong(visit.get(1)));
					if(pactive == null)
						distint2 = true;
					else
						if(np.getPkuser().longValue() == pactive.getPkuser().longValue())
							distint2 = false;
						else 
							distint2 = true;
				} catch (Exception e) {
					np = null;
					if(pactive == null)
						distint2 = false;
					else
						distint2 = true;
				}
				if(np != null && distint2){
					pos = 0;
					exist = false;
					for(LsMails u: lmails2){
						pos++;
						if(u.getUser().getPkuser().longValue() == np.getPkuser().longValue()){
							LsMails mail = lmails2.get(pos - 1);
							mail.setQty(mail.getQty() + 1);
							lmails2.set(pos - 1, mail);
							exist = true;
							break;
						}
					}
					if(!exist){
						lmails2.add(new LsMails(np, 1));
					}
				}
				detail.setAdTusers(np);
				Util.dao.saveOrUpdate(detail);
			}
			try {
				for(LsMails m: lmails1){
					AdTusers p = m.getUser();
					String content = Util.loadHTMLFile("templates/cancel_v.jsp");
					AdTusers patient = entrys.getAdTentry().getAdTusersByFkpatient();
					String proname = p.getUserName1() + " " + p.getUserName2() + " " + p.getUserSurname1() + " " + p.getUserSurname2();
					String patname = patient.getUserName1() + " " + patient.getUserName2() + " " + patient.getUserSurname1() + " " + patient.getUserSurname2();
					content = content.replace("{@name}", proname.toUpperCase());
					content = content.replace("{@patient_id}", patient.getAdTtypeDocument().getTypedocumentShortname() + " - " + patient.getUserDocument());
					content = content.replace("{@patient_name}", patname.toUpperCase());
					content = content.replace("{@service_name}", entrys.getAdTservices().getServiceName().toUpperCase());
					content = content.replace("{@service_qty_retired}", "" + m.getQty());
					if(entrys.getAdTservices().getServiceType() == 2){
						new Mail("Cambios en tus servicios - ADOM", content, p.getUserEmail() + ",adomsalud1978@gmail.com").start();
					}
				}
				for(LsMails m: lmails2){
					AdTusers p = m.getUser();
					String copago_f = "";
					if(entrys.getEntryserviceFecuencycopago() == 1)
						copago_f = "CADA SESI&Oacute;N";
					else if(entrys.getEntryserviceFecuencycopago() == 5)
						copago_f = "CADA 5 SESIONES";
					else copago_f = "OTRO";
					String content = Util.loadHTMLFile("templates/assign_v.jsp");
					AdTusers patient = entrys.getAdTentry().getAdTusersByFkpatient();
					String proname = p.getUserName1() + " " + p.getUserName2() + " " + p.getUserSurname1() + " " + p.getUserSurname2();
					String patname = patient.getUserName1() + " " + patient.getUserName2() + " " + patient.getUserSurname1() + " " + patient.getUserSurname2();
					content = content.replace("{@name}", proname.toUpperCase());
					content = content.replace("{@patient_id}", patient.getAdTtypeDocument().getTypedocumentShortname() + " - " + patient.getUserDocument());
					content = content.replace("{@patient_name}", patname.toUpperCase());
					content = content.replace("{@patient_address}", patient.getUserAddress().toUpperCase());
					content = content.replace("{@patient_phone}", patient.getUserPhone1());
					content = content.replace("{@service_auto}", entrys.getEntryserviceNumerauth());
					content = content.replace("{@service_nro_auto}", entrys.getEntryserviceNumerauth());
					content = content.replace("{@service_name}", entrys.getAdTservices().getServiceName().toUpperCase());
					content = content.replace("{@service_qty}", "" + m.getQty());
					content = content.replace("{@service_startdate}",formatter.format(entrys.getEntryserviceStartdate()));
					content = content.replace("{@service_enddate}", formatter.format(entrys.getEntryserviceEnddate()));
					content = content.replace("{@service_frecuency}", entrys.getAdTfrencuency().getFrecuencyName());
					content = content.replace("{@service_copago}", "$" + entrys.getEntryserviceCopago().intValue());
					content = content.replace("{@service_copago_frecuency}", copago_f);
					if(entrys.getAdTservices().getServiceType() == 2){
						new Mail("Nueva visita asignada - ADOM", content, p.getUserEmail()).start();
						new Mail("Nueva visita asignada - ADOM", content, "adomsalud1978@gmail.com").start();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return getEntryDetails(Long.parseLong(XmlUtilities.getValueTag(xml_data, "eid")));
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public String updateVisits(String xml_data){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try{
			List<List<String>> lvisits = XmlUtilities.convertToList(xml_data, "visit");
			List<LsMails> lmails1 = new ArrayList<LsMails>();
			List<LsMails> lmails2 = new ArrayList<LsMails>();
			AdTentryservices entrys = null;
			for(List<String> visit : lvisits){
				AdTdetails detail = Util.dao.findById(AdTdetails.class, Long.parseLong(visit.get(0)));
				entrys = detail.getAdTentryservices();
				Long vid;
				try{
					vid = Long.parseLong(visit.get(1));
				}catch(Exception e){
					vid = null;
				}
				if(vid == null || visit.size() == 3){
					AdTusers pactive = detail.getAdTusers();
					entrys = detail.getAdTentryservices();
					int pos = 0;
					boolean exist = false;
					if(pactive != null){
						for(LsMails u: lmails1){
							pos++;
							if(u.getUser().getPkuser().longValue() == pactive.getPkuser().longValue()){
								LsMails mail = lmails1.get(pos - 1);
								mail.setQty(mail.getQty() + 1);
								lmails1.set(pos - 1, mail);
								exist = true;
								break;
							}
						}
						if(!exist){
							lmails1.add(new LsMails(pactive, 1));
						}
					} 
					AdTusers np = null;
					try {
						np = Util.dao.findById(AdTusers.class, Long.parseLong(visit.get(1)));
					} catch (Exception e) {
						np = null;
					}
					detail.setAdTusers(np);
					AdTstatus status = new AdTstatus();
					if(visit.size() > 3){
						status.setPkstatus(Integer.parseInt(visit.get(3)));
						detail.setAdTstatus(status);
					}
					if(!visit.get(1).toString().equals("null"))
						detail.setDetailDate(formatter.parse(visit.get(1)));
					Util.dao.saveOrUpdate(detail);
				}else{
					boolean especial;
					try{
						visit.get(3);
						especial = true;
					}catch(Exception e){
						especial = false;
					}
					Long uid = detail.getAdTusers() == null ? vid.longValue() +1 : detail.getAdTusers().getPkuser().longValue(); 
					if(vid.longValue() != uid.longValue()){
						AdTusers professional = null;
						try{
						    professional = Util.dao.findById(AdTusers.class, Long.parseLong(visit.get(1)));
						}catch(Exception e){
							professional = null;
						}
						AdTusers pactive = null;
				    	if(detail.getAdTusers() != null)
				    		pactive = Util.dao.findById(AdTusers.class, detail.getAdTusers().getPkuser());
				    	
						try{
						    professional = Util.dao.findById(AdTusers.class, Long.parseLong(visit.get(1)));
						}catch(Exception e){
							professional = null;
						}
						detail.setAdTusers(professional);
						AdTstatus status = new AdTstatus();
						status.setPkstatus(Integer.parseInt(visit.get(3)));
						detail.setAdTstatus(status);
						if(!visit.get(2).toString().trim().equals(""))
							detail.setDetailDate(formatter.parse(visit.get(2)));
						Util.dao.saveOrUpdate(detail);
				    	
						int pos = 0;
						boolean exist = false;
						if(pactive != null){
							for(LsMails u: lmails1){
								pos++;
								if(u.getUser().getPkuser().longValue() == pactive.getPkuser().longValue()){
									LsMails mail = lmails1.get(pos - 1);
									mail.setQty(mail.getQty() + 1);
									lmails1.set(pos - 1, mail);
									exist = true;
									break;
								}
							}
							if(!exist){
								lmails1.add(new LsMails(pactive, 1));
							}
						}
						if(professional != null){
							pos = 0;
							exist = false;
							for(LsMails u: lmails2){
								pos++;
								if(u.getUser().getPkuser().longValue() == professional.getPkuser().longValue()){
									LsMails mail = lmails2.get(pos - 1);
									mail.setQty(mail.getQty() + 1);
									lmails2.set(pos - 1, mail);
									exist = true;
									break;
								}
							}
							if(!exist){
								lmails2.add(new LsMails(professional, 1));
							}
						}
					}
				    if(especial){
						AdTusers professional = null;
						try{
						    professional = Util.dao.findById(AdTusers.class, Long.parseLong(visit.get(1)));
						}catch(Exception e){
							professional = null;
						}
						detail.setAdTusers(professional);
						AdTstatus status = new AdTstatus();
						status.setPkstatus(Integer.parseInt(visit.get(3)));
						detail.setAdTstatus(status);
						if(!visit.get(2).toString().trim().equals(""))
							detail.setDetailDate(formatter.parse(visit.get(2)));
						Util.dao.saveOrUpdate(detail);
				    }
				}
			}
			try {
				for(LsMails m: lmails1){
					AdTusers p = m.getUser();
					String content = Util.loadHTMLFile("templates/cancel_v.jsp");
					AdTusers patient = entrys.getAdTentry().getAdTusersByFkpatient();
					String proname = p.getUserName1() + " " + p.getUserName2() + " " + p.getUserSurname1() + " " + p.getUserSurname2();
					String patname = patient.getUserName1() + " " + patient.getUserName2() + " " + patient.getUserSurname1() + " " + patient.getUserSurname2();
					content = content.replace("{@name}", proname.toUpperCase());
					content = content.replace("{@patient_id}", patient.getAdTtypeDocument().getTypedocumentShortname() + " - " + patient.getUserDocument());
					content = content.replace("{@patient_name}", patname.toUpperCase());
					content = content.replace("{@service_name}", entrys.getAdTservices().getServiceName().toUpperCase());
					content = content.replace("{@service_qty_retired}", "" + m.getQty());
					if(entrys.getAdTservices().getServiceType() == 2){
						new Mail("Cambios en tus servicios - ADOM", content, p.getUserEmail() + ",adomsalud1978@gmail.com").start();
					}
				}
				for(LsMails m: lmails2){
					AdTusers p = m.getUser();
					String copago_f = "";
					if(entrys.getEntryserviceFecuencycopago() == 1)
						copago_f = "CADA SESI&Oacute;N";
					else if(entrys.getEntryserviceFecuencycopago() == 5)
						copago_f = "CADA 5 SESIONES";
					else copago_f = "OTRO";
					String content = Util.loadHTMLFile("templates/assign_v.jsp");
					AdTusers patient = entrys.getAdTentry().getAdTusersByFkpatient();
					String proname = p.getUserName1() + " " + p.getUserName2() + " " + p.getUserSurname1() + " " + p.getUserSurname2();
					String patname = patient.getUserName1() + " " + patient.getUserName2() + " " + patient.getUserSurname1() + " " + patient.getUserSurname2();
					content = content.replace("{@name}", proname.toUpperCase());
					content = content.replace("{@patient_id}", patient.getAdTtypeDocument().getTypedocumentShortname() + " - " + patient.getUserDocument());
					content = content.replace("{@patient_name}", patname.toUpperCase());
					content = content.replace("{@patient_address}", patient.getUserAddress().toUpperCase());
					content = content.replace("{@patient_phone}", patient.getUserPhone1());
					content = content.replace("{@service_auto}", entrys.getEntryserviceNumerauth());
					content = content.replace("{@service_nro_auto}", entrys.getEntryserviceNumerauth());
					content = content.replace("{@service_name}", entrys.getAdTservices().getServiceName().toUpperCase());
					content = content.replace("{@service_qty}", "" + m.getQty());
					content = content.replace("{@service_startdate}",formatter.format(entrys.getEntryserviceStartdate()));
					content = content.replace("{@service_enddate}", formatter.format(entrys.getEntryserviceEnddate()));
					content = content.replace("{@service_frecuency}", entrys.getAdTfrencuency().getFrecuencyName());
					content = content.replace("{@service_copago}", "$" + entrys.getEntryserviceCopago().intValue());
					content = content.replace("{@service_copago_frecuency}", copago_f);
					if(entrys.getAdTservices().getServiceType() == 2){
						new Mail("Nueva visita asignada - ADOM", content, p.getUserEmail() + ",adomsalud1978@gmail.com").start();
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return getEntryDetails(Long.parseLong(XmlUtilities.getValueTag(xml_data, "eid")));
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public String updateVisit(String xml_data){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try{
			AdTstatus status = new AdTstatus();
			status.setPkstatus(3);
			AdTdetails visit = Util.dao.findById(AdTdetails.class, Long.parseLong(XmlUtilities.getValueTag(xml_data, "esid")));
			visit.setDetailDate(formatter.parse(XmlUtilities.getValueTag(xml_data, "date")));
			try{
				visit.setDetailMoney(Long.parseLong(XmlUtilities.getValueTag(xml_data, "money")));
			}catch(Exception e){
				visit.setDetailMoney(Long.parseLong("0"));
			}
			try{
				visit.setDetailOthermoney(Long.parseLong(XmlUtilities.getValueTag(xml_data, "othermoney")));
			}catch(Exception e){
				visit.setDetailOthermoney(Long.parseLong("0"));
			}
			visit.setDetailPay(XmlUtilities.getValueTag(xml_data, "pay"));
			visit.setAdTstatus(status);
			if(XmlUtilities.getValueTag(xml_data, "pay").equals("PIN"))
				visit.setDetailPin(XmlUtilities.getValueTag(xml_data, "nro_pin"));
			visit.setDetailDateregister(new Date());
			Util.dao.saveOrUpdate(visit);
			return "OK";
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public int getEntryServicesQtyByEntry(Long eid) {
		String hql = "from AdTdetails as d where d.adTentryservices.pkentryservices = " + eid;
		hql += " and (d.adTstatus.pkstatus = 2 or d.adTstatus.pkstatus = 3)";
		List<AdTdetails> ldetails = Util.dao.findByQuery(hql);
		return ldetails.size();
	}
	
	@Override
	public int getEntryServicesQtyByServices(Set<AdTdetails> details) {
		int qty = 0;
		for(AdTdetails d: details){
			if(d.getAdTstatus().getPkstatus() == 2 || d.getAdTstatus().getPkstatus() == 3)
				qty++;
		}
		return qty;
	}
	
	@Override
	public int getEntryServicesRealizadosQtyByEntry(Long eid) {
		String hql = "from AdTdetails as d where d.adTentryservices.pkentryservices = " + eid;
		hql += " and d.adTstatus.pkstatus = 3";
		List<AdTdetails> ldetails = Util.dao.findByQuery(hql);
		return ldetails.size();
	}
	
	@Override
	public int getEntryServicesRealizadosQtyByServices(Set<AdTdetails> details) {
		int qty = 0;
		for(AdTdetails d: details){
			if(d.getAdTstatus().getPkstatus() == 3)
				qty++;
		}
		return qty;
	}
	
	@Override
	public int getEntryServicesRealizadosQtyByServicesAndByUser(Set<AdTdetails> details, Long uid) {
		int qty = 0;
		for(AdTdetails d: details){
			if(d.getAdTstatus().getPkstatus() == 3 && d.getAdTusers().getPkuser() == uid)
				qty++;
		}
		return qty;
	}
	
	@Override
	public int getEntryServicesQtyByUser(Long eid, AdTusers u) {
		String hql = "from AdTdetails as d where d.adTentryservices.pkentryservices = " + eid;
		hql += " and (d.adTstatus.pkstatus = 2 or d.adTstatus.pkstatus = 3) and adTusers.pkuser = " + u.getPkuser();
		List<AdTdetails> ldetails = Util.dao.findByQuery(hql);
		return ldetails.size();
	}
	
	@Override
	public int getEntryServicesRealizadosQtyByUser(Long eid, AdTusers u) {
		String hql = "from AdTdetails as d where d.adTentryservices.pkentryservices = " + eid;
		hql += " and d.adTstatus.pkstatus = 3  and adTusers.pkuser = " + u.getPkuser();
		List<AdTdetails> ldetails = Util.dao.findByQuery(hql);
		return ldetails.size();
	}
	
	@Override
	public int getEntryServicesByUser(Long eid, AdTusers u, int status) {
		String hql = "from AdTdetails as d where d.adTentryservices.pkentryservices = " + eid;
		hql += " and d.adTstatus.pkstatus = " + status + "  and adTusers.pkuser = " + u.getPkuser();
		List<AdTdetails> ldetails = Util.dao.findByQuery(hql);
		return ldetails.size();
	}
	
	@Override
	public String getIdsDetails(Long eid, AdTusers u, int status) {
		String ids = "";
		String hql = "from AdTdetails as d where d.adTentryservices.pkentryservices = " + eid;
		hql += " and d.adTstatus.pkstatus = " + status + "  and adTusers.pkuser = " + u.getPkuser();
		List<AdTdetails> ldetails = Util.dao.findByQuery(hql);
		for(AdTdetails d: ldetails)
			ids += d.getPkdetail() + ":";
		if(!ids.equals(""))
			ids = ids.substring(0, ids.length() - 1);
		else
			ids += "";
		return ids;
	}
	
	@Override
	public long getEntryServicesTotalCopago(Long eid, AdTusers u, int status) {
		String hql = "select sum(detailMoney) from AdTdetails as d where d.adTentryservices.pkentryservices = " + eid;
		hql += " and d.adTstatus.pkstatus = " + status + "  and adTusers.pkuser = " + u.getPkuser();
		return Util.dao.sumByHql(hql);
	}
	
	@Override
	public long getEntryServicesTotalOtherCopago(Long eid, AdTusers u, int status) {
		String hql = "select sum(detailOthermoney) from AdTdetails as d where d.adTentryservices.pkentryservices = " + eid;
		hql += " and d.adTstatus.pkstatus = " + status + "  and adTusers.pkuser = " + u.getPkuser();
		return Util.dao.sumByHql(hql);
	}
	
	@Override
	public boolean saveHistory(String xml_data) {
		try {
			AdTentry entry = Util.dao.findById(AdTentry.class, Integer.parseInt(XmlUtilities.getValueTag(xml_data, "entry")));
			AdTup up = new AdTup();
			up.setAdTusersByFkpatient(entry.getAdTusersByFkpatient());
			up.setAdTusersByFkprofessional(Util.getUserActive());
			Util.dao.saveOrUpdate(up);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@Override
	public boolean add_observation(String xml_data) {
		try {
			AdTentry entry = Util.dao.findById(AdTentry.class, Integer.parseInt(XmlUtilities.getValueTag(xml_data, "eid")));
			AdTusers user = Util.getUserActive();
			AdTobservations observation = new AdTobservations();
			observation.setAdTentry(entry);
			observation.setAdTusers(user);
			observation.setObservationDate(new Date());
			observation.setObservationText(XmlUtilities.getValueTag(xml_data, "message"));
			Util.dao.saveOrUpdate(observation);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@Override
	public boolean add_insumo(String xml_data) {
		try {
			AdTentryservices entrys = Util.dao.findById(AdTentryservices.class, Long.parseLong(XmlUtilities.getValueTag(xml_data, "entrys")));
			AdTentryserviceinsumo i = new AdTentryserviceinsumo();
			AdTinsumo insumo = new AdTinsumo();
			AdTinvoiced invoiced = new AdTinvoiced();
			invoiced.setPkinvoiced(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "invoiced")));
			insumo.setPkinsumo(Long.parseLong(XmlUtilities.getValueTag(xml_data, "insumo")));
			i.setAdTentryservices(entrys);
			i.setAdTinsumo(insumo);
			i.setAdTinvoiced(invoiced);
			i.setEntryserviceinsumoQty(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "qty")));
			Util.dao.saveOrUpdate(i);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@Override
	public boolean deleteinsumo(String xml_data) {
		try {
			AdTentryserviceinsumo i = Util.dao.findById(AdTentryserviceinsumo.class, Long.parseLong(XmlUtilities.getValueTag(xml_data, "ei")));
			Util.dao.delete(i);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	
	@Override
	public List<AdTobservations> get_observation(String xml_data) {
		try {
			String hql = "from AdTobservations as o where o.adTentry.pkentry = " + XmlUtilities.getValueTag(xml_data, "entry");
			hql += " order by o.pkobservation DESC";
			List<AdTobservations> obs = Util.dao.findByQuery(hql);
			return obs;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<AdTentryserviceinsumo> get_insumos(String xml_data) {
		try {
			String hql = "from AdTentryserviceinsumo as i where i.adTentryservices.pkentryservices = " + XmlUtilities.getValueTag(xml_data, "entrys");
			List<AdTentryserviceinsumo> obs = Util.dao.findByQuery(hql);
			return obs;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public String verifyClose(Long eid){
		String hql = "from AdTentryservices es where es.adTentry.pkentry = " + eid;
		List<AdTentryservices> entrys = Util.dao.findByQuery(hql);
		String r = "<root>";
		boolean completed = true;
		for(AdTentryservices es: entrys){
			hql = "from AdTdetails as d where d.adTentryservices.pkentryservices = " + es.getPkentryservices();
			List<AdTdetails> ld = Util.dao.findByQuery(hql);
			for(AdTdetails d: ld){
				if(d.getAdTstatus().getPkstatus() == 2){
					completed = false;
					break;
				}
			}
		}
		if(completed)
			r += "<code>100</code>";
		else
			r += "<code>200</code>";
		return r += "</root>";
	}
	
	@Override
	public String verify_complete(List<String> eids){
		String hql = "from AdTentryservices es where ";
		boolean first = true;
		for(String eid : eids){
			if(!eid.equals("undefined")){
				if(first){
					hql += "es.pkentryservices = " + eid;
					first = false;
				}else{
					hql += " or es.pkentryservices = " + eid;
				}
			}
		}
		List<AdTentryservices> entrys = Util.dao.findByQuery(hql);
		String r = "<root>";
		boolean completed = true;
		String patient = "";
		String service = "";
		for(AdTentryservices es: entrys){
			hql = "from AdTdetails as d where d.adTentryservices.pkentryservices = " + es.getPkentryservices();
			List<AdTdetails> ld = Util.dao.findByQuery(hql);
			for(AdTdetails d: ld){
				if(d.getAdTstatus().getPkstatus() == 2){
					completed = false;
					patient = d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserName1() + " ";
					patient += d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserName2() + " ";
					patient += d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserSurname1() + " ";
					patient += d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserSurname2();
					service += d.getAdTentryservices().getAdTservices().getServiceName();
					break;
				}
			}
			if(!completed)
				break;
		}
		if(completed)
			r += "<code>100</code>";
		else
			r += "<code>200</code><msg>El paciente " + patient + " tiene el servicio '" + service + "' sin completar</msg>";
		return r += "</root>";
	}
	
	@Override
	public List<String> getEntryServicesStatus(Long eid) {
		List<String> status = new ArrayList<String>();
		String hql = "from AdTdetails as d where d.adTentryservices.pkentryservices = " + eid;
		hql += " and (d.adTstatus.pkstatus = 2 or d.adTstatus.pkstatus = 3)";
		List<AdTdetails> ldetails = Util.dao.findByQuery(hql);
		boolean sp = true;
		for(AdTdetails d: ldetails){
			if(d.getAdTstatus().getPkstatus() != 4){
				sp = false;
				break;
			}
		}
		if(sp){
			status.add("CANCELADA");
			status.add("red");
		}else{
			int cp = getEntryServicesQtyByEntry(eid);
			int cr = getEntryServicesRealizadosQtyByEntry(eid);
			if(cp == cr){
				status.add("COMPLETADA");
				status.add("#08D808");
			}else{
				status.add("EN PROCESO");
				status.add("blue");
			}
		}
		return status;
	}
	
	@Override
	public List<String> getEntryServicesStatusByServices(Set<AdTdetails> details) {
		List<String> status = new ArrayList<String>();
		List<AdTdetails> ldetails = new ArrayList<AdTdetails>();
		for(AdTdetails d: details)
			if(d.getAdTstatus().getPkstatus() == 2 || d.getAdTstatus().getPkstatus() == 3)
				ldetails.add(d);
		boolean sp = true;
		for(AdTdetails d: ldetails){
			if(d.getAdTstatus().getPkstatus() != 4){
				sp = false;
				break;
			}
		}
		if(sp){
			status.add("CANCELADA");
			status.add("red");
		}else{
			int cp = getEntryServicesQtyByServices(details);
			int cr = getEntryServicesRealizadosQtyByServices(details);
			if(cp == cr){
				status.add("COMPLETADA");
				status.add("green");
			}else{
				status.add("EN PROCESO");
				status.add("blue");
			}
		}
		return status;
	}
	
	@Override
	public List<String> getEntryServicesStatusByUser(Long eid, AdTusers u) {
		List<String> status = new ArrayList<String>();
		String hql = "from AdTdetails as d where d.adTentryservices.pkentryservices = " + eid;
		hql += " and (d.adTstatus.pkstatus = 2 or d.adTstatus.pkstatus = 3) and d.adTusers.pkuser = " + u.getPkuser();
		List<AdTdetails> ldetails = Util.dao.findByQuery(hql);
		boolean sp = true;
		for(AdTdetails d: ldetails){
			if(d.getAdTstatus().getPkstatus() != 4){
				sp = false;
				break;
			}
		}
		if(sp){
			status.add("CANCELADA");
			status.add("red");
		}else{
			int cp = getEntryServicesQtyByEntry(eid);
			int cr = getEntryServicesRealizadosQtyByEntry(eid);
			if(cp == cr){
				status.add("COMPLETADA");
				status.add("green");
			}else{
				status.add("EN PROCESO");
				status.add("blue");
			}
		}
		return status;
	}
	
	@Override
	public List<String> getEntryServicesStatusByUser(Set<AdTdetails> details, Long uid, Long eid) {
		List<String> status = new ArrayList<String>();
		List<AdTdetails> ldetails = new ArrayList<AdTdetails>();
		for(AdTdetails d: details){
			if(d.getAdTstatus().getPkstatus() == 2 || d.getAdTstatus().getPkstatus() == 3 && d.getAdTusers().getPkuser() == uid){
				System.out.println("Estatus: " + d.getAdTstatus().getPkstatus());
				ldetails.add(d);
			}
		}
		boolean sp = true;
		for(AdTdetails d: ldetails){
			if(d.getAdTstatus().getPkstatus() != 4){
				sp = false;
				break;
			}
		}
		System.out.println("El valor de sp es : " + sp);
		if(sp){
			status.add("CANCELADA");
			status.add("red");
		}else{
			int cp = getEntryServicesQtyByServices(details);
			int cr = getEntryServicesRealizadosQtyByServicesAndByUser(details, uid);
			if(cp == cr){
				status.add("COMPLETADA");
				status.add("green");
			}else{
				status.add("EN PROCESO");
				status.add("blue");
			}
		}
		return status;
	}
	
	@Override
	public List<NoP> getNotProfessional() {
		List<NoP> lsinp = new ArrayList<NoP>();
		String hql = "from AdTdetails as d where ";
		hql += " d.adTstatus.pkstatus = 2 and d.adTusers is null";
		hql += " group by d.pkdetail";
		List<AdTdetails> ldetails = Util.dao.findByQuery(hql);
		for(AdTdetails d: ldetails){
			if(d.getAdTusers() == null){
				NoP nop = new NoP();
				AdTusers p = d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient();
				nop.setId("" + d.getAdTentryservices().getAdTentry().getPkentry());
				nop.setName(p.getUserName1() + " " + p.getUserName2() + " " + p.getUserSurname1() + " " + p.getUserSurname2());
				nop.setService(d.getAdTentryservices().getAdTservices().getServiceName());
				lsinp.add(nop);
				break;
			}
		}
		return lsinp;
	}
	
	@Override
	public List<AdTentryservices> getIrregularServices(){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Calendar calendar = Calendar.getInstance();
	        calendar.setTime(new Date());
	        calendar.add(Calendar.DAY_OF_YEAR, -1);
			String hql = "from AdTentryservices as es where not exists";
	        hql += "(select 1 from AdTdetails as d where d.adTentryservices.pkentryservices = es.pkentryservices and d.adTstatus.pkstatus <> 2)";
	        hql += " and es.entryserviceStartdate <= '" + formatter.format(calendar.getTime()) + "'";
	        hql += " and es.adTservices.serviceType = 2";
	        hql += "group by es.pkentryservices";
			List<AdTentryservices> ls = Util.dao.findByQuery(hql);
			return ls;
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public List<AdTentryservices> getServicesNotInvoiced(){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		try {
			Calendar calendar = Calendar.getInstance();
	        calendar.setTime(new Date());
	        calendar.add(Calendar.DAY_OF_YEAR, -3);
			String hql = "from AdTentryservices as e where e.entryserviceInvoicenumber is null";
			hql += " and e.entryserviceEnddate = '" + formatter.format(calendar.getTime()) + "'";
			return Util.dao.findByQuery(hql);
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public List<List<Object>> getCopagoLimits(){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		List<List<Object>> lp = new ArrayList<List<Object>>();
		try {
			AdTadom a = Util.dao.findById(AdTadom.class, 1);
			Long limit = a.getAdomLimit().longValue();
			Calendar calendar = Calendar.getInstance();
	        calendar.setTime(new Date());
	        calendar.add(Calendar.DAY_OF_YEAR, - a.getAdomLimitdays());
			String hql = "select d.adTusers as user, sum(d.detailMoney) as total from AdTdetails as d";
			hql += " where d.detailDate >= '" + formatter.format(calendar.getTime()) + "'";
			hql += " group by d.adTusers.pkuser";
			List<Object> r = Util.dao.findByQuery(hql);
			for(Object o: r){
				Object[] c = (Object[]) o;
				if(c[0] != null && c[1] != null){
					Long value = Long.parseLong(c[1].toString());
					if(value.longValue() >= limit){
						List<Object> ltmp = new ArrayList<Object>();
						ltmp.add(c[0]);
						ltmp.add(c[1]);
						lp.add(ltmp);
					}
				}
			}
			return lp;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}