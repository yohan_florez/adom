package com.adom.ecreativos.services;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileWriter;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Set;

import javax.imageio.ImageIO;

import jxl.write.WritableSheet;
import sun.misc.BASE64Decoder;

import com.adom.ecreativos.util.ExcelExport;
import com.adom.ecreativos.util.ExcelUtil;
import com.adom.ecreativos.util.Mail;
import com.adom.ecreativos.util.PdfData;
import com.adom.ecreativos.util.PdfExport;
import com.adom.ecreativos.util.Util;
import com.adom.ecreativos.util.XmlUtilities;
import com.adom.ecreativos.vo.AdTadom;
import com.adom.ecreativos.vo.AdTadverts;
import com.adom.ecreativos.vo.AdTcopago;
import com.adom.ecreativos.vo.AdTcopagoDetail;
import com.adom.ecreativos.vo.AdTcopagodetailDetail;
import com.adom.ecreativos.vo.AdTdetails;
import com.adom.ecreativos.vo.AdTemails;
import com.adom.ecreativos.vo.AdTentityplan;
import com.adom.ecreativos.vo.AdTentry;
import com.adom.ecreativos.vo.AdTentryserviceinsumo;
import com.adom.ecreativos.vo.AdTentryservices;
import com.adom.ecreativos.vo.AdTlogin;
import com.adom.ecreativos.vo.AdTrate;
import com.adom.ecreativos.vo.AdTuserpermit;
import com.adom.ecreativos.vo.AdTusers;

public class AdomClass implements Adom{

	@Override
	public AdTadom updateadom(String xml_data) {
		try{
			AdTadom adom = Util.dao.findById(AdTadom.class, 1);
			adom.setAdomNit(XmlUtilities.getValueTag(xml_data, "nit"));
			adom.setAdomName(XmlUtilities.getValueTag(xml_data, "name"));
			adom.setAdomCode(XmlUtilities.getValueTag(xml_data, "code"));
			adom.setAdomDepartament(XmlUtilities.getValueTag(xml_data, "dpto"));
			adom.setAdomTown(XmlUtilities.getValueTag(xml_data, "mun"));
			adom.setAdomZone(XmlUtilities.getValueTag(xml_data, "zone"));
			Util.dao.saveOrUpdate(adom);
			return adom;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public boolean changepass(String xml_data) {
		try{
			String uid;
			try{
				uid = XmlUtilities.getValueTag(xml_data, "uid");
				if(uid == null)
					uid = "24";
			}catch(Exception e){
				uid = "24";
			}
			AdTlogin u = (AdTlogin) Util.dao.findByQuery("from AdTlogin as l where l.adTusers.pkuser = " + uid).get(0);
					u.setLoginPassword(XmlUtilities.getValueTag(xml_data, "p"));
			Util.dao.saveOrUpdate(u);
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	@Override
	public AdTadverts saveadvert(String xml_data) {
		try{
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			AdTadverts a = new AdTadverts();
			a.setAdTusers(Util.getUserActive());
			a.setAdvertBody(XmlUtilities.getValueTag(xml_data, "content"));
			a.setAdvertTitle(XmlUtilities.getValueTag(xml_data, "title"));
			a.setAdvertDate(formatter.format(new Date()));
			if(!XmlUtilities.getValueTag(xml_data, "image").equals("undefined")){
				BASE64Decoder decoder = new BASE64Decoder();
				System.out.println("Image value: " + (XmlUtilities.getValueTag(xml_data, "image")));
		        byte[] imgBytes = decoder.decodeBuffer(XmlUtilities.getValueTag(xml_data, "image").split(",")[1]);        
		        BufferedImage bufImg = ImageIO.read(new ByteArrayInputStream(imgBytes));
		        //File imgOutFile = new File("/Users/yohan/Documents/Exports_Adom/file_" + formatter.format(new Date()) + ".png");
		        File imgOutFile = new File(System.getProperty("catalina.base") + "/adom_images/file_" + formatter.format(new Date()) + ".png");
		        ImageIO.write(bufImg, "png", imgOutFile);
		        a.setAdvertImage(imgOutFile.getAbsolutePath());
			}
			Util.dao.saveOrUpdate(a);
			return a;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	@Override
	public AdTadverts deleteadvert(String xml_data) {
		try{
			AdTadverts a = Util.dao.findById(AdTadverts.class, Integer.parseInt(XmlUtilities.getValueTag(xml_data, "id")));
			Util.dao.delete(a);
			return a;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<AdTadverts> getalladverts() {
		try{
			String hql = "from AdTadverts as a order by a.pkadvert DESC";
			return Util.dao.findByQuery(hql);
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public AdTadom updatelimits(String xml_data) {
		try{
			AdTadom adom = Util.dao.findById(AdTadom.class, 1);
			adom.setAdomLimit(Long.parseLong(XmlUtilities.getValueTag(xml_data, "limit")));
			adom.setAdomLimitdays(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "days")));
			Util.dao.saveOrUpdate(adom);
			return adom;
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public AdTemails getEmail(int pk) {
		return Util.dao.findById(AdTemails.class, pk);
	}

	@Override
	public boolean saveEmail1(String xml_data) {
		try{
			String file = "";
			if(XmlUtilities.getValueTag(xml_data, "option").equals("1"))
				file = "templates/new.jsp";
			if(XmlUtilities.getValueTag(xml_data, "option").equals("2"))
				file = "templates/update.jsp";
			if(XmlUtilities.getValueTag(xml_data, "option").equals("3"))
				file = "templates/assign_s.jsp";
			if(XmlUtilities.getValueTag(xml_data, "option").equals("4"))
				file = "templates/cancel_s.jsp";
			if(XmlUtilities.getValueTag(xml_data, "option").equals("5"))
				file = "templates/assign_v.jsp";
			if(XmlUtilities.getValueTag(xml_data, "option").equals("6"))
				file = "templates/cancel_v.jsp";
			File myFoo = new File(System.getProperty("catalina.base") + "/" + file);
			FileWriter fooWriter = new FileWriter(myFoo, false);
			fooWriter.write("<html><body style='width: 40%;margin-left: auto;margin-right: auto;'><center>" + XmlUtilities.getValueTag(xml_data, "content") + "</center></body></html>");
			fooWriter.close();
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean changePermit(String xml_data) {
		try{
			AdTuserpermit up = Util.dao.findById(AdTuserpermit.class, Long.parseLong(XmlUtilities.getValueTag(xml_data, "pid")));
			up.setUserpermitValue(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "value")));
			Util.dao.saveOrUpdate(up);
			return true;
		}catch(Exception e){
			e.printStackTrace();
			return false;
		}
	}
	
	@Override
	public int getHoursProg() {
		try{
			int year = Calendar.getInstance().get(Calendar.YEAR);
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			int month = cal.get(Calendar.MONTH) + 1;
			String hql = "select sum(d.adTentryservices.adTservices.serviceHours) from AdTdetails as d where d.detailDate >= '" + year + "-" + month + "-01'";
			hql += " and d.detailDate < '" + year + "-" + (month + 1) + "-01' and (d.adTstatus.pkstatus = 2 or d.adTstatus.pkstatus = 3)";
			hql += " and d.adTentryservices.adTservices.serviceType = 1";
			List<Long> suma = Util.dao.findByQuery(hql);
			return suma.get(0).intValue();
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}
	}
	
	@Override
	public int getHoursReal() {
		try{
			int year = Calendar.getInstance().get(Calendar.YEAR);
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			int month = cal.get(Calendar.MONTH) + 1;
			String hql = "select sum(d.adTentryservices.adTservices.serviceHours) from AdTdetails as d where d.detailDate >= '" + year + "-" + month + "-01'";
			hql += " and d.detailDate < '" + year + "-" + (month + 1) + "-01' and d.adTstatus.pkstatus = 3";
			hql += " and d.adTentryservices.adTservices.serviceType = 1";
			List<Long> suma = Util.dao.findByQuery(hql);
			return suma.get(0).intValue();
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}
	}
	
	@Override
	public int getTeraProg() {
		try{
			int year = Calendar.getInstance().get(Calendar.YEAR);
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			int month = cal.get(Calendar.MONTH) + 1;
			String hql = " AdTdetails as d where";
			hql += "(d.adTentryservices.entryserviceRequestdate >= '" + year +"-" + month + "-01'";
			hql += " and d.adTentryservices.entryserviceRequestdate < '" + year + "-" + (month + 1) + "-01') ";
			hql += " and d.adTentryservices.adTservices.serviceType = 2";
			hql += " and (d.adTstatus.pkstatus = 2 or d.adTstatus.pkstatus = 3)";
			Long qty = Util.dao.count(hql);
			return qty.intValue();
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}
	}
	
	@Override
	public int getTeraReal() {
		try{
			int year = Calendar.getInstance().get(Calendar.YEAR);
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			int month = cal.get(Calendar.MONTH) + 1;
			String hql = " AdTdetails as d where";
			hql += "(d.detailDate >= '" + year + "-" + month + "-01'";
			hql += " and d.detailDate < '" + year + "-" + (month + 1) + "-01') ";
			hql += " and d.adTentryservices.adTservices.serviceType = 2";
			hql += " and d.adTstatus.pkstatus = 3";
			Long qty = Util.dao.count(hql);
			return qty.intValue();
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}
	}
	
	@Override
	public int getTeraCancel() {
		try{
			int year = Calendar.getInstance().get(Calendar.YEAR);
			Calendar cal = Calendar.getInstance();
			cal.setTime(new Date());
			int month = cal.get(Calendar.MONTH) + 1;
			String hql = " AdTdetails as d where d.detailDatecancel >= '" + year + "-" + month + "-01'";
			hql += " and d.detailDatecancel < '" + year + "-" + (month + 1) + "-01' and d.adTstatus.pkstatus = 4";
			hql += " and d.adTentryservices.adTservices.serviceType = 2";
			Long qty = Util.dao.count(hql);
			return qty.intValue();
		}catch(Exception e){
			e.printStackTrace();
			return 0;
		}
	}

	@Override
	public List<AdTuserpermit> getPermissions(Long uid) {
		String hql = "from AdTuserpermit as p where p.adTusers.pkuser = " + uid;
		return Util.dao.findByQuery(hql);
	}
	
	@Override
	public AdTusers generate_password(String xml_data) {
		try {
			String pass = Util.getCode(7);
			AdTlogin login = (AdTlogin) Util.dao.findByQuery("from AdTlogin as l where l.adTusers.pkuser = " + XmlUtilities.getValueTag(xml_data, "uid")).get(0);
			login.setLoginPassword(Util.encriptar(pass));
			Util.dao.saveOrUpdate(login);
			String content = Util.loadHTMLFile("templates/update.jsp");
			content = content.replace("{@username}", login.getAdTusers().getUserEmail());
			content = content.replace("{@password}", pass);
			new Mail("Nueva contrase\u00f1a - ADOM", content, "" + login.getAdTusers().getUserEmail() + ",adomsalud1978@gmail.com").start();
			return login.getAdTusers();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public AdTusers change_status_user(String xml_data) {
		try {
			AdTlogin login = (AdTlogin) Util.dao.findByQuery("from AdTlogin as l where l.adTusers.pkuser = " + XmlUtilities.getValueTag(xml_data, "uid")).get(0);
			AdTusers u = login.getAdTusers();
			if(u.getUserEnabled())
				u.setUserEnabled(false);
			else
				u.setUserEnabled(true);
			Util.dao.saveOrUpdate(u);
			login.setAdTusers(u);
			Util.dao.saveOrUpdate(login);
			return login.getAdTusers();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public AdTentityplan change_status_plan(String xml_data) {
		try {
			AdTentityplan p = (AdTentityplan) Util.dao.findByQuery("from AdTentityplan as p where p.pkentityplan = " + XmlUtilities.getValueTag(xml_data, "pid")).get(0);
			if(p.getEntityplanStatus())
				p.setEntityplanStatus(false);
			else
				p.setEntityplanStatus(true);
			Util.dao.saveOrUpdate(p);
			return p;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<AdTentryservices> search(String xml_data) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String plan = XmlUtilities.getValueTag(xml_data, "pid");
			String entity = XmlUtilities.getValueTag(xml_data, "eid");
			Date start1 = null;
			Date end1 = null;
			Date start2 = null;
			Date end2 = null;
			if(XmlUtilities.getValueTag(xml_data, "start1").length() > 0){
				start1 = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "start1")));
				end1 = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "end1")));
			}
			if(XmlUtilities.getValueTag(xml_data, "start2").length() > 0){
				start2 = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "start2")));
				end2 = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "end2")));
			}
			String hql = " from AdTentryservices as s where s.adTservices.serviceType = " + XmlUtilities.getValueTag(xml_data, "type");
			hql += " and s.adTentry.adTentityplan.adTentity.pkentity = " + entity;
			if(!plan.equals("*"))
				hql += " and s.adTentry.adTentityplan.pkentityplan = " + plan;
			if(start1 != null && start2 != null){
				hql += " and ((s.entryserviceStartdate >= '" + formatter.format(start1) + "' and s.entryserviceStartdate <= '" + formatter.format(end1) + "')";
				hql += " or (s.entryserviceEnddate >= '" + formatter.format(start2) + "' and s.entryserviceEnddate <= '" + formatter.format(end2) + "'))";
			}else{
				if(start1 != null)
					hql += " and (s.entryserviceStartdate >= '" + formatter.format(start1) + "' and s.entryserviceStartdate <= '" + formatter.format(end1) + "')";
				else
					hql += " and (s.entryserviceEnddate >= '" + formatter.format(start2) + "' and s.entryserviceEnddate <= '" + formatter.format(end2) + "')";
			}
			hql += " group by s.pkentryservices";
			List<AdTentryservices> response = Util.dao.findByQuery(hql);
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<AdTentryservices> invoiced(String xml_data) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			List<String> lservices = XmlUtilities.getCollectionValuesTag(xml_data, "service");
			List<AdTentryservices> lreturn = new ArrayList<AdTentryservices>();
			for(String s: lservices){
				if(!s.equals("undefined")){
					AdTentryservices e = Util.dao.findById(AdTentryservices.class, Long.parseLong(s));
					if(e.getEntryserviceInvoicenumber() == null){
						String hql = "from AdTrate as r where r.adTentityplan.pkentityplan = " + e.getAdTentry().getAdTentityplan().getPkentityplan();
						hql += " and r.adTservices.pkservice = " + e.getAdTservices().getPkservice();
						AdTrate r = (AdTrate) Util.dao.findByQuery(hql).get(0);
						e.setEntryserviceInvoicenumber(XmlUtilities.getValueTag(xml_data, "invoice_number"));
						e.setEntryserviceInvoicevalue(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "invoice_value")));
						e.setEntryserviceInvoicedate(formatter.parse(XmlUtilities.getValueTag(xml_data, "invoice_date")));
						e.setEntryserviceInvoicecopago(Long.parseLong(XmlUtilities.getValueTag(xml_data, "invoice_copago")));
						e.setEntryserviceInvoicetarifa(r.getRateCost().longValue());
						Util.dao.saveOrUpdate(e);
					}
					lreturn.add(e);
				}
			}
			return lreturn;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<AdTentryservices> invoiced2(String xml_data) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			List<List<String>> lservices = XmlUtilities.convertToList(xml_data, "service");
			List<AdTentryservices> lreturn = new ArrayList<AdTentryservices>();
			for(List<String> s: lservices){
				if(!s.get(0).equals("undefined")){
					AdTentryservices e = Util.dao.findById(AdTentryservices.class, Long.parseLong(s.get(0)));
					if(e.getEntryserviceInvoicenumber() == null){
						String hql = "from AdTrate as r where r.adTentityplan.pkentityplan = " + e.getAdTentry().getAdTentityplan().getPkentityplan();
						hql += " and r.adTservices.pkservice = " + e.getAdTservices().getPkservice();
						AdTrate r = (AdTrate) Util.dao.findByQuery(hql).get(0);
						e.setEntryserviceInvoicenumber(s.get(4));
						e.setEntryserviceInvoicevalue(Integer.parseInt(s.get(2)));
						e.setEntryserviceInvoicedate(formatter.parse(s.get(3)));
						e.setEntryserviceInvoicecopago(Long.parseLong(s.get(1)));
						e.setEntryserviceInvoicetarifa(r.getRateCost().longValue());
						Util.dao.saveOrUpdate(e);
					}
					lreturn.add(e);
				}
			}
			return lreturn;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<String> generate_rip(List<AdTentryservices> lservices, int type){
		try {
			List<String> res = new ArrayList<String>();
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			ExcelExport e = ExcelUtil.createBook();
			res.add(e.getFile().getAbsolutePath());
			AdTadom adom = Util.dao.findById(AdTadom.class, 1);
			int row = 1;
			WritableSheet sheetaf = e.getSheet("AF");
			WritableSheet sheetus = e.getSheet("US");
			WritableSheet sheetac = e.getSheet("AC");
			WritableSheet sheetap = e.getSheet("AP");
			WritableSheet sheetat = e.getSheet("AT");
			WritableSheet sheetct = e.getSheet("CT");
			sheetct.addCell(e.newLabel(0, 1, adom.getAdomCode()));
			sheetct.addCell(e.newLabel(0, 2, adom.getAdomCode()));
			sheetct.addCell(e.newLabel(0, 3, adom.getAdomCode()));
			sheetct.addCell(e.newLabel(0, 4, adom.getAdomCode()));
			sheetac.addCell(e.newLabel(0, 1, "Cód dx Principal"));
			sheetac.addCell(e.newLabel(10, 1, "Cód dx Rel 1"));
			sheetac.addCell(e.newLabel(11, 1, "Cód dx Rel 2"));
			sheetac.addCell(e.newLabel(12, 1, "Cód dx Rel 3"));
			AdTentryservices stmp = lservices.get(0);
			if(type == 1){
				sheetaf.addCell(e.newLabel(0, row, adom.getAdomCode()));
				sheetaf.addCell(e.newLabel(1, row, adom.getAdomName()));
				sheetaf.addCell(e.newLabel(2, row, "NI"));
				sheetaf.addCell(e.newLabel(3, row, adom.getAdomNit()));
				sheetaf.addCell(e.newLabel(4, row, stmp.getEntryserviceInvoicenumber()));
				sheetaf.addCell(e.newLabel(5, row, formatter.format(stmp.getEntryserviceInvoicedate())));
				sheetaf.addCell(e.newLabel(6, row, formatter.format(stmp.getEntryserviceStartdate())));
				sheetaf.addCell(e.newLabel(7, row, formatter.format(stmp.getEntryserviceEnddate())));
				sheetaf.addCell(e.newLabel(8, row, stmp.getAdTentry().getAdTentityplan().getAdTentity().getEntityCode()));
				sheetaf.addCell(e.newLabel(9, row, stmp.getAdTentry().getAdTentityplan().getAdTentity().getEntityName()));
				sheetaf.addCell(e.newLabel(10, row, ""));
				sheetaf.addCell(e.newLabel(11, row, ""));
				sheetaf.addCell(e.newLabel(12, row, ""));
				sheetaf.addCell(e.newLabel(13, row, "" + stmp.getEntryserviceInvoicecopago()));
				sheetaf.addCell(e.newLabel(14, row, "0"));
				sheetaf.addCell(e.newLabel(15, row, "0"));
				sheetaf.addCell(e.newLabel(16, row, "" + stmp.getEntryserviceInvoicevalue()));
			}
			List<AdTentryservices> ladd = new ArrayList<AdTentryservices>();
			List<AdTentryservices> ladd2 = new ArrayList<AdTentryservices>();
			int row_details = 1;
			int row_details2 = 1;
			int row_user = 1;
			int row_insumo = 1;
			for(AdTentryservices s : lservices){
				/***** Hoja AF ****/
				if(type == 2){
					boolean exist2 = false;
					for(AdTentryservices es: ladd2){
						if(es.getEntryserviceInvoicenumber().equals(s.getEntryserviceInvoicenumber())){
							exist2 = true;
							break;
						}
					}
					ladd2.add(s);
					if(!exist2){
						sheetaf.addCell(e.newLabel(0, row, adom.getAdomCode()));
						sheetaf.addCell(e.newLabel(1, row, adom.getAdomName()));
						sheetaf.addCell(e.newLabel(2, row, "NI"));
						sheetaf.addCell(e.newLabel(3, row, adom.getAdomNit()));
						sheetaf.addCell(e.newLabel(4, row, s.getEntryserviceInvoicenumber()));
						sheetaf.addCell(e.newLabel(5, row, formatter.format(s.getEntryserviceInvoicedate())));
						sheetaf.addCell(e.newLabel(6, row, ""));
						sheetaf.addCell(e.newLabel(7, row, ""));
						sheetaf.addCell(e.newLabel(8, row, s.getAdTentry().getAdTentityplan().getAdTentity().getEntityCode()));
						sheetaf.addCell(e.newLabel(9, row, s.getAdTentry().getAdTentityplan().getAdTentity().getEntityName()));
						sheetaf.addCell(e.newLabel(10, row, ""));
						sheetaf.addCell(e.newLabel(11, row, ""));
						sheetaf.addCell(e.newLabel(12, row, ""));
						sheetaf.addCell(e.newLabel(13, row, "" + s.getEntryserviceInvoicecopago().intValue()));
						sheetaf.addCell(e.newLabel(14, row, "0"));
						sheetaf.addCell(e.newLabel(15, row, "0"));
						sheetaf.addCell(e.newLabel(16, row, "" + s.getEntryserviceInvoicevalue().intValue()));
					}
				}
				/***** Fin Hoja AF ****/
				/***** Hoja US ****/
				boolean exist = false;
				for(AdTentryservices es: ladd){
					if(es.getAdTentry().getAdTusersByFkpatient().getPkuser().longValue() == s.getAdTentry().getAdTusersByFkpatient().getPkuser().longValue()){
						exist = true;
						break;
					}
				}
				ladd.add(s);
				if(!exist){
					sheetus.addCell(e.newLabel(0, row_user, s.getAdTentry().getAdTusersByFkpatient().getAdTtypeDocument().getTypedocumentShortname()));
					sheetus.addCell(e.newLabel(1, row_user, s.getAdTentry().getAdTusersByFkpatient().getUserDocument()));
					sheetus.addCell(e.newLabel(2, row_user, s.getAdTentry().getAdTentityplan().getAdTentity().getEntityCode()));
					sheetus.addCell(e.newLabel(3, row_user, s.getAdTentry().getAdTusersByFkpatient().getUserTyperips().toString()));
					sheetus.addCell(e.newLabel(4, row_user, s.getAdTentry().getAdTusersByFkpatient().getUserSurname1().trim().toUpperCase()));
					sheetus.addCell(e.newLabel(5, row_user, s.getAdTentry().getAdTusersByFkpatient().getUserSurname2().trim().toUpperCase()));
					sheetus.addCell(e.newLabel(6, row_user, s.getAdTentry().getAdTusersByFkpatient().getUserName1().trim().toUpperCase()));
					sheetus.addCell(e.newLabel(7, row_user, s.getAdTentry().getAdTusersByFkpatient().getUserName2().trim().toUpperCase()));
					sheetus.addCell(e.newLabel(8, row_user, s.getAdTentry().getAdTusersByFkpatient().getUserAge().toString()));
					sheetus.addCell(e.newLabel(9, row_user, (s.getAdTentry().getAdTusersByFkpatient().getUserAgetype().equals("A") ? "1" : "2")));
					sheetus.addCell(e.newLabel(10, row_user, s.getAdTentry().getAdTusersByFkpatient().getUserGender()));
					sheetus.addCell(e.newLabel(11, row_user, adom.getAdomDepartament()));
					sheetus.addCell(e.newLabel(12, row_user, adom.getAdomTown()));
					sheetus.addCell(e.newLabel(13, row_user, adom.getAdomZone()));
					row_user++;
				}
				/***** Fin Hoja US ****/
				/***** Hoja AC ****/
				Set<AdTdetails> services = s.getAdTdetailses();
				Entry entry = new EntryClass();
				double qtyC = entry.getEntryServicesQtyByEntry(s.getPkentryservices());
				double f = s.getEntryserviceFecuencycopago();
				Double qtyPay = qtyC < f ? 1 : qtyC / f;
				int qtyPayInt = qtyPay.intValue();
				qtyPayInt = qtyPayInt == qtyPay ? qtyPayInt : qtyPayInt + 1;
				//int countqty = 0;
				for(AdTdetails d: services){
					if(d.getAdTstatus().getPkstatus() == 3 && d.getAdTentryservices().getAdTservices().getServiceType2().endsWith("C")){
						sheetac.addCell(e.newLabel(0, row_details2 + 1, s.getEntryserviceInvoicenumber()));
						sheetac.addCell(e.newLabel(1, row_details2 + 1, adom.getAdomCode()));
						sheetac.addCell(e.newLabel(2, row_details2 + 1, s.getAdTentry().getAdTusersByFkpatient().getAdTtypeDocument().getTypedocumentShortname()));
						sheetac.addCell(e.newLabel(3, row_details2 + 1, s.getAdTentry().getAdTusersByFkpatient().getUserDocument()));
						sheetac.addCell(e.newLabel(4, row_details2 + 1, (d.getDetailDate() == null ? "N/A" : formatter.format(d.getDetailDate()))));
						sheetac.addCell(e.newLabel(5, row_details2 + 1, s.getEntryserviceNumerauth()));
						sheetac.addCell(e.newLabel(6, row_details2 + 1, s.getAdTservices().getServiceCode()));
						sheetac.addCell(e.newLabel(7, row_details2 + 1, s.getEntryserviceConsultation().toString()));
						sheetac.addCell(e.newLabel(8, row_details2 + 1, s.getEntryserviceExternal().toString()));
						sheetac.addCell(e.newLabel(9, row_details2 + 1, s.getAdTentry().getEntryCie10()));
						sheetac.addCell(e.newLabel(10, row_details2 + 1, ""));
						sheetac.addCell(e.newLabel(11, row_details2 + 1, ""));
						sheetac.addCell(e.newLabel(12, row_details2 + 1, ""));
						sheetac.addCell(e.newLabel(13, row_details2 + 1, "2"));
						sheetac.addCell(e.newLabel(14, row_details2 + 1, "" + s.getEntryserviceInvoicetarifa()));
						sheetac.addCell(e.newLabel(15, row_details2 + 1, d.getDetailPay() != null && d.getDetailPay().equals("EFECTIVO") ? "" + d.getDetailMoney().intValue() : "0"));
						sheetac.addCell(e.newLabel(16, row_details2 + 1, "" + (s.getEntryserviceInvoicetarifa() - (d.getDetailMoney() == null ? 0 : d.getDetailMoney().longValue()))));
						row_details2++;
						//countqty++;
					}
				}
				/***** Fin Hoja AC ****/
				/***** Hoja AP ****/
				for(AdTdetails d: services){
					if(d.getAdTstatus().getPkstatus() == 3 && d.getAdTentryservices().getAdTservices().getServiceType2().endsWith("P")){
						sheetap.addCell(e.newLabel(0, row_details, s.getEntryserviceInvoicenumber()));
						sheetap.addCell(e.newLabel(1, row_details, adom.getAdomCode()));
						sheetap.addCell(e.newLabel(2, row_details, s.getAdTentry().getAdTusersByFkpatient().getAdTtypeDocument().getTypedocumentShortname()));
						sheetap.addCell(e.newLabel(3, row_details, s.getAdTentry().getAdTusersByFkpatient().getUserDocument()));
						sheetap.addCell(e.newLabel(4, row_details, (d.getDetailDate() == null ? "N/A" : formatter.format(d.getDetailDate()))));
						sheetap.addCell(e.newLabel(5, row_details, s.getEntryserviceNumerauth().toUpperCase()));
						sheetap.addCell(e.newLabel(6, row_details, s.getAdTservices().getServiceCode()));
						sheetap.addCell(e.newLabel(7, row_details, "1"));
						sheetap.addCell(e.newLabel(8, row_details, "2"));
						sheetap.addCell(e.newLabel(9, row_details, "4"));
						sheetap.addCell(e.newLabel(10, row_details, ""));
						sheetap.addCell(e.newLabel(11, row_details, ""));
						sheetap.addCell(e.newLabel(12, row_details, ""));
						sheetap.addCell(e.newLabel(13, row_details, ""));
						sheetap.addCell(e.newLabel(14, row_details, s.getEntryserviceInvoicetarifa().toString()));
						row_details++;
					}
				}
				/***** Fin Hoja AP ****/
				/***** Hoja AP ****/
				for(AdTentryserviceinsumo d: s.getAdTentryserviceinsumos()){
					sheetat.addCell(e.newLabel(0, row_insumo, s.getEntryserviceInvoicenumber()));
					sheetat.addCell(e.newLabel(1, row_insumo, adom.getAdomCode()));
					sheetat.addCell(e.newLabel(2, row_insumo, s.getAdTentry().getAdTusersByFkpatient().getAdTtypeDocument().getTypedocumentShortname()));
					sheetat.addCell(e.newLabel(3, row_insumo, s.getAdTentry().getAdTusersByFkpatient().getUserDocument()));
					sheetat.addCell(e.newLabel(4, row_insumo, s.getAdTentry().getEntryNumber()));
					sheetat.addCell(e.newLabel(5, row_insumo, "1"));
					sheetat.addCell(e.newLabel(6, row_insumo, d.getAdTinsumo().getInsumoCode()));
					sheetat.addCell(e.newLabel(7, row_insumo, d.getAdTinsumo().getInsumoName()));
					sheetat.addCell(e.newLabel(8, row_insumo, d.getEntryserviceinsumoQty().toString()));
					sheetat.addCell(e.newLabel(10, row_insumo, ""));
					sheetat.addCell(e.newLabel(11, row_insumo, ""));
					row_insumo++;
				}
				/***** Fin Hoja AP ****/
				row++;
			}
			e.write();
			res.add(e.getFileName());
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<String> generate_copago(String xml_data){
		try {
			List<String> res = new ArrayList<String>();
			List<List<PdfData>> listData = new ArrayList<List<PdfData>>();
			List<List<String>> details = XmlUtilities.convertToList(xml_data, "detail");
			DecimalFormat formatterNumber = new DecimalFormat("###,###.##");
			boolean first = true;
			AdTcopago copago = null;
			AdTcopagoDetail copagod = null;
			String professionalName = "";
			String document = "";
			Integer total_qty_services = 0;
			Integer total_efr = 0;
			Integer total_efe = 0;
			Integer total_dto = 0;
			Integer total_general = 0;
			String professionalDoc = "";
			for(List<String> d: details){
				String[] ids = d.get(0).split(":");
				AdTdetails detail = Util.dao.findById(AdTdetails.class, Long.parseLong(ids[0]));
				
				Long neto = ids.length * detail.getAdTentryservices().getAdTservices().getServiceValue().longValue();
				Long total = neto - (Long.parseLong(d.get(1)) - Long.parseLong(d.get(2))) - Long.parseLong(d.get(3));
				
				total_general += total.intValue();
				
				if(first){
					copago = new AdTcopago();
					AdTusers professional = detail.getAdTusers();
					professionalName = professional.getUserName1();
					professionalName += " " + professional.getUserName2();
					professionalName += " " + professional.getUserSurname1();
					professionalName += " " + professional.getUserSurname2();
					document = professional.getUserDocument();
					copago.setAdTusersByFkuser(professional);
					copago.setCopagoDate(new Date());
					copago.setAdTusersByFkuserreceived(Util.getUserActive());
					Util.dao.saveOrUpdate(copago);
					professionalDoc = professional.getUserDocument();
					first = false;
				}
				
				AdTcopagoDetail copagosave = new AdTcopagoDetail();
				copagosave.setAdTcopago(copago);
				copagosave.setCopagodetailCashdelivered(Long.parseLong(d.get(2)));
				copagosave.setCopagodetailCashreceived(Long.parseLong(d.get(1)));
				copagosave.setCopagodetailDto(Long.parseLong(d.get(3)));
				copagosave.setCopagodetailOthercash(Long.parseLong("0"));
				copagosave.setCopagodetailNeto(neto);
				copagosave.setCopagodetailTotal(total);
				Util.dao.saveOrUpdate(copagosave);
				copagod = copagosave;
				
				for(int i=0; i<ids.length; i++){
					AdTcopagodetailDetail cdetail = new AdTcopagodetailDetail();
					cdetail.setAdTcopagoDetail(copagod);
					AdTdetails mydetail = Util.dao.findById(AdTdetails.class, Long.parseLong(ids[i]));
					cdetail.setAdTdetails(mydetail);
					Util.dao.saveOrUpdate(cdetail);
					mydetail.setDetailCopago(1);
					Util.dao.saveOrUpdate(mydetail);
				}
				
				String fullname = detail.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserName1();
				fullname += " " + detail.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserName2();
				fullname += " " + detail.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserSurname1();
				fullname += " " + detail.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserSurname2();
				
				List<PdfData> data = new ArrayList<PdfData>();
				data.add(new PdfData("$" + formatterNumber.format(total), -52));
		        data.add(new PdfData("$" + formatterNumber.format(Integer.parseInt(d.get(3))), -55));
		        data.add(new PdfData("$" + formatterNumber.format(Integer.parseInt(d.get(2))), -52));
		        data.add(new PdfData("$" + formatterNumber.format(Integer.parseInt(d.get(1))), -37));
		        data.add(new PdfData("" + ids.length, -50));
		        data.add(new PdfData("$" + formatterNumber.format(detail.getAdTentryservices().getAdTservices().getServiceValue().intValue()), -150));
		        data.add(new PdfData(detail.getAdTentryservices().getAdTservices().getServiceName(), -135));
		        data.add(new PdfData(detail.getAdTentryservices().getAdTentry().getAdTentityplan().getAdTentity().getEntityName(), -163));
		        data.add(new PdfData(fullname.toUpperCase(), 0));
		        listData.add(data);
		        total_dto += Integer.parseInt(d.get(3));
		        total_efe += Integer.parseInt(d.get(2));
		        total_efr += Integer.parseInt(d.get(1));
		        total_qty_services += ids.length;
			}
			
			/*** Totales ***/
			List<PdfData> data = new ArrayList<PdfData>();
			data.add(new PdfData("", -52));
	        data.add(new PdfData("", -55));
	        data.add(new PdfData("", -52));
	        data.add(new PdfData("", -37));
	        data.add(new PdfData("", -50));
	        data.add(new PdfData("", -150));
	        data.add(new PdfData("", -135));
	        data.add(new PdfData("", -163));
	        data.add(new PdfData("", 0));
	        listData.add(data);
			data = new ArrayList<PdfData>();
			data.add(new PdfData("", -52));
	        data.add(new PdfData("$" + formatterNumber.format(total_dto), -55, true));
	        data.add(new PdfData("$"  + formatterNumber.format(total_efe), -52, true));
	        data.add(new PdfData("$" + formatterNumber.format(total_efr), -37, true));
	        data.add(new PdfData("" + total_qty_services, -50, true));
	        data.add(new PdfData("Totales", -150, true));
	        data.add(new PdfData("", -135));
	        data.add(new PdfData("", -163));
	        data.add(new PdfData("", 0));
	        listData.add(data);
			data = new ArrayList<PdfData>();
			data.add(new PdfData("", -52));
	        data.add(new PdfData("", -55));
	        data.add(new PdfData("", -52));
	        data.add(new PdfData("", -37));
	        data.add(new PdfData("", -50));
	        data.add(new PdfData("", -150));
	        data.add(new PdfData("", -135));
	        data.add(new PdfData("", -163));
	        data.add(new PdfData("", 0));
	        listData.add(data);
	        data = new ArrayList<PdfData>();
			data.add(new PdfData("", -52));
	        data.add(new PdfData("", -55));
	        data.add(new PdfData("", -52));
	        data.add(new PdfData("", -37));
	        data.add(new PdfData("", -50));
	        data.add(new PdfData("", -150));
	        data.add(new PdfData("", -135));
	        data.add(new PdfData("", -163));
	        data.add(new PdfData("", 0));
	        listData.add(data);
	        data = new ArrayList<PdfData>();
			data.add(new PdfData("$" + formatterNumber.format(total_general), -52));
	        data.add(new PdfData("TOTAL", -55, true));
	        data.add(new PdfData("", -52));
	        data.add(new PdfData("", -37));
	        data.add(new PdfData("", -50));
	        data.add(new PdfData("", -150));
	        data.add(new PdfData("", -135));
	        data.add(new PdfData("", -163));
	        data.add(new PdfData("", 0));
	        listData.add(data);
	        data = new ArrayList<PdfData>();
			data.add(new PdfData("", -52));
	        data.add(new PdfData("", -55));
	        data.add(new PdfData("", -52));
	        data.add(new PdfData("", -37));
	        data.add(new PdfData("", -50));
	        data.add(new PdfData("", -150));
	        data.add(new PdfData("", -135));
	        data.add(new PdfData("", -163));
	        data.add(new PdfData("", 0));
	        listData.add(data);
	        data = new ArrayList<PdfData>();
			data.add(new PdfData("", -52));
	        data.add(new PdfData("", -55));
	        data.add(new PdfData("", -52));
	        data.add(new PdfData("", -37));
	        data.add(new PdfData("", -50));
	        data.add(new PdfData("", -150));
	        data.add(new PdfData("", -135));
	        data.add(new PdfData("", -163));
	        AdTusers userActive = Util.getUserActive();
	        String fullname = userActive.getUserName1();
			fullname += " " + userActive.getUserName2();
			fullname += " " + userActive.getUserSurname1();
			fullname += " " + userActive.getUserSurname2();
	        data.add(new PdfData("Recibi\u00f3 en ADOM: " + fullname, 0));
	        listData.add(data);
	        /*** Totales ***/
	        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String fileName = professionalDoc + "_" + formatter.format(new Date()).replaceAll("-", "");
			//String filePath = "/home/yflorezr/Documentos/Exports_Adom/";
			String filePath = System.getProperty("catalina.base") + "/pdf/";
			boolean response = PdfExport.generate_copago(filePath + fileName + ".pdf", listData,
					professionalName, document, formatter.format(copago.getCopagoDate()), "" + copago.getPkcopago());
			if(response)
				res.add(fileName);
			else{
				return null;
			}
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			Util.SaveDateInFile("Error: " + e.getMessage());
			Util.SaveDateInFile("- Localized: " + e.getLocalizedMessage());
			return null;
		}
	}
	
	@Override
	public List<String> generate_report_general(){
		try {
			List<String> res = new ArrayList<String>();
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			ExcelExport e = ExcelUtil.createBook_nocolumns();
			res.add(e.getFile().getAbsolutePath());
			WritableSheet sheet = e.createSheet("REPORTE GENERAL", 0);
			List<String> lcolumns = new ArrayList<String>();
			lcolumns.add("Nombre Paciente");
			lcolumns.add("Número Documento");
			lcolumns.add("Tipo Documento");
			lcolumns.add("Edad");
			lcolumns.add("Direcci\u00f3n");
			lcolumns.add("Tel\u00e9fono 1");
			lcolumns.add("Tel\u00e9fono 2");
			lcolumns.add("Email");
			lcolumns.add("Entidad");
			lcolumns.add("Plan");
			lcolumns.add("Contrato");
			lcolumns.add("Autorizaci\u00f3n");
			lcolumns.add("Servicio");
			lcolumns.add("Cantidad");
			lcolumns.add("Fecha solicitud");
			lcolumns.add("Fecha Inicio");
			lcolumns.add("CIE10");
			sheet.setRowView(0, 26*20);
			int c = 0;
			for(String sc : lcolumns){
				try {
					sheet.addCell(e.newLabelTitle(c++, 0, sc));
					sheet.setColumnView(c, sc.length() + 10);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			List<AdTentryservices> allServices = Util.dao.findAll(AdTentryservices.class);
			int row = 1; 
			int qtyprofessionals = 0;
			int column_1 = 0;
			for(AdTentryservices s : allServices){
				int column = 0;
				AdTentry entry = s.getAdTentry();
				AdTusers patient = s.getAdTentry().getAdTusersByFkpatient();
				String fullname = entry.getAdTusersByFkpatient().getUserName1();
				fullname += " " + entry.getAdTusersByFkpatient().getUserName2();
				fullname += " " + entry.getAdTusersByFkpatient().getUserSurname1();
				fullname += " " + entry.getAdTusersByFkpatient().getUserSurname2();
				String age = patient.getUserAge() == null ? "" : patient.getUserAge() + " " + (patient.getUserAgetype().equals("A") ? "A\u00d1OS" : "MESES");
				sheet.addCell(e.newLabel(column++, row, fullname.trim().toUpperCase()));
				sheet.addCell(e.newLabel(column++, row, patient.getUserDocument()));
				sheet.addCell(e.newLabel(column++, row, patient.getAdTtypeDocument().getTypedocumentName()));
				sheet.addCell(e.newLabel(column++, row, age));
				sheet.addCell(e.newLabel(column++, row, patient.getUserAddress()));
				sheet.addCell(e.newLabel(column++, row, patient.getUserPhone1()));
				sheet.addCell(e.newLabel(column++, row, patient.getUserPhone2()));
				sheet.addCell(e.newLabel(column++, row, patient.getUserEmail().toUpperCase()));
				sheet.addCell(e.newLabel(column++, row, entry.getAdTentityplan().getAdTentity().getEntityName().toUpperCase()));
				sheet.addCell(e.newLabel(column++, row, entry.getAdTentityplan().getEntityplanName().toUpperCase()));
				sheet.addCell(e.newLabel(column++, row, entry.getEntryNumber().toUpperCase()));
				sheet.addCell(e.newLabel(column++, row, s.getEntryserviceNumerauth().toUpperCase()));
				sheet.addCell(e.newLabel(column++, row, s.getAdTservices().getServiceName().toUpperCase()));
				sheet.addCell(e.newLabel(column++, row, "" + s.getAdTdetailses().size()));
				sheet.addCell(e.newLabel(column++, row, formatter.format(s.getEntryserviceRequestdate())));
				sheet.addCell(e.newLabel(column++, row, formatter.format(s.getEntryserviceStartdate())));
				sheet.addCell(e.newLabel(column++, row, entry.getEntryCie10().toUpperCase()));
				column_1 = column;
				List<AdTusers> lprof = new ArrayList<>();
				int qtyprof = 0;
				for(AdTdetails d: s.getAdTdetailses()){
					boolean exist = false;
					for(AdTusers element : lprof){
						if(d.getAdTusers() != null && element != null && element.getPkuser().longValue() == d.getAdTusers().getPkuser().longValue()){
							exist = true;
							break;
						}
					}
					if(!exist)
						lprof.add(d.getAdTusers());
				}
				for(AdTusers u : lprof){
					if(u != null){
						String fullname2 = u.getUserName1();
						fullname2 += " " + u.getUserName2();
						fullname2 += " " + u.getUserSurname1();
						fullname2 += " " + u.getUserSurname2();
						sheet.addCell(e.newLabel(column++, row, fullname2.trim().toUpperCase()));
						qtyprof++;
					}
				}
				qtyprofessionals = qtyprofessionals < qtyprof ? qtyprof : qtyprofessionals;
				qtyprof = 0;
				row++;
			}
			for(int i=0;i<qtyprofessionals; i++){
				try {
					sheet.addCell(e.newLabelTitle(column_1, 0, "PROFESIONAL ASIGNADO " + (i+1)));
					sheet.setColumnView(column_1, "PROFESIONAL ASIGNADO ".length() + 20);
					column_1++;
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
				
			e.write();
			res.add(e.getFileName());
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			List<String> l = new ArrayList<String>();
			l.add(e.getMessage());
			return l;
		}
	}
	
	@Override
	public List<String> generate_report_general2(String xml_data){
		try {
			List<String> res = new ArrayList<String>();
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
			ExcelExport e = ExcelUtil.createBook_nocolumns();
			res.add(e.getFile().getAbsolutePath());
			WritableSheet sheet = e.createSheet("CONSOLIDADO", 0);
			WritableSheet sheet1 = e.createSheet("DETALLADO", 1);
			List<String> lcolumns = new ArrayList<String>();
			lcolumns.add("Nombre Paciente".toUpperCase());
			lcolumns.add("Número Documento".toUpperCase());
			lcolumns.add("Tipo Documento".toUpperCase());
			lcolumns.add("Entidad".toUpperCase());
			lcolumns.add("Contrato".toUpperCase());
			lcolumns.add("Plan");
			lcolumns.add("Autorizaci\u00f3n");
			lcolumns.add("Servicio");
			lcolumns.add("N. Sesiones Programadas");
			lcolumns.add("N. Sesiones Completadas");
			lcolumns.add("Frec. Servicio");
			lcolumns.add("Estado");
			lcolumns.add("tarifa");
			lcolumns.add("copago");
			lcolumns.add("Frec, Copago");
			lcolumns.add("Fecha Solicitud");
			lcolumns.add("Fecha Inicio");
			lcolumns.add("Fecha Fin");
			lcolumns.add("Diagnostico");
			lcolumns.add("Edad");
			lcolumns.add("Genero");
			lcolumns.add("Tel\u00e9fono 1");
			lcolumns.add("Tel\u00e9fono 2");
			lcolumns.add("Direcci\u00f3n");
			lcolumns.add("Email");
			sheet.setRowView(0, 26*20);
			int c = 0;
			for(String sc : lcolumns){
				try {
					sheet.addCell(e.newLabelTitle(c++, 2, sc));
					sheet.setColumnView(c, sc.length() + 10);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			
			lcolumns = new ArrayList<String>();
			lcolumns.add("Nombre Paciente".toUpperCase());
			lcolumns.add("Número Documento".toUpperCase());
			lcolumns.add("Tipo Documento".toUpperCase());
			lcolumns.add("Entidad".toUpperCase());
			lcolumns.add("Contrato".toUpperCase());
			lcolumns.add("Plan");
			lcolumns.add("Autorizaci\u00f3n");
			lcolumns.add("Servicio");
			lcolumns.add("Frec. Servicio");
			lcolumns.add("Estado");
			lcolumns.add("tarifa");
			lcolumns.add("copago");
			lcolumns.add("Frec, Copago");
			lcolumns.add("Fecha Solicitud");
			lcolumns.add("Fecha Atenci\u00f3n");
			lcolumns.add("Copago Pagado");
			lcolumns.add("pin");
			lcolumns.add("profesional");
			lcolumns.add("doc. prof.");
			lcolumns.add("fecha de registro");
			lcolumns.add("Diagnostico");
			lcolumns.add("Edad");
			lcolumns.add("Genero");
			lcolumns.add("Tel\u00e9fono 1");
			lcolumns.add("Tel\u00e9fono 2");
			lcolumns.add("Direcci\u00f3n");
			lcolumns.add("Email");
			sheet.setRowView(0, 26*20);
			c = 0;
			for(String sc : lcolumns){
				try {
					sheet1.addCell(e.newLabelTitle(c++, 2, sc));
					sheet1.setColumnView(c, sc.length() + 10);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			
			String hql = "from AdTdetails as d ";
			String eid = XmlUtilities.getValueTag(xml_data, "eid");
			Date start = null;
			Date start1 = null;
			Date start2 = null;
			Date end = null;
			Date end1 = null;
			Date end2 = null;
			if(XmlUtilities.getValueTag(xml_data, "start").length() > 0){
				start = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "start")));
				end = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "end")));
			}
			if(XmlUtilities.getValueTag(xml_data, "start1").length() > 0){
				start1 = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "start1")));
				end1 = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "end1")));
			}
			if(XmlUtilities.getValueTag(xml_data, "start2").length() > 0){
				start2 = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "start2")));
				end2 = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "end2")));
			}
			if(eid.equals("*")){
				boolean where= false;
				if(start != null){
					where = true;
					hql += " where (d.adTentryservices.entryserviceStartdate >= '" + formatter2.format(start) + "'";
					hql += " and d.adTentryservices.entryserviceStartdate <= '" + formatter2.format(end) + "')";
				}
				if(start1 != null){
					if(where)
						hql += " and (d.adTentryservices.entryserviceRequestdate >= '" + formatter2.format(start1) + "'";
					else{
						where = true;
						hql += " where (d.adTentryservices.entryserviceRequestdate >= '" + formatter2.format(start1) + "'";
					}
					hql += " and d.adTentryservices.entryserviceRequestdate <= '" + formatter2.format(end1) + "')";
				}
				if(start2 != null){
					if(where)
						hql += " and (d.detailDate >= '" + formatter2.format(start2) + "'";
					else
						hql += " where (d.detailDate >= '" + formatter2.format(start2) + "'";
					hql += " and d.detailDate <= '" + formatter2.format(end2) + "')";
				}
			}else{
				hql += " where d.adTentryservices.adTentry.adTentityplan.adTentity.pkentity = " + eid;
				if(start != null){
					hql += " and (d.adTentryservices.entryserviceStartdate >= '" + formatter2.format(start) + "'";
					hql += " and d.adTentryservices.entryserviceStartdate <= '" + formatter2.format(end) + "')";
				}
				if(start1 != null){
					hql += " and (d.adTentryservices.entryserviceRequestdate >= '" + formatter2.format(start1) + "'";
					hql += " and d.adTentryservices.entryserviceRequestdate <= '" + formatter2.format(end1) + "')";
				}
				if(start2 != null){
					hql += " and (d.detailDate >= '" + formatter2.format(start2) + "'";
					hql += " and d.detailDate <= '" + formatter2.format(end2) + "')";
				}
			}
			hql += " order by d.adTentryservices.pkentryservices DESC";
			List<AdTdetails> ldetails = Util.dao.findByQuery(hql);
			System.out.println("Total: " + ldetails.size());
			int row = 3;
			boolean first = true;
			int active = 0;
			AdTentryservices entry = null;
			String cfrec = "";
			int row_conso = 3;
			int qtyprofessionals = 0;
			SimpleDateFormat formatte_ = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			sheet1.addCell(e.newLabel(0, 0, "Fecha y hora del reporte"));
			sheet1.addCell(e.newLabel(1, 0, formatte_.format(new Date())));
			sheet.addCell(e.newLabel(0, 0, "Fecha y hora del reporte"));
			sheet.addCell(e.newLabel(1, 0, formatte_.format(new Date())));
			for(AdTdetails d: ldetails){
				if(first){
					active = d.getAdTentryservices().getPkentryservices().intValue();
					entry = d.getAdTentryservices();
					first = false;
				}
				if(d.getAdTentryservices().getEntryserviceFecuencycopago() == 1)
					cfrec = "CADA SESI\u00d3N";
				if(d.getAdTentryservices().getEntryserviceFecuencycopago() == 5)
					cfrec = "CADA 5 SESIONES";
				if(d.getAdTentryservices().getEntryserviceFecuencycopago() == 0)
					cfrec = "OTRO";
				String fullname = d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserName1();
				fullname += " " + d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserName2();
				fullname += " " + d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserSurname1();
				fullname += " " + d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserSurname2();
				String age = d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserAge().toString();
				String ageT = d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserAgetype();
				age += ageT.equals("A") ? " AÑOS" : " MESES";
				sheet1.addCell(e.newLabel(0, row, fullname.trim().toUpperCase()));
				String fullname2 = "";
				String DocProf = "";
				if(d.getAdTusers() != null){
					fullname2 = d.getAdTusers().getUserName1();
					fullname2 += " " + d.getAdTusers().getUserName2();
					fullname2 += " " + d.getAdTusers().getUserSurname1();
					fullname2 += " " + d.getAdTusers().getUserSurname2();
					DocProf = d.getAdTusers().getUserDocument();
				}else
					fullname2 = "POR ASIGNAR";
				sheet1.addCell(e.newLabel(1, row, d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserDocument()));
				sheet1.addCell(e.newLabel(2, row, d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getAdTtypeDocument().getTypedocumentShortname()));
				sheet1.addCell(e.newLabel(3, row, d.getAdTentryservices().getAdTentry().getAdTentityplan().getAdTentity().getEntityName().toUpperCase()));
				sheet1.addCell(e.newLabel(4, row, d.getAdTentryservices().getAdTentry().getEntryNumber()));
				sheet1.addCell(e.newLabel(5, row, d.getAdTentryservices().getAdTentry().getAdTentityplan().getEntityplanName().toUpperCase()));
				sheet1.addCell(e.newLabel(6, row, d.getAdTentryservices().getEntryserviceNumerauth()));
				sheet1.addCell(e.newLabel(7, row, d.getAdTentryservices().getAdTservices().getServiceName().toUpperCase()));
				sheet1.addCell(e.newLabel(8, row, d.getAdTentryservices().getAdTfrencuency().getFrecuencyName().toUpperCase()));
				sheet1.addCell(e.newLabel(9, row, "" + d.getAdTstatus().getStatusName()));
				sheet1.addCell(e.newLabel(10, row, "" + d.getAdTentryservices().getEntryserviceInvoicetarifa().intValue()));
				sheet1.addCell(e.newLabel(11, row, "" + d.getAdTentryservices().getEntryserviceCopago().intValue()));
				sheet1.addCell(e.newLabel(12, row, cfrec));
				sheet1.addCell(e.newLabel(13, row, formatter.format(d.getAdTentryservices().getEntryserviceRequestdate())));
				sheet1.addCell(e.newLabel(14, row, d.getDetailDate() == null || d.getAdTstatus().getPkstatus() != 3 ? "" : formatter.format(d.getDetailDate())));
				sheet1.addCell(e.newLabel(15, row, d.getDetailMoney() == null ? "" : "" + d.getDetailMoney()));
				sheet1.addCell(e.newLabel(16, row, d.getDetailPin() == null ? "" : "" + d.getDetailPin()));
				sheet1.addCell(e.newLabel(17, row, fullname2));
				sheet1.addCell(e.newLabel(18, row, DocProf));
				sheet1.addCell(e.newLabel(19, row, d.getDetailDateregister() == null ? "" : formatter.format(d.getDetailDateregister())));
				sheet1.addCell(e.newLabel(20, row, d.getAdTentryservices().getAdTentry().getEntryCie10description()));
				sheet1.addCell(e.newLabel(21, row, age));
				sheet1.addCell(e.newLabel(22, row, d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserGender()));
				sheet1.addCell(e.newLabel(23, row, d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserPhone1()));
				sheet1.addCell(e.newLabel(24, row, d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserPhone2()));
				sheet1.addCell(e.newLabel(25, row, d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserAddress()));
				sheet1.addCell(e.newLabel(26, row, d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserEmail()));
				row++;
				if(active != d.getAdTentryservices().getPkentryservices().intValue()){
					int prog = 0, compl = 0; String state = "";
					for(AdTdetails dtemp : entry.getAdTdetailses()){
						prog += dtemp.getAdTstatus().getPkstatus() == 2 || dtemp.getAdTstatus().getPkstatus() == 3 ? 1 : 0;
						compl += dtemp.getAdTstatus().getPkstatus() == 3 ? 1 : 0;
					}
					state = prog > compl ? "EN PROCESO" : "COMPLETADO";
					state = prog == 0 ? "CANCELADO" : state;
					if(entry.getEntryserviceFecuencycopago() == 1)
						cfrec = "CADA SESI\u00d3N";
					if(entry.getEntryserviceFecuencycopago() == 5)
						cfrec = "CADA 5 SESIONES";
					if(entry.getEntryserviceFecuencycopago() == 0)
						cfrec = "OTRO";
					fullname = entry.getAdTentry().getAdTusersByFkpatient().getUserName1();
					fullname += " " + entry.getAdTentry().getAdTusersByFkpatient().getUserName2();
					fullname += " " + entry.getAdTentry().getAdTusersByFkpatient().getUserSurname1();
					fullname += " " + entry.getAdTentry().getAdTusersByFkpatient().getUserSurname2();
					age = entry.getAdTentry().getAdTusersByFkpatient().getUserAge().toString();
					ageT = entry.getAdTentry().getAdTusersByFkpatient().getUserAgetype();
					age += ageT.equals("A") ? " AÑOS" : " MESES";
					sheet.addCell(e.newLabel(0, row_conso, fullname.toUpperCase()));
					sheet.addCell(e.newLabel(1, row_conso, entry.getAdTentry().getAdTusersByFkpatient().getUserDocument()));
					sheet.addCell(e.newLabel(2, row_conso, entry.getAdTentry().getAdTusersByFkpatient().getAdTtypeDocument().getTypedocumentShortname()));
					sheet.addCell(e.newLabel(3, row_conso, entry.getAdTentry().getAdTentityplan().getAdTentity().getEntityName().toUpperCase()));
					sheet.addCell(e.newLabel(4, row_conso, entry.getAdTentry().getEntryNumber()));
					sheet.addCell(e.newLabel(5, row_conso, entry.getAdTentry().getAdTentityplan().getEntityplanName().toUpperCase()));
					sheet.addCell(e.newLabel(6, row_conso, entry.getEntryserviceNumerauth()));
					sheet.addCell(e.newLabel(7, row_conso, entry.getAdTservices().getServiceName().toUpperCase()));
					sheet.addCell(e.newLabel(8, row_conso, "" + prog));
					sheet.addCell(e.newLabel(9, row_conso, "" + compl));
					sheet.addCell(e.newLabel(10, row_conso, entry.getAdTfrencuency().getFrecuencyName().toUpperCase()));
					sheet.addCell(e.newLabel(11, row_conso, state));
					sheet.addCell(e.newLabel(12, row_conso, "" + entry.getEntryserviceInvoicetarifa().intValue()));
					sheet.addCell(e.newLabel(13, row_conso, "" + entry.getEntryserviceCopago().intValue()));
					sheet.addCell(e.newLabel(14, row_conso, cfrec));
					sheet.addCell(e.newLabel(15, row_conso, formatter.format(entry.getEntryserviceRequestdate())));
					sheet.addCell(e.newLabel(16, row_conso, entry.getEntryserviceStartdate() == null ? "" : formatter.format(entry.getEntryserviceStartdate())));
					sheet.addCell(e.newLabel(17, row_conso, entry.getEntryserviceEnddate() == null ? "" : formatter.format(entry.getEntryserviceEnddate())));
					sheet.addCell(e.newLabel(18, row_conso, entry.getAdTentry().getEntryCie10description()));
					sheet.addCell(e.newLabel(19, row_conso, age));
					sheet.addCell(e.newLabel(20, row_conso, entry.getAdTentry().getAdTusersByFkpatient().getUserGender()));
					sheet.addCell(e.newLabel(21, row_conso, entry.getAdTentry().getAdTusersByFkpatient().getUserPhone1()));
					sheet.addCell(e.newLabel(22, row_conso, entry.getAdTentry().getAdTusersByFkpatient().getUserPhone2()));
					sheet.addCell(e.newLabel(23, row_conso, entry.getAdTentry().getAdTusersByFkpatient().getUserAddress()));
					sheet.addCell(e.newLabel(24, row_conso, entry.getAdTentry().getAdTusersByFkpatient().getUserEmail()));
					
					int column = 25;
					List<AdTusers> lprof = new ArrayList<>();
					int qtyprof = 0;
					for(AdTdetails d_: entry.getAdTdetailses()){
						boolean exist = false;
						for(AdTusers element : lprof){
							if(d_.getAdTusers() != null && element != null && element.getPkuser().longValue() == d_.getAdTusers().getPkuser().longValue()){
								exist = true;
								break;
							}
						}
						if(!exist)
							lprof.add(d_.getAdTusers());
					}
					for(AdTusers u : lprof){
						if(u != null){
							String fullname3 = u.getUserName1();
							fullname3 += " " + u.getUserName2();
							fullname3 += " " + u.getUserSurname1();
							fullname3 += " " + u.getUserSurname2();
							sheet.addCell(e.newLabel(column++, row_conso, fullname3.trim().toUpperCase()));
							sheet.addCell(e.newLabel(column++, row_conso, u.getUserDocument()));
							qtyprof++;
						}
					}
					qtyprofessionals = qtyprofessionals < qtyprof ? qtyprof : qtyprofessionals;
					qtyprof = 0;
					row_conso++;
					active = d.getAdTentryservices().getPkentryservices().intValue();
					entry = d.getAdTentryservices();
				}
			}
			int column_1 = 25;
			for(int i=0;i<qtyprofessionals; i++){
				try {
					sheet.addCell(e.newLabelTitle(column_1, 2, "PROFESIONAL ASIGNADO " + (i+1)));
					sheet.setColumnView(column_1++, "PROFESIONAL ASIGNADO ".length() + 20);
					sheet.addCell(e.newLabelTitle(column_1, 2, "DOC. PROF " + (i+1)));
					sheet.setColumnView(column_1++, "DOC. PROF ".length() + 20);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			e.write();
			res.add(e.getFileName());
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			List<String> l = new ArrayList<String>();
			l.add(e.getMessage());
			return l;
		}
	}
	
	@Override
	public List<String> generate_report_copago(String xml_data){
		try {
			List<String> res = new ArrayList<String>();
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
			ExcelExport e = ExcelUtil.createBook_nocolumns();
			res.add(e.getFile().getAbsolutePath());
			WritableSheet sheet = e.createSheet("REPORTE COPAGOS", 0);
			List<String> lcolumns = new ArrayList<String>();
			lcolumns.add("Nombre Paciente");
			lcolumns.add("Número Documento");
			lcolumns.add("Fecha inicio");
			lcolumns.add("Fecha finalizaci\u00f3n");
			lcolumns.add("Entidad");
			lcolumns.add("Plan");
			lcolumns.add("Número autorizaci\u00f3n");
			lcolumns.add("Servicio");
			lcolumns.add("Valor copago");
			lcolumns.add("Frec. copago");
			lcolumns.add("Valor Cop. Reportados");
			lcolumns.add("Valor Cop. Entregados");
			lcolumns.add("Descuento");
			lcolumns.add("Valor Pagar");
			lcolumns.add("Saldo");
			lcolumns.add("Nombre Profesional");
			lcolumns.add("Documento Profesional");
			lcolumns.add("Fecha Entrega");
			lcolumns.add("Recibio en ADOM");
			lcolumns.add("Nro. Comprobante");
			sheet.setRowView(0, 26*20);
			int c = 0;
			for(String sc : lcolumns){
				try {
					sheet.addCell(e.newLabelTitle(c++, 0, sc));
					sheet.setColumnView(c, sc.length() + 10);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			String pid = XmlUtilities.getValueTag(xml_data, "pid");
			String number = XmlUtilities.getValueTag(xml_data, "nro");
			String date_start = XmlUtilities.getValueTag(xml_data, "start");
			String date_end = XmlUtilities.getValueTag(xml_data, "end");
			Date start = null, end = null;
			if(!date_start.equals(""))
				start = new Date(Long.parseLong(date_start));
			if(!date_end.equals(""))
				end = new Date(Long.parseLong(date_end));
			String hql = "from AdTcopago as c";
			if(!number.equals("")){
				hql += " where c.pkcopago = " + number;
			}else{
				if(XmlUtilities.getValueTag(xml_data, "start").length() > 0){
					//start = Util.AddDaysToDate(start, 1);
					//end = Util.AddDaysToDate(end, 1);
					hql += " where (c.copagoDate >= '" + formatter2.format(start) + "'";
					hql += " and c.copagoDate <= '" + formatter2.format(end) + "')";
				}
				if(!pid.equals("*") && XmlUtilities.getValueTag(xml_data, "start").length() > 0)
					hql += " and c.adTusersByFkuser.pkuser = " + pid;
				else
					if(!pid.equals("*"))
						hql += " where c.adTusersByFkuser.pkuser = " + pid;
			}
			//Util.SaveDateInFile("hql: " + hql);
			List<AdTcopago> allCopagos = Util.dao.findByQuery(hql);
			int row = 1; 
			for(AdTcopago co : allCopagos){
				for(AdTcopagoDetail cd : co.getAdTcopagoDetails()){
					int column = 0;
					AdTcopagodetailDetail detaild = cd.getAdTcopagodetailDetails().iterator().next();
					AdTdetails detail = detaild.getAdTdetails();
					AdTentryservices s = detail.getAdTentryservices();
					AdTentry entry = s.getAdTentry();
					AdTusers patient = entry.getAdTusersByFkpatient();
					AdTusers professional = co.getAdTusersByFkuser();
					AdTusers received = co.getAdTusersByFkuserreceived();
					String fullNamePatient = patient.getUserName1().trim();
					fullNamePatient += " " + patient.getUserName2().trim();
					fullNamePatient += " " + patient.getUserSurname1().trim();
					fullNamePatient += " " + patient.getUserSurname2().trim();
					String fullNameProfessional = professional.getUserName1().trim();
					fullNameProfessional += " " + professional.getUserName2().trim();
					fullNameProfessional += " " + professional.getUserSurname1().trim();
					fullNameProfessional += " " + professional.getUserSurname2().trim();
					String fullNameReceived = received.getUserName1().trim();
					fullNameReceived += " " + received.getUserName2().trim();
					fullNameReceived += " " + received.getUserSurname1().trim();
					fullNameReceived += " " + received.getUserSurname2().trim();
					
					sheet.addCell(e.newLabel(column++, row, fullNamePatient.toUpperCase()));
					sheet.addCell(e.newLabel(column++, row, patient.getUserDocument()));
					sheet.addCell(e.newLabel(column++, row, formatter.format(detail.getAdTentryservices().getEntryserviceStartdate())));
					sheet.addCell(e.newLabel(column++, row, formatter.format(detail.getAdTentryservices().getEntryserviceEnddate())));
					sheet.addCell(e.newLabel(column++, row, entry.getAdTentityplan().getAdTentity().getEntityName().toUpperCase()));
					sheet.addCell(e.newLabel(column++, row, entry.getAdTentityplan().getEntityplanName().toUpperCase()));
					sheet.addCell(e.newLabel(column++, row, s.getEntryserviceNumerauth().toUpperCase()));
					sheet.addCell(e.newLabel(column++, row, s.getAdTservices().getServiceName().toUpperCase()));
					sheet.addCell(e.newLabel(column++, row, "" + s.getEntryserviceCopago().intValue()));
					String cfrec = "";
					if(s.getEntryserviceFecuencycopago() == 1)
						cfrec = "CADA SESI\u00d3N";
					if(s.getEntryserviceFecuencycopago() == 5)
						cfrec = "CADA 5 SESIONES";
					if(s.getEntryserviceFecuencycopago() == 0)
						cfrec = "OTRO";
					sheet.addCell(e.newLabel(column++, row, cfrec));
					sheet.addCell(e.newLabel(column++, row, "" + cd.getCopagodetailCashreceived().intValue()));
					sheet.addCell(e.newLabel(column++, row, "" + cd.getCopagodetailCashdelivered().intValue()));
					sheet.addCell(e.newLabel(column++, row, "" + cd.getCopagodetailDto().intValue()));
					sheet.addCell(e.newLabel(column++, row, "" + cd.getCopagodetailNeto().intValue()));
					sheet.addCell(e.newLabel(column++, row, "" + cd.getCopagodetailTotal().intValue()));
					sheet.addCell(e.newLabel(column++, row, fullNameProfessional.toUpperCase()));
					sheet.addCell(e.newLabel(column++, row, professional.getUserDocument()));
					sheet.addCell(e.newLabel(column++, row, formatter.format(co.getCopagoDate())));
					sheet.addCell(e.newLabel(column++, row, fullNameReceived.toUpperCase()));
					int pos = 10 - co.getPkcopago().toString().length();
					String compNumber = "0000000000".substring(0, pos);
			        compNumber += "" + co.getPkcopago();
					sheet.addCell(e.newLabel(column++, row, compNumber));
					row++;
				}
			}
			e.write();
			res.add(e.getFileName());
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			List<String> l = new ArrayList<String>();
			l.add(e.getMessage());
			return l;
		}
	}
	
	@Override
	public List<String> generate_report_terapias(String xml_data){
		try {
			List<String> res = new ArrayList<String>();
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
			ExcelExport e = ExcelUtil.createBook_nocolumns();
			res.add(e.getFile().getAbsolutePath());
			WritableSheet sheetrt = e.createSheet("REPORTE DE TERAPIAS", 0);
			List<String> lcolumns = new ArrayList<String>();
			lcolumns.add("Nombre Paciente");
			lcolumns.add("N\u00famero Documento");
			lcolumns.add("Tipo Documento");
			lcolumns.add("Entidad");
			lcolumns.add("CIE10");
			lcolumns.add("Servicio");
			lcolumns.add("Frecuencia Servicio");
			lcolumns.add("Fecha Inicio");
			lcolumns.add("Fecha Atenci\u00f3n");
			lcolumns.add("Estado");
			lcolumns.add("Tipo Copago");
			lcolumns.add("N\u00famero PIN");
			lcolumns.add("Valor (Efectivo)");
			lcolumns.add("Nombre Profesional");
			lcolumns.add("C\u00e9dula Profesional");
			lcolumns.add("Fecha Solicitud");
			sheetrt.setRowView(0, 26*20);
			int c = 0;
			for(String sc : lcolumns){
				try {
					sheetrt.addCell(e.newLabelTitle(c++, 0, sc));
					sheetrt.setColumnView(c, sc.length() + 10);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			sheetrt.setColumnView(0, 50);
			String hql = "from AdTdetails as d where";
			String eid = XmlUtilities.getValueTag(xml_data, "eid");
			Date start = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "start")));
			Date end = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "end")));
			
			/****** Se agrega 1 día para que funcione con las fechas correctas en el servidor ****/
			/** Para pruebas locales comentar estas 2 funciones **/
			start = Util.AddDaysToDate(start, 1);
			end = Util.AddDaysToDate(end, 1);
			//Util.SaveDateInFile("generate_report_terapias -> Start: " + formatter2.format(start));
			//Util.SaveDateInFile("generate_report_terapias -> End: " + formatter2.format(end));
			/************************************************************************************/
			
			if(eid.equals("*")){
				hql += " (d.detailDate >= '" + formatter2.format(start) + "'";
				hql += " and d.detailDate <= '" + formatter2.format(end) + "')";
			}else{
				String pid = XmlUtilities.getValueTag(xml_data, "pid");
				if(pid.equals("*"))
					hql += " d.adTentryservices.adTentry.adTentityplan.adTentity.pkentity = " + eid;
				else
					hql += " d.adTentryservices.adTentry.adTentityplan.pkentityplan = " + pid;
				hql += " and (d.detailDate >= '" + formatter2.format(start) + "'";
				hql += " and d.detailDate <= '" + formatter2.format(end) + "')";
			}
			hql += " and d.adTstatus.pkstatus = 3";
			hql += " and d.adTentryservices.adTservices.serviceType = 2";
			List<AdTdetails> ldetails = Util.dao.findByQuery(hql);
			int row = 1;
			for(AdTdetails d: ldetails){
				String fullname = d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserName1();
				fullname += " " + d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserName2();
				fullname += " " + d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserSurname1();
				fullname += " " + d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserSurname2();
				String fullname2 = d.getAdTusers().getUserName1();
				fullname2 += " " + d.getAdTusers().getUserName2();
				fullname2 += " " + d.getAdTusers().getUserSurname1();
				fullname2 += " " + d.getAdTusers().getUserSurname2();
				sheetrt.addCell(e.newLabel(0, row, fullname.trim().toUpperCase()));
				sheetrt.addCell(e.newLabel(1, row, d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserDocument()));
				sheetrt.addCell(e.newLabel(2, row, d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getAdTtypeDocument().getTypedocumentShortname()));
				sheetrt.addCell(e.newLabel(3, row, d.getAdTentryservices().getAdTentry().getAdTentityplan().getAdTentity().getEntityName().toUpperCase()));
				sheetrt.addCell(e.newLabel(4, row, d.getAdTentryservices().getAdTentry().getEntryCie10().toUpperCase()));
				sheetrt.addCell(e.newLabel(5, row, d.getAdTentryservices().getAdTservices().getServiceName().toUpperCase()));
				sheetrt.addCell(e.newLabel(6, row, d.getAdTentryservices().getAdTfrencuency().getFrecuencyName().toUpperCase()));
				sheetrt.addCell(e.newLabel(7, row, formatter.format(d.getAdTentryservices().getEntryserviceStartdate())));
				sheetrt.addCell(e.newLabel(8, row, formatter.format(d.getDetailDate())));
				sheetrt.addCell(e.newLabel(9, row, d.getAdTstatus().getStatusName().toUpperCase()));
				sheetrt.addCell(e.newLabel(10, row, d.getDetailPay()));
				sheetrt.addCell(e.newLabel(11, row, d.getDetailPin() == null ? "" : d.getDetailPin()));
				sheetrt.addCell(e.newLabel(12, row, "" + d.getDetailMoney()));
				sheetrt.addCell(e.newLabel(13, row, fullname2.trim().toUpperCase()));
				sheetrt.addCell(e.newLabel(14, row, d.getAdTusers().getUserDocument()));
				sheetrt.addCell(e.newLabel(15, row, formatter.format(d.getAdTentryservices().getEntryserviceRequestdate())));
				row++;
			}
			e.write();
			res.add(e.getFileName());
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			List<String> l = new ArrayList<String>();
			l.add(e.getMessage());
			return l;
		}
	}
	
	@Override
	public List<String> generate_report_pagos(String xml_data){
		try {
			List<String> res = new ArrayList<String>();
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
			ExcelExport e = ExcelUtil.createBook_nocolumns();
			res.add(e.getFile().getAbsolutePath());
			WritableSheet sheetrt = e.createSheet("REPORTE DE PAGOS", 0);
			List<String> lcolumns = new ArrayList<String>();
			lcolumns.add("Nombre Profesional");
			lcolumns.add("N\u00famero Documento");
			lcolumns.add("Tipo Documento");
			lcolumns.add("Entidad");
			lcolumns.add("Servicio");
			lcolumns.add("Fecha Atenci\u00f3n");
			lcolumns.add("Tipo Copago");
			lcolumns.add("Valor (PIN)");
			lcolumns.add("Valor (Efectivo)");
			lcolumns.add("Valor a pagar al prof.");
			lcolumns.add("Nombre Paciente");
			lcolumns.add("Fecha Solicitud");
			lcolumns.add("Horas invertidas del servicio");
			sheetrt.setRowView(0, 26*20);
			int c = 0;
			for(String sc : lcolumns){
				try {
					sheetrt.addCell(e.newLabelTitle(c++, 0, sc));
					sheetrt.setColumnView(c, sc.length() + 10);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			sheetrt.setColumnView(0, 50);
			String hql = "from AdTdetails as d where";
			String eid = XmlUtilities.getValueTag(xml_data, "eid");
			String type = XmlUtilities.getValueTag(xml_data, "type");
			Date start = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "start")));
			Date end = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "end")));
			
			/****** Se agrega 1 día para que funcione con las fechas correctas en el servidor ****/
			/** Para pruebas locales comentar estas 2 funciones **/
			//start = Util.AddDaysToDate(start, 1);
			//end = Util.AddDaysToDate(end, 1);
			//Util.SaveDateInFile("generate_report_pagos -> Start: " + formatter2.format(start));
			//Util.SaveDateInFile("generate_report_pagos -> End: " + formatter2.format(end));
			/************************************************************************************/
			
			if(eid.equals("*")){
				hql += " d.detailDate >= '" + formatter2.format(start) + "'";
				hql += " and d.detailDate <= '" + formatter2.format(end) + "'";
			}else{
				String pid = XmlUtilities.getValueTag(xml_data, "pid");
				if(pid.equals("*"))
					hql += " d.adTentryservices.adTentry.adTentityplan.adTentity.pkentity = " + eid;
				else
					hql += " d.adTentryservices.adTentry.adTentityplan.pkentityplan = " + pid;
				hql += " and d.detailDate >= '" + formatter2.format(start) + "'";
				hql += " and d.detailDate <= '" + formatter2.format(end) + "'";
			}
			hql += " and d.adTstatus.pkstatus = 3";
			if(!type.equals("*"))
				hql += " and d.adTentryservices.adTservices.serviceType = " + type;
			//hql += " and d.adTentryservices.adTservices.serviceType = 2";
			List<AdTdetails> ldetails = Util.dao.findByQuery(hql);
			int row = 1;
			for(AdTdetails d: ldetails){
				String fullname = d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserName1();
				fullname += " " + d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserName2();
				fullname += " " + d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserSurname1();
				fullname += " " + d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserSurname2();
				String fullname2 = "";
				if(d.getAdTusers() != null){
					fullname2 += d.getAdTusers().getUserName1();
					fullname2 += " " + d.getAdTusers().getUserName2();
					fullname2 += " " + d.getAdTusers().getUserSurname1();
					fullname2 += " " + d.getAdTusers().getUserSurname2();
				}
				sheetrt.addCell(e.newLabel(0, row, fullname2.trim().toUpperCase()));
				sheetrt.addCell(e.newLabel(1, row, d.getAdTusers() == null ? "" : d.getAdTusers().getUserDocument()));
				sheetrt.addCell(e.newLabel(2, row, d.getAdTusers() == null ? "" : d.getAdTusers().getAdTtypeDocument().getTypedocumentShortname()));
				sheetrt.addCell(e.newLabel(3, row, d.getAdTentryservices().getAdTentry().getAdTentityplan().getAdTentity().getEntityName().toUpperCase()));
				sheetrt.addCell(e.newLabel(4, row, d.getAdTentryservices().getAdTservices().getServiceName().toUpperCase()));
				sheetrt.addCell(e.newLabel(5, row, formatter.format(d.getDetailDate())));
				sheetrt.addCell(e.newLabel(6, row, d.getDetailPay()));
				sheetrt.addCell(e.newLabel(7, row, d.getDetailPin() == null ? "" : d.getDetailPin()));
				sheetrt.addCell(e.newLabel(8, row, d.getDetailMoney() == null ? "" : "" + d.getDetailMoney()));
				sheetrt.addCell(e.newLabel(9, row, "" + d.getAdTentryservices().getAdTservices().getServiceValue().longValue()));
				sheetrt.addCell(e.newLabel(10, row, fullname.trim().toUpperCase()));
				sheetrt.addCell(e.newLabel(11, row, formatter.format(d.getAdTentryservices().getEntryserviceRequestdate())));
				sheetrt.addCell(e.newLabel(12, row, d.getAdTentryservices().getAdTservices().getServiceType() == 2 ? "1" : "" + d.getAdTentryservices().getAdTservices().getServiceHours()));
				row++;
			}
			e.write();
			res.add(e.getFileName());
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<String> generate_report_factura(String xml_data){
		try {
			List<String> res = new ArrayList<String>();
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
			ExcelExport e = ExcelUtil.createBook_nocolumns();
			res.add(e.getFile().getAbsolutePath());
			WritableSheet sheetrt = e.createSheet("FACTURACI\u00d3N", 0);
			List<String> lcolumns = new ArrayList<String>();
			lcolumns.add("Nombre Paciente");
			lcolumns.add("N\u00famero Documento");
			lcolumns.add("Tipo Documento");
			lcolumns.add("Entidad");
			lcolumns.add("Plan");
			lcolumns.add("CIE10");
			lcolumns.add("Servicio/Insumo");
			lcolumns.add("Cant. Insumo");
			lcolumns.add("Nombre profesional");
			lcolumns.add("Fecha Inicio Programada");
			lcolumns.add("Fecha Atenci\u00f3n");
			lcolumns.add("Fecha Reporte Terapeuta");
			lcolumns.add("Nro. Contrato");
			lcolumns.add("Nro. Autorizaci\u00f3n");
			lcolumns.add("Nro. Factura");
			lcolumns.add("Estado");
			lcolumns.add("Programada Por");
			lcolumns.add("Cancelada por");
			lcolumns.add("Fecha Cancelaci\u00f3n");
			lcolumns.add("Fecha Solicitud");
			lcolumns.add("Tarifa del servicio");
			lcolumns.add("Copago");
			lcolumns.add("Frencuencia Copago");
			lcolumns.add("Horas invertidas del servicio");
			sheetrt.setRowView(0, 26*20);
			int c = 0;
			for(String sc : lcolumns){
				try {
					sheetrt.addCell(e.newLabelTitle(c++, 0, sc));
					sheetrt.setColumnView(c, sc.length() + 10);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			sheetrt.setColumnView(0, 50);
			String hql = "from AdTdetails as d ";
			String eid = XmlUtilities.getValueTag(xml_data, "eid");
			Date start = null;
			Date start1 = null;
			Date end = null;
			Date end1 = null;
			String type = XmlUtilities.getValueTag(xml_data, "type");
			if(XmlUtilities.getValueTag(xml_data, "start").length() > 0){
				start = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "start")));
				end = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "end")));
				/****** Se agrega 1 día para que funcione con las fechas correctas en el servidor ****/
				/** Para pruebas locales comentar estas 2 funciones **/
				//start = Util.AddDaysToDate(start, 1);
				//end = Util.AddDaysToDate(end, 1);
				//Util.SaveDateInFile("generate_report_factura -> Start: " + formatter2.format(start));
				//Util.SaveDateInFile("generate_report_factura-> End: " + formatter2.format(end));
				/************************************************************************************/
			}
			if(XmlUtilities.getValueTag(xml_data, "start1").length() > 0){
				start1 = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "start1")));
				end1 = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "end1")));
				/****** Se agrega 1 día para que funcione con las fechas correctas en el servidor ****/
				/** Para pruebas locales comentar estas 2 funciones **/
				//start1 = Util.AddDaysToDate(start1, 1);
				//end1 = Util.AddDaysToDate(end1, 1);
				/*Util.SaveDateInFile("generate_report_factura -> Start1: " + formatter2.format(start1));
				Util.SaveDateInFile("generate_report_factura -> End1: " + formatter2.format(end1));*/
				/************************************************************************************/
			}
			if(eid.equals("*")){
				boolean where= false;
				if(start != null){
					where = true;
					hql += " where (d.adTentryservices.entryserviceStartdate >= '" + formatter2.format(start) + "'";
					hql += " and d.adTentryservices.entryserviceStartdate <= '" + formatter2.format(end) + "')";
				}
				if(start1 != null){
					if(where)
						hql += " and (d.adTentryservices.entryserviceRequestdate >= '" + formatter2.format(start1) + "'";
					else
						hql += " where (d.adTentryservices.entryserviceRequestdate >= '" + formatter2.format(start1) + "'";
					hql += " and d.adTentryservices.entryserviceRequestdate <= '" + formatter2.format(end1) + "')";
				}
			}else{
				String pid = XmlUtilities.getValueTag(xml_data, "pid");
				if(pid.equals("*")){
					hql += " where d.adTentryservices.adTentry.adTentityplan.adTentity.pkentity = " + eid;
				}else
					hql += " where d.adTentryservices.adTentry.adTentityplan.pkentityplan = " + pid;
				if(start != null){
					hql += " and (d.adTentryservices.entryserviceStartdate >= '" + formatter2.format(start) + "'";
					hql += " and d.adTentryservices.entryserviceStartdate <= '" + formatter2.format(end) + "')";
				}
				if(start1 != null){
					hql += " and (d.adTentryservices.entryserviceRequestdate >= '" + formatter2.format(start1) + "'";
					hql += " and d.adTentryservices.entryserviceRequestdate <= '" + formatter2.format(end1) + "')";
				}
			}
			if(!type.equals("*"))
				hql += " and d.adTentryservices.adTservices.serviceType = " + type;
			hql += " order by d.adTentryservices.pkentryservices DESC";
			List<AdTdetails> ldetails = Util.dao.findByQuery(hql);
			int row = 1;
			boolean first = true;
			int active = 0;
			for(AdTdetails d: ldetails){
				if(first){
					active = d.getAdTentryservices().getPkentryservices().intValue();
					first = false;
				}
				String fullname = d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserName1();
				fullname += " " + d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserName2();
				fullname += " " + d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserSurname1();
				fullname += " " + d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserSurname2();
				String fullname2 = "";
				if(d.getAdTusers() != null){
					fullname2 = d.getAdTusers().getUserName1();
					fullname2 += " " + d.getAdTusers().getUserName2();
					fullname2 += " " + d.getAdTusers().getUserSurname1();
					fullname2 += " " + d.getAdTusers().getUserSurname2();
				}else
					fullname2 = "POR ASIGNAR";
				String fullname3 = "";
				if(d.getAdTusersByFkusercancelled() != null){
					fullname3 = d.getAdTusersByFkusercancelled().getUserName1();
					fullname3 += " " + d.getAdTusersByFkusercancelled().getUserName2();
					fullname3 += " " + d.getAdTusersByFkusercancelled().getUserSurname1();
					fullname3 += " " + d.getAdTusersByFkusercancelled().getUserSurname2();
				}
				String fullname4 = "";
				if(d.getAdTentryservices().getAdTusers() != null){
					fullname4 = d.getAdTentryservices().getAdTusers().getUserName1();
					fullname4 += " " + d.getAdTentryservices().getAdTusers().getUserName2();
					fullname4 += " " + d.getAdTentryservices().getAdTusers().getUserSurname1();
					fullname4 += " " + d.getAdTentryservices().getAdTusers().getUserSurname2();
				}
				sheetrt.addCell(e.newLabel(0, row, fullname.trim().toUpperCase()));
				sheetrt.addCell(e.newLabel(1, row, d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserDocument()));
				sheetrt.addCell(e.newLabel(2, row, d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getAdTtypeDocument().getTypedocumentShortname()));
				sheetrt.addCell(e.newLabel(3, row, d.getAdTentryservices().getAdTentry().getAdTentityplan().getAdTentity().getEntityName().toUpperCase()));
				sheetrt.addCell(e.newLabel(4, row, d.getAdTentryservices().getAdTentry().getAdTentityplan().getEntityplanName().toUpperCase()));
				sheetrt.addCell(e.newLabel(5, row, d.getAdTentryservices().getAdTentry().getEntryCie10()));
				sheetrt.addCell(e.newLabel(6, row, d.getAdTentryservices().getAdTservices().getServiceName().toUpperCase()));
				sheetrt.addCell(e.newLabel(7, row, ""));
				sheetrt.addCell(e.newLabel(8, row, fullname2.trim().toUpperCase()));
				sheetrt.addCell(e.newLabel(9, row, formatter.format(d.getAdTentryservices().getEntryserviceStartdate())));
				sheetrt.addCell(e.newLabel(10, row, d.getDetailDate() == null || d.getAdTstatus().getPkstatus() != 3 ? "" : formatter.format(d.getDetailDate())));
				sheetrt.addCell(e.newLabel(11, row, d.getDetailDateregister() == null ? "" : formatter.format(d.getDetailDateregister())));
				sheetrt.addCell(e.newLabel(12, row, d.getAdTentryservices().getAdTentry().getEntryNumber()));
				sheetrt.addCell(e.newLabel(13, row, d.getAdTentryservices().getEntryserviceNumerauth()));
				sheetrt.addCell(e.newLabel(14, row, d.getAdTentryservices().getEntryserviceInvoicenumber()));
				sheetrt.addCell(e.newLabel(15, row, "" + d.getAdTstatus().getStatusName()));
				sheetrt.addCell(e.newLabel(16, row, fullname4.trim().toUpperCase()));
				sheetrt.addCell(e.newLabel(17, row, "" + fullname3.trim().toUpperCase()));
				sheetrt.addCell(e.newLabel(18, row, d.getDetailDatecancel() == null ? "" : formatter.format(d.getDetailDatecancel())));
				sheetrt.addCell(e.newLabel(19, row, formatter.format(d.getAdTentryservices().getEntryserviceRequestdate())));
				hql = "from AdTrate as r where r.adTservices.pkservice = " + d.getAdTentryservices().getAdTservices().getPkservice();
				hql += " and r.adTentityplan.pkentityplan = " + d.getAdTentryservices().getAdTentry().getAdTentityplan().getPkentityplan();
				try{
					sheetrt.addCell(e.newLabel(20, row, "" + d.getAdTentryservices().getEntryserviceInvoicetarifa()));
				}catch(Exception ex){
					sheetrt.addCell(e.newLabel(20, row, ""));
				}
				sheetrt.addCell(e.newLabel(21, row, "" + d.getAdTentryservices().getEntryserviceCopago().intValue()));
				String cfrec = "";
				if(d.getAdTentryservices().getEntryserviceFecuencycopago() == 1)
					cfrec = "CADA SESI\u00d3N";
				if(d.getAdTentryservices().getEntryserviceFecuencycopago() == 5)
					cfrec = "CADA 5 SESIONES";
				if(d.getAdTentryservices().getEntryserviceFecuencycopago() == 0)
					cfrec = "OTRO";
				sheetrt.addCell(e.newLabel(22, row, cfrec));
				sheetrt.addCell(e.newLabel(23, row, d.getAdTentryservices().getAdTservices().getServiceType() == 2 ? "1" : "" + d.getAdTentryservices().getAdTservices().getServiceHours()));
				row++;
				if(active != d.getAdTentryservices().getPkentryservices().intValue()){
					for(AdTentryserviceinsumo i : d.getAdTentryservices().getAdTentryserviceinsumos()){
						sheetrt.addCell(e.newLabel(0, row, fullname.trim().toUpperCase()));
						sheetrt.addCell(e.newLabel(1, row, d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getUserDocument()));
						sheetrt.addCell(e.newLabel(2, row, d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient().getAdTtypeDocument().getTypedocumentShortname()));
						sheetrt.addCell(e.newLabel(3, row, d.getAdTentryservices().getAdTentry().getAdTentityplan().getAdTentity().getEntityName().toUpperCase()));
						sheetrt.addCell(e.newLabel(4, row, d.getAdTentryservices().getAdTentry().getAdTentityplan().getEntityplanName()));
						sheetrt.addCell(e.newLabel(5, row, ""));
						sheetrt.addCell(e.newLabel(6, row, i.getAdTinsumo().getInsumoName().toUpperCase()));
						sheetrt.addCell(e.newLabel(7, row, "" + i.getEntryserviceinsumoQty()));
						sheetrt.addCell(e.newLabel(8, row, ""));
						sheetrt.addCell(e.newLabel(9, row, ""/*formatter.format(d.getAdTentryservices().getEntryserviceStartdate())*/));
						sheetrt.addCell(e.newLabel(10, row, ""));
						sheetrt.addCell(e.newLabel(11, row, ""));
						sheetrt.addCell(e.newLabel(12, row, ""));
						sheetrt.addCell(e.newLabel(13, row, d.getAdTentryservices().getEntryserviceNumerauth()));
						sheetrt.addCell(e.newLabel(14, row, d.getAdTentryservices().getEntryserviceInvoicenumber()));
						sheetrt.addCell(e.newLabel(15, row, ""));
						sheetrt.addCell(e.newLabel(16, row, ""));
						sheetrt.addCell(e.newLabel(17, row, ""));
						sheetrt.addCell(e.newLabel(18, row, formatter.format(d.getAdTentryservices().getEntryserviceRequestdate())));
						row++;
					}
					active = d.getAdTentryservices().getPkentryservices().intValue();
				}
			}
			e.write();
			res.add(e.getFileName());
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<String> generate_report_professionals(){
		try {
			List<String> res = new ArrayList<String>();
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			ExcelExport e = ExcelUtil.createBook_nocolumns();
			res.add(e.getFile().getAbsolutePath());
			WritableSheet sheetrt = e.createSheet("PROFESIONALES", 0);
			List<String> lcolumns = new ArrayList<String>();
			lcolumns.add("Tipo Documento");
			lcolumns.add("N\u00famero Documento");
			lcolumns.add("Nombre Paciente");
			lcolumns.add("G\u00e9nero");
			lcolumns.add("Especialidad");
			lcolumns.add("Fecha Nacimiento");
			lcolumns.add("Barrio");
			lcolumns.add("Direcci\u00f3n");
			lcolumns.add("Zona");
			lcolumns.add("Email");
			lcolumns.add("Tel\u00e9fono 1");
			lcolumns.add("Tel\u00e9fono 2");
			lcolumns.add("Nro. Cuenta");
			lcolumns.add("Cod. Banco");
			lcolumns.add("Estatus");
			sheetrt.setRowView(0, 26*20);
			int c = 0;
			for(String sc : lcolumns){
				try {
					sheetrt.addCell(e.newLabelTitle(c++, 0, sc));
					sheetrt.setColumnView(c, sc.length() + 10);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			List<AdTusers> lprofs = Util.dao.findByQuery("from AdTusers u where u.userType = 2");
			int row = 1;
			for(AdTusers u: lprofs){
				String fullname = u.getUserName1().toUpperCase();
				fullname += " " + u.getUserName2().toUpperCase();
				fullname += " " + u.getUserSurname1().toUpperCase();
				fullname += " " + u.getUserSurname2().toUpperCase();
				sheetrt.addCell(e.newLabel(0, row, u.getAdTtypeDocument().getTypedocumentShortname()));
				sheetrt.addCell(e.newLabel(1, row, u.getUserDocument()));
				sheetrt.addCell(e.newLabel(2, row, fullname.trim()));
				sheetrt.addCell(e.newLabel(3, row, u.getUserGender()));
				sheetrt.addCell(e.newLabel(4, row, u.getAdTspecialty().getSpecialtyName().toUpperCase()));
				sheetrt.addCell(e.newLabel(5, row, u.getUserBirthdate() == null ? "" : formatter.format(u.getUserBirthdate())));
				sheetrt.addCell(e.newLabel(6, row, u.getUserDistrict().toUpperCase()));
				sheetrt.addCell(e.newLabel(7, row, u.getUserZone().toUpperCase()));
				sheetrt.addCell(e.newLabel(8, row, u.getUserAddress().toUpperCase()));
				sheetrt.addCell(e.newLabel(9, row, u.getUserEmail().toUpperCase()));
				sheetrt.addCell(e.newLabel(10, row, u.getUserPhone1()));
				sheetrt.addCell(e.newLabel(11, row, u.getUserPhone2()));
				sheetrt.addCell(e.newLabel(12, row, u.getUserNumberaccount()));
				sheetrt.addCell(e.newLabel(13, row, u.getUserCodebank()));
				try{
					Util.dao.findByQuery("from AdTlogin as l where l.adTusers.pkuser = " + u.getPkuser()).get(0);
					if(u.getUserEnabled())
						sheetrt.addCell(e.newLabel(14, row, "ACTIVO"));
					else
						sheetrt.addCell(e.newLabel(14, row, "DESACTIVADO"));
				}catch(Exception ex){
					sheetrt.addCell(e.newLabel(14, row, "DESACTIVADO"));
				}
				row++;
			}
			e.write();
			res.add(e.getFileName());
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<String> generate_report_patients(String xml_data){
		try {
			List<String> res = new ArrayList<String>();
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd");
			ExcelExport e = ExcelUtil.createBook_nocolumns();
			res.add(e.getFile().getAbsolutePath());
			WritableSheet sheetrt = e.createSheet("PACIENTES", 0);
			List<String> lcolumns = new ArrayList<String>();
			lcolumns.add("Tipo Documento");
			lcolumns.add("N\u00famero Documento");
			lcolumns.add("Nombre Paciente");
			lcolumns.add("G\u00e9nero");
			lcolumns.add("Fecha Nacimiento");
			lcolumns.add("Barrio");
			lcolumns.add("Direcci\u00f3n");
			lcolumns.add("Zona");
			lcolumns.add("Email");
			lcolumns.add("Tel\u00e9fono 1");
			lcolumns.add("Tel\u00e9fono 2");
			sheetrt.setRowView(0, 26*20);
			int c = 0;
			for(String sc : lcolumns){
				try {
					sheetrt.addCell(e.newLabelTitle(c++, 0, sc));
					sheetrt.setColumnView(c, sc.length() + 10);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
			String hql = "from AdTdetails as d ";
			Date start = null;
			Date end = null;
			if(XmlUtilities.getValueTag(xml_data, "start").length() > 0){
				start = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "start")));
				end = new Date(Long.parseLong(XmlUtilities.getValueTag(xml_data, "end")));
				/****** Se agrega 1 día para que funcione con las fechas correctas en el servidor ****/
				/** Para pruebas locales comentar estas 2 funciones **/
				start = Util.AddDaysToDate(start, 1);
				end = Util.AddDaysToDate(end, 1);
				/************************************************************************************/
			}
			if(start != null){
				hql += " where d.detailDate >= '" + formatter2.format(start) + "'";
				hql += " and d.detailDate <= '" + formatter2.format(end) + "'";
			}
			hql += " group by d.adTentryservices.adTentry.adTusersByFkpatient.pkuser";
			List<AdTdetails> ldetails = Util.dao.findByQuery(hql);
			int row = 1;
			for(AdTdetails d: ldetails){
				AdTusers u = d.getAdTentryservices().getAdTentry().getAdTusersByFkpatient();
				String fullname = u.getUserName1().toUpperCase();
				fullname += " " + u.getUserName2().toUpperCase();
				fullname += " " + u.getUserSurname1().toUpperCase();
				fullname += " " + u.getUserSurname2().toUpperCase();
				sheetrt.addCell(e.newLabel(0, row, u.getAdTtypeDocument().getTypedocumentShortname()));
				sheetrt.addCell(e.newLabel(1, row, u.getUserDocument()));
				sheetrt.addCell(e.newLabel(2, row, fullname.trim()));
				sheetrt.addCell(e.newLabel(3, row, u.getUserGender()));
				sheetrt.addCell(e.newLabel(4, row, u.getUserBirthdate() == null ? "" : formatter.format(u.getUserBirthdate())));
				sheetrt.addCell(e.newLabel(5, row, u.getUserDistrict().toUpperCase()));
				sheetrt.addCell(e.newLabel(6, row, u.getUserAddress().toUpperCase()));
				sheetrt.addCell(e.newLabel(7, row, u.getUserZone().toUpperCase()));
				sheetrt.addCell(e.newLabel(8, row, u.getUserEmail().toUpperCase()));
				sheetrt.addCell(e.newLabel(9, row, u.getUserPhone1()));
				sheetrt.addCell(e.newLabel(10, row, u.getUserPhone2()));
				row++;
			}
			e.write();
			res.add(e.getFileName());
			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
