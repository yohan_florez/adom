package com.adom.ecreativos.services;

import java.util.List;

import com.adom.ecreativos.vo.AdTinsumo;
import com.adom.ecreativos.vo.AdTservices;

public interface Service {
	
	public AdTservices saveService(String xml_data);
	public AdTinsumo saveInsumo(String xml_data);
	public List<AdTservices> getAllServices();
	public List<AdTservices> getServicesByEntry(int eid);
	public List<AdTinsumo> getAllInsumos();
	public AdTservices getService(String xml_data);
	public AdTservices updateService(String xml_data);
	public AdTinsumo updateInsumo(String xml_data);
	public AdTinsumo getInsumo(String xml_data);
}
