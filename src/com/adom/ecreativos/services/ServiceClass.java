package com.adom.ecreativos.services;

import java.util.ArrayList;
import java.util.List;

import com.adom.ecreativos.util.Util;
import com.adom.ecreativos.util.XmlUtilities;
import com.adom.ecreativos.vo.AdTentry;
import com.adom.ecreativos.vo.AdTinsumo;
import com.adom.ecreativos.vo.AdTrate;
import com.adom.ecreativos.vo.AdTservices;

public class ServiceClass implements Service{

	@Override
	public AdTservices saveService(String xml_data) {
		try {
			AdTservices service = new AdTservices();
			service.setServiceName(XmlUtilities.getValueTag(xml_data, "name"));
			service.setServiceValue(Double.parseDouble(XmlUtilities.getValueTag(xml_data, "value")));
			service.setServiceCode(XmlUtilities.getValueTag(xml_data, "code"));
			service.setServiceHours(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "hours")));
			service.setServiceType(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "type")));
			service.setServiceType2(XmlUtilities.getValueTag(xml_data, "type2"));
			Util.dao.saveOrUpdate(service);
			return Util.dao.findById(AdTservices.class, service.getPkservice());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public AdTservices updateService(String xml_data) {
		try {
			AdTservices service = Util.dao.findById(AdTservices.class, Long.parseLong(XmlUtilities.getValueTag(xml_data, "sid")));
			service.setServiceName(XmlUtilities.getValueTag(xml_data, "name"));
			service.setServiceValue(Double.parseDouble(XmlUtilities.getValueTag(xml_data, "value")));
			service.setServiceCode(XmlUtilities.getValueTag(xml_data, "code"));
			service.setServiceHours(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "hours")));
			service.setServiceType(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "type")));
			service.setServiceType2(XmlUtilities.getValueTag(xml_data, "type2"));
			Util.dao.saveOrUpdate(service);
			return Util.dao.findById(AdTservices.class, service.getPkservice());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public AdTinsumo saveInsumo(String xml_data) {
		try {
			AdTinsumo insumo = new AdTinsumo();
			insumo.setInsumoName(XmlUtilities.getValueTag(xml_data, "name"));
			insumo.setInsumoCode(XmlUtilities.getValueTag(xml_data, "code"));
			Util.dao.saveOrUpdate(insumo);
			return Util.dao.findById(AdTinsumo.class, insumo.getPkinsumo());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public AdTinsumo updateInsumo(String xml_data) {
		try {
			AdTinsumo insumo = Util.dao.findById(AdTinsumo.class, Long.parseLong(XmlUtilities.getValueTag(xml_data, "id")));
			insumo.setInsumoName(XmlUtilities.getValueTag(xml_data, "name"));
			insumo.setInsumoCode(XmlUtilities.getValueTag(xml_data, "code"));
			Util.dao.saveOrUpdate(insumo);
			return Util.dao.findById(AdTinsumo.class, insumo.getPkinsumo());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public List<AdTservices> getServicesByEntry(int eid) {
		AdTentry entry =  Util.dao.findById(AdTentry.class, eid);
		List<AdTrate> rate = Util.dao.findByQuery("from AdTrate r where r.adTentityplan = " + entry.getAdTentityplan().getPkentityplan());
		List<AdTservices> listServices = new ArrayList<AdTservices>();
		for(AdTrate r : rate)
			listServices.add(r.getAdTservices());
		return listServices;
	}
	
	@Override
	public AdTservices getService(String xml_data) {
		return Util.dao.findById(AdTservices.class, Long.parseLong(XmlUtilities.getValueTag(xml_data, "sid")));
	}
	
	@Override
	public AdTinsumo getInsumo(String xml_data) {
		return Util.dao.findById(AdTinsumo.class, Long.parseLong(XmlUtilities.getValueTag(xml_data, "id")));
	}
	
	@Override
	public List<AdTservices> getAllServices() {
		return Util.dao.findAll(AdTservices.class);
	}
	
	@Override
	public List<AdTinsumo> getAllInsumos() {
		return Util.dao.findAll(AdTinsumo.class);
	}
}
