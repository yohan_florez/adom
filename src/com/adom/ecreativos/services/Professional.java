package com.adom.ecreativos.services;

import java.util.List;

import com.adom.ecreativos.vo.AdTusers;

public interface Professional {
	
	public AdTusers saveProfessional(String xml_data);
	public List<AdTusers> getAllProfessionals();
	public List<AdTusers> search(String xml_data);
	public AdTusers updateProfessional(String xml_data);

}
