package com.adom.ecreativos.services;

import java.util.List;

import com.adom.ecreativos.util.Util;
import com.adom.ecreativos.util.XmlUtilities;
import com.adom.ecreativos.vo.AdTentity;
import com.adom.ecreativos.vo.AdTentityplan;
import com.adom.ecreativos.vo.AdTrate;
import com.adom.ecreativos.vo.AdTservices;

public class EntityClass implements Entity{

	@Override
	public AdTentity saveEntity(String xml_data) {
		try {
			AdTentity entity = new AdTentity();
			entity.setEntityNit(XmlUtilities.getValueTag(xml_data, "nit"));
			entity.setEntityRz(XmlUtilities.getValueTag(xml_data, "rs"));
			entity.setEntityCode(XmlUtilities.getValueTag(xml_data, "code"));
			entity.setEntityName(XmlUtilities.getValueTag(xml_data, "name"));
			Util.dao.saveOrUpdate(entity);
			return Util.dao.findById(AdTentity.class, entity.getPkentity());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public String savePlan(String xml_data) {
		try {
			AdTentityplan plan = new AdTentityplan();
			AdTentity entity = new AdTentity();
			entity.setPkentity(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "entity")));
			plan.setEntityplanName(XmlUtilities.getValueTag(xml_data, "name"));
			plan.setAdTentity(entity);
			plan.setEntityplanStatus(true);
			Util.dao.saveOrUpdate(plan);
			return "OK";
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
	
	@Override
	public AdTrate saveRate(String xml_data) {
		try {
			AdTentityplan plan = new AdTentityplan();
			AdTservices service = new AdTservices();
			AdTrate rate = new AdTrate();
			service.setPkservice(Long.parseLong(XmlUtilities.getValueTag(xml_data, "service")));
			plan.setPkentityplan(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "plan")));
			rate.setRateCost(Double.parseDouble(XmlUtilities.getValueTag(xml_data, "cost")));
			rate.setAdTentityplan(plan);
			rate.setAdTservices(service);
			Util.dao.saveOrUpdate(rate);
			return Util.dao.findById(AdTrate.class, rate.getPkrate());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public boolean removeRate(String xml_data) {
		try {
			AdTrate rate = Util.dao.findById(AdTrate.class, Integer.parseInt(XmlUtilities.getValueTag(xml_data, "rid")));
			Util.dao.delete(rate);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public List<AdTentity> getAllEntities() {
		return Util.dao.findAll(AdTentity.class);
	}
	
	@Override
	public AdTentity getEntity(String xml_data) {
		return Util.dao.findById(AdTentity.class, Integer.parseInt(XmlUtilities.getValueTag(xml_data, "eid")));
	}

	@Override
	public List<AdTrate> getRate(Long pid) {
		return Util.dao.findByQuery("from AdTrate r where r.adTentityplan.pkentityplan = " + pid);
	}

	@Override
	public List<AdTentityplan> getPlans(Long eid) {
		return Util.dao.findByQuery("from AdTentityplan p where p.adTentity.pkentity = " + eid);
	}
	
	@Override
	public AdTentity updateEntity(String xml_data) {
		try {
			AdTentity entity = Util.dao.findById(AdTentity.class, Integer.parseInt(XmlUtilities.getValueTag(xml_data, "eid")));
			entity.setEntityNit(XmlUtilities.getValueTag(xml_data, "nit"));
			entity.setEntityRz(XmlUtilities.getValueTag(xml_data, "rs"));
			entity.setEntityCode(XmlUtilities.getValueTag(xml_data, "code"));
			entity.setEntityName(XmlUtilities.getValueTag(xml_data, "name"));
			Util.dao.saveOrUpdate(entity);
			return Util.dao.findById(AdTentity.class, entity.getPkentity());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
