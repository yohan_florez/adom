package com.adom.ecreativos.services;

import java.util.List;

import com.adom.ecreativos.vo.AdTdetails;

public interface Copago {
	public List<AdTdetails> search(String xml_data);
	public List<AdTdetails> searchByServicios(String xml_data);
}
