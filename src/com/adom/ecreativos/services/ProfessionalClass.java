package com.adom.ecreativos.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.adom.ecreativos.util.Mail;
import com.adom.ecreativos.util.Util;
import com.adom.ecreativos.util.XmlUtilities;
import com.adom.ecreativos.vo.AdTlogin;
import com.adom.ecreativos.vo.AdTrol;
import com.adom.ecreativos.vo.AdTspecialty;
import com.adom.ecreativos.vo.AdTtypeDocument;
import com.adom.ecreativos.vo.AdTusers;

public class ProfessionalClass implements Professional{

	@Override
	public AdTusers saveProfessional(String xml_data) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			AdTusers professional = new AdTusers();
			AdTtypeDocument tdoc = new AdTtypeDocument();
			AdTspecialty espec = new AdTspecialty();
			espec.setPkspecialty(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "espec")));
			tdoc.setPktypedocument(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "type_doc")));
			professional.setUserDocument(XmlUtilities.getValueTag(xml_data, "num_doc").trim());
			professional.setAdTtypeDocument(tdoc);
			professional.setUserName1(XmlUtilities.getValueTag(xml_data, "name1"));
			professional.setUserName2(XmlUtilities.getValueTag(xml_data, "name2"));
			professional.setUserSurname1(XmlUtilities.getValueTag(xml_data, "last1"));
			professional.setUserSurname2(XmlUtilities.getValueTag(xml_data, "last2"));
			professional.setUserGender(XmlUtilities.getValueTag(xml_data, "gender"));
			professional.setUserAddress(XmlUtilities.getValueTag(xml_data, "address"));
			professional.setUserType(2);
			professional.setAdTspecialty(espec);
			professional.setUserEnabled(true);
			professional.setUserBirthdate(formatter.parse(XmlUtilities.getValueTag(xml_data, "age")));
			professional.setUserDistrict(XmlUtilities.getValueTag(xml_data, "distric"));
			professional.setUserZone(XmlUtilities.getValueTag(xml_data, "zone"));
			professional.setUserPhone1(XmlUtilities.getValueTag(xml_data, "phone1"));
			professional.setUserPhone2(XmlUtilities.getValueTag(xml_data, "phone2"));
			professional.setUserEmail(XmlUtilities.getValueTag(xml_data, "email"));
			professional.setUserNumberaccount(XmlUtilities.getValueTag(xml_data, "account"));
			professional.setUserCodebank(XmlUtilities.getValueTag(xml_data, "codebank"));
			Util.dao.saveOrUpdate(professional);
			try {
				String pass = Util.getCode(7);
				AdTrol rol = new AdTrol();
				rol.setPkrol(3);
				AdTlogin login = new AdTlogin();
				login.setAdTrol(rol);
				login.setAdTusers(professional);
				login.setLoginPassword(Util.encriptar(pass));
				Util.dao.saveOrUpdate(login);
				String content = Util.loadHTMLFile("templates/new.jsp");
				content = content.replace("{@username}", professional.getUserEmail());
				content = content.replace("{@password}", pass);
				new Mail("Confirmaci\u00f3n de registro - ADOM", content, "adomsalud1978@gmail.com," + professional.getUserEmail()).start();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return Util.dao.findById(AdTusers.class, professional.getPkuser());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public AdTusers updateProfessional(String xml_data) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			AdTusers professional = Util.dao.findById(AdTusers.class, Long.parseLong(XmlUtilities.getValueTag(xml_data, "pid")));
			AdTspecialty espec = new AdTspecialty();
			String emailActive = professional.getUserEmail();
			espec.setPkspecialty(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "espec")));
			professional.setUserName1(XmlUtilities.getValueTag(xml_data, "name1"));
			professional.setUserName2(XmlUtilities.getValueTag(xml_data, "name2"));
			professional.setUserSurname1(XmlUtilities.getValueTag(xml_data, "last1"));
			professional.setUserSurname2(XmlUtilities.getValueTag(xml_data, "last2"));
			professional.setUserGender(XmlUtilities.getValueTag(xml_data, "gender"));
			professional.setUserAddress(XmlUtilities.getValueTag(xml_data, "address"));
			professional.setUserType(2);
			professional.setAdTspecialty(espec);
			professional.setUserEnabled(true);
			professional.setUserBirthdate(formatter.parse(XmlUtilities.getValueTag(xml_data, "age")));
			professional.setUserDistrict(XmlUtilities.getValueTag(xml_data, "distric"));
			professional.setUserZone(XmlUtilities.getValueTag(xml_data, "zone"));
			professional.setUserPhone1(XmlUtilities.getValueTag(xml_data, "phone1"));
			professional.setUserPhone2(XmlUtilities.getValueTag(xml_data, "phone2"));
			professional.setUserEmail(XmlUtilities.getValueTag(xml_data, "email"));
			professional.setUserNumberaccount(XmlUtilities.getValueTag(xml_data, "account"));
			professional.setUserCodebank(XmlUtilities.getValueTag(xml_data, "codebank"));
			Util.dao.saveOrUpdate(professional);
			try {
				if(!emailActive.equalsIgnoreCase(XmlUtilities.getValueTag(xml_data, "email"))){
					String pass = Util.getCode(7);
					AdTlogin login = (AdTlogin) Util.dao.findByQuery("from AdTlogin as l where l.adTusers.pkuser = " + professional.getPkuser()).get(0);
					login.setLoginPassword(Util.encriptar(pass));
					Util.dao.saveOrUpdate(login);
					String content = Util.loadHTMLFile("templates/new.jsp");
					content = content.replace("{@username}", professional.getUserEmail());
					content += content.replace("{@password}", pass);
					new Mail("Actualizaci\u00f3n de datos - ADOM", content, "adomsalud1978@gmail.com," + professional.getUserEmail()).start();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return Util.dao.findById(AdTusers.class, professional.getPkuser());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<AdTusers> search(String xml_data) {
		boolean isnumber;
		try{
			Integer.parseInt(XmlUtilities.getValueTag(xml_data, "search_txt"));
			isnumber = true;
		}catch(Exception e){
			isnumber = false;
		}
		String txt = XmlUtilities.getValueTag(xml_data, "search_txt");
		String hql = "";
		if(isnumber)
			hql = "from AdTusers as u where u.userType = 2 and u.userDocument LIKE '" + txt.toUpperCase() + "%'";
		else{
			List<AdTusers> complete = Util.dao.findByQuery("from AdTusers as u where u.userType = 2");
			List<AdTusers> r = new ArrayList<AdTusers>();
			for(AdTusers u: complete){
				String fullname = u.getUserName1().trim();
				fullname += u.getUserName2().trim() == null || u.getUserName2().trim().equals("") ? "" : " " + u.getUserName2().trim();
				fullname += " " + u.getUserSurname1().trim();
				fullname += u.getUserSurname2().trim() == null || u.getUserSurname2().trim().equals("") ? "" : " " + u.getUserSurname2().trim();
				if(fullname.toUpperCase().indexOf(txt.toUpperCase().trim()) != -1){
					r.add(u);
				}
			}
			return r;
		}
		return Util.dao.findByQuery(hql);
	}

	@Override
	public List<AdTusers> getAllProfessionals() {
		return Util.dao.findByQuery("from AdTusers u where u.userType = 2");
	}

}
