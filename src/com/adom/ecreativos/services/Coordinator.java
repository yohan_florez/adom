package com.adom.ecreativos.services;

import java.util.List;

import com.adom.ecreativos.vo.AdTusers;

public interface Coordinator {
	
	public AdTusers saveCoordinator(String xml_data);
	public List<AdTusers> getAllCoordinators();
	public List<AdTusers> search(String xml_data);
	public AdTusers getCoordinator(long pkcoordinator);
	public AdTusers updateCoordinator(String xml_data);

}
