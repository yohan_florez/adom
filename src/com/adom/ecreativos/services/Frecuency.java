package com.adom.ecreativos.services;

import java.util.List;

import com.adom.ecreativos.vo.AdTfrencuency;

public interface Frecuency {
	
	public List<AdTfrencuency> getAllFrencuency();
	public AdTfrencuency getFrecuency(int fid);

}
