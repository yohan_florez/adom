package com.adom.ecreativos.services;

import java.util.List;

import com.adom.ecreativos.util.Util;
import com.adom.ecreativos.vo.AdTfrencuency;

public class FrecuencyClass implements Frecuency{

	@Override
	public List<AdTfrencuency> getAllFrencuency() {
		return Util.dao.findAll(AdTfrencuency.class);
	}

	@Override
	public AdTfrencuency getFrecuency(int fid) {
		return Util.dao.findById(AdTfrencuency.class, fid);
	}

}
