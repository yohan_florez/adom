package com.adom.ecreativos.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.adom.ecreativos.util.Util;
import com.adom.ecreativos.util.XmlUtilities;
import com.adom.ecreativos.vo.AdTtypeDocument;
import com.adom.ecreativos.vo.AdTup;
import com.adom.ecreativos.vo.AdTusers;

public class PatientClass implements Patient{

	@Override
	public AdTusers savePatient(String xml_data) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			AdTusers patient = new AdTusers();
			AdTtypeDocument tdoc = new AdTtypeDocument();
			tdoc.setPktypedocument(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "type_doc")));
			patient.setUserDocument(XmlUtilities.getValueTag(xml_data, "num_doc").trim());
			patient.setAdTtypeDocument(tdoc);
			patient.setUserName1(XmlUtilities.getValueTag(xml_data, "name1"));
			patient.setUserName2(XmlUtilities.getValueTag(xml_data, "name2"));
			patient.setUserSurname1(XmlUtilities.getValueTag(xml_data, "last1"));
			patient.setUserSurname2(XmlUtilities.getValueTag(xml_data, "last2"));
			patient.setUserGender(XmlUtilities.getValueTag(xml_data, "gender"));
			patient.setUserAddress(XmlUtilities.getValueTag(xml_data, "address"));
			patient.setUserEnabled(true);
			try{
				patient.setUserBirthdate(formatter.parse(XmlUtilities.getValueTag(xml_data, "fn")));
			}catch(Exception e){}
			patient.setUserType(1);
			patient.setUserTyperips(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "type_user")));
			patient.setUserAge(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "age")));
			patient.setUserAgetype(XmlUtilities.getValueTag(xml_data, "type_age"));
			patient.setUserDistrict(XmlUtilities.getValueTag(xml_data, "distric"));
			patient.setUserZone(XmlUtilities.getValueTag(xml_data, "zone"));
			patient.setUserPhone1(XmlUtilities.getValueTag(xml_data, "phone1"));
			patient.setUserPhone2(XmlUtilities.getValueTag(xml_data, "phone2"));
			patient.setUserEmail(XmlUtilities.getValueTag(xml_data, "email"));
			Util.dao.saveOrUpdate(patient);
			return Util.dao.findById(AdTusers.class, patient.getPkuser());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public AdTusers updatePatient(String xml_data) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			AdTusers patient = Util.dao.findById(AdTusers.class, Long.parseLong(XmlUtilities.getValueTag(xml_data, "uid")));
			AdTtypeDocument tdoc = new AdTtypeDocument();
			tdoc.setPktypedocument(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "type_doc")));
			patient.setUserDocument(XmlUtilities.getValueTag(xml_data, "num_doc"));
			patient.setAdTtypeDocument(tdoc);
			patient.setUserName1(XmlUtilities.getValueTag(xml_data, "name1"));
			patient.setUserName2(XmlUtilities.getValueTag(xml_data, "name2"));
			patient.setUserSurname1(XmlUtilities.getValueTag(xml_data, "last1"));
			patient.setUserSurname2(XmlUtilities.getValueTag(xml_data, "last2"));
			patient.setUserGender(XmlUtilities.getValueTag(xml_data, "gender"));
			patient.setUserAddress(XmlUtilities.getValueTag(xml_data, "address"));
			try{
				patient.setUserBirthdate(formatter.parse(XmlUtilities.getValueTag(xml_data, "fn")));
			}catch(Exception e){}
			patient.setUserType(1);
			patient.setUserTyperips(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "type_user")));
			patient.setUserAge(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "age")));
			patient.setUserAgetype(XmlUtilities.getValueTag(xml_data, "type_age"));
			patient.setUserDistrict(XmlUtilities.getValueTag(xml_data, "distric"));
			patient.setUserZone(XmlUtilities.getValueTag(xml_data, "zone"));
			patient.setUserPhone1(XmlUtilities.getValueTag(xml_data, "phone1"));
			patient.setUserPhone2(XmlUtilities.getValueTag(xml_data, "phone2"));
			patient.setUserEmail(XmlUtilities.getValueTag(xml_data, "email"));
			Util.dao.saveOrUpdate(patient);
			return Util.dao.findById(AdTusers.class, patient.getPkuser());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public AdTusers getPatient(long pkpatient) {
		try {
			return Util.dao.findById(AdTusers.class, pkpatient);
		} catch (Exception e) {
			return null;
		}
	}

	@Override
	public List<AdTusers> getAllPatient() {
		return Util.dao.findByQuery("from AdTusers u where u.userType = 1");
	}
	
	@Override
	public List<AdTusers> getHistoryPatients() {
		AdTusers active = Util.getUserActive();
		List<AdTusers> lusers = new ArrayList<AdTusers>();
		String hql = "from AdTup as u where u.adTusersByFkprofessional.pkuser = " + active.getPkuser();
		hql += " group by u.adTusersByFkpatient.pkuser order by u.pkup DESC LIMIT 50";
		List<AdTup> lups = Util.dao.findByQuery(hql);
		for(AdTup u: lups)
			lusers.add(u.getAdTusersByFkpatient());
		return lusers;
	}

	@Override
	public List<AdTusers> search(String xml_data) {
		boolean isnumber;
		try{
			Integer.parseInt(XmlUtilities.getValueTag(xml_data, "search_txt"));
			isnumber = true;
		}catch(Exception e){
			isnumber = false;
		}
		String txt = XmlUtilities.getValueTag(xml_data, "search_txt");
		String hql = "";
		if(isnumber)
			hql = "from AdTusers as u where u.userType = 1 and u.userDocument LIKE '" + txt.trim() + "%'";
		else{
			List<AdTusers> complete = Util.dao.findByQuery("from AdTusers as u where u.userType = 1");
			List<AdTusers> r = new ArrayList<AdTusers>();
			for(AdTusers u: complete){
				String fullname = u.getUserName1().trim();
				fullname += u.getUserName2().trim() == null || u.getUserName2().trim().equals("") ? "" : " " + u.getUserName2().trim();
				fullname += " " + u.getUserSurname1().trim();
				fullname += u.getUserSurname2().trim() == null || u.getUserSurname2().trim().equals("") ? "" : " " + u.getUserSurname2().trim();
				if(fullname.toUpperCase().indexOf(txt.toUpperCase().trim()) != -1){
					r.add(u);
				}
			}
			return r;
		}
		return Util.dao.findByQuery(hql);
	}

}
