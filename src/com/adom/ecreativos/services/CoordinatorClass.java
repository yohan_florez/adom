package com.adom.ecreativos.services;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import com.adom.ecreativos.util.Mail;
import com.adom.ecreativos.util.Util;
import com.adom.ecreativos.util.XmlUtilities;
import com.adom.ecreativos.vo.AdTlogin;
import com.adom.ecreativos.vo.AdTpermits;
import com.adom.ecreativos.vo.AdTrol;
import com.adom.ecreativos.vo.AdTtypeDocument;
import com.adom.ecreativos.vo.AdTuserpermit;
import com.adom.ecreativos.vo.AdTusers;

public class CoordinatorClass implements Coordinator{

	@Override
	public AdTusers saveCoordinator(String xml_data) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			AdTusers coordinator = new AdTusers();
			AdTtypeDocument tdoc = new AdTtypeDocument();
			tdoc.setPktypedocument(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "type_doc")));
			coordinator.setUserDocument(XmlUtilities.getValueTag(xml_data, "num_doc").trim());
			coordinator.setAdTtypeDocument(tdoc);
			coordinator.setUserName1(XmlUtilities.getValueTag(xml_data, "name1"));
			coordinator.setUserName2(XmlUtilities.getValueTag(xml_data, "name2"));
			coordinator.setUserSurname1(XmlUtilities.getValueTag(xml_data, "last1"));
			coordinator.setUserSurname2(XmlUtilities.getValueTag(xml_data, "last2"));
			coordinator.setUserGender(XmlUtilities.getValueTag(xml_data, "gender"));
			coordinator.setUserType(3);
			coordinator.setUserEnabled(true);
			coordinator.setUserBirthdate(formatter.parse(XmlUtilities.getValueTag(xml_data, "age")));
			coordinator.setUserPhone1(XmlUtilities.getValueTag(xml_data, "phone1"));
			coordinator.setUserPhone2(XmlUtilities.getValueTag(xml_data, "phone2"));
			coordinator.setUserEmail(XmlUtilities.getValueTag(xml_data, "email"));
			coordinator.setUserNumberaccount(XmlUtilities.getValueTag(xml_data, "account"));
			coordinator.setUserCodebank(XmlUtilities.getValueTag(xml_data, "codebank"));
			Util.dao.saveOrUpdate(coordinator);
			List<AdTpermits> lpermits = Util.dao.findAll(AdTpermits.class);
			for(AdTpermits p: lpermits){
				AdTuserpermit up = new AdTuserpermit();
				up.setAdTpermits(p);
				up.setAdTusers(coordinator);
				if(p.getPkpermit() == 7 || p.getPkpermit() == 8 || p.getPkpermit() == 9 || p.getPkpermit() == 10 || p.getPkpermit() == 11 || p.getPkpermit() == 12)
					up.setUserpermitValue(0);
				else
					up.setUserpermitValue(1);
				Util.dao.saveOrUpdate(up);
			}
			try {
				String pass = Util.getCode(7);
				AdTrol rol = new AdTrol();
				rol.setPkrol(2);
				AdTlogin login = new AdTlogin();
				login.setAdTrol(rol);
				login.setAdTusers(coordinator);
				login.setLoginPassword(Util.encriptar(pass));
				Util.dao.saveOrUpdate(login);
				String content = Util.loadHTMLFile("templates/new.jsp");
				content = content.replace("{@username}", coordinator.getUserEmail());
				content = content.replace("{@password}", pass);
				new Mail("Confirmaci\u00f3n de registro - ADOM", content, coordinator.getUserEmail() + ",adomsalud1978@gmail.com").start();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return Util.dao.findById(AdTusers.class, coordinator.getPkuser());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public AdTusers updateCoordinator(String xml_data) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			String cid = XmlUtilities.getValueTag(xml_data, "cid");
			AdTusers coordinator = Util.dao.findById(AdTusers.class, Long.parseLong(cid));
			AdTtypeDocument tdoc = new AdTtypeDocument();
			String email = coordinator.getUserEmail();
			tdoc.setPktypedocument(Integer.parseInt(XmlUtilities.getValueTag(xml_data, "type_doc")));
			coordinator.setUserDocument(XmlUtilities.getValueTag(xml_data, "num_doc"));
			coordinator.setAdTtypeDocument(tdoc);
			coordinator.setUserName1(XmlUtilities.getValueTag(xml_data, "name1"));
			coordinator.setUserName2(XmlUtilities.getValueTag(xml_data, "name2"));
			coordinator.setUserSurname1(XmlUtilities.getValueTag(xml_data, "last1"));
			coordinator.setUserSurname2(XmlUtilities.getValueTag(xml_data, "last2"));
			coordinator.setUserBirthdate(formatter.parse(XmlUtilities.getValueTag(xml_data, "fn")));
			coordinator.setUserPhone1(XmlUtilities.getValueTag(xml_data, "phone1"));
			coordinator.setUserPhone2(XmlUtilities.getValueTag(xml_data, "phone2"));
			coordinator.setUserEmail(XmlUtilities.getValueTag(xml_data, "email"));
			Util.dao.saveOrUpdate(coordinator);
			try {
				if(!email.equals(coordinator.getUserEmail())){
					String pass = Util.getCode(7);
					AdTlogin login = (AdTlogin) Util.dao.findByQuery("from AdTlogin as l where l.adTusers.pkuser = " + coordinator.getPkuser()).get(0);
					login.setLoginPassword(Util.encriptar(pass));
					Util.dao.saveOrUpdate(login);
					String content = Util.loadHTMLFile("templates/new.jsp");
					content = content.replace("{@username}", coordinator.getUserEmail());
					content += content.replace("{@password}", pass);
					new Mail("Actualizaci\u00f3n de datos - ADOM", content, coordinator.getUserEmail() + ",adomsalud1978@gmail.com").start();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return Util.dao.findById(AdTusers.class, coordinator.getPkuser());
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public AdTusers getCoordinator(long pkcoordinator) {
		try {
			return Util.dao.findById(AdTusers.class, pkcoordinator);
		} catch (Exception e) {
			return null;
		}
	}
	
	@Override
	public List<AdTusers> search(String xml_data) {
		boolean isnumber;
		try{
			Integer.parseInt(XmlUtilities.getValueTag(xml_data, "search_txt"));
			isnumber = true;
		}catch(Exception e){
			isnumber = false;
		}
		String txt = XmlUtilities.getValueTag(xml_data, "search_txt");
		String hql = "";
		if(isnumber)
			hql = "from AdTusers as u where u.userType = 3 and u.userDocument LIKE '" + txt + "%'";
		else{
			List<AdTusers> complete = Util.dao.findByQuery("from AdTusers as u where u.userType = 3");
			List<AdTusers> r = new ArrayList<AdTusers>();
			for(AdTusers u: complete){
				String fullname = u.getUserName1();
				fullname += u.getUserName2().trim() == null || u.getUserName2().trim().equals("") ? "" : " " + u.getUserName2().trim();
				fullname += " " + u.getUserSurname1().trim();
				fullname += u.getUserSurname2().trim() == null || u.getUserSurname2().trim().equals("") ? "" : " " + u.getUserSurname2().trim();
				if(fullname.toUpperCase().indexOf(txt.toUpperCase().trim()) != -1){
					r.add(u);
				}
			}
			return r;
		}
		return Util.dao.findByQuery(hql);
	}

	@Override
	public List<AdTusers> getAllCoordinators() {
		return Util.dao.findByQuery("from AdTusers u where u.userType = 3");
	}

}
