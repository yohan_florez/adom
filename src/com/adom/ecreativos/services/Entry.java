package com.adom.ecreativos.services;

import java.util.List;
import java.util.Set;

import com.adom.ecreativos.util.NoP;
import com.adom.ecreativos.vo.AdTdetails;
import com.adom.ecreativos.vo.AdTentry;
import com.adom.ecreativos.vo.AdTentryserviceinsumo;
import com.adom.ecreativos.vo.AdTentryservices;
import com.adom.ecreativos.vo.AdTinvoiced;
import com.adom.ecreativos.vo.AdTobservations;
import com.adom.ecreativos.vo.AdTusers;

public interface Entry {
	
	public AdTentry saveEntry(String xml_data);
	public List<AdTentry> getEntrys(Long pid);
	public AdTentry getEntry(int pid);
	public AdTentryservices saveEntryService(String xml_data);
	public List<AdTinvoiced> getAllInvoiced();
	public List<AdTentryservices> getEntryServices(int eid);
	public List<Object[]> getActiveServicesByProfessional(Long pid);
	public String getEntryDetails(Long eid);
	public String cancelVisits(String xml_data);
	public String updateVisits(String xml_data);
	public String updateVisitsCoordinator(String xml_data);
	public String updateVisit(String xml_data);
	public AdTentryservices getEntryService(Long esid);
	public AdTentry close_entry(Long eid);
	public int getEntryServicesQtyByEntry(Long eid);
	public int getEntryServicesRealizadosQtyByEntry(Long eid);
	public int getEntryServicesRealizadosQtyByServices(Set<AdTdetails> details);
	public List<String> getEntryServicesStatus(Long eid);
	public int getEntryServicesRealizadosQtyByUser(Long eid, AdTusers u);
	public int getEntryServicesQtyByUser(Long eid, AdTusers u);
	public List<String> getEntryServicesStatusByUser(Long eid, AdTusers u);
	public String verifyClose(Long eid);
	public List<NoP> getNotProfessional();
	public boolean saveHistory(String xml_data);
	public boolean add_observation(String xml_data);
	public List<AdTobservations> get_observation(String xml_data);
	public List<AdTentryserviceinsumo> get_insumos(String xml_data);
	public boolean add_insumo(String xml_data);
	public boolean deleteinsumo(String xml_data);
	public String verify_complete(List<String> eids);
	public List<AdTentryservices> getIrregularServices();
	public List<AdTentryservices> getServicesNotInvoiced();
	public List<List<Object>> getCopagoLimits();
	public int getEntryServicesByUser(Long eid, AdTusers u, int status);
	public int getEntryServicesQtyByServices(Set<AdTdetails> details);
	public long getEntryServicesTotalCopago(Long eid, AdTusers u, int status);
	public long getEntryServicesTotalOtherCopago(Long eid, AdTusers u, int status);
	public String getIdsDetails(Long eid, AdTusers u, int status);
	public List<String> getEntryServicesStatusByUser(Set<AdTdetails> details, Long uid, Long eid);
	public List<String> getEntryServicesStatusByServices(Set<AdTdetails> details);
	public int getEntryServicesRealizadosQtyByServicesAndByUser(Set<AdTdetails> details, Long uid);
}
