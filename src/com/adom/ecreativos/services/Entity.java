package com.adom.ecreativos.services;

import java.util.List;

import com.adom.ecreativos.vo.AdTentity;
import com.adom.ecreativos.vo.AdTentityplan;
import com.adom.ecreativos.vo.AdTrate;

public interface Entity {
	
	public AdTentity saveEntity(String xml_data);
	public String savePlan(String xml_data);
	public AdTrate saveRate(String xml_data);
	public List<AdTentity> getAllEntities();
	public List<AdTrate> getRate(Long pid);
	public List<AdTentityplan> getPlans(Long eid);
	public AdTentity getEntity(String xml_data);
	public AdTentity updateEntity(String xml_data);
	public boolean removeRate(String xml_data);

}
