package com.adom.ecreativos.services;

import java.util.List;

import com.adom.ecreativos.vo.AdTadom;
import com.adom.ecreativos.vo.AdTadverts;
import com.adom.ecreativos.vo.AdTemails;
import com.adom.ecreativos.vo.AdTentityplan;
import com.adom.ecreativos.vo.AdTentryservices;
import com.adom.ecreativos.vo.AdTuserpermit;
import com.adom.ecreativos.vo.AdTusers;

public interface Adom {
	
	public AdTadom updateadom(String xml_data);
	public AdTemails getEmail(int pk);
	public boolean saveEmail1(String xml_data);
	public boolean changePermit(String xml_data);
	public int getHoursProg();
	public int getHoursReal();
	public int getTeraProg();
	public int getTeraReal();
	public int getTeraCancel();
	public List<AdTuserpermit> getPermissions(Long uid);
	public AdTusers generate_password(String xml_data);
	public AdTusers change_status_user(String xml_data);
	public List<AdTentryservices> search(String xml_data);
	public List<AdTentryservices> invoiced(String xml_data);
	public List<AdTentryservices> invoiced2(String xml_data);
	public List<String> generate_rip(List<AdTentryservices> lservices, int type);
	public AdTadom updatelimits(String xml_data);
	public AdTadverts saveadvert(String xml_data);
	public List<AdTadverts> getalladverts();
	public AdTadverts deleteadvert(String xml_data);
	public boolean changepass(String xml_data);
	public List<String> generate_report_terapias(String xml_data);
	public List<String> generate_report_pagos(String xml_data);
	public List<String> generate_report_factura(String xml_data);
	public List<String> generate_report_professionals();
	public List<String> generate_report_patients(String xml_data);
	public List<String> generate_report_general();
	public List<String> generate_copago(String xml_data);
	public AdTentityplan change_status_plan(String xml_data);
	public List<String> generate_report_copago(String xml_data);
	public List<String> generate_report_general2(String xml_data);
}
