package com.adom.ecreativos.services;

import java.util.List;
import java.util.Set;

import com.adom.ecreativos.util.Util;
import com.adom.ecreativos.util.XmlUtilities;
import com.adom.ecreativos.vo.AdTdetails;

public class CopagoClass implements Copago{

	@Override
	public List<AdTdetails> search(String xml_data) {
		try {
			String professional = XmlUtilities.getValueTag(xml_data, "pid");
			String status = XmlUtilities.getValueTag(xml_data, "e2");
			String hql = " from AdTdetails as d where d.adTusers.pkuser = " + professional;
			if(!status.equals("*"))
				hql += " and d.detailCopago = " + status;
			hql += " group by d.adTentryservices.pkentryservices";
			return Util.dao.findByQuery(hql);
		} catch (Exception e) {
			Util.SaveDateInFile("1: " + e.getMessage());
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public List<AdTdetails> searchByServicios(String xml_data) {
		try {
			List<String> services = XmlUtilities.getCollectionValuesTag(xml_data, "serviceid");
			String professional = XmlUtilities.getValueTag(xml_data, "pid");
			String ands = "";
			for(String s: services)
				ands += "d.adTentryservices.pkentryservices = " + s + " or ";
			ands = ands.substring(0, ands.length() - 3);
			String hql = " from AdTdetails as d where d.adTusers.pkuser = " + professional + " and ";
			hql += ands + " group by d.adTentryservices.pkentryservices";
			return Util.dao.findByQuery(hql);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int totalByProfessional(Set<AdTdetails> details, String user){
		int total = 0;
		for(AdTdetails d: details)
			if((d.getAdTstatus().getPkstatus() == 2 || d.getAdTstatus().getPkstatus() == 3) && d.getAdTusers().getPkuser() == Long.parseLong(user))
				total++; 
		return total;
	}
	
	public static int totalByProfessionalInList(Set<AdTdetails> details, String user){
		int total = 0;
		for(AdTdetails d: details)
			if((d.getAdTstatus().getPkstatus() == 2 || d.getAdTstatus().getPkstatus() == 3) && d.getAdTusers().getPkuser() == Long.parseLong(user))
				total++; 
		return total;
	}
	
	public static int statusInList(Set<AdTdetails> details, int status){
		int total = 0;
		for(AdTdetails d: details)
			if(d.getAdTstatus().getPkstatus() == status)
				total++; 
		return total;
	}
	
	public static int statusInListByProfessional(Set<AdTdetails> details, int status, Long professional){
		int total = 0;
		for(AdTdetails d: details)
			if(d.getAdTstatus().getPkstatus() == status && d.getAdTusers().getPkuser().longValue() == professional)
				total++; 
		return total;
	}
	
	public static String getIds(Set<AdTdetails> details, int status, Long professional){
		String data = "";
		for(AdTdetails d: details)
			if(d.getAdTstatus().getPkstatus() == status && d.getAdTusers().getPkuser().longValue() == professional)
				data += d.getPkdetail() + ":"; 
		return data.substring(0, data.length() -1);
	}
	
	public static Long totalCopago(Set<AdTdetails> details){
		long total = 0;
		for(AdTdetails d: details)
			total = d.getDetailMoney() == null ? total : (total + d.getDetailMoney()); 
		return total;
	}
	
	public static Long totalOthersCopagos(Set<AdTdetails> details){
		long total = 0;
		for(AdTdetails d: details)
			total = total + d.getDetailOthermoney(); 
		return total;
	}
	
	public static int getEntryServicesStatusInList(Set<AdTdetails> ldetails, String user) {
		boolean sp = true;
		for(AdTdetails d: ldetails){
			if(d.getAdTstatus().getPkstatus() != 4){
				sp = false;
				break;
			}
		}
		if(sp){
			return 4;//Cancelada
		}else{
			int cp = totalByProfessionalInList(ldetails, user);
			int cr = getEntryServicesRealizadosQtyInList(ldetails, user);
			if(cp == cr){
				return 3;//Completada
			}else{
				return 2; //En proceso
			}
		}
	}
	
	public static int getEntryServicesRealizadosQtyInList(Set<AdTdetails> details, String user){
		int total = 0;
		for(AdTdetails d: details)
			if(d.getAdTstatus().getPkstatus() == 3 && d.getAdTusers().getPkuser() == Long.parseLong(user))
				total++; 
		return total;
	}
}
